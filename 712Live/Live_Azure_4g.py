import os
import re
import pandas as pd
import datetime
import os
import time
import pytz
import pyodbc


server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

startpath="/home/admin/Start/"
path='/home/admin/Dropbox/SERIS_Live_Data/[IN-015S]/'

tz = pytz.timezone('Asia/Calcutta')
start=datetime.datetime.now(tz).date()
timenow1=datetime.datetime.now(tz)
while(timenow1.second<50):
        time.sleep(1)
        timenow1=datetime.datetime.now(tz)
starttime=time.time()

if(os.path.exists(startpath+"712Live_Azure.txt")):
    with open(startpath+"712Live_Azure.txt") as f:
        curr = f.readline(10)
else:
    with open(startpath+"712Live_Azure.txt", "w") as file:
        file.write(str(start))
    curr = str(start)

def convert_data_type(fields):
    for i in range(len(fields)):
        if fields[i] == 'NULL':
            fields[i] = None
    return fields

def delete(date):
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    with cursor.execute("DELETE FROM [dbo].[IN-015S-L] WHERE [Time Stamp] < '"+str(date)+"'"): 
        print('Successfuly Deleted cause already present!') 

#Insert Function
def insert(df):
    df=df.fillna("NULL")
    df=df.values.tolist()
    counter=0
    i=0
    while True:            
        try:
            cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
            cursor = cnxn.cursor()
            #Dumping data in the new table
            while(i<len(df)):
                convert_data_type(df[i])
                with cursor.execute("INSERT INTO [Cleantech Meter Readings].dbo.[IN-015S-L] ([Time Stamp],[AvgGsi00],[AvgGmod10N],[AvgGmod10S],[AvgGmod10E],[AvgGmod10W],[AvgHamb],[AvgTamb] ,[AvgTmodN] ,[AvgTmodS],[Act_Pwr-Tot_LV],[Act_Pwr-Tot_HV],[Act_E-Del-Rec_LV],[Act_E-Del-Rec_HV]) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",df[i]): 
                    print('Successfuly Inserted Row!',df[i]) 
                i=i+1 
            cnxn.commit()
            cursor.close()
            cnxn.close()
            break 
        except Exception as e:
            print(e)
            err_num = e[0]
            if(err_num=='23000'):
                print ('DUPLICATE:', err_num, df[i])
            else:
                print ('ERROR:', err_num, df[i])
            i=i+1
            cursor.close()
            cnxn.close()
            if err_num == '08S01':
                counter = counter + 1
                print ('Sleeping for 5')
                time.sleep(300)
                if counter > 10:
                    exit('Persistent connection error')                    
                print ('%%%Re-trying connection%%%')             
            else:
                pass

#Historical
end=datetime.datetime.now(tz).date()
endstr=str(end)
if(endstr!=curr):
    curr2=datetime.datetime.strptime(curr, "%Y-%m-%d").date()
    tot=end-curr2
    tot=tot.days
    for k in range(0,tot):
        curr3=str(curr2)
        temppath=path+curr3[0:4]+'/'+curr3[0:7]+'/'+'[IN-015S] '+curr3[0:10]+'.txt'
        data=pd.read_csv(temppath,sep='\t')
        insert(data)
        curr2=curr2+datetime.timedelta(days=1)
        with open(startpath+"712Live_Azure.txt", "w") as file:
            file.write(str(curr2))
#Live
flag=0
while(1):
    timenow=datetime.datetime.now(tz)
    timenow=timenow.replace(tzinfo=None)
    timenowstr=str(timenow)
    print(timenowstr)
    if(flag==0):
        with open(startpath+"712Live_Azure.txt") as f:
            curr = f.readline(10)
        path2=path+curr[0:4]+'/'+curr[0:7]+'/'+'[IN-015S] '+curr+'.txt'
        data=pd.read_csv(path2,sep='\t')
        insert(data)
        start=timenow
        with open(startpath+"712Live_Azure.txt", "w") as file:
            file.write(curr)
        flag=1
    else:
        if(timenow.date()+datetime.timedelta(days=-1)>start.date()):
            delete(start.date())
            start=timenow
        path2=path+timenowstr[0:4]+'/'+timenowstr[0:7]+'/'+'[IN-015S] '+timenowstr[0:10]+'.txt'
        data=pd.read_csv(path2,sep='\t')
        try:
            row=data.loc[data['Time Stamp'] == timenowstr[0:16]]
            if(row.empty):
                 print("Row not there for ",timenowstr[0:16])
            else:
                insert(row)
        except:
            print("Failed to write")
            pass
        with open(startpath+"712Live_Azure.txt", "w") as file:
            file.write(timenowstr[0:10])
    time.sleep(60.0 - ((time.time() - starttime) % 60.0))
    
