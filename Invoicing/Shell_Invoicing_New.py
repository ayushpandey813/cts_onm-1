import pandas as pd
import sharepy
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import time
import pyodbc
import sys
import numpy as np
import os
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import smtplib

'''def send_mail(info,attachment_path_list=None):
    s = smtplib.SMTP("smtp.office365.com")
    s.starttls()
    s.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    #s.set_debuglevel(1)
    msg = MIMEMultipart()
    sender = 'operations@cleantechsolar.com'
    #recipients = ['sai.pranav@cleantechsolar.com']
    recipients = ['Daniel.Boey@meinhardtgroup.com','Navin.Ramudaram@shell.com','Sze-Lin.Toh@shell.com']
    msg['Subject'] = "Shell Retail MY - Monthly Generation Data"
    if sender is not None:
        msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    msg['Cc'] = ", ".join(['operationsMY@cleantechsolar.com'])
    msg['Bcc'] = ", ".join(['sai.pranav@cleantechsolar.com','amira.meliki@cleantechsolar.com'])
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
                file_name=each_file_path.split("/")[-1]
                part = MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
    msg.attach(MIMEText(info,'plain/text'))
    s.sendmail(sender, recipients, msg.as_string())'''

##Change this
user = 'admin'

#Authentication means for SP
username = 'test-account@cleantechsolar.com'
password = 'Cleantechsolarpv@123'
base_path = 'https://cleantechenergycorp.sharepoint.com'
auth = sharepy.connect(base_path, username = username, password = password)

#Authentication means for Azure
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

#Declaring dates for summary
today = datetime.today()
start = datetime((today + timedelta(days = -28)).year, (today + timedelta(days = -28)).month, 1)
end = start + relativedelta(months=1) + timedelta(days=-1)
month_name=start.strftime("%B")
print("Start Date: ", start.date())
print("End Date  : ", end.date())
print('/home/' + user + '/Dropbox/Customer/Shell_Invoicing/Shell Invoicing ' + str(end.strftime('%B-%Y')) + '.csv')

#Fetching and cleaning data from MY SHELL RETAIL
filedownload1 = 'https://cleantechenergycorp.sharepoint.com/sites/CSDC-OMMY/Shared%20Documents/MY-400%20Shell-MY%20Retail/Shell MY-Retail Details.xlsx'
fileread1 = '/home/' + user + '/Dropbox/Customer/Shell_Invoicing/Shell MY-Retail Details_Raw.xlsx'
response1 = auth.getfile(filedownload1, filename = fileread1)
dfread1 = pd.read_excel(fileread1,engine='openpyxl')
dfread1 = dfread1[['System Code O&M', 'Name ', 'System Size [kWp]', 'COD', 'Project Code']]
dfread1 = dfread1.dropna(subset=['System Code O&M'])
dfread1 = dfread1.dropna(subset=['COD'])

#Fetching and cleaning data from MY SHELL Program Tracker
filedownload2 = 'https://cleantechenergycorp.sharepoint.com/:x:/r/sites/Malaysia/Shared%20Documents/Shell/01%20-%20Program%20Tracker/200420-Program%20tracker-Rev.00.xlsx'
fileread2 = '/home/' + user + '/Dropbox/Customer/Shell_Invoicing/200420-Program tracker-Rev.00.xlsx'
response2 = auth.getfile(filedownload2, filename = fileread2)
dfread2 = pd.read_excel(fileread2,engine='openpyxl')
dfread2.columns = dfread2[16:17].values[0]
dfread2.drop(dfread2.index[0:17], inplace=True)
dfread2.reset_index(drop = True, inplace=True)
dfread2 = dfread2[['Shell Station ID', 'Cleantech reference']]

#Merging both dataframes
dfread = pd.merge(dfread1, dfread2, how='left', left_on='Project Code', right_on='Cleantech reference')
dfread = dfread[['Shell Station ID', 'System Code O&M', 'Name ', 'System Size [kWp]']]

#Creating data frame
Site_Ids = list(dfread['Shell Station ID'])
Site_Ids.insert(0, 'Shell Reference')

Site_Codes = list(dfread['System Code O&M'])
Site_Codes.insert(0, 'Cleantech Reference')

Site_Names = list(dfread['Name '])
Site_Names.insert(0, 'Name')

Site_Capacities = list(dfread['System Size [kWp]'])
Site_Capacities.insert(0, 'Capacity (kWp)')

header = ['Date', 'Daily Generation (kWh)']

df = pd.DataFrame([Site_Ids])
df = df.append([Site_Codes])
df = df.append([Site_Names])
df = df.append([Site_Capacities])
df = df.append([header])

#Creating Summary
while (start <= end):
    curr_date = start.strftime('%Y-%m-%d')
    curr_read = [curr_date]
    for i in range(1, len(Site_Codes)):
      print("Processing: " + curr_date + " " + str(Site_Codes[i]))
      tempvalue = pd.read_sql_query("SELECT [Value] FROM [Portfolio].[ProcessedData] where ParameterId=5 and SiteId = (SELECT [SiteId] FROM [Portfolio].[Site] WHERE SiteCode = '" + str(Site_Codes[i]) + "') and [Date] = '" +  str(curr_date) + "'", connStr)
      if tempvalue.values[0][0] is None:
        curr_read.append('NULL')
      else:
        curr_read.append(round(float(tempvalue.values[0][0])))
        
    df = df.append([curr_read])
    start += timedelta(days=1)

df.to_csv('/home/' + user + '/Dropbox/Customer/Shell_Invoicing/Shell Invoicing ' + str(end.strftime('%B-%Y')) + '.csv', header = False, index = False)
#send_mail('Please find the Generation Report for the month of '+month_name+' attached.',['/home/' + user + '/Dropbox/Customer/Shell_Invoicing/Shell Invoicing ' + str(end.strftime('%B-%Y')) + '.csv'])