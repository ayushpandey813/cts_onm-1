errHandle = file('/home/admin/Logs/LogsKH002Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/KH002Digest/HistoricalAnalysis2G3GKH002.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/KH002Digest/aggregateInfo.R')
GLOBALENERGYDELIV <<-" "
SOLARMETERDENOM <<-1
initDigest = function(df,no)
{
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'Solar'
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'Loads'
	}
	}
	if(no==1)
	{
		SOLARMETERDENOM <<- as.numeric(df[,3])
	}
	ratspec = round(as.numeric(df[,2])/63,2)
	body=""
	if(as.numeric(no)==1)
	{
	body = paste(body,"Site Name: PPSEZ - System 2",sep="")
	body = paste(body,"\n\nLocation: Phnom Penh, Cambodia")
	body = paste(body,"\n\nO&M Code: KH-002")
	body = paste(body,"\n\nSystem Size: 63 kWp")
	body = paste(body,"\n\nNumber of Energy Meters: 2")
	body = paste(body,"\n\nModule Brand / Model / Nos: Canadian Solar / CS6X-315P / 200")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / STP 25000TL / 2")
	body = paste(body,"\n\nSite COD: 2016-06-20")
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(DAYSALIVE)+34)))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(DAYSALIVE)+34)/365,1)))
  }
	gsicolno = 11
	prcolno = 13
	gsicolno2 = 14
	prcolno2 = 15
	if(no==2)
	{
		gsicolno = 12
		prcolno = 14
	}
	body = paste(body,"\n\n______________________________________________\n\n")
  body = paste(body,as.character(df[,1]),no2)
  body = paste(body,"\n\n______________________________________________")
  if(no!=2)
	{
	body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(ratspec),sep="")
  body = paste(body,"\n\nEac",no,"-1 [kWh]: ",as.character(df[,2]),sep="")
  body = paste(body,"\n\nEac",no,"-2 [kWh]: ",as.character(df[,3]),sep="")
	body = paste(body,"\n\nDaily irradiation [kWh/m2, from KH-003S]: ",as.character(df[,gsicolno]),sep="")
  body = paste(body,"\n\nPR [%]: ",as.character(df[,prcolno]),sep="")
	body = paste(body,"\n\nDaily irradiation pyranometer [kWh/m2, from KH-714S]: ",as.character(df[,gsicolno2]),sep="")
  body = paste(body,"\n\nPR [%]: ",as.character(df[,prcolno2]),sep="")
	}
  acpts = round(as.numeric(df[,6]) * 2.88)
  body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,6]),"%)",sep="")
  body = paste(body,"\n\nDowntime (%):",as.character(df[,7]))
  body = paste(body,"\n\nLast recorded timestamp:",as.character(df[,8]))
	if(no==2)
	{
  body = paste(body,"\n\nSolar energy fed to the grid [kWh]:",as.character(df[,5]))
	perctouse = round((as.numeric(df[,5])*100/SOLARMETERDENOM),1)
  body = paste(body,"\n\nPercentage solar energy fed to the grid [%]",as.character(perctouse))
  body = paste(body,"\n\nLast recorded reading export meter [kWh]:",as.character(df[,10]))
	}
  if(no==1)
		body = paste(body,"\n\nLast recorded energy meter reading [kWh]:",as.character(df[,9]))
  return(body)
}
printtsfaults = function(TS,num,body)
{
  {
	if(as.numeric(num) == 1)
	{
	  num = 'Solar'
	}
	else if(as.numeric(num)==2)
	{
	  num =- 'Loads'
	}
	}
	if(length(TS) > 1)
	{
		body = paste(body,"\n______________________________________________\n")
		body = paste(body,paste("\nTimestamps for",num,"where Pac < 1 between 8am - 6pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
		body = paste(body,"\n______________________________________________\n")
	}
	return(body)
}
sendMail = function(df1,df2,pth1,pth2,pth3)
{
  filetosendpath = c(pth1,pth2)
  if(file.exists(pth3))
	{
	filetosendpath = c(pth1,pth2,pth3)
  }
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	data3G = read.table(pth3,header =T,sep = "\t")
	dateineed = unlist(strsplit(filenams[1]," "))
	dateineed = unlist(strsplit(dateineed[2],"\\."))
	dateineed = dateineed[1] 
	idxineed = match(dateineed,as.character(data3G[,1]))
	print('Filenames Processed')
	GLOBALENERGYDELIV <<- as.character(df1[1,10])
  body = initDigest(df2,1)  #its correct, dont change
	body = paste(body,initDigest(df1,2))  #its correct, dont change
  body = printtsfaults(TIMESTAMPSALARM,1,body)
	print('2G data processed')
	body = paste(body,"\n\n______________________________________________\n\n")
  body = paste(body,"Station Data")
  body = paste(body,"\n\n______________________________________________\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive:",yrsalive)
	if(is.finite(idxineed))
	{
	body = paste(body,"\n\n# Daily specific yied solar[kWh/kWp] :",as.character(data3G[idxineed,16]))
	body = paste(body,"\n\n# Daily specific yied loads[kWh/kWp] :",as.character(data3G[idxineed,17]))
  body = paste(body,"\n\n# Daily Irradiation [kWh/m2]:",as.character(data3G[idxineed,4]))
  body = paste(body,"\n\n# Eac load consumed [kWh]:",as.character(data3G[idxineed,5]))
	body = paste(body,"\n\n# Eac load delivered [kWh]:",as.character(data3G[idxineed,6]))
	body = paste(body,"\n\n# Energy delivered [kWh]:",as.character(data3G[idxineed,7]))
	body = paste(body,"\n\n# Energy received [kWh]:",as.character(data3G[idxineed,8]))
	body = paste(body,"\n\n# Solar power generated [kWh]:",as.character(data3G[idxineed,9]))
	body = paste(body,"\n\n# Percentage pumped into loads (%):",as.character(data3G[idxineed,10]))
	body = paste(body,"\n\n# Solar power consumed by loads [kWh]:",as.character(data3G[idxineed,11]))
	body = paste(body,"\n\n# Total power consumed by loads [kWh]:",as.character(data3G[idxineed,12]))
	body = paste(body,"\n\n# PR (%):",as.character(data3G[idxineed,13]))
	body = paste(body,"\n\n# Last recorded timestamp :",as.character(data3G[idxineed,14]))
	body = paste(body,"\n\n# Last recorded reading [kWh] :",as.character(data3G[idxineed,15]))
	}
	body = gsub("\n ","\n",body)
	print('3G data processed')
 
  graph_command = paste("Rscript /home/admin/CODE/EmailGraphs/PR_Graph_Azure.R", "KH-002X", 75.5, 0.008, "2016-06-20", substr(currday,14,23), sep=" ")
  system(graph_command, wait=TRUE)
  graph_path = paste("/home/admin/Graphs/Graph_Output/KH-002/[KH-002] Graph ", substr(currday,14,23), " - PR Evolution.pdf",sep="")
  graph_extract_path = paste("/home/admin/Graphs/Graph_Extract/KH-002/[KH-002] Graph ", substr(currday,14,23), " - PR Evolution.txt",sep="")
  filetosendpath = c(graph_path, graph_extract_path, filetosendpath)
 
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [KH-002X] Digest",substr(currday,14,23)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            debug = F)
  recordTimeMaster("KH-002X","Mail",substr(currday,14,23))
}
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'

# recipients = getRecipients("KH-002X","m")
recipients = c('operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','bunhak@teams-cambodia.com','vandy@teams-cambodia.com')
#recipients = c('shravan1994@gmail.com')

pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	# recipients = getRecipients("KH-002X","m")
	recipients = c('operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','bunhak@teams-cambodia.com','vandy@teams-cambodia.com')
  recordTimeMaster("KH-002X","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
		irrpathyears = paste(irrpath,years[x],sep = "/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
			irrpathmonth = paste(irrpathyears,months[y],sep = "/")
			daysirr = dir(irrpathmonth)
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[KH-002X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      stations = dir(pathmonths)
      pathdays = paste(pathmonths,stations[1],sep="/")
      pathdays2 = paste(pathmonths,stations[2],sep="/")
      writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
      writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
      checkdir(writepath2Gdays)
      checkdir(writepath2Gdays2)
      days = dir(pathdays)
      days2 = dir(pathdays2)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
      for(t in startdays : length(days2))
      {
         condn1  = (t != length(days2))
         condn2 = ((t == length(days2)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
         }
				 todisp = 1
         sendmail = 1
		if(!(condn1 || condn2))
				 {
				 	sendmail = 0
				 }
				 else
				{
         	DAYSALIVE = DAYSALIVE + 1
				 }
				 t2 = t
				 if(t > length(days))
				 {
				 print(paste('Processing',days[t2]))
				 t2 = length(days)
				 }
				 print(paste('Processing',days2[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t2],sep="")
         writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
         readpath = paste(pathdays,days[t2],sep="/")
         readpath2 = paste(pathdays2,days2[t],sep="/")
				 dataprev = read.table(readpath2,header = T,sep = "\t")
				 METERCALLED <<- 1 # its correct dont change
         df2 = secondGenData(readpath2,writepath2Gfinal2)
				 METERCALLED <<- 2  #its correct dont change
         df1 = secondGenData(readpath,writepath2Gfinal)
				 thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath3Gfinal)
  if(sendmail ==0)
  {
	  Sys.sleep(3600)
    next
  }
	print('Sending Mail')
  sendMail(df1,df2,writepath2Gfinal,writepath2Gfinal2,writepath3Gfinal)
	print('Mail Sent')
      }
    }
  }
	gc()
}
sink()
