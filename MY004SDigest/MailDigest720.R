rm(list = ls())
errHandle = file('/home/admin/Logs/LogsMY004SMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
system('rm -R "/home/admin/Dropbox/Second Gen/[MY-004S]"')
APPENDWARNING = 0
source('/home/admin/CODE/MY004SDigest/runHistory720.R')
require('mailR')
print('History done')
source('/home/admin/CODE/MY004SDigest/3rdGenData720.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/MY004SDigest/aggregateInfo.R')
print('3rd gen data called')
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
  min = as.numeric(hr[2])
  hr = as.numeric(hr[1])
  return((hr * 60 + min + 1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

preparebody = function(path)
{
  print("Enter Body Func")
	body=""
	body = paste(body,"Site Name: Penang Skills Development Centre\n",sep="")
	body = paste(body,"\nLocation: 1, Jalan Sultan Azlan Shah, Kawasan Perindustrian Bayan Lepas, 11900 Bayan Lepas, Pulau Pinang, Malaysia\n")
	body = paste(body,"\nO&M Code: MY-004\n")
	body = paste(body,"\nSystem Size: 414.64  kWp\n")
	body = paste(body,"\nNumber of Energy Meters: 1\n")
	body = paste(body,"\nModule Brand / Model / Nos: Jinko / Eagle PERC 72 / 1136\n")
	body = paste(body,"\nInverter Brand / Model / Nos: SMA / Solid Q-50 / 7\n")
	body = paste(body,"\nSite COD: 2018-12-15\n")
	body = paste(body,"\nSystem age [days]:",((daysactive-30)),"\n")
	body = paste(body,"\nSystem age [years]:",round((daysactive-30)/365,2))
  for(x in 1 : length(path))
  {
		print(path[x])
		dataread = read.table(path[x],header = T,sep="\t",stringsAsFactors=F)
		body = paste(body,"\n\n_____________________________________________\n")
		days = unlist(strsplit(path[x],"/"))
		days = substr(days[length(days)],11,20)
		body = paste(body,days)
		body  = paste(body,"\n_____________________________________________\n\n")
		body = paste(body,"Pts Recorded: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,2])/14.4,1),"%)",sep="")
		body = paste(body,"\n\nDaily Irradiation Pyr [kWh/m2]:",as.character(dataread[1,3]))
		body = paste(body,"\n\nDaily Irradiation Si [kWh/m2]:",as.character(dataread[1,4]))
		body = paste(body,"\n\nDaily Irradiation Gmod [kWh/m2]:",as.character(dataread[1,5]))
		body = paste(body,"\n\nPyr/Si ratio:",as.character(dataread[1,6]))
		body = paste(body,"\n\nGMod/Si ratio:",as.character(dataread[1,7]))
		body = paste(body,"\n\nEac-1 [kWh]:",as.character(dataread[1,30]))
		body = paste(body,"\n\nEac-2 [kWh]:",as.character(dataread[1,31]))
		#body = paste(body,"\n\nFull Site Generation [kWh]:",as.character(dataread[1,24]))
		body = paste(body,"\n\nYield-1 [kWh/kWp]:",as.character((dataread[1,27])))
		body = paste(body,"\n\nYield-2 [kWh/kWp]:",as.character((dataread[1,35])))
		body = paste(body,"\n\nPR-1 [%]:",as.character(dataread[1,28]))
		body = paste(body,"\n\nPR-2 [%]:",as.character(dataread[1,36]))
		body = paste(body,"\n\nFull Site PR-1-Si [%]:",as.character(dataread[1,29]))
		body = paste(body,"\n\nFull Site PR-2-Si [%]:",as.character(dataread[1,37]))
    body = paste(body,"\n\nGrid Availability [%]:",as.character(dataread[1,39]))
    body = paste(body,"\n\nPlant Availability [%]:",as.character(dataread[1,40]))
		body = paste(body,"\n\nMean Tamb [C]:",as.character(dataread[1,8]))
		body = paste(body,"\n\nMean Tamb solar hours [C]:",as.character(dataread[1,11]))
		body = paste(body,"\n\nMax Tamb [C]:",as.character(dataread[1,9]))
		body = paste(body,"\n\nMean Tmod [C]:",as.character(dataread[1,20]))
		body = paste(body,"\n\nMean Tmod solar hours [C]:",as.character(dataread[1,23]))
		body = paste(body,"\n\nMax Tmod [C]:",as.character(dataread[1,21]))
		body = paste(body,"\n\nMean Hamb [%]:",as.character(dataread[1,14]))
		body = paste(body,"\n\nMean Hamb solar hours [%]:",as.character(dataread[1,17]))
		body = paste(body,"\n\nLast recorded [kWh]:",as.character(dataread[1,32]))
		body = paste(body,"\n\nLast recorded time:",as.character(dataread[1,33]))
		body  = paste(body,"\n\n_____________________________________________")
  }
print('Daily Summary for Body Done')
		body  = paste(body,"\n\n_____________________________________________")
  body = paste(body,"\n\nStation History\n")
		body = paste(body,"\n_____________________________________________\n\n")

	body = paste(body,"Station Birth Date:",dobstation)
	body = paste(body,"\n\n# Days station active:",daysactive)
	body = paste(body,"\n\n# Years station active:",rf1((daysactive/365)))
	#body = paste(body,"\n\nTotal irradiation from uncleaned Si sensor this month [kWh/m2]:",MONTHLYIRR1)
	#body = paste(body,"\n\nTotal irradiation from cleaned Si sensor this month [kWh/m2]:",MONTHLYIRR2)
	#body = paste(body,"\n\nTotal irradiation  measured by Pyranometer [kWh/m2]:",MONTHLYIRR3)
	# body = paste(body,"\n\nAverage irradiation from uncleaned Si sensor this month [kWh/m2]:",MONTHAVG1)
	#   body = paste(body,"\n\nAverage irradiation from cleaned Si sensor this month [kWh/m2]:",MONTHAVG2)
#		   body = paste(body,"\n\nAverage irradiation measured by Pyranometer this month [kWh/m2]:",MONTHAVG3)
#	body = paste(body,"\n\nForecasted irradiation total for uncleaned Si sensor this month [kWh/m2]:",FORECASTIRR1);
#	body = paste(body,"\n\nForecasted irradiance total for cleaned Si sensor this month [kWh/m2]:",FORECASTIRR2);
#	body = paste(body,"\n\nForecasted irradiance total for Pyranamoter this month [kWh/m2]:",FORECASTIRR3)
#	body = paste(body,"\n\nIrradiation total last 30 days from cleaned Si sensor [kWh/m2]:",LAST30DAYSTOT)
#  body = paste(body,"\n\nIrradiation total last 30 days from Pyranometer [kWh/m2]:",LAST30DAYSTOT2)
#	body = paste(body,"\n\nIrradiation average last 30 days from cleaned Si sensor [kWh/m2]:",LAST30DAYSMEAN)
#	body = paste(body,"\n\nIrradiance avg last 30 days from Pyranometer[kWh/m2]:",LAST30DAYSMEAN2)
#	body = paste(body,"\n\nSoiling loss (%):",SOILINGDEC)
#	body = paste(body,"\n\nSoiling loss per day (%)",SOILINGDECPD)
#	body = paste(body,"\n\nPyr/Si loss (%):",SNPDEC)
#	body = paste(body,"\n\nPyr/Si loss per day (%)",SNPDECPD)
print('3G Data Done')	
body = gsub("\n ","\n",body)
return(body)
}
path = "/home/admin/Dropbox/SerisData/1min/[720]"
pathwrite = "/home/admin/Dropbox/Second Gen/[MY-004S]"
checkdir(pathwrite)
date = format(Sys.time(),tz = "Singapore")
years = dir(path)
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevsumname = paste("[MY-004S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")
#if(length(dir(pathmonth)) > 1)
#{
#  prevday = dir(pathmonth[length(dir(pathmonth))-1])
#}
stnnickName = "720"
stnnickName2 = "MY-004S"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
if(!is.null(lastdatemail))
{
	yr = substr(lastdatemail,1,4)
	monyr = substr(lastdatemail,1,7)
	prevpathyear = paste(path,yr,sep="/")
	prevsumname = paste("[",stnnickName2,"] ",substr(lastdatemail,3,4),substr(lastdatemail,6,7),".txt",sep="")
	prevpathmonth = paste(prevpathyear,monyr,sep="/")
	currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
	prevwriteyears = paste(pathwrite,yr,sep="/")
	prevwritemonths = paste(prevwriteyears,monyr,sep="/")
}

check = 0
sender <- "operations@cleantechsolar.com"
uname <- "shravan.karthik@cleantechsolar.com" 
pwd = 'CTS&*(789'
# recipients <- getRecipients("MY-004S","m")
recipients = c('operationsMY@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','solarmy@cutechgroup.com')
 
#"jeeva.govindarasu@cleantechsolar.com")

wt =1
idxdiff = 1
while(1)
{
# recipients <- getRecipients("MY-004S","m")
recipients = c('operationsMY@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','solarmy@cutechgroup.com')
recordTimeMaster("MY-004S","Bot")
years = dir(path)
writeyears = paste(pathwrite,years[length(years)],sep="/")
checkdir(writeyears)
pathyear = paste(path,years[length(years)],sep="/")
months = dir(pathyear)
writemonths = paste(writeyears,months[length(months)],sep="/")
sumname = paste("[MY-004S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
checkdir(writemonths)
pathmonth = paste(pathyear,months[length(months)],sep="/")
tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
currday = dir(pathmonth)[length(dir(pathmonth))]
if(prevday == currday)
{
   if(wt == 1)
{
 print("")
 print("")
 print("__________________________________________________________")
 print(substr(currday,7,16))
 print("__________________________________________________________")
 print("")
 print("")
 print('Waiting')
wt = 0
}
{
  if(tm > 1319 || tm < 30)
  {
      Sys.sleep(120)
  }        
  else
  {
    Sys.sleep(3600)
  }
}
next
}
print('New Data found.. sleeping to ensure sync complete')
Sys.sleep(60)
print('Sleep Done')
m1 = match(currday,dir(pathmonth)) 
m2 = match(prevday,dir(prevpathmonth))
idxdiff = m1 - m2
if(!is.finite(idxdiff))
{
print('############## Error in idx ####################')
print(paste('Currday',currday))
print(paste('pathmonth',pathmonth))
print(paste('prevday',prevday))
print(paste('prevpathmonth',prevpathmonth))
print(paste('m1',m1))
print(paste('m2',m2))
print('###############################################')
next	
}
if(idxdiff < 0)
{
	idxdiff = length(dir(prevpathmonth)) - match(prevday,dir(prevpathmonth)) + length(dir(pathmonth))
	newmonth = pathmonth
	newwritemonths = writemonths
	pathmonth = prevpathmonth
	writemonths = prevwritemonths
}
while(idxdiff)
{
{
if(prevday == dir(prevpathmonth)[length(dir(prevpathmonth))])
{
  pathmonth = newmonth
	writemonths = newwritemonths
  currday = dir(pathmonth)[1]
  monthlyirr1 = monthlyirr2 = monthlyirr3 = 0
  strng = unlist(strsplit(months[length(months)],"-"))
  daystruemonth = nodays(strng[1],strng[2])
  dayssofarmonth = 0
}	
else
currday = dir(pathmonth)[(match(prevday,dir(pathmonth)) +1)]
}
dataread = read.table(paste(pathmonth,currday,sep='/'),header = T,sep = "\t",stringsAsFactors=F)
datawrite = prepareSumm(dataread)
datasum = rewriteSumm(datawrite)
currdayw = gsub("720","MY-004S",currday)
write.table(datawrite,file = paste(writemonths,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)



#monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3])
#monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4])
#monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5])
#globirr1 = globirr1 + as.numeric(datawrite[1,3])
#globirr2 = globirr2 + as.numeric(datawrite[1,4])
#globirr3 = globirr3 + as.numeric(datawrite[1,5])
#dayssofarmonth = dayssofarmonth + 1    
daysactive = daysactive + 1
#last30days[[last30daysidx]] = as.numeric(datawrite[1,4])
#last30days2[[last30daysidx]] = as.numeric(datawrite[1,5])



print('Digest Written');
{
  if(!file.exists(paste(writemonths,sumname,sep="/")))
  {
    write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    print('Summary file created')
}
  else 
  {
    write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
   print('Summary file updated')  
}
}
filetosendpath = c(paste(writemonths,currdayw,sep="/"))
filename = c(currday)
filedesc = c("Daily Digest")
dataread = read.table(paste(prevpathmonth,prevday,sep='/'),header = T,sep = "\t",stringsAsFactors=F)
datawrite = prepareSumm(dataread)
prevdayw = gsub("720","MY-004S",prevday)
dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t",stringsAsFactors=F)
print('Summary read')
if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
{
print('Difference in old file noted')



#previdx = (last30daysidx - 1) %% 31

#if(previdx == 0) {previdx = 1}
  datasum = rewriteSumm(datawrite)
  write.table(datawrite,file = paste(prevwritemonths,prevdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)


#if(prevpathmonth == pathmonth)
#{
#	monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
#  monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
#	monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
#}
#globirr1 = globirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
#globirr2 = globirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
#globirr3 = globirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
#last30days[[previdx]] = as.numeric(datawrite[1,4])
#last30days2[[previdx]] = as.numeric(datawrite[1,5])



datarw = read.table(paste(prevwritemonths,prevsumname,sep="/"),header = T,sep = "\t",stringsAsFactors=F)
  rn = match(as.character(datawrite[,1]),as.character(datarw[,1]))
  print(paste('Match found at row no',rn))
  datarw[rn,] = datasum[1,]
  write.table(datarw,file = paste(prevwritemonths,prevsumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
  filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
  filename[2] = prevday
  filedesc[2] = c("Updated Digest")
print('Updated old files.. both digest and summary file')
}

#last30daysidx = (last30daysidx + 1 ) %% 31
#if(last30daysidx == 0) {last30daysidx = 1}

#LAST30DAYSTOT = rf(sum(last30days))
#LAST30DAYSMEAN = rf(mean(last30days))
#LAST30DAYSTOT2 = rf(sum(last30days2))
#LAST30DAYSMEAN2 = rf(mean(last30days2))
#DAYSACTIVE = daysactive
#MONTHLYIRR1 = monthlyirr1
#MONTHLYIRR2 = monthlyirr2
#MONTHLYIRR3 = monthlyirr3
#MONTHAVG1 = rf(monthlyirr1 / dayssofarmonth)
#MONTHAVG2 = rf(monthlyirr2 / dayssofarmonth)
#MONTHAVG3 = rf(monthlyirr3 / dayssofarmonth)
#FORECASTIRR1 = rf(as.numeric(MONTHAVG1) * daystruemonth)
#FORECASTIRR2 = rf(as.numeric(MONTHAVG2) * daystruemonth)
#FORECASTIRR3 = rf(as.numeric(MONTHAVG3) * daystruemonth)
#SOILINGDEC = rf3(((globirr1 / globirr2) - 1) * 100)
#SNPDEC = rf3(((globirr3 / globirr2) - 1) * 100)
#SOILINGDECPD = rf3(as.numeric(SOILINGDEC) / DAYSACTIVE)
#SNPDECPD = rf3(as.numeric(SNPDEC) / DAYSACTIVE)

print('Sending mail')
body = ""
body = preparebody(filetosendpath)
graph_command=paste("Rscript /home/admin/CODE/EmailGraphs/PR_Graph_Azure.R", "MY-004", 76, 0.008, "2018-12-15", substr(currday, 7, 16), sep=" ")
system(graph_command,wait=TRUE)	
graph_path=paste("/home/admin/Graphs/Graph_Output/MY-004/[MY-004] Graph ", substr(currday, 7, 16), " - PR Evolution.pdf", sep="")	
print(graph_path)
graph_extract_path=paste("/home/admin/Graphs/Graph_Extract/MY-004/[MY-004] Graph ", substr(currday, 7, 16), " - PR Evolution.txt", sep="")
print(graph_extract_path)

filetosendpath = c(graph_path,graph_extract_path,filetosendpath)

send.mail(from = sender,
          to = recipients,
          subject = paste("Station [MY-004S] Digest",substr(currday,7,16)),
          body = body,
          smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
          authenticate = TRUE,
          send = TRUE,
          attach.files = filetosendpath,
          debug = F)
print('Mail Sent');
recordTimeMaster("MY-004S","Mail",substr(currday,7,16))
prevday = currday
prevpathyear = pathyear
prevpathmonth = pathmonth
prevwriteyears = writeyears
prevwritemonths = writemonths
prevsumname = sumname
idxdiff = idxdiff - 1;
wt = 1
}
gc()
}
sink()
