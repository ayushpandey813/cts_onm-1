# Introducing dependencies
from windrose import WindroseAxes
from matplotlib import pyplot as plt
import matplotlib.cm as cm
import numpy as np
import pandas as pd

 # Randomly generate an array of wind speed and direction using nmupy
col_names=['Timestamp', 'GHI', 'GTI', 'WS', 'WD'] 
data = pd.read_csv("/home/admin/Dropbox/GIS_API3/RAW_DATA/BEED/BEED_AGGREGATE.txt", names=col_names, delimiter='\t')
data.fillna(0)
print(data['WS'])
ws = data['WS']
wd = data['WD']

 # 
ax = WindroseAxes.from_ax()
ax.bar(wd, ws, normed = True, edgecolor='white')
ax.set_legend()
ax.set_facecolor('lightcyan')
plt.suptitle('WIND ROSE- BEED - 2021', fontsize=20)
plt.savefig('/home/anusha/windrose/[IN-306L] Graph - Wind Rose.pdf')
plt.show()