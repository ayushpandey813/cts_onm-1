import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
import numpy as np
from email.mime.text import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders
import logging

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

bot_type = sys.argv[1]
start = sys.argv[2]

tz = pytz.timezone('Asia/Calcutta')

#Change these
stn = '[IN-040R]'
irr_stn = 'NA'    #NA if WMS is present
wms_name = 'NA'   #NA if WMS is present
components = ['WMS_1']    #Enter WMS name if present else empty
meters = {'MFM_1' : ['MFM_1', 365.625]}
NO_MFM = len(meters)
inverters = ["Inverter_1","Inverter_2", "Inverter_3", "Inverter_4", "Inverter_5","Inverter_6","Inverter_7", "Inverter_8"]
INVCAP = {"Inverter_1":['Inverter 1',41.275],"Inverter_2":['Inverter 2',46.8],"Inverter_3":['Inverter 3',46.8],"Inverter_4":['Inverter 4',46.8],"Inverter_5":['Inverter 5',46.8],"Inverter_6":['Inverter 6',46.8],"Inverter_7":['Inverter 7',46.8],"Inverter_8":['Inverter 8',43.55]}
user = 'admin'
cod_date = '2018-06-14'
recipients = ['operationsCentralIN@cleantechsolar.com','om-it-digest@cleantechsolar.com','oms@avisolar.com']
Site_Name = 'Franke Faber Pune'
Location = 'Pune, India'
brands = "Module Brand / Model / Nos: JA solar / 325 Wp / 1125" + "\n\n" + "Inverter Brand / Model / Nos: Huawei/2000-36KTL/08" + "\n\n"

capacity = sum([i[1] for i in meters.values()])
path_read = '/home/admin/Dropbox/Gen 1 Data/' + stn
startpath = '/home/' + user + '/Start/MasterMail/'
path_write_2g = '/home/' + user + '/Dropbox/Second Gen/' + stn
path_write_3g = '/home/' + user + '/Dropbox/Third Gen/' + stn
path_write_4g = '/home/' + user + '/Dropbox/Fourth_Gen/' + stn + '/' + stn + '-lifetime.txt'

#Creating directories if not present
chkdir(path_write_2g)
chkdir(path_write_3g)
chkdir(path_write_4g[:-22])

#Ordering Components in the series WMS, MFM, Inverters. Done since PR requires GHI to be updated first.
for i in sorted(meters):
  components.append(i)

for i in inverters:
  components.append(i)

print(components)

#Creating dataframes for respective components
def create_template(Type, date):
  if(Type == 'INV'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'Eac1':'NA', 'Eac2':'NA', 'Yld1':'NA', 'Yld2':'NA', 'LastRead':'NA', 'LastTime':'NA'}, columns =['Date', 'DA', 'Eac1', 'Eac2', 'Yld1', 'Yld2', 'LastRead', 'LastTime'])
  elif(Type == 'WMS'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'GHI':'NA'}, columns =['Date', 'DA', 'GHI'])
  elif(Type == 'MFM'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'Eac1':'NA', 'Eac2':'NA', 'Yld1':'NA', 'Yld2':'NA', 'PR1':'NA', 'PR2':'NA', 'LastRead':'NA', 'LastTime':'NA'}, columns =['Date', 'DA', 'Eac1', 'Eac2', 'Yld1', 'Yld2', 'PR1', 'PR2', 'LastRead', 'LastTime'])
  return df_template

#Generating site information for Mail
def site_info(date, site_name, location, stn, capacity, nometers, cod_date):
  name = "Site Name: " + site_name + "\n\n"
  loc = "Location: " + location + "\n\n"
  code = "O&M Code: " + stn + "\n\n"
  size = "System Size [kWp]: " + str(capacity) + "\n\n"
  no_meters = "Number of Energy Meters: " + str(nometers) + "\n\n"
  if cod_date == 'TBC':
    cod = "Site COD: " + cod_date + "\n\nSystem age [days]: " + cod_date + "\n\nSystem age [years]: "  + cod_date + "\n\n"
  else:
    cod = "Site COD: " + str(cod_date) + "\n\nSystem age [days]: " + str((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days)+ "\n\nSystem age [years]: " + "%.2f"%(float(((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days))/365) + "\n\n"
  info = name + loc + code + size + no_meters + brands + cod
  return info

#Getting Irradiance Value
def get_irr(date, irr_stn):
  irr_data={'GHI':'NA', 'DA':'NA'}
  gen2 = '/home/admin/Dropbox/Second Gen/' + irr_stn + '/' + date[:4] + '/' + date[:7] + '/' + wms_name + '/' + irr_stn + '-' + wms_name[:3] + wms_name[4] + '-' + date + '.txt' 
  gen1 = '/home/admin/Dropbox/Gen 1 Data/' + irr_stn + '/' + date[:4] + '/' + date[:7] + '/' + wms_name + '/' + irr_stn + '-' + wms_name[:3] + wms_name[4] + '-' + date + '.txt'
  
  if(os.path.exists(gen2)):
    df = pd.read_csv(gen2, sep = '\t')
    irr_data['GHI'] = df['GHI'][0]
    irr_data['DA'] = df['DA'][0]
  
  if(os.path.exists(gen1)):
    df = pd.read_csv(gen1, sep = '\t')
    GTIGreater20 = df[df['irradiance']>20]['time']
  else:	
    GTIGreater20 = pd.DataFrame({'time' : []})
  
  return irr_data, GTIGreater20
  
#Sending Message
def send_mail(date, stn, info, recipients, attachment_path_list=None):
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  msg['Subject'] = "Station " + stn + " Digest " + str(date)
  #if sender is not None:
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  if attachment_path_list is not None:
    for each_file_path in attachment_path_list:
      try:
        file_name = each_file_path.split("/")[-1]
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(each_file_path, "rb").read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
        msg.attach(part)
      except:
        print("could not attache file")
  msg.attach(MIMEText(info,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())

#checking Stn_Mail.txt
if(os.path.exists(startpath + stn[1:8] + "_Mail.txt")):
  print('Exists')
else:
  try:
    shutil.rmtree(path_write_2g, ignore_errors=True)	
    shutil.rmtree(path_write_3g, ignore_errors=True)	
    shutil.rmtree(path_write_4g, ignore_errors=True)
  except Exception as e:
    print(e)
  with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
    timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    file.write(timenow + "\n" + start)
    
with open(startpath + stn[1:8] + "_Mail.txt") as f:
  startdate = f.readlines()[1].strip()
print(startdate)
startdate = datetime.datetime.strptime(startdate, "%Y-%m-%d")

def process(date, stn):
  file_path = []
  inv_ylds = []
  meter_ylds = []
  total_eac = 0
  total_pr = 0
  Dyield = ''
  Dia = ''
#   GHI = 'NA'
  digest_body = site_info(date, Site_Name, Location, stn[1:7], capacity, NO_MFM, cod_date)
  fullsite_pos = len(digest_body)
  
  #If WMS is not present
  if(irr_stn != 'NA'):
    irr_data, GTIGreater20 = get_irr(date, irr_stn)
    df_2g = pd.DataFrame({'Date': [date], 'DA': [irr_data['DA']], 'GHI' : [irr_data['GHI']]}, columns =['Date', 'DA', 'GHI'])
    GHI = df_2g['GHI'][0]
    digest_body += '------------------------------\nWMS\n------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nGHI [kWh/m^2] (From ' + irr_stn + '): ' + str(df_2g['GHI'][0]) + "\n\n"
    df_merge = df_2g.copy()
    cols_4g = ['Date', 'WMS.DA', 'WMS.GHI']
  else:
    cols_4g = ['Date']

  for i in components:
    chkdir(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i)
    if(os.path.exists(path_write_3g + '/' + i + '/' + date[:7] + '.txt')):
      df_3g = pd.read_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', sep='\t')
      
    if(os.path.exists(path_write_4g)):
      df_4g = pd.read_csv(path_write_4g, sep='\t')
      
    if('Inv' in i):
      if len(inverters) < 9:	
        file_name = i[0] + i[9]	
      else:	
        file_name = i[0] + i[9:11]
      df_2g = create_template('INV', date)
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt',sep='\t')
        
        df_2g['DA'] = round(len(df['e-day'].dropna())/2.88, 1)
        df_2g['Eac1'] = round((df['ac-power'].sum())/12, 2)
        print(INVCAP[i][1])
        df_2g['Yld1'] = round((df_2g['Eac1'][0]/INVCAP[i][1]), 2)
        
        if len(df['e-day'].dropna()) > 0:
          dataint = df.dropna(subset=['e-day'])
          df_2g['Eac2'] = round(dataint['e-day'].tail(1).values[0], 2) # - dataint['e-day'].head(1).values[0])
          df_2g['LastRead'] = round((dataint['e-day'].tail(1).values[0]), 2)
          df_2g['LastTime'] = dataint['time'].tail(1).values[0]
        
        if df_2g['Eac2'][0] != 'NA':
          df_2g['Yld2'] = round((df_2g['Eac2'][0]/INVCAP[i][1]), 2) 
        
        # if len(GTIGreater20)>0:
        #   if len(df[df['Hz_avg']>40])>0:
        #     FrequencyGreaterthan40 = df[df['Hz_avg']>40]['ts']
        #     common = pd.merge(GTIGreater20, FrequencyGreaterthan40, how='inner')
        #   else:
        #     VoltageGreaterthan150 = df[df['PhVphB_avg']>150]['ts']
        #     common = pd.merge(GTIGreater20, VoltageGreaterthan150, how='inner')
        #   DCPowerGreater2 = df[df['W_avg']>0]['ts']
        #   common2 = pd.merge(common, DCPowerGreater2, how='inner')
        #   if len(common) > 0:
        #     df_2g['IA'] = round((len(common2)*100/len(common)), 1)
        if(df_2g['Eac2'][0]=='NA'):	
          pass	
        else:	
          inv_ylds.append(df_2g['Yld2'][0])
  
      Dyield +=  '\n\nYield INV-' + i[9:] + ': ' + str(df_2g['Yld2'][0])
    #   Dia += '\n\nInverter Availability INV-' + i[9:] + ' [%]: ' + str(df_2g['IA'][0])
        
    elif('WMS' in i):
      file_name = i[:3]
      df_2g = create_template('WMS', date)
      print("filename",file_name)
      print(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt',sep='\t')
        print("DA",round(len(df['irradiance'].dropna())/2.88, 1))
        df_2g['DA'] = round(len(df['irradiance'].dropna())/2.88, 1)
        
        if len(df['irradiance'].dropna()) > 0:
          GHI = round(((df['irradiance'].dropna()).sum())/12000, 2)
          df_2g['GHI'] = GHI
        else:
          GHI = 'NA'
        print("$$$GHI")
        print(GHI)
        # if len(df['TmpBOM_avg'].dropna()) > 0:
        #   df_2g['Tmod'] = round((df['TmpBOM_avg'].dropna()).mean(), 1)
        
        # GTIGreater20 = df[df['irradiance']>20]['time']
        
      digest_body += '------------------------------\nWMS\n------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nGHI [kWh/m^2]: ' + str(df_2g['GHI'][0]) + '\n\n'
      
    elif('MFM' in i):
      if NO_MFM < 9:
        file_name = i[:3] + i[4]
      else:
        file_name = i[:3] + i[4:6]
      df_2g = create_template('MFM', date)
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt', sep='\t')
        
        df_2g['DA'] = round(len(df['ac-power'].dropna())/2.88, 1)
        df_2g['Eac1'] = round(df['ac-power'].sum()/12,2)
        df_2g['Yld1'] = round((df['ac-power'].sum()/(meters[i][1]*12)),2)
        
        if len(df['energy-net'].dropna())>0:
          dataint = df.dropna(subset=['energy-net'])
          df_2g['Eac2'] = round((dataint['energy-net'].tail(1).values[0] - dataint['energy-net'].head(1).values[0])/1,2)
          df_2g['Yld2'] = round(((dataint['energy-net'].tail(1).values[0] - dataint['energy-net'].head(1).values[0])/(meters[i][1]*1)),2)
          df_2g['LastRead'] = round((dataint['energy-net'].tail(1).values[0]), 2)
          df_2g['LastTime'] = dataint['time'].tail(1).values[0]
        
        if(GHI == 0 or GHI == 'NA' or df_2g['Yld1'][0] == 'NA'):
          pass
        else:
          df_2g['PR1'] = round((df_2g['Yld1'][0]*100)/GHI,1)
          
        if(GHI == 0 or GHI == 'NA' or df_2g['Yld2'][0] == 'NA'):
          pass
        else:
          df_2g['PR2'] = round((df_2g['Yld2'][0]*100)/GHI,1)
        
        if(df_2g['Eac2'][0]!='NA'):
            total_eac += df_2g['Eac2'][0]
            meter_ylds.append(df_2g['Yld2'][0])
        
        if(df_2g['PR2'][0]!='NA'):
            total_pr += df_2g['PR2'][0] * meters[i][1]
        
        # if len(GTIGreater20)>0:
        #   FrequencyGreater = df[df['Hz_avg']>40]['time']
        #   PowerGreater = df[df['Hz_avg']>2]['time']
        #   common = pd.merge(GTIGreater20, FrequencyGreater, how='inner')
        #   common2 = pd.merge(common, PowerGreater, how='inner')
        #   if len(common)>0:
        #     df_2g['GA'] = round((len(common)*100/len(GTIGreater20)),1)
        #     df_2g['PA'] = round((len(common2)*100/len(common)),1)
        
      digest_body += '------------------------------\n' + meters[i][0] + '\n------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nSize [kWp]: ' + str(meters[i][1]) + '\n\nEAC method-1 (Pac) [kWh]: ' + str(df_2g['Eac1'][0]) + '\n\nEAC method-2 (Eac) [kWh]: ' + str(df_2g['Eac2'][0]) + '\n\nYield-1 [kWh/kWp]: ' + str(df_2g['Yld1'][0]) + '\n\nYield-2 [kWh/kWp]: ' + str(df_2g['Yld2'][0]) + '\n\nPR-1 (GHI) [%]: ' + str(df_2g['PR1'][0]) + '\n\nPR-2 (GHI) [%]: ' + str(df_2g['PR2'][0]) + '\n\nLast recorded value [kWh]: ' + str(df_2g['LastRead'][0]) + '\n\nLast recorded time: ' + str(df_2g['LastTime'][0]) +'\n\n'      
    
    #Saving 2G 
    df_2g.to_csv(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt', na_rep='NA', sep='\t',index=False)
    file_path.append(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')
    
    #Saving 3G
    chkdir(path_write_3g + '/' + i)
    if(os.path.exists(path_write_3g + '/' + i + '/' + date[:7] + '.txt')):
      if((df_3g['Date'] == date).any()):
        vals = df_2g.values.tolist()
        df_3g.loc[df_3g['Date'] == date,df_3g.columns.tolist()] = vals[0]
        df_3g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='w', index=False, header=True)
      else:
        df_2g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='a', index=False, header=False)
    else:
      df_2g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='a', index=False, header=True) #First time
    
    
    #If WMS is present
    if("WMS" in i and irr_stn == 'NA'):
      df_merge = df_2g.copy()
    else:
      df_merge = df_merge.merge(df_2g, on = 'Date')
      
    cols_4g = cols_4g + [i + '.' + n for n in df_2g.columns.tolist()[1:]] 
  df_merge.columns = cols_4g
  
  #Saving 4G
  if not os.path.exists(path_write_4g):
    print('firsttime')
    df_merge.to_csv(path_write_4g, sep='\t', mode='a', index=False, header=True)
  elif((df_4g['Date'] == date).any()):
    vals = df_merge.values.tolist()
    df_4g.loc[df_4g['Date']== date, df_4g.columns.tolist()] = vals[0]
    df_4g.to_csv(path_write_4g, sep = '\t', mode='w', index=False, header=True)
  else:
    df_merge.to_csv(path_write_4g, sep='\t', mode='a', index=False, header=False)
  print('4G Done')
  
  std=np.std(inv_ylds)
  cov=(std*100/np.mean((inv_ylds)))
  digest_body += '------------------------------\nInverters\n------------------------------' + Dyield + '\n\nStdev/COV Yields: ' + str(round(std,1)) + ' / ' + str(round(cov,1)) + '%' + Dia
  
  if NO_MFM > 1:
    stdm = np.std(meter_ylds)
    covm = std*100/np.mean((meter_ylds))
    fullsite = '--------------------------\nFull Site\n--------------------------\n\n' + 'System Full Generation [kWh]: ' + str(total_eac) + '\n\nSystem Full Yield [kWh/kWp]: ' + str(round(total_eac/capacity, 2)) + '\n\nSystem Full PR [%]: ' + str(round(total_pr/capacity, 1)) + '\n\nStdev/COV Yields: ' + str(round(stdm,1)) + ' / ' + str(round(covm,1)) + '%' + "\n\n"
    digest_body = digest_body[:fullsite_pos] + fullsite + digest_body[fullsite_pos:]
  return [digest_body, file_path]

#Historical
print('Historical Started!')
startdate = startdate + datetime.timedelta(days = 1) #Add one cause startdate represent last day that mail was sent successfully
while((datetime.datetime.now(tz).date() - startdate.date()).days>0):
  print('Historical Processing!')
  processed_data = process(str(startdate)[:10], stn)
  if(bot_type == 'Mail'):
    send_mail(str(startdate)[:10], stn, processed_data[0], recipients, processed_data[1])
  startdate = startdate + datetime.timedelta(days = 1)
startdate = startdate + datetime.timedelta(days = -1) #Last day for which mail was sent
with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
  timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S') 
  file.write(timenow + "\n" + (startdate).strftime('%Y-%m-%d'))  
print('Historical Done!')

while(1):
  try:
    date = (datetime.datetime.now(tz)).strftime('%Y-%m-%d')
    timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    with open(startpath + stn[1:8] + "_Bot.txt", "w") as file:
      file.write(timenow)   
    processed_data = process(date, stn)
    print('Done Processing')
    if(datetime.datetime.now(tz).hour == 1 and (datetime.datetime.now(tz).date()-startdate.date()).days>1):
      print('Sending')
      date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
      processed_data = process(date_yesterday, stn)
      send_mail(date_yesterday, stn, processed_data[0], recipients, processed_data[1])
      with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
          file.write(timenow + "\n" + date_yesterday)   
      startdate= datetime.datetime.strptime(date_yesterday,'%Y-%m-%d')
    print('Sleeping')
  except:
    print('error')
    logging.exception('msg')
  time.sleep(300)