import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
import time
import pytz
import datetime
import os
import shutil
import sys
import json
import pytz
username = "cleantech"
password = "Cleantech@321"
api_key = "fc5bbbd2-90e5-4923-bb77-3aead9a148ce"
base_url = "http://api-service.resyncportal.com/resync/api/v1/authenticate"
auth = HTTPBasicAuth(username,password)
header = {"x-resync-apiKey":api_key}
log_file = '/home/admin/Logs/LogsIN040RHistory.txt'
tz = pytz.timezone("Asia/Kolkata")

mac_id = '40623111d65e'
num_of_invs = 8
inv_params = ['ac-power','ac-power-contrl-enable','ac-power-deration-gradient','ac-power-peak','ac-power-setting-actual','ac-power-setting-percent','ac-power-setting-percent-highfreq','ac-reactivepower','ac-v-ab','dc-power','e-day','e-hour','inv-efficiency','inv-status']
id_mfm = [10]
num_of_mfm = 1
em_params = ['ac-power','ac-power-a','ac-power-b','ac-power-c','ac-powerfactor','ac-reactivepower','ac-reactivepower-a','ac-reactivepower-b','ac-reactivepower-c','energy-in','energy-net','energy-out']
value_type = 'average'
resolution = '5M'
id_wms = 18
wms_params = ['irradiance']
def get_token():
    reply = requests.get(base_url,headers=header,auth=(username,password))
    if (reply.json()['token']!=None):
        return reply.json()['token']
    else:
        return get_token()
def convert_date(date):
    # date = date.astype('datetime64[ns]')
    converted_date = date.strftime('%Y%m%d')
    return converted_date

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        pass
    else:
        os.makedirs(path)

def get_timestamps(start_date,end_date):
    start_date = start_date[:4] + "-" + start_date[4:6] + "-"+start_date[6:]
    timestamps = pd.date_range(start_date,freq='5T',periods=288).strftime("%Y-%m-%d %H:%M:%S").tolist()
    return timestamps

def get_em_data(mac_id,id_mfm,data_param,value_type,resolution,start_date,end_date):
    token = get_token()
    mac_id = mac_id
    id_mfm = id_mfm
    data_param = data_param
    value_type = value_type
    resolution = resolution
    start_date = start_date
    end_date = end_date
    # get values for all the params
    processed_data_url = """http://api-service.resyncportal.com/resync/api/v1/processed?token={0}&macid={1}&id={2}&datatype={3}&valuetype={4}&resolution={5}&startdate={6}&enddate={7}&timezone=Asia/Kolkata""".format(token,mac_id,id_mfm,data_param,value_type,resolution,start_date,end_date)
    response = requests.get(processed_data_url)
    print("fetching now : ##",data_param)
    try:
        if (response.json()['message'] == 'Not Found' or response.json()['message'] == 'Time-out'):
            print(response.json()['message'])
    except:
        print("fetched data")
    
    # if the response is empty skip the parameter
    try:
        if (len(response.json()['data']) != 0 and str(response) == '<Response [200]>'):
            print(len(response.json()['data']))
            temp = pd.DataFrame(response.json()['data'])
            timestamps=[]
            for i in range(len(temp)):
                timestamps.append((datetime.datetime.fromtimestamp(temp.loc[i][1]/1e3,tz)).strftime('%Y-%m-%d %H:%M:%S'))
            temp['time'] = timestamps
            temp[data_param] = temp['value']
            temp.set_index('time',inplace=True)
            del temp['value']
            return temp
        else:
            print("No data for : ##",data_param)
    # except json.decoder.JSONDecodeError:
    #     print("encountered JSONDecodeError")
    #     print(response.text)
    #     return get_em_data(mac_id,id_mfm,data_param,value_type,resolution,start_date,end_date)
    except:
        print('------------------------------Exception------------------------------')
        print(response.text)
        print(data_param)
        print("---------------------------------------------------------------------")
        return get_em_data(mac_id,id_mfm,data_param,value_type,resolution,start_date,end_date)
    

def get_inv_data(mac_id,id_m,data_param,value_type,resolution,start_date,end_date):
    token = get_token()
    mac_id = mac_id
    id_m = id_m
    data_param = data_param
    value_type = value_type
    resolution = resolution
    start_date = start_date
    end_date = end_date
    # get values for all the params
    processed_data_url = """http://api-service.resyncportal.com/resync/api/v1/processed?token={0}&macid={1}&id={2}&datatype={3}&valuetype={4}&resolution={5}&startdate={6}&enddate={7}&timezone=Asia/Kolkata""".format(token,mac_id,id_m,data_param,value_type,resolution,start_date,end_date)
    response = requests.get(processed_data_url)
    print("fetching now : ##",data_param)
    try:
        if (response.json()['message'] == 'Not Found' or response.json()['message'] == 'Time-out'):
            print(response.json()['message'])
    except:
        print("fetched data")
    
    # if the response is empty skip the parameter
    try:
        if (len(response.json()['data']) != 0 and str(response) == '<Response [200]>'):
            print(len(response.json()['data']))
            temp = pd.DataFrame(response.json()['data'])
            timestamps=[]
            for i in range(len(temp)):
                timestamps.append((datetime.datetime.fromtimestamp(temp.loc[i][1]/1e3,tz)).strftime('%Y-%m-%d %H:%M:%S'))
            temp['time'] = timestamps
            temp[data_param] = temp['value']
            temp.set_index('time',inplace=True)
            del temp['value']
            return temp
        else:
            print("No data for : ##",data_param)
            return "No Data"
    # except json.decoder.JSONDecodeError :
    #     print("encountered JSONDecodeError")
    #     print(response.text)
    #     return get_inv_data(mac_id,id_m,data_param,value_type,resolution,start_date,end_date)
    except:
        print('------------------------------Exception------------------------------')
        print(response.text)
        print(start_date)
        print(end_date)
        print("---------------------------------------------------------------------")
        return get_inv_data(mac_id,id_m,data_param,value_type,resolution,start_date,end_date)


def get_wms_data(mac_id,id_wms,data_param,value_type,resolution,start_date,end_date):
    token = get_token()
    mac_id = mac_id
    id_wms = id_wms
    data_param = data_param
    value_type = value_type
    resolution = resolution
    start_date = start_date
    end_date = end_date
    # get values for all the params
    processed_data_url = """http://api-service.resyncportal.com/resync/api/v1/processed?token={0}&macid={1}&id={2}&datatype={3}&valuetype={4}&resolution={5}&startdate={6}&enddate={7}&timezone=Asia/Kolkata""".format(token,mac_id,id_wms,data_param,value_type,resolution,start_date,end_date)
    response = requests.get(processed_data_url)
    print("fetching now : ##",data_param)
    try:
        if (response.json()['message'] == 'Not Found' or response.json()['message'] == 'Time-out'):
            print(response.json()['message'])
    except:
        print("fetched data")
    
    # if the response is empty skip the parameter
    try:
        if (len(response.json()['data']) != 0 and str(response) == '<Response [200]>'):
            print(len(response.json()['data']))
            temp = pd.DataFrame(response.json()['data'])
            print(temp)
            timestamps=[]
            for i in range(len(temp)):
                timestamps.append((datetime.datetime.fromtimestamp(temp.loc[i][1]/1e3,tz)).strftime('%Y-%m-%d %H:%M:%S'))
            temp['time'] = timestamps
            temp[data_param] = temp['value']
            temp.set_index('time',inplace=True)
            del temp['value']
            return temp
        else:
            print("No data for : ##",data_param)
            return "No Data"
    # except json.decoder.JSONDecodeError:
    #     print("encountered JSONDecodeError")
    #     print(response.text)
    #     return get_wms_data(mac_id,id_wms,data_param,value_type,resolution,start_date,end_date)
    except :
        print('------------------------------Exception------------------------------')
        print(response.text)
        print(start_date)
        print(end_date)
        print("---------------------------------------------------------------------")
        return get_wms_data(mac_id,id_wms,data_param,value_type,resolution,start_date,end_date)
         

def get_mfm(path,start_date,end_date):
    for no in range(1,num_of_mfm+1):
        sdf = pd.DataFrame()
        try:
            sdf['time'] = get_timestamps(start_date,end_date)
            sdf.set_index('time',inplace=True)
        except:
            print("Couldn't get timestamps : NO DATA")
            continue
        print("Getting MFM:",no)
        for i in em_params:
            values = pd.DataFrame()
            try:
                values = get_em_data(mac_id,id_mfm[no-1],i,value_type,resolution,start_date,end_date)
                if (values.empty == False and values is not None):
                    sdf = sdf.merge(right = values, on='time', how='left')
            # except json.decoder.JSONDecodeError:
            #     values = get_em_data(mac_id,id_mfm[no-1],i,value_type,resolution,start_date,end_date)
            #     if (values is not None):
            #         sdf = pd.merge(left = sdf,right = values, left_index=True, right_index=True, how='outer')
            except:
                print("Unexpected error:", sys.exc_info()[0])
                sdf[i] = None
                # sys.exit(0)
        chkdir(path+'/'+'MFM_'+str(no)+'/')
        path_W = path+'/'+'MFM_'+str(no)+'/'+'[IN-040R]-'+'MFM'+str(no)+'-'+date[:4]+'-'+date[4:6]+'-'+date[6:]+'.txt'
        sdf.to_csv(path_W,header=True,sep='\t')



def get_inv(path,start_date,end_date):
    for no in range(1,num_of_invs+1):
        sdf = pd.DataFrame()
        try:
            sdf['time'] = get_timestamps(start_date,end_date)
            sdf.set_index('time',inplace=True)
        except:
            print("Couldn't get timestamps : NO DATA")
            continue
        print("Getting Inverter:",no)
        for i in inv_params:
            values = pd.DataFrame()
            try:
                values = get_em_data(mac_id,no,i,value_type,resolution,start_date,end_date)
                if (values.empty == False and values is not None):
                    sdf = sdf.merge(right = values, on='time', how='left')
            # except json.decoder.JSONDecodeError:
            #     values = get_em_data(mac_id,no,i,value_type,resolution,start_date,end_date)
            #     if (values is not None):
            #         sdf = pd.merge(left = sdf,right = values, left_index=True, right_index=True, how='outer')
            except:
                print("Unexpected error:", sys.exc_info()[0])
                sdf[i] = None
                # sys.exit(0)
        chkdir(path+'/'+'Inverter_'+str(no)+'/')
        path_W = path+'/'+'Inverter_'+str(no)+'/'+'[IN-040R]-'+'I'+str(no)+'-'+date[:4]+'-'+date[4:6]+'-'+date[6:]+'.txt'
        sdf.to_csv(path_W,header=True,sep='\t')



def get_wms(path,start_date,end_date):
    sdf = pd.DataFrame()
    try:
        sdf['time'] = get_timestamps(start_date,end_date)
        sdf.set_index('time',inplace=True)
    except:
        print("Couldn't get timestamps : NO DATA")
    print("Getting WMS:")
    print(sdf.head())
    for i in wms_params:
        values = pd.DataFrame()
        try:
            values = get_em_data(mac_id,id_wms,i,value_type,resolution,start_date,end_date)
            if (values.empty == False and values is not None):
                sdf = sdf.merge(right = values, on='time', how='left')
        # except json.decoder.JSONDecodeError:
        #     values = get_em_data(mac_id,id_wms,i,value_type,resolution,start_date,end_date)
        #     if (values is not None):
        #         sdf = pd.merge(left = sdf,right = values, left_index=True, right_index=True, how='outer')
        except:
            print("Unexpected Error:", sys.exc_info())
            sdf[i] = None
            # sys.exit(0)
    chkdir(path+'/'+'WMS_1'+'/')
    path = path+'/'+'WMS_1'+'/'+'[IN-040R]-'+'WMS'+'-'+date[:4]+'-'+date[4:6]+'-'+date[6:]+'.txt'
    sdf.to_csv(path,header=True,sep='\t')

path='/home/admin/Dropbox/Gen 1 Data/[IN-040R]/'
start_path="/home/admin/Start/IN040R_Bot.txt"
sys.stdout = open(log_file,"w")
if (os.path.exists(start_path)):
    pass
else:
    try:
        shutil.rmtree(path,ignore_errors=True)
    except Exception as e:
        print(e)
    with open(start_path, "w") as file:
        file.write("2021-01-10\n00:00:00")   
with open(start_path) as f:
    startdate = f.readline(11)[:-1]
    starttime = f.readline(10)

start=datetime.datetime.strptime(startdate, "%Y-%m-%d")

while(start.date()<datetime.datetime.now(tz=tz).date()):
    
    date=start.strftime("%Y%m%d")
    end_date = (start + datetime.timedelta(days=+1)).strftime("%Y%m%d")
    path_temp=path+date[0:4]+'/'+date[0:4]+'-'+date[4:6]
    print("#############")
    print(start.strftime("%d-%m-%Y"))
    print("#############")
    get_wms(path_temp,start.strftime("%Y%m%d"),end_date)
    print("WMS done")
    get_mfm(path_temp,start.strftime("%Y%m%d"),end_date)
    print("MFM done")
    get_inv(path_temp,start.strftime("%Y%m%d"),end_date)
    print("Inverters done")
    start=start+datetime.timedelta(days=+1)

while(1):
    print('Live')
    date=(datetime.datetime.now(tz=tz)).strftime("%Y%m%d")
    date_time=datetime.datetime.now(tz=tz)
    path_temp=path+date[0:4]+'/'+date[0:4]+'-'+date[4:6]
    start=date_time
    end_date = (start + datetime.timedelta(days=+1)).strftime("%Y%m%d")
    print("#############")
    print(start.strftime("%d-%m-%Y"))
    print("#############")
    get_wms(path_temp,start.strftime("%Y%m%d"),end_date)
    print("WMS done")
    get_mfm(path_temp,start.strftime("%Y%m%d"),end_date)
    print("MFM done")
    get_inv(path_temp,start.strftime("%Y%m%d"),end_date)
    print("Inverters done")
    with open(start_path, "w") as file:
        date_w = date[:4]+'-'+date[4:6]+'-'+date[6:]
        file.write(str(date[:4]+'-'+date[4:6]+'-'+date[6:])+"\n"+str(date_time.time().replace(microsecond=0)))
    time.sleep(1200)