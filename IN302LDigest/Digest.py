import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
import numpy as np
from email.mime.text import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders
import logging

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

bot_type = sys.argv[1]
start = sys.argv[2]

tz = pytz.timezone('Asia/Calcutta')

#Change these
stn = '[IN-302L]'
irr_stn = 'NA'    #NA if WMS is present
wms_name = 'NA'   #NA if WMS is present
components = ['WMS_1_WMS-1','WMS_2_GHI-2']    #Enter WMS name if present else empty
meters = {'MFM_1_Main PV MFM' : ['Main PV MFM', 14000]}
NO_MFM = len(meters)

inverters = ['INVERTER_24_LT Panel-01 Inverter-01','INVERTER_25_LT Panel-01 Inverter-02','INVERTER_26_LT Panel-01 Inverter-03','INVERTER_27_LT Panel-01 Inverter-04','INVERTER_28_LT Panel-01 Inverter-05', 'INVERTER_29_LT Panel-01 Inverter-06', 'INVERTER_30_LT Panel-01 Inverter-07', 'INVERTER_31_LT Panel-02 Inverter-8', 'INVERTER_32_LT Panel-02 Inverter-9', 'INVERTER_33_LT Panel-02 Inverter-10', 'INVERTER_34_LT Panel-02 Inverter-11', 'INVERTER_35_LT Panel-02 Inverter-12', 'INVERTER_36_LT Panel-02 Inverter-13', 'INVERTER_37_LT Panel-03 Inverter-14', 'INVERTER_38_LT Panel-03 Inverter-15', 'INVERTER_39_LT Panel-03 Inverter-16', 'INVERTER_40_LT Panel-03 Inverter-17', 'INVERTER_41_LT Panel-03 Inverter-18', 'INVERTER_42_LT Panel-03 Inverter-19', 'INVERTER_43_LT Panel-03 Inverter-20', 'INVERTER_44_LT Panel-04 Inverter-21', 'INVERTER_45_LT Panel-04 Inverter-22', 'INVERTER_46_LT Panel-04 Inverter-23', 'INVERTER_47_LT Panel-04 Inverter-24', 'INVERTER_48_LT Panel-04 Inverter-25', 'INVERTER_49_LT Panel-04 Inverter-26','INVERTER_1_LT Panel-01 Inverter-27','INVERTER_2_LT Panel-01 Inverter-28', 'INVERTER_3_LT Panel-01 Inverter-29','INVERTER_4_LT Panel-01 Inverter-30','INVERTER_5_LT Panel-01 Inverter-31', 'INVERTER_6_LT Panel-01 Inverter-32', 'INVERTER_7_LT Panel-02 Inverter-33', 'INVERTER_13_LT Panel-03 Inverter-34', 'INVERTER_14_LT Panel-03 Inverter-35','INVERTER_50_LT Panel-01 Inverter-36','INVERTER_18_LT Panel-04 Inverter-37', 'INVERTER_19_LT Panel-04 Inverter-38', 'INVERTER_20_LT Panel-04 Inverter-39','INVERTER_15_LT Panel-03 Inverter-40', 'INVERTER_16_LT Panel-03 Inverter-41', 'INVERTER_17_LT Panel-03 Inverter-42', 'INVERTER_21_LT Panel-04 Inverter-43', 'INVERTER_22_LT Panel-04 Inverter-44', 'INVERTER_23_LT Panel-04 Inverter-45', 'INVERTER_8_LT Panel-02 Inverter-46', 'INVERTER_9_LT Panel-02 Inverter-47','INVERTER_10_LT Panel-02 Inverter-48', 'INVERTER_11_LT Panel-02 Inverter-49', 'INVERTER_12_LT Panel-02 Inverter-50']
#invcap = {'INVERTER_24_LT Panel-01 Inverter-01': 277.2, 'INVERTER_25_LT Panel-01 Inverter-02': 257.4, 'INVERTER_26_LT Panel-01 Inverter-03': 217.8, 'INVERTER_27_LT Panel-01 Inverter-04': 277.2, 'INVERTER_28_LT Panel-01 Inverter-05': 277.2, 'INVERTER_29_LT Panel-01 Inverter-06': 277.2, 'INVERTER_30_LT Panel-01 Inverter-07': 277.2, 'INVERTER_31_LT Panel-02 Inverter-8': 277.2, 'INVERTER_32_LT Panel-02 Inverter-9': 277.2, 'INVERTER_33_LT Panel-02 Inverter-10': 277.2, 'INVERTER_34_LT Panel-02 Inverter-11': 277.2, 'INVERTER_35_LT Panel-02 Inverter-12': 277.2, 'INVERTER_36_LT Panel-02 Inverter-13': 257.5, 'INVERTER_37_LT Panel-03 Inverter-14': 267.3, 'INVERTER_38_LT Panel-03 Inverter-15': 277.3, 'INVERTER_39_LT Panel-03 Inverter-16': 277.2, 'INVERTER_40_LT Panel-03 Inverter-17': 277.2, 'INVERTER_41_LT Panel-03 Inverter-18': 277.2, 'INVERTER_42_LT Panel-03 Inverter-19': 277.2, 'INVERTER_43_LT Panel-03 Inverter-20': 277.2, 'INVERTER_44_LT Panel-04 Inverter-21': 277.2, 'INVERTER_45_LT Panel-04 Inverter-22': 277.2, 'INVERTER_46_LT Panel-04 Inverter-23': 277.2, 'INVERTER_47_LT Panel-04 Inverter-24': 277.2, 'INVERTER_48_LT Panel-04 Inverter-25': 277.2, 'INVERTER_49_LT Panel-04 Inverter-26': 277.2, 'INVERTER_1_LT Panel-01 Inverter-27': 277.2, 'INVERTER_2_LT Panel-01 Inverter-28': 277.2, 'INVERTER_3_LT Panel-01 Inverter-29': 283.65, 'INVERTER_4_LT Panel-01 Inverter-30': 283.65, 'INVERTER_5_LT Panel-01 Inverter-31': 283.65, 'INVERTER_6_LT Panel-01 Inverter-32': 283.65, 'INVERTER_7_LT Panel-02 Inverter-33': 283.65, 'INVERTER_13_LT Panel-03 Inverter-34': 293.4, 'INVERTER_14_LT Panel-03 Inverter-35': 277.2, 'INVERTER_50_LT Panel-01 Inverter-36': 234.9, 'INVERTER_18_LT Panel-04 Inverter-37': 283.65, 'INVERTER_19_LT Panel-04 Inverter-38': 146.715, 'INVERTER_20_LT Panel-04 Inverter-39': 208, 'INVERTER_15_LT Panel-03 Inverter-40': 264.087, 'INVERTER_16_LT Panel-03 Inverter-41': 227.7, 'INVERTER_17_LT Panel-03 Inverter-42': 227.7, 'INVERTER_21_LT Panel-04 Inverter-43': 273.9, 'INVERTER_22_LT Panel-04 Inverter-44': 283.65, 'INVERTER_23_LT Panel-04 Inverter-45': 273.868, 'INVERTER_8_LT Panel-02 Inverter-46': 283.65, 'INVERTER_9_LT Panel-02 Inverter-47': 283.65, 'INVERTER_10_LT Panel-02 Inverter-48': 283.65, 'INVERTER_11_LT Panel-02 Inverter-49': 263.7, 'INVERTER_12_LT Panel-02 Inverter-50': 292.5}
invcap = {'INVERTER_24_LT Panel-01 Inverter-01': 277.2, 
'INVERTER_25_LT Panel-01 Inverter-02': 277.2, 
'INVERTER_26_LT Panel-01 Inverter-03': 254.8, 
'INVERTER_27_LT Panel-01 Inverter-04': 277.2,
 'INVERTER_28_LT Panel-01 Inverter-05': 277.2,
 'INVERTER_29_LT Panel-01 Inverter-06': 277.2, 
 'INVERTER_30_LT Panel-01 Inverter-07': 277.2, 
 'INVERTER_31_LT Panel-02 Inverter-8': 277.2,
 'INVERTER_32_LT Panel-02 Inverter-9': 277.2,
 'INVERTER_33_LT Panel-02 Inverter-10': 277.2,
 'INVERTER_34_LT Panel-02 Inverter-11': 277.2,
 'INVERTER_35_LT Panel-02 Inverter-12': 277.2,
 'INVERTER_36_LT Panel-02 Inverter-13': 277.2, 
 'INVERTER_37_LT Panel-03 Inverter-14': 277.2,
 'INVERTER_38_LT Panel-03 Inverter-15': 277.2,
 'INVERTER_39_LT Panel-03 Inverter-16': 277.2, 
 'INVERTER_40_LT Panel-03 Inverter-17': 277.2,
 'INVERTER_41_LT Panel-03 Inverter-18': 277.2,
 'INVERTER_42_LT Panel-03 Inverter-19': 277.2,
 'INVERTER_43_LT Panel-03 Inverter-20': 277.2,
 'INVERTER_44_LT Panel-04 Inverter-21': 277.2,
 'INVERTER_45_LT Panel-04 Inverter-22': 277.2,
 'INVERTER_46_LT Panel-04 Inverter-23': 277.2, 
 'INVERTER_47_LT Panel-04 Inverter-24': 277.2,
 'INVERTER_48_LT Panel-04 Inverter-25': 277.2, 
 'INVERTER_49_LT Panel-04 Inverter-26': 277.2,
 'INVERTER_1_LT Panel-01 Inverter-27': 277.2,
 'INVERTER_2_LT Panel-01 Inverter-28': 277.2,
 'INVERTER_3_LT Panel-01 Inverter-29': 283.65,
 'INVERTER_4_LT Panel-01 Inverter-30': 283.65,
 'INVERTER_5_LT Panel-01 Inverter-31': 283.65,
 'INVERTER_6_LT Panel-01 Inverter-32': 283.65,
 'INVERTER_7_LT Panel-02 Inverter-33': 283.65, 
 'INVERTER_13_LT Panel-03 Inverter-34': 283.65,
 'INVERTER_14_LT Panel-03 Inverter-35': 283.65, 
 'INVERTER_50_LT Panel-01 Inverter-36': 283.65, 
 'INVERTER_18_LT Panel-04 Inverter-37': 283.65,
 'INVERTER_19_LT Panel-04 Inverter-38': 283.65, 
 'INVERTER_20_LT Panel-04 Inverter-39': 283.65,
 'INVERTER_15_LT Panel-03 Inverter-40': 283.65, 
 'INVERTER_16_LT Panel-03 Inverter-41': 283.65,
 'INVERTER_17_LT Panel-03 Inverter-42': 283.65,
 'INVERTER_21_LT Panel-04 Inverter-43': 283.65, 
 'INVERTER_22_LT Panel-04 Inverter-44': 283.65,
 'INVERTER_23_LT Panel-04 Inverter-45': 297, 
 'INVERTER_8_LT Panel-02 Inverter-46': 283.65,
 'INVERTER_9_LT Panel-02 Inverter-47': 283.65, 
 'INVERTER_10_LT Panel-02 Inverter-48': 283.65,
 'INVERTER_11_LT Panel-02 Inverter-49': 283.65, 
 'INVERTER_12_LT Panel-02 Inverter-50': 292.5}
user = 'admin'
cod_date = '2020-07-01'
recipients = ['operationsCentralIN@cleantechsolar.com','om-it-digest@cleantechsolar.com']
Site_Name = 'Ceat MH OA phase-1'
Location = 'Savargaon, India'
brands = "Module Brand / Model / Nos: Trina Solar / Polycrystalline / 42660" + "\n\n" + "Inverter Brand / Model / Nos: Sungrow / SG250HX-IN / 50" + "\n\n"
budget_pr=76
rate=0.01
inv_limit=3
logging.basicConfig(filename='/home/admin/Logs/LogsIN302LMail.txt')

capacity = sum([i[1] for i in meters.values()])
path_read = '/home/admin/Dropbox/Gen 1 Data/' + stn
startpath = '/home/' + user + '/Start/MasterMail/'
path_write_2g = '/home/' + user + '/Dropbox/Second Gen/' + stn
path_write_3g = '/home/' + user + '/Dropbox/Third Gen/' + stn
path_write_4g = '/home/' + user + '/Dropbox/Fourth_Gen/' + stn + '/' + stn + '-lifetime.txt'

#Creating directories if not present
chkdir(path_write_2g)
chkdir(path_write_3g)
chkdir(path_write_4g[:-22])

#Ordering Components in the series WMS, MFM, Inverters. Done since PR requires GTI to be updated first.
for i in sorted(meters):
  components.append(i)

for i in inverters:
  components.append(i)

print(components)

#Creating dataframes for respective components
def create_template(Type, date):
  if(Type == 'INV'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'Eac1':'NA', 'Eac2':'NA', 'Yld1':'NA', 'Yld2':'NA', 'LastRead':'NA', 'LastTime':'NA', 'IA':'NA'}, columns =['Date', 'DA', 'Eac1', 'Eac2', 'Yld1', 'Yld2', 'LastRead', 'LastTime', 'IA'])
  elif(Type == 'WMS'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'Py_GTI':'NA','Py_GHI':'NA','Si_GTI':'NA','Tmod':'NA','Tamb':'NA','WS':'NA','WD':'NA','Hamb':'NA'}, columns =['Date', 'DA', 'Py_GTI','Py_GHI','Si_GTI','Tmod','Tamb','WS','WD','Hamb'])
  elif(Type == 'MFM'):
    df_template = pd.DataFrame({'Date':[date], 'DA':'NA', 'Eac1':'NA', 'Eac2':'NA', 'Yld1':'NA', 'Yld2':'NA', 'PR1':'NA', 'PR2':'NA', 'LastRead':'NA', 'LastTime':'NA', 'GA':'NA', 'PA':'NA','PR1_GHI':'NA','PR2_GHI':'NA','PR1_Si_GTI':'NA','PR2_Si_GTI':'NA'}, columns =['Date', 'DA', 'Eac1', 'Eac2', 'Yld1', 'Yld2', 'PR1', 'PR2', 'LastRead', 'LastTime', 'GA', 'PA','PR1_GHI','PR2_GHI','PR1_Si_GTI','PR2_Si_GTI'])
  return df_template

#Generating site information for Mail
def site_info(date, site_name, location, stn, capacity, nometers, cod_date):
  name = "Site Name: " + site_name + "\n\n"
  loc = "Location: " + location + "\n\n"
  code = "O&M Code: " + stn + "\n\n"
  size = "System Size [kWp]: " + str(capacity) + "\n\n"
  no_meters = "Number of Energy Meters: " + str(nometers) + "\n\n"
  if cod_date == 'TBC':
    cod = "Site COD: " + cod_date + "\n\nSystem age [days]: " + cod_date + "\n\nSystem age [years]: "  + cod_date + "\n\n"
  else:
    cod = "Site COD: " + str(cod_date) + "\n\nSystem age [days]: " + str((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days)+ "\n\nSystem age [years]: " + "%.2f"%(float(((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days))/365) + "\n\n"
  info = name + loc + code + size + no_meters + brands + cod
  return info

#Getting Irradiance Value
def get_irr(date, irr_stn):
  irr_data={'GTI':'NA', 'DA':'NA'}
  gen2 = '/home/admin/Dropbox/Second Gen/' + irr_stn + '/' + date[:4] + '/' + date[:7] + '/' + wms_name + '/' + irr_stn + '-' + wms_name[:3] + wms_name[4] + '-' + date + '.txt' 
  gen1 = '/home/admin/Dropbox/Gen 1 Data/' + irr_stn + '/' + date[:4] + '/' + date[:7] + '/' + wms_name + '/' + irr_stn + '-' + wms_name[:3] + wms_name[4] + '-' + date + '.txt'
  
  if(os.path.exists(gen2)):
    df = pd.read_csv(gen2, sep = '\t')
    irr_data['GTI'] = df['GTI'][0]
    irr_data['DA'] = df['DA'][0]
  
  if(os.path.exists(gen1)):
    df = pd.read_csv(gen1, sep = '\t')
    GTIGreater20 = df[df['POAI_avg']>20]['ts']
  else:	
    GTIGreater20 = pd.DataFrame({'ts' : []})
  
  return irr_data, GTIGreater20

def add_graphs(enddate, startdate):
  os.system(" ".join(["Rscript /home/" + user + "/CODE/EmailGraphs/PR_Graph_Azure.R", stn[1:-2], str(budget_pr), str(rate), cod_date, enddate]))
  os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/Inverter_CoV_Graph.py", stn[1:-2], enddate, str(inv_limit), '2020-09-01','" "','"Inverter CoV"']))
  os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/GHI_vs_PR.py", stn[1:-2],'" "', '2020-09-01', enddate]))
  if(cod_date == 'TBC'):
    os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/Grid_Availability_Graph_Azure_COD.py", stn[1:-2], enddate, '2020-10-01']))
  else:
    os.system(" ".join(["python3 /home/" + user + "/CODE/EmailGraphs/Grid_Availability_Graph_Azure_COD.py", stn[1:-2], enddate, '2020-10-01']))
  
  graph_list = ['GHI vs PR','PR Evolution', 'Inverter CoV', 'Meter CoV', 'Grid Availability', 'Plant Availability', 'Data Availability', 'System Availability']
  path_list = []
  
  for i in range(len(graph_list)):
    check = '/home/' + user + '/Graphs/Graph_Output/' + stn[1:-2] + '/[' + stn[1:-2] + '] Graph ' + enddate + ' - ' + graph_list[i] + '.pdf'
    if(os.path.exists(check)):
      path_list.append(check)
    
    check = '/home/' + user + '/Graphs/Graph_Extract/' + stn[1:-2] + '/[' + stn[1:-2] + '] Graph ' + enddate + ' - ' + graph_list[i] + '.txt'
    if(os.path.exists(check)):
      path_list.append(check)
  
  check = '/home/' + user + '/Graphs/Graph_Extract/' + stn[1:-2] + '/[' + stn[1:-2] + '] - Master Extract Daily.txt'
  if(os.path.exists(check)):
      path_list.append(check)
  
  check = '/home/' + user + '/Graphs/Graph_Extract/' + stn[1:-2] + '/[' + stn[1:-2] + '] - Master Extract Monthly.txt'
  if(os.path.exists(check)):
      path_list.append(check)
  
  return(path_list) 

#Sending Message
def send_mail(date, stn, info, recipients, attachment_path_list=None):
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  msg['Subject'] = "Station " + stn + " Digest " + str(date)
  #if sender is not None:
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  if attachment_path_list is not None:
    for each_file_path in attachment_path_list:
      try:
        file_name = each_file_path.split("/")[-1]
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(each_file_path, "rb").read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
        msg.attach(part)
      except:
        print("could not attache file")
  msg.attach(MIMEText(info,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())
print(path_write_4g)
#checking Stn_Mail.txt
if(os.path.exists(startpath + stn[1:8] + "_Mail.txt")):
  print('Exists')
else:
  try:
    shutil.rmtree(path_write_2g, ignore_errors=True)	
    shutil.rmtree(path_write_3g, ignore_errors=True)	
    os.remove(path_write_4g)
  except Exception as e:
    print(e)
  with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
    timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    file.write(timenow + "\n" + start)
    
with open(startpath + stn[1:8] + "_Mail.txt") as f:
  startdate = f.readlines()[1].strip()
print(startdate)
startdate = datetime.datetime.strptime(startdate, "%Y-%m-%d")

def process(date, stn):
  file_path = []
  inv_ylds = []
  meter_ylds = []
  total_eac = 0
  total_pr = 0
  mfm_yld1_global = 0
  mfm_yld2_global = 0
  Dyield = ''
  Dia = ''
  
  digest_body = site_info(date, Site_Name, Location, stn[1:7], capacity, NO_MFM, cod_date)
  fullsite_pos = len(digest_body)
  
  #If WMS is not present
  if(irr_stn != 'NA'):
    irr_data, GTIGreater20 = get_irr(date, irr_stn)
    df_2g = pd.DataFrame({'Date': [date], 'DA': [irr_data['DA']], 'GTI' : [irr_data['GTI']]}, columns =['Date', 'DA', 'GTI'])
    GTI = df_2g['GTI'][0]
    digest_body += '----------------------------------------\nWMS\n----------------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nGTI [kWh/m^2] (From ' + irr_stn + '): ' + str(df_2g['GTI'][0]) + "\n\n"
    df_merge = df_2g.copy()
    cols_4g = ['Date', 'WMS.DA', 'WMS.GTI']
  else:
    cols_4g = ['Date']

  for i in components:
    chkdir(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i)
    if(os.path.exists(path_write_3g + '/' + i + '/' + date[:7] + '.txt')):
      df_3g = pd.read_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', sep='\t')
      
    if(os.path.exists(path_write_4g)):
      df_4g = pd.read_csv(path_write_4g, sep='\t')
      
    if('INV' in i):
      if len(inverters) < 9:	
        file_name = i[0] + i[9]	
      else:	
        file_name = i[0] + i.split('_')[1]
      df_2g = create_template('INV', date)
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt',sep='\t')
        
        df_2g['DA'] = round(len(df['Wh_sum'].dropna())/2.88, 1)
        df_2g['Eac1'] = round((df['AphA_avg'].sum())/12000, 2)
        df_2g['Yld1'] = round((df_2g['Eac1'][0]/invcap[i]), 2)
        
        if len(df['Wh_sum'].dropna()) > 0:
          dataint = df.dropna(subset=['Wh_sum'])
          df_2g['Eac2'] = round(dataint['Wh_sum'].sum()/1000, 2)
          df_2g['LastRead'] = round((dataint['Wh_sum'].tail(1).values[0])/1000, 2)
          df_2g['LastTime'] = dataint['ts'].tail(1).values[0]
        
        if df_2g['Eac2'][0] != 'NA':
          df_2g['Yld2'] = round((df_2g['Eac2'][0]/invcap[i]), 2) 
        
        if len(GTIGreater20)>0:
          if('Hz_avg' in df.columns.tolist()):
            if len(df[df['Hz_avg']>40])>0:
              FrequencyGreaterthan40 = df[df['Hz_avg']>40]['ts']
              common = pd.merge(GTIGreater20, FrequencyGreaterthan40, how='inner')
              DCPowerGreater2 = df[df['W_avg']>0]['ts']
              common2 = pd.merge(common, DCPowerGreater2, how='inner')
              if len(common) > 0:
                df_2g['IA'] = round((len(common2)*100/len(common)), 1)
          elif('PPVphAB_avg' in df.columns.tolist()):
            VoltageGreaterthan150 = df[df['PPVphAB_avg']>150]['ts']
            common = pd.merge(GTIGreater20, VoltageGreaterthan150, how='inner')
            DCPowerGreater2 = df[df['W_avg']>0]['ts']
            common2 = pd.merge(common, DCPowerGreater2, how='inner')
            if len(common) > 0:
              df_2g['IA'] = round((len(common2)*100/len(common)), 1)

        if(df_2g['Eac2'][0]=='NA'):	
          pass	
        else:	
          inv_ylds.append(df_2g['Yld2'][0])
  
      Dyield +=  '\n\nYield INV-' + i.split('-')[-1] + ': ' + str(df_2g['Yld2'][0])
      Dia += '\n\nInverter Availability INV-' + i.split('-')[-1] + ' [%]: ' + str(df_2g['IA'][0])
        
    elif('WMS' in i):
      file_name = i[:3] + i[4]
      df_2g = create_template('WMS', date)
      if 'WMS_1' in i:
        GTI = 'NA'
        GHI = 'NA'
      Si_GTI = 'NA'
      GTIGreater20 = pd.DataFrame({'ts' : []})
      
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt',sep='\t')
        
        df_2g['DA'] = round(len(df['POAI_avg'].dropna())/2.88, 1)
        
        if 'WMS_1' in i:
          if len(df['POAI_avg'].dropna()) > 0:
            GTI = round(((df['POAI_avg'].dropna()).sum())/12000, 2)
            df_2g['Py_GTI'] = GTI
          else:
            GTI = 'NA'
        else:
          if len(df['POAI_avg'].dropna()) > 0:
            Si_GTI = round(((df['POAI_avg'].dropna()).sum())/12000, 2)
            df_2g['Si_GTI'] = Si_GTI
          else:
            Si_GTI = 'NA'

        if 'TmpBOM_avg' in df.columns.tolist():
          if len(df['TmpBOM_avg'].dropna()) > 0:
            df_2g['Tmod'] = round((df['TmpBOM_avg'].dropna()).mean(), 1)
        
        if 'Humidity_avg' in df.columns.tolist():
          if len(df['Humidity_avg'].dropna()) > 0:
            df_2g['Hamb'] = round((df['Humidity_avg'].dropna()).mean(), 1)

        if 'TmpAmb_avg' in df.columns.tolist():
          if len(df['TmpAmb_avg'].dropna()) > 0:
            df_2g['Tamb'] = round((df['TmpAmb_avg'].dropna()).mean(), 1)

        if 'WndSpd_avg' in df.columns.tolist():
          if len(df['WndSpd_avg'].dropna()) > 0:
            df_2g['WS'] = round((df['WndSpd_avg'].dropna()).mean(), 1)

        if 'WndDir_avg' in df.columns.tolist():
          if len(df['WndDir_avg'].dropna()) > 0:
            df_2g['WD'] = round((df['WndDir_avg'].dropna()).mean(), 1)

        if 'POAI_avg' in df.columns.tolist():
          GTIGreater20 = df[df['POAI_avg']>20]['ts']
        
        if 'GHI_avg' in df.columns.tolist():
          if len(df['GHI_avg'].dropna()) > 0:
            GHI = round(((df['GHI_avg'].dropna()).sum())/12000, 2)
            df_2g['Py_GHI'] = GHI
          else:
            GHI = 'NA'
        
      if('WMS_1' in i):
        digest_body += '----------------------------------------\n'+ str(date) +' WMS_1\n----------------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nPyranometer GTI [kWh/m^2]: ' + str(df_2g['Py_GTI'][0]) + '\n\nPyranometer GHI [kWh/m^2]: ' + str(df_2g['Py_GHI'][0]) + '\n\nAvg Tmod [C]: ' + str(df_2g['Tmod'][0])+ '\n\nAvg Tamb [C]: ' + str(df_2g['Tamb'][0])+ '\n\nAvg Wind Speed [m/s]: ' + str(df_2g['WS'][0])+ '\n\nAvg Wind Direction [o]: ' + str(df_2g['WD'][0])+'\n\n' + 'Humidity [%]: ' + str(df_2g['Hamb'][0]) +'\n\n'
      else:
        digest_body += '----------------------------------------\n'+ str(date) +' WMS_2\n----------------------------------------\n\nDA [%]: '+ str(df_2g['DA'][0]) +'\n\nSilicon Sensor GTI [kWh/m^2]: '  + str(df_2g['Si_GTI'][0]) + '\n\n'
      
    elif('MFM' in i):
      if NO_MFM < 9:
        file_name = i[:3] + i[4]
      else:
        file_name = i[:3] + i[4:6]
      df_2g = create_template('MFM', date)
      if(os.path.exists(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')):
        df = pd.read_csv(path_read + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt', sep='\t')
        df_2g['DA'] = round(len(df['W_avg'].dropna())/2.88, 1)
        df_2g['Eac1'] = round(df['W_avg'].sum()/12000,2)
        df_2g['Yld1'] = round((df['W_avg'].sum()/(meters[i][1]*12000)),2)
        mfm_yld1_global = df_2g['Yld1']
        if len(df['TotWhExp_max'].dropna())>0:
          dataint = df.dropna(subset=['TotWhExp_max'])
          df_2g['Eac2'] = round((dataint['TotWhExp_max'].tail(1).values[0] - dataint['TotWhExp_max'].head(1).values[0])/1000,2)
          df_2g['Yld2'] = round(((dataint['TotWhExp_max'].tail(1).values[0] - dataint['TotWhExp_max'].head(1).values[0])/meters[i][1])/1000,2)
          mfm_yld2_global = df_2g['Yld2']
          df_2g['LastRead'] = round((dataint['TotWhExp_max'].tail(1).values[0])/1000, 2)
          df_2g['LastTime'] = dataint['ts'].tail(1).values[0]
        
        if(GTI == 0 or GTI == 'NA' or df_2g['Yld1'][0] == 'NA'):
          pass
        else:
          df_2g['PR1'] = round((df_2g['Yld1'][0]*100)/GTI,1)
          
        if(GTI == 0 or GTI == 'NA' or df_2g['Yld2'][0] == 'NA'):
          pass
        else:
          df_2g['PR2'] = round((df_2g['Yld2'][0]*100)/GTI,1)
        
        if(GHI == 0 or GHI == 'NA' or df_2g['Yld1'][0] == 'NA'):
          pass
        else:
          df_2g['PR1_GHI'] = round((df_2g['Yld1'][0]*100)/GHI,1)
          
        if(GHI == 0 or GHI == 'NA' or df_2g['Yld2'][0] == 'NA'):
          pass
        else:
          df_2g['PR2_GHI'] = round((df_2g['Yld2'][0]*100)/GHI,1)

        if(GHI == 0 or Si_GTI == 'NA' or df_2g['Yld1'][0] == 'NA'):
          pass
        else:
          df_2g['PR1_Si_GTI'] = round((df_2g['Yld1'][0]*100)/Si_GTI,1)

        if(GHI == 0 or Si_GTI == 'NA' or df_2g['Yld2'][0] == 'NA'):
          pass
        else:
          df_2g['PR2_Si_GTI'] = round((df_2g['Yld2'][0]*100)/Si_GTI,1)

        if(df_2g['Eac2'][0]!='NA'):
            total_eac += df_2g['Eac2'][0]
            meter_ylds.append(df_2g['Yld2'][0])
        
        if(df_2g['PR2'][0]!='NA'):
            total_pr += df_2g['PR2'][0] * meters[i][1]
        
        if len(GTIGreater20)>0:
          FrequencyGreater = df[df['Hz_avg']>40]['ts']
          PowerGreater = df[df['W_avg']>2]['ts']
          common = pd.merge(GTIGreater20, FrequencyGreater, how='inner')
          common2 = pd.merge(common, PowerGreater, how='inner')
          if len(common)>0:
            df_2g['GA'] = round((len(common)*100/len(GTIGreater20)),1)
            df_2g['PA'] = round((len(common2)*100/len(common)),1)
        
      digest_body += '----------------------------------------\n' + str(date) +' '+ meters[i][0] + '\n----------------------------------------\n\nDA [%]: ' + str(df_2g['DA'][0]) + '\n\nSize [kWp]: ' + str(meters[i][1]) + '\n\nEAC method-1 (Pac) [kWh]: ' + str(df_2g['Eac1'][0]) + '\n\nEAC method-2 (Eac) [kWh]: ' + str(df_2g['Eac2'][0]) + '\n\nYield-1 [kWh/kWp]: ' + str(df_2g['Yld1'][0]) + '\n\nYield-2 [kWh/kWp]: ' + str(df_2g['Yld2'][0]) + '\n\nPR-1 (Pyranometer GTI) [%]: ' + str(df_2g['PR1'][0]) + '\n\nPR-2 (Pyranometer GTI) [%]: ' + str(df_2g['PR2'][0]) + '\n\nPR-1 (Pyranometer GHI) [%]: ' + str(df_2g['PR1_GHI'][0]) + '\n\nPR-2 (Pyranometer GHI) [%]: ' + str(df_2g['PR2_GHI'][0])+ '\n\nPR-1 (Silicon Sensor GTI) [%]: ' + str(df_2g['PR1_Si_GTI'][0])+ '\n\nPR-2 (Silicon Sensor GTI) [%]: ' + str(df_2g['PR2_Si_GTI'][0]) +'\n\nLast recorded value [kWh]: ' + str(df_2g['LastRead'][0]) + '\n\nLast recorded time: ' + str(df_2g['LastTime'][0]) + '\n\nGrid Availability [%]: ' + str(df_2g['GA'][0]) + '\n\nPlant Availability [%]: ' + str(df_2g['PA'][0]) + '\n\n'      
    
    #Saving 2G 
    df_2g.to_csv(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt', na_rep='NA', sep='\t',index=False)
    file_path.append(path_write_2g + '/' + date[:4] + '/' + date[:7] + '/' + i + '/' + stn + '-' + file_name + '-' + date + '.txt')
    
    #Saving 3G
    chkdir(path_write_3g + '/' + i)
    if(os.path.exists(path_write_3g + '/' + i + '/' + date[:7] + '.txt')):
      if((df_3g['Date'] == date).any()):
        vals = df_2g.values.tolist()
        df_3g.loc[df_3g['Date'] == date,df_3g.columns.tolist()] = vals[0]
        df_3g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='w', index=False, header=True)
      else:
        df_2g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='a', index=False, header=False)
    else:
      df_2g.to_csv(path_write_3g + '/' + i + '/' + date[:7] + '.txt', na_rep='NA', sep='\t', mode='a', index=False, header=True) #First time
    
    
    #If WMS is present
    if("WMS_1" in i and irr_stn == 'NA'):
      df_merge = df_2g.copy()
    else:
      df_merge = df_merge.merge(df_2g, on = 'Date')
      
    cols_4g = cols_4g + [i + '.' + n for n in df_2g.columns.tolist()[1:]] 
  df_merge.columns = cols_4g
  
  #Saving 4G
  if not os.path.exists(path_write_4g):
    print('firsttime')
    df_merge.to_csv(path_write_4g, sep='\t', mode='a', index=False, header=True)
  elif((df_4g['Date'] == date).any()):
    vals = df_merge.values.tolist()
    df_4g.loc[df_4g['Date']== date, df_4g.columns.tolist()] = vals[0]
    df_4g.to_csv(path_write_4g, sep = '\t', mode='w', index=False, header=True)
  else:
    df_merge.to_csv(path_write_4g, sep='\t', mode='a', index=False, header=False)
  print('4G Done')
  
  std=np.std(inv_ylds)
  cov=(std*100/np.mean((inv_ylds)))
  
  digest_body += '----------------------------------------\n'+ str(date) +' Inverters\n----------------------------------------' + Dyield + '\n\nStdev/COV Yields: ' + str(round(std,1)) + ' / ' + str(round(cov,1)) + '%' + Dia
  malumbra_path = "/home/admin/Dropbox/GIS_API3/MALUMBRA/MALUMBRA_AGGREGATE.txt"
  temp = pd.read_csv(malumbra_path,sep='\t')
  malumbra_gti = temp['GTI'].to_list()[-2]
  malumbra_PR1 = round((mfm_yld1_global*100)/malumbra_gti,1)
  malumbra_PR2 = round((mfm_yld2_global*100)/malumbra_gti,1)
  yesterday = (datetime.datetime.strptime(date,"%Y-%m-%d") - datetime.timedelta(days=1)).strftime("%Y-%m-%d")
  digest_body += '\n\n----------------------------------------\n'+ str(yesterday) +' MALUMBRA API\n----------------------------------------'+'\n\nMALUMBRA API - GTI [kWh/m^2]: ' + str(malumbra_gti) + '\n\nPR-1 (MALUMBRA API - GTI) [%]: ' + str(malumbra_PR1)+ '\n\nPR-2 (MALUMBRA API - GTI) [%]: ' + str(malumbra_PR2) +'\n\n'
  if NO_MFM > 1:
    stdm = np.std(meter_ylds)
    covm = std*100/np.mean((meter_ylds))
    fullsite = '----------------------------------------\nFull Site\n----------------------------------------\n\n' + 'System Full Generation [kWh]: ' + str(total_eac) + '\n\nSystem Full Yield [kWh/kWp]: ' + str(round(total_eac/capacity, 2)) + '\n\nSystem Full PR [%]: ' + str(round(total_pr/capacity, 1)) + '\n\nStdev/COV Yields: ' + str(round(stdm,1)) + ' / ' + str(round(covm,1)) + '%' + "\n\n"
    digest_body = digest_body[:fullsite_pos] + fullsite + digest_body[fullsite_pos:]
  return [digest_body, file_path]

#Historical
print('Historical Started!')
startdate = startdate + datetime.timedelta(days = 1) #Add one cause startdate represent last day that mail was sent successfully
while((datetime.datetime.now(tz).date() - startdate.date()).days>0):
  print('Historical Processing!')
  processed_data = process(str(startdate)[:10], stn)
  if(bot_type == 'Mail'):
    graph_paths = add_graphs(str(startdate)[:10], start)
    processed_data[1] = graph_paths + processed_data[1]
    send_mail(str(startdate)[:10], stn, processed_data[0], recipients, processed_data[1])
  startdate = startdate + datetime.timedelta(days = 1)
startdate = startdate + datetime.timedelta(days = -1) #Last day for which mail was sent
with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
  timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S') 
  file.write(timenow + "\n" + (startdate).strftime('%Y-%m-%d'))  
print('Historical Done!')

while(1):
  date = (datetime.datetime.now(tz)).strftime('%Y-%m-%d')
  timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
  with open(startpath + stn[1:8] + "_Bot.txt", "w") as file:
    file.write(timenow)   
  processed_data = process(date, stn)
  print('Done Processing')
  if(datetime.datetime.now(tz).hour == 1 and (datetime.datetime.now(tz).date()-startdate.date()).days>1):
    print('Sending')
    date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
    processed_data = process(date_yesterday, stn)
    graph_paths = add_graphs(date_yesterday, start)
    processed_data[1] = graph_paths + processed_data[1]
    send_mail(date_yesterday, stn, processed_data[0], recipients, processed_data[1])
    with open(startpath + stn[1:8] + "_Mail.txt", "w") as file:
      file.write(timenow + "\n" + date_yesterday)   
    startdate= datetime.datetime.strptime(date_yesterday,'%Y-%m-%d')
  print('Sleeping')
  time.sleep(600)