import os
import glob
import pandas as pd
import numpy as np
from numpy import *
from datetime import timedelta 
import matplotlib.pyplot as plt
from matplotlib.offsetbox import AnchoredText
import datetime as DT
import statistics


today = DT.date.today()
yesterday = today - DT.timedelta(days=1)
today = yesterday
today = str(today)
#today = str(date.today())
print(today)
year = today[0:4]
content = []
count_pr = 0
count_energy = 0
month = today[0:7]
inv_eff_list = []
loading = []
system_size = [85800, 66000, 85800, 85800, 92400, 92400]
 
def Inv_eff(path,i):
  df11 = pd.read_csv(path,sep='\t')
  df11.dropna(how='any') 
  df11=(df11[['ts','W_avg','DCW_avg']].dropna())
  df11=df11.loc[((df11['W_avg']>0) & (df11['DCW_avg']>df11['W_avg'])) ,] #Filters
  df11['Loading'] = df11['W_avg']/system_size[i]
  df11['Eff'] = df11['W_avg']/df11['DCW_avg']
  df11['sumpro1'] = df11['Loading']*df11['Eff']
  sp = df11['sumpro1'].sum()
  sl = df11['Loading'].sum()
  inv_eff11 = round((sp/sl)*100,2)
  inv_eff_list.append(inv_eff11)
  loading.append(sl)
  
Inv_eff('/home/admin/Dropbox/Gen 1 Data/[IN-054L]/'+year+'/'+month+'/INVERTER_1_Inverter-01/[IN-054L]-I1-'+today+'.txt' ,0)
Inv_eff('/home/admin/Dropbox/Gen 1 Data/[IN-054L]/'+year+'/'+month+'/INVERTER_2_Inverter-02/[IN-054L]-I2-'+today+'.txt' ,1)
Inv_eff('/home/admin/Dropbox/Gen 1 Data/[IN-054L]/'+year+'/'+month+'/INVERTER_3_Inverter-03/[IN-054L]-I3-'+today+'.txt' ,2)
Inv_eff('/home/admin/Dropbox/Gen 1 Data/[IN-054L]/'+year+'/'+month+'/INVERTER_4_Inverter-04/[IN-054L]-I4-'+today+'.txt' ,3)
Inv_eff('/home/admin/Dropbox/Gen 1 Data/[IN-054L]/'+year+'/'+month+'/INVERTER_5_Inverter-05/[IN-054L]-I5-'+today+'.txt' ,4)
Inv_eff('/home/admin/Dropbox/Gen 1 Data/[IN-054L]/'+year+'/'+month+'/INVERTER_6_Inverter-06/[IN-054L]-I6-'+today+'.txt' ,5)

x_axis = [1, 2, 3, 4, 5, 6]
LABELS = ["INV-1", "INV-2", "INV-3","INV-4","INV-5","INV-6"]
plot = plt.bar(x_axis,inv_eff_list,width=0.4,align='center',color='lightsteelblue',  edgecolor='black')
for rect in plot:
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()/2.0, height, '%.2f' % float(height), ha='center', va='bottom')
plt.xticks(x_axis,LABELS)       
plt.title("Inverter Efficiency - "+str(today))
plt.ylabel("Inverter Efficiency [%]")

axes = plt.gca()
mean = statistics.mean(inv_eff_list) 
mean = round(mean,2)
plt.axhline(y=mean, xmin=-10, xmax=100, linewidth=2, linestyle='--',color='black')
axes.set_ylim([0,105])

plt.text(4.8, 89.82, 'Mean ='+str(mean), va="bottom")
plt.savefig('/home/admin/Graphs/Graph_Output/IN-054/[IN-054] Graph '+str(today)+' - Inverter Efficiency.pdf')


