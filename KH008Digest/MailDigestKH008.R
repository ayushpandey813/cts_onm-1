errHandle = file('/home/admin/Logs/LogsKH008Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/KH008Digest/HistoricalAnalysis2G3GKH008.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/KH008Digest/aggregateInfo.R')
initDigest = function(df,no)
{
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'Pond'
		noac="1"
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'R06'
		noac="2"
	}
	else if(as.numeric(no)==3)
	{
		no2 = "R11"
		noac="3"
	}
	else if(as.numeric(no)==4)
	{
		no2 = "R02"
		noac="4"
	}
	else if(as.numeric(no)==5)
	{
		no2 = "R03"
		noac="5"
	}
	else if(as.numeric(no)==7)
	{
		no2 = "WHR"
		noac="7"
	}
	}
	#ratspec = round(as.numeric(df[,2])/2624,2)
  body = "\n\n_________________________________________\n\n"
  body = paste(body,as.character(df[,1])," (",
	substr(as.character(weekdays(as.Date(as.character(df[,1])))),1,3),") ",no2,sep="")
  body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"DA [%]: ",as.character(df[,2]),sep="")
  body = paste(body,"\n\nDowntime [%]: ",as.character(df[,3]),sep="")
  body = paste(body,"\n\nEac-1 [kWh]: ",as.character(df[,4]),sep="")
  body = paste(body,"\n\nEac-2 [kWh]: ",as.character(df[,5]),sep="")
  body = paste(body,"\n\nYield-1 [kWh/kWp]: ",as.character(df[,8]),sep="")
  body = paste(body,"\n\nYield-2 [kWh/kWp]: ",as.character(df[,9]),sep="")
  body = paste(body,"\n\nPR-1 [kWh/kWp]: ",as.character(df[,11]),sep="")
  body = paste(body,"\n\nPR-2 [kWh/kWp]: ",as.character(df[,12]),sep="")
  body = paste(body,"\n\nIrradiation (from Gsi00 shore) [kWh/m2]: ",as.character(df[,10]),sep="")
  body = paste(body,"\n\nLast recorded value [kWh]: ",as.character(df[,6]),sep="")
  body = paste(body,"\n\nLast recorded time [kWh/kWp]: ",as.character(df[,7]),sep="")
  return(body)
}

printtsfaults = function(TS,num,body)
{
  no = num
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'Pond'
		noac="1"
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'R02'
		noac="2"
	}
	else if(as.numeric(no)==3)
	{
		no2 = "R11"
		noac="3"
	}
	else if(as.numeric(no)==4)
	{
		no2 = "R06"
		noac="4"
	}
	else if(as.numeric(no)==5)
	{
		no2 = "R11"
		noac="5"
	}
	else if(as.numeric(no)==6)
	{
		no2 = "WHR"
		noac="7"
	}
	}

	num = no2
	if(length(TS) > 1)
	{
		if(as.numeric(no)==2 || as.numeric(no)==4)
		{
		body = paste(body,"\n\n_________________________________________\n\n",sep="")
		body = paste(body,paste("Timestamps for",num,"where Pac < 1 between 8am -5pm\n"),sep="")
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
		}
	}
	return(body)
}

sendMail = function(df1,df2,df3,df4,df5,pth1,pth2,pth3,pth4,pthm5,pth5,df7,pathm7)
{
  filetosendpath = c(pth1,pth2,pth3,pth4,pthm5,pth5,pathm7)
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	data3G = read.table(pth5,header=T,sep="\t",stringsAsFactors=F)
	date = substr(currday,14,23)
	idxineed = match(date,as.character(data3G[,1]))
	
	print('Filenames Processed')
	body=""
	body = paste(body,"Site Name: CMIC",sep="")
	body = paste(body,"\n\nLocation: Touk Meas, Cambodia",sep="")
	body = paste(body,"\n\nO&M Code: KH-008",sep="")
	body = paste(body,"\n\nSystem Size: 9833.5 kWp",sep="")
	body = paste(body,"\n\nNumber of Energy Meters: 5",sep="")
	body = paste(body,"\n\nModule Brand / Model / Nos: Jinko Solar / JKM365M / 7768 / JKM325PP / 21533",sep="")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / Solid-Q50 / 154",sep="")
	body = paste(body,"\n\nSite COD: 2019-04-12")
	body = paste(body,"\n\nSystem age [days]: ",as.character((as.numeric(DAYSALIVE))),sep="")
	body = paste(body,"\n\nSystem age [years]: ",as.character(round((as.numeric(DAYSALIVE))/365,2)),sep="")
	if(is.finite(idxineed))
	{
	body = paste(body,"\n\n_________________________________________\n\n",sep="")
	body= paste(body,"Total Site")
	body = paste(body,"\n\n_________________________________________\n\n",sep="")
  body = paste(body,"Irradiation (from KH-008S) [kWh/m2]: ",as.character(df1[,10]),sep="")
	body = paste(body,"\n\nEac-1 [kWh]:",data3G[idxineed,34])
	body = paste(body,"\n\nEac-2 [kWh]:",data3G[idxineed,35])
	body = paste(body,"\n\nYld-1 [kWh]:",data3G[idxineed,36])
	body = paste(body,"\n\nYld-2 [kWh]:",data3G[idxineed,37])
	body = paste(body,"\n\nPR-1 [kWh]:",data3G[idxineed,38])
	body = paste(body,"\n\nPR-2 [kWh]:",data3G[idxineed,39])
	body = paste(body,"\n\nSTDEV / COV (%) Yield-1 :",data3G[idxineed,51],"/",data3G[idxineed,53])
	body = paste(body,"\n\nSTDEV / COV (%) Yield-2 :",data3G[idxineed,52],"/",data3G[idxineed,54])
	body = paste(body,"\n\nMains Intake [kWh]:",data3G[idxineed,55])
	intake =  data3G[idxineed,55]
	print("Printing Intake")
	print(intake)
	print("Printing column")
	max_intake = min((data3G[,56]))
	today_date = Sys.Date()
    yr = substring(today_date,0,4)
    mon = substring(today_date, 0,7)
	
	path_exp = "/home/admin/Dropbox/Gen 1 Data/[KH-008X]"
    filename = paste("[KH-008X-M6] ",today_date,".txt",sep = "")
    pathread_exp = paste(path_exp, yr, mon,"Load",filename, sep = "/")
    dataread_exp = read.table(pathread_exp, sep = "\t", header = T)
	dataread_exp[dataread_exp == "N/A"]  <- 0
	dataread_exp[dataread_exp == "NA"]  <- 0
	dataread_exp = na.omit(dataread_exp)
	print("Printing exp")
    exp = isTRUE(any(dataread_exp['Active.Power..W.'] < 0))
	print(exp)
	print("Dataread exp")
	print(dataread_exp)
	#if(is.na(exp))
	#{
	#exp = 0
	#}
	if(exp)
	{
	body = paste(body,"\n\nExport Today [Y/N]: Y")
	max_exp = x['Active.Power..W.']
	n = max(max_exp[max_exp<0]);
	body = paste(body,"\n\n-Z value of Daily Exports [kWh]:",round(intake/6000,2))
	body = paste(body,"\n\nMax Export Value [kWh]:",n)
	}
	if(!exp)
	{
   	body = paste(body,"\n\nExport Today [Y/N]: N")
	body = paste(body,"\n\nMax Export Value [kWh]: 0")
	}

	
	body = paste(body,"\n\nWaste Heat Recovery (WHR) Generation [kWh]:",data3G[idxineed,59])
	body = paste(body,"\n\nSolar PV Generation [kWh]:",data3G[idxineed,35])
	body = paste(body,"\n\nFactory Loads [kWh]:",data3G[idxineed,56])
	body = paste(body,"\n\nPercentage of Factory Loads running on solar PV [%]:",data3G[idxineed,57])
	body = paste(body,"\n\nPercentage of Factory Loads running on WHR [%]:",data3G[idxineed,66])
	}
	body = paste(body,initDigest(df3,3))  #its correct, dont change
	body = paste(body,initDigest(df4,4))  #its correct, dont change
	body = paste(body,initDigest(df1,1))  #its correct, dont change
	body = paste(body,initDigest(df2,2))  #its correct, dont change
	body = paste(body,initDigest(df5,5))  #its correct, dont change
	body = paste(body,initDigest(df7,7))  #its correct, dont change
	body = printtsfaults(TIMESTAMPSALARM,1,body)
  body = printtsfaults(TIMESTAMPSALARM2,2,body)
  body = printtsfaults(TIMESTAMPSALARM3,3,body)
  body = printtsfaults(TIMESTAMPSALARM4,4,body)
	print('2G data processed')
	body = paste(body,"\n\n_________________________________________\n\n",sep="")
  body = paste(body,"Station Data",sep="")
  body = paste(body,"\n\n_________________________________________\n\n",sep="")
  body = paste(body,"Station DOB: ",as.character(DOB),sep="")
  body = paste(body,"\n\n# Days alive: ",as.character(DAYSALIVE),sep="")
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive: ",yrsalive,sep="")
	print('3G data processed')
	body = gsub("\n ","\n",body)
	retryCnt = 0
	print(filetosendpath)
	#graph_command2=paste('python3 /home/admin/CODE/KH008Digest/Export.py ',sep=" ")
	#graph_command_energy = paste('sudo python3 /home/admin/CODE/EmailGraphs/KH-008_Energy_Sources.py',today_date,sep=" ")
	#print("Printing the command")
	#print(graph_command_energy)
  #system(graph_command2, wait = TRUE)
  #system(graph_command_energy,wait=TRUE)
  #graph_path1=paste("/home/admin/Jason/cec intern/results/[KH-008X]/Graph [KH-008X] Daily Export - ",today_date,".pdf",sep="")
  #graph_path2=paste("/home/admin/Jason/cec intern/results/[KH-008X]/Graph [KH-008X] Max Export - ",today_date,".pdf",sep="")
  #graph_path3=paste("/home/admin/Graphs/Graph_Output/KH-008/[KH-008] Graph ",today_date," - CMIC Electricity Sources.pdf",sep="")
  #print("Printing Graph path 3")
  #print(graph_path3)
  #graph_path4=paste("/home/admin/Graphs/Graph_Output/KH-008/[KH-008] Graph Monthly - CMIC Electricity Sources.pdf",sep="")
  #print("Printing Graph path")
  #print(graph_path1)
  #print(graph_path2)
  #filestosendpath = c(graph_path1,graph_path2,graph_path3,graph_path4, filetosendpath)
	while(retryCnt <= 10)
	{
  mailSuccess = try(send.mail(from = sender,
            to = recipients,
            subject = paste("Station [KH-008X] Digest",substr(currday,14,23)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            #file.names = filenams, # optional parameter
            debug = F),silent=T)
	if(class(mailSuccess)=='try-error')
	{
		print('Retrying')
		Sys.sleep(300)
		retryCnt = retryCnt + 1
		next
	}
	break
	}
	if(retryCnt < 10)
		recordTimeMaster("KH-008X","Mail",substr(currday,14,23))
}

sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = c('operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','youbunheng@yahoo.com','visalmeas27@gmail.com','vuthymao0714321918@gmail.com','phann.thul168@gmail.com','om-interns@cleantechsolar.com')
pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	recipients = c('operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','youbunheng@yahoo.com','visalmeas27@gmail.com','vuthymao0714321918@gmail.com','phann.thul168@gmail.com','om-interns@cleantechsolar.com')
	recordTimeMaster("KH-008X","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
			reOrderStns = c(1,4,5,2,3) #Stn order needs to be Pond, R02, R11, R03 and R06
      pathmonths = paste(pathyear,months[y],sep="/")
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[KH-008X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
			offsetUse = 0
      stations = dir(pathmonths)
			pathdays7=NA
			if(length(stations) > 5)
			{
				offsetUse = 1
				if(length(stations)==7)
					offsetUse=2
			}
			if(offsetUse)
			{
				pathdays6 = paste(pathmonths,stations[1],sep="/")
				writepath2Gdays6 = paste(writepath2Gmon,stations[1],sep="/")
				checkdir(writepath2Gdays6)
				days6 = dir(pathdays6)
				if(offsetUse==2)
				{
					pathdays7 = paste(pathmonths,stations[7],sep="/")
					writepath2Gdays7 = paste(writepath2Gmon,stations[7],sep="/")
					checkdir(writepath2Gdays7)
					days7 = dir(pathdays7)
					offsetUse=1
				}
			}
			stations = stations[(reOrderStns + offsetUse)]
			#if(length(stations) > 2)
			#stations = c(stations[2],stations[3],stations[1])
      pathdays = paste(pathmonths,stations[1],sep="/")
      pathdays2 = paste(pathmonths,stations[2],sep="/")
      pathdays3 = paste(pathmonths,stations[3],sep="/")
      pathdays4 = paste(pathmonths,stations[4],sep="/")
      pathdays5 = paste(pathmonths,stations[5],sep="/")
      writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
      writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
      writepath2Gdays3 = paste(writepath2Gmon,stations[3],sep="/")
      writepath2Gdays4 = paste(writepath2Gmon,stations[4],sep="/")
      writepath2Gdays5 = paste(writepath2Gmon,stations[5],sep="/")
      checkdir(writepath2Gdays)
      checkdir(writepath2Gdays2)
      checkdir(writepath2Gdays3)
      checkdir(writepath2Gdays4)
      checkdir(writepath2Gdays5)
      days = dir(pathdays)
      days2 = dir(pathdays2)
      days3 = dir(pathdays3)
      days4 = dir(pathdays4)
      days5 = dir(pathdays5)
			if(length(days) != length(days4))
			{
				minlen = min(length(days),length(days2),length(days3),length(days4),length(days5))
				days = tail(days,n=minlen)
				days2 = tail(days2,n=minlen)
				days3 = tail(days3,n=minlen)
				days4 = tail(days4,n=minlen)
				days5 = tail(days5,n=minlen)
			}
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
           Sys.sleep(3600)
           next
         }
				 if(grepl("Copy",c(days[t],days2[t],days3[t],days4[t])))
				 {
				 	daycop = c(days[t],days2[t],days3[t],days4[t])
					daycop = daycop[grepl("Copy",daycop)]
					for(inner in 1 : length(daycop))
					{
				 	print('Copy file found so removing it')
					command = paste('rm "',pathdays,'/',daycop[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
					}
				 	Sys.sleep(3600)
					next
				 }

				 if(is.na(days[t]) && is.na(days2[t]) && is.na(days3[t]) && is.na(days4[t]))
				 {
					 	print('Files all NA restarting after hour')
           Sys.sleep(3600)
           next
				 }
				 todisp = 1
         sendmail = 1
         DAYSALIVE = DAYSALIVE + 1
				 print(paste('Processing',days[t]))
				 print(paste('Processing',days2[t]))
				 print(paste('Processing',days3[t]))
				 print(paste('Processing',days4[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
         writepath2Gfinal3 = paste(writepath2Gdays3,"/",days3[t],sep="")
         writepath2Gfinal4 = paste(writepath2Gdays4,"/",days4[t],sep="")
         writepath2Gfinal5 = paste(writepath2Gdays5,"/",days5[t],sep="")
         readpath = paste(pathdays,days[t],sep="/")
         readpath2 = paste(pathdays2,days2[t],sep="/")
         readpath3 = paste(pathdays3,days3[t],sep="/")
         readpath4 = paste(pathdays4,days4[t],sep="/")
         readpath5 = paste(pathdays5,days5[t],sep="/")
				 METERCALLED <<- 1  #its correct dont change
         df1 = secondGenData(readpath,writepath2Gfinal)
				 METERCALLED <<- 4 # its correct dont change
         df2 = secondGenData(readpath2,writepath2Gfinal2)
				 METERCALLED <<- 5 # its correct dont change
         df3 = secondGenData(readpath3,writepath2Gfinal3)
				 METERCALLED <<- 2 # its correct dont change
         df4 = secondGenData(readpath4,writepath2Gfinal4)
				 METERCALLED <<- 3 # its correct dont change
         df5 = secondGenData(readpath5,writepath2Gfinal5)
				writepath2Gfinal6 = NA
				writepath2Gfinal7 = NA
				if(offsetUse)
				{
        	readpath6 = paste(pathdays6,days6[t],sep="/")
        	writepath2Gfinal6 = paste(writepath2Gdays6,"/",days6[t],sep="")
					METERCALLED <<- 6
					df6 = secondGenData(readpath6,writepath2Gfinal6)
					if(!is.na(pathdays7))
					{
        		readpath7 = paste(pathdays7,days7[t],sep="/")
        		writepath2Gfinal7 = paste(writepath2Gdays7,"/",days7[t],sep="")
						METERCALLED <<- 7
						df7 = secondGenData(readpath7,writepath2Gfinal7)
					}
				}
				 datemtch = unlist(strsplit(days[t]," "))
				thirdGenData(writepath2Gfinal,writepath2Gfinal3,writepath2Gfinal2,writepath2Gfinal4,writepath2Gfinal5,
				writepath2Gfinal6,writepath2Gfinal7,writepath3Gfinal) #order is correct don't change
  if(sendmail ==0)
  {
    next
  }
	print('Sending Mail')
  sendMail(df1,df2,df3,df4,df5,writepath2Gfinal,writepath2Gfinal2,
	writepath2Gfinal3,writepath2Gfinal4,writepath2Gfinal5,writepath3Gfinal,df7,writepath2Gfinal7)
	print('Mail Sent')
      }
    }
  }
	gc()
}
sink()
