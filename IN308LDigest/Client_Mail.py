import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
import math
import numpy as np
import logging
import pyodbc
import sharepy
tz = pytz.timezone('Asia/Calcutta')
from openpyxl import load_workbook
import xlsxwriter
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders


from datetime import date
from datetime import timedelta
today = date.today()
yesterday = today - timedelta(days = 1)


  
#Authentication means for Azure
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
ALL_SITES = pd.read_sql_query("SELECT * FROM [dbo].Stations_Data WHERE Station_id = 357 AND Meter_id = 514", connStr)
print(ALL_SITES)
  
  

date =  yesterday.strftime("%Y-%m-%d")
tz = pytz.timezone('Asia/Calcutta')
def send_mail(date, recipients, attachment_path_list=None):
     server = smtplib.SMTP("smtp.office365.com")
     server.starttls()
     server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
     msg = MIMEMultipart()
     sender = 'operations@cleantechsolar.com'
     print(date)
     dates = pd.to_datetime(date).strftime("%Y-%m-%d")
     msg['Subject'] = "Station [IN-9308L] Client Digest "+ str(dates)
     #if sender is not None:
     msg['From'] = sender
     msg['To'] = ", ".join(recipients)
     # text = "Dear Sir,\n\n Please find the Generation Data for "+str(dates)+"\n\nNote- You can also access the day wise generation data in the following link which is updated daily by 9-AM.\n<a href=""https://rb.gy/32thws"">Energy Generation</a>\n"
     email_body = "Dear Sir,\n\nPlease find the Generation Data for "+str(dates)+".\n\nRegards,\nTeam Cleantech\n "
     if attachment_path_list is not None:
         for each_file_path in attachment_path_list:
             # try:
             file_name = each_file_path.split("/")[-1]
             part = MIMEBase('application', "octet-stream")
             part.set_payload(open(each_file_path, "rb").read())
             encoders.encode_base64(part)
             part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
             msg.attach(part)
         #   except:
                 # print("could not attache file")

     msg.attach(MIMEText(email_body))
     server.sendmail(sender, recipients, msg.as_string())


recipients = ['operationsCentralIN@cleantechsolar.com', 'om-it-digest@cleantechsolar.com', 'padwal.dattatreya@cie-india.com']

path_write = '/home/admin/Mahindra Generation Data.xlsx'
# path_write = 'Mahindra Generation Data.xlsx'
workbook = xlsxwriter.Workbook(path_write)
workbook.close()
book = load_workbook(path_write)
writer = pd.ExcelWriter(path_write, engine = 'openpyxl',options={'strings_to_numbers': True})
writer.book = book
tdf = ALL_SITES
tdf = tdf.fillna(0)
tdf['Date'] = pd.to_datetime(tdf['Date'])
tdf['months'] = tdf['Date'].apply(lambda x:x.strftime('%B %y'))
tdf['Date'] = tdf['Date'].apply(lambda x:x.strftime('%d-%m-%Y'))
months = list(tdf.months.unique())
for i in months:
    print(i)
    sheet_name = i
    df = pd.DataFrame()
    #df = df.fillna(0)
    df['Date'] = tdf[tdf['months']==i]['Date']
    df['Mahindra CIE block-3'] = tdf[tdf['months']==i]['Eac-MFM']
    if(datetime.datetime.now(tz).strftime('%Y-%m-%d')==pd.datetime.strptime(df.Date.values[-1],"%d-%m-%Y").strftime("%Y-%m-%d")):
        df = df[:-1]
    finalrow = pd.DataFrame({'Date':'Total','Mahindra CIE block-3': round(df['Mahindra CIE block-3'].sum(), 2)},index=[len(df)])
    df = pd.concat([df,finalrow],axis=0)
    df['Mahindra CIE block-3'] = df['Mahindra CIE block-3'].apply(lambda x: "{:,}".format(x))

    df.to_excel(writer,sheet_name=sheet_name,index=None)
try:
    std=book.get_sheet_by_name('Sheet1')
    book.remove_sheet(std)
    print(book.get_sheet_names())
except:
    pass
writer.save()
writer.close()
attachement = []
attachement.append(path_write)
send_mail(date,recipients,attachement)