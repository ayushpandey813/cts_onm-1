rf_inv = function(x){
  return(format(round(x,2),nsmall=2))
}

coerce_num_inv <- function(x)
{
  v1 <- as.numeric(as.character(x))
  return(v1)
}

#Peak condition 1 
#Condition 1; defining peak time
computeInvoice = function(pathGen1)
{
	timemin <- format(as.POSIXct("2017-01-07 08:59:00"), format="%H:%M:%S")
	timemax <- format(as.POSIXct("2017-01-07 22:01:00"), format="%H:%M:%S")

	#Public holidays obs in 2018 APRIL [Thailand] 
	#Condition 2; defining off peak days on week days)
	##https://www.officeholidays.com/countries/thailand/index.php
	PH = c(
	as.Date("2018-04-06"),    #Chakri Day
	as.Date("2018-04-12"),    #Songkran 4
	as.Date("2018-04-13"),    #Songkran
	as.Date("2018-04-14"),    #Songkran 2
	as.Date("2018-04-15"),    #Songkran 3
	as.Date("2018-04-16"),    #Songkran obs
	as.Date("2018-05-01"),    #Labour day
	as.Date("2018-05-14"),    #Royal ploughing ceremony
	as.Date("2018-05-29"),    #Visakha Bucha
	as.Date("2018-07-27"),   #Asaalha Bucha
	as.Date("2018-08-12"),   #Queen's bday
	as.Date("2018-08-13"),   #Queen's bday obs
	as.Date("2018-10-13"),   #Anniversary of Death of King Bhumibol
	as.Date("2018-10-15"),   #Anniversary of Death of King Bhumibol obs
	as.Date("2018-10-23"),   #Chulalongkorn Day
	as.Date("2018-12-05"),   #Father's day
 	as.Date("2018-12-10"),   #Constitution Day
	as.Date("2018-12-31"),
	as.Date("2019-01-01"),
	as.Date("2019-02-19"),
	as.Date("2019-04-15"),
	as.Date("2019-05-01"),
	as.Date("2019-07-16"),
	as.Date("2019-07-17"),
	as.Date("2019-08-12"),
	as.Date("2019-10-23"),
	as.Date("2019-12-05"),
	as.Date("2019-12-10"),
	as.Date("2019-12-31"),
	as.Date("2020-01-01"),
	as.Date("2020-04-06"),
	as.Date("2020-05-01"),
	as.Date("2020-05-04"),
	as.Date("2020-05-06"),
	as.Date("2020-06-03"),
	as.Date("2020-07-06"),
	as.Date("2020-07-28"),
	as.Date("2020-08-12"),
	as.Date("2020-10-13"),
	as.Date("2020-10-23"),
	as.Date("2020-12-10"),
	as.Date("2020-12-31")
)   #New year's eve


i=0
	df=dff=NULL
  temp <- read.table(pathGen1, header = T, sep= '\t', stringsAsFactors = F)
  #data availability
  da <- (length(temp[,1])/1440)*100
  condition0 <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > 
			format(as.POSIXct("2017-01-07 05:59:00"), format="%H:%M:%S") &&  
    	format(as.POSIXct(temp[,1]), format="%H:%M:%S") <
			format(as.POSIXct("2017-01-07 20:00:00"), format="%H:%M:%S")  #for DA
  
	temp2 <- temp[condition0,]
	da_2 <- (length(temp2[,1])/840)*100
	condition1 <- c(format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin) && 
	(format(as.POSIXct(temp[,1]), format="%H:%M:%S") < timemax)
	temp[,19][temp[,19] < 0] <- 0
  pac_TH <- coerce_num_inv(temp[,19])
  total_eac <- as.numeric(sum(pac_TH, na.rm = T)/60000)          #Eac here appears as kWh
  pacTemp <- temp[condition1,]
  date=as.Date(temp[1,1])
	if(is.finite(match(date,PH)))
	{
    peak_eac = 0
    nonpeak_eac= total_eac
    holi <- "Y"
  } 
  else{
    pac_TH2 <- coerce_num_inv(pacTemp[,19])
    peak_eac <- as.numeric(rf_inv(sum(pac_TH2, na.rm = T)/60000))
    nonpeak_eac = total_eac - peak_eac
    holi <- "N"
  }
  d <- format(head(date), format="%w")
  day <- format(date, format = "%a")
  #Condition 3 ; weekends = Off Peak
  #defining non peak period at 0 (sunday) and 6 (saturday)
  if(d == 0 | d == 6) {
    peak_eac = 0
    nonpeak_eac = total_eac
  }
  #powertotal column BW ; 71
  #percentages
  pk_eac_per <- (peak_eac/total_eac)*100
  offpk_eac_per <- (nonpeak_eac/total_eac)*100
  start_read <- as.numeric(temp[1,46])
  last_read <- as.numeric(temp[length(temp[,1]),46])   # in Wh
  total_eac_m2 <-  last_read-start_read
  time_stamp <- as.character(as.POSIXct(temp[length(temp[,1]),1]))
  df <- cbind(as.character(date),
  day,
	holi,
	rf_inv(da),
	rf_inv(da_2),
	last_read/1000,
	total_eac_m2/1000, 
	rf_inv(peak_eac),
	rf_inv(nonpeak_eac),
	rf_inv(pk_eac_per),
	rf_inv(offpk_eac_per),
  total_eac,
  time_stamp
	)
  dff <- rbind(dff,df)

	dff=data.frame(dff)

	colnames(dff) <- c("Date", "Day" ,"Holiday (Y/N)","DA (%)", "DA_6AM_TO_8PM (%)",
                    "Eac delivered (kWh)",  "Eac2 (kWh)","Peak_Eac (kWh)",
									 "Offpeak_Eac (kWh)", "Peak_Eac (%)", "Offpeak_Eac (%)", "Eac1 (kWh)" ,"Time")
	return(dff)
}

