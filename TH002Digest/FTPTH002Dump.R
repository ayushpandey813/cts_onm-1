require('RCurl')
require('utils')
require('R.utils')
source('/home/admin/CODE/MasterMail/timestamp.R')
FIREMAIL = 0
retryCnt = 0
serverdownmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "TH-002X FTP server down for more than 30'",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
serverupmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "TH-002X FTP server up and running",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}

dumpftp = function(days,path)
{
	print('Calling function')
  url = "ftp://TH-002X:ksfhL9HX4PsauKu3@ftpnew.cleantechsolar.com/"
  filenames = try(withTimeout(getURLContent(url,ftp.use.epsv = FALSE,dirlistonly = TRUE,timeout = 600,ssl.verifypeer = FALSE,useragent="R"),
	 timeout = 600, onTimeout = "error"),silent=T)
	if(class(filenames) == 'try-error')
	{
		print('Error in FTP server, will reconnect in 10 mins')
		retryCnt <<- retryCnt + 1
		if(FIREMAIL==0 && retryCnt > 3)
		{
			serverdownmail()
			FIREMAIL<<-1
		}
		return(0)
	}
	retryCnt <<- 0
	if(FIREMAIL==1)
	{
		print('Server Back running...')
		serverupmail()
		FIREMAIL<<-0
	}
	print('Passed test')
	if(class(filenames) != 'character')
	{
		print('filenames isnt character')
		return(1)
	}
	recordTimeMaster("TH-002X","FTPProbe")
	newfilenames = unlist(strsplit(filenames,"\n"))
	print('Strsplit done')
  newfilenames = newfilenames[!grepl("tmp",newfilenames)]
  newfilenames = newfilenames[grepl("zip",newfilenames)]
	newfilenames = c(newfilenames)
	seq1 = seq(from =1 ,to=(length(newfilenames)*2),by=2)
	newfilenames2 = unlist(strsplit(newfilenames,"\\."))
	newfilenames2 = newfilenames2[seq1]
	days2 = unlist(strsplit(days,"\\."))
	days2=days2[seq1]
	print('zip match found')
  match = match(days2,newfilenames2)
	match=match[complete.cases(match)]
	if(length(match) < 1)
	{
	  print('No new files')
		return(1)
	}
	print('Matches found')
  newfilenames = newfilenames[-match]
  if(length(newfilenames) < 1)
	{
	  print('No new files')
		return(1)
	}

	for(y in 1 : length(newfilenames))
  {
    urlnew = paste(url,newfilenames[y],sep="")
    vals = try(download.file(urlnew,destfile = paste(path,newfilenames[y],sep="/")))
		if(class(vals)=='try-error')
		{
			print(paste('Error downloading file',newfilenames))
			next
		}
	  unzipping = try(unzip(paste(path,newfilenames[y],sep="/"),exdir = path),silent = T)
		if(class(unzipping) == 'try-error')
		{
			print(paste('Couldnt extract',newfilenames[y]))
		}
		system(paste('rm "',path,'/',newfilenames[y],'"',sep = ""))
  }
	recordTimeMaster("TH-002X","FTPNewFiles",as.character(newfilenames[length(newfilenames)]))
	rm(filenames,newfilenames)
	gc()
	return(1)
}
