#!/bin/bash
RUNALL=0
x=""
if [ $# == 0 ] 
then
	RUNALL=1
	echo "Running all scripts"
fi

RUNPYTHONONLY=0
RUNRONLY=0

if [ $# == 1 ]
	then
	if [ "$1" == "python-only" ]
	then
		RUNPYTHONONLY=1
		RUNALL=1
		echo "Running only python scripts"
	elif [ "$1" == "R-only" ]
	then
		RUNALL=1
		RUNRONLY=1
		echo "Running only R scripts"
	fi
fi

echo "Value of RUNALL $RUNALL"
echo "Value of RUNPYTHONONLY $RUNPYTHONONLY"
echo "Value of RUNRONLY $RUNRONLY"

if [ $RUNPYTHONONLY -eq 0 ] 
then
if [ "$1" == "GIS_Live" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/GIS/FTPCapture.R &
	x=`echo $!`
	x="$x --GIS_Live"
	if [ "$1" == "GIS_Live" ] 
	then
		x="\n$x"
	fi
	sleep 10
fi

if [ "$1" == "Station_All_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Alarms/Station_All_Alarm.py > /home/admin/Logs/LogsStationAllAlarm.txt &
	y=`echo $!`
	x="$x\n$y --Station_All_Alarm"
	sleep 10
fi

if [ "$1" == "Seris_Sharepoint" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/SerisSharepoint/Seris_Sharepoint.py > /home/admin/Logs/LogsSeris_Sharepoint.txt &
	y=`echo $!`
	x="$x\n$y --Seris_Sharepoint"
	sleep 10
fi

if [ "$1" == "IN-711S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/711Digest/MailDigest711.R &
	y=`echo $!`
	x="$x\n$y --IN-711S_Mail"
	sleep 10
fi

if [ "$1" == "KH-714S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/714Digest/MailDigest714.R &
	y=`echo $!`
	x="$x\n$y --KH-714S_Mail"
	sleep 10
fi

if [ "$1" == "KH-003S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH003NIDigest/MailDigestKH003.R &
	y=`echo $!`
	x="$x\n$y --KH-003S_Mail"
	sleep 10
fi

if [ "$1" == "KH-009L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH009LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --KH-009L_Mail"
	sleep 10
fi


if [ "$1" == "IN-015S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN015Digest/MailDigestIN015.R &
	y=`echo $!`
	x="$x\n$y --IN-015S_Mail"
	sleep 10
fi

if [ "$1" == "IN-713S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/713Digest/MailDigest713.R &
	y=`echo $!`
	x="$x\n$y --IN-713S_Mail"
	sleep 10
fi

if [ "$1" == "SG-003S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/716Digest/MailDigest716.R &
	y=`echo $!`
	x="$x\n$y --SG-003S_Mail"
	sleep 10
fi

if [ "$1" == "SG-007L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/SG007LDigest/Digest.py "Mail" "2021-04-13"> /home/admin/Logs/LogsSG007LMail.txt &
	y=`echo $!`
	x="$x\n$y --SG-007L_Mail"
	sleep 10
fi

if [ "$1" == "SG-008L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/SG008LDigest/Digest.py "Mail" "2021-04-13"> /home/admin/Logs/LogsSG008LMail.txt &
	y=`echo $!`
	x="$x\n$y --SG-008L_Mail"
	sleep 10
fi


if [ "$1" == "SG-004S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG004SDigest/MailDigest717.R &
	y=`echo $!`
	x="$x\n$y --SG-004S_Mail"
	sleep 10
fi


if [ "$1" == "IN-036S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN036Digest/MailDigestIN036.R &
	y=`echo $!`
	x="$x\n$y --IN-036S_Mail"
	sleep 10
fi

if [ "$1" == "IN-003W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN003WDigest/Live.py  > /home/admin/Logs/LogsIN003WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-003W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-004W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN004WDigest/Live.py  > /home/admin/Logs/LogsIN004WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-004W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-005W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN005WDigest/Live.py  > /home/admin/Logs/LogsIN005WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-005W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-006W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN006WDigest/Live.py  > /home/admin/Logs/LogsIN006WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-006W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-007W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN007WDigest/Live.py  > /home/admin/Logs/LogsIN007WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-007W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-009W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN009WDigest/Live.py  > /home/admin/Logs/LogsIN009WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-009W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-012L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3864177" "[IN-012L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN012LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-012L_History"
	sleep 30
fi

if [ "$1" == "IN-013W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN013WDigest/Live.py  > /home/admin/Logs/LogsIN013WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-013W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-014L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3850755" "[IN-014L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-10"> /home/admin/Logs/LogsIN014LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-014L_History"
	sleep 30
fi

if [ "$1" == "IN-017A_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN017ADigest/Live.py  > /home/admin/Logs/LogsIN017AHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-017A_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-018L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3818604" "[IN-018L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN018LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-018L_History"
	sleep 30
fi

if [ "$1" == "IN-019L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3878389" "[IN-019L]" "Asia/Kolkata" "Asia/Calcutta" "2020-09-26"> /home/admin/Logs/LogsIN019LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-019L_History"
	sleep 30
fi

if [ "$1" == "IN-021L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835525" "[IN-021L]" "Asia/Kolkata" "Asia/Calcutta" "2019-11-01"> /home/admin/Logs/LogsIN021LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-021L_History"
	sleep 30
fi

if [ "$1" == "IN-022L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3858321" "[IN-022L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-10"> /home/admin/Logs/LogsIN022LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-022L_History"
	sleep 30
fi

if [ "$1" == "IN-023L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3863937" "[IN-023L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-15"> /home/admin/Logs/LogsIN023LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-023L_History"
	sleep 30
fi

if [ "$1" == "IN-025L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3864693" "[IN-025L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-25"> /home/admin/Logs/LogsIN025LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-025L_History"
	sleep 30
fi


if [ "$1" == "IN-026L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3866779" "[IN-026L]" "Asia/Kolkata" "Asia/Calcutta" "2020-10-01"> /home/admin/Logs/LogsIN026LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-026L_History"
	sleep 30
fi


if [ "$1" == "IN-027L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3866025" "[IN-027L]" "Asia/Kolkata" "Asia/Calcutta" "2020-08-22"> /home/admin/Logs/LogsIN027LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-027L_History"
	sleep 30
fi

if [ "$1" == "IN-029W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN029WDigest/Live.py  > /home/admin/Logs/LogsIN029WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-029W_Gen_1"
	sleep 30 
fi


if [ "$1" == "IN-028L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3883745" "[IN-028L]" "Asia/Kolkata" "Asia/Calcutta" "2020-09-25"> /home/admin/Logs/LogsIN028LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-028L_History"
	sleep 30
fi

if [ "$1" == "IN-030L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3878378" "[IN-030L]" "Asia/Kolkata" "Asia/Calcutta" "2020-08-22"> /home/admin/Logs/LogsIN030LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-030L_History"
	sleep 30
fi


if [ "$1" == "IN-031L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3802193" "[IN-031L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN031LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-031L_History"
	sleep 30
fi

if [ "$1" == "IN-032W_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN032WDigest/Live.py  > /home/admin/Logs/LogsIN032WHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-032W_Gen_1"
	sleep 30 
fi

if [ "$1" == "IN-033L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3869286" "[IN-033L]" "Asia/Kolkata" "Asia/Calcutta" "2020-09-08"> /home/admin/Logs/LogsIN033LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-033L_History"
	sleep 30
fi


if [ "$1" == "IN-034L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898372" "[IN-034L]" "Asia/Kolkata" "Asia/Calcutta" "2021-01-01"> /home/admin/Logs/LogsIN034LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-034L_History"
	sleep 30
fi


if [ "$1" == "IN-035L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898418" "[IN-035L]" "Asia/Kolkata" "Asia/Calcutta" "2021-01-01"> /home/admin/Logs/LogsIN035LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-035L_History"
	sleep 30
fi

if [ "$1" == "IN-036A_Gen_1" ] || [ $RUNALL == 1 ] 
then
	python3 -u /home/admin/CODE/IN036ADigest/Live.py  > /home/admin/Logs/LogsIN036AHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-036A_Gen_1"
	sleep 10 
fi

if [ "$1" == "IN-038L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3765443" "[IN-038L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN038LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-038L_History"
	sleep 30
fi

if [ "$1" == "IN-037L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3860495" "[IN-037L]" "Asia/Kolkata" "Asia/Calcutta" "2020-05-28"> /home/admin/Logs/LogsIN037LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-037L_History"
	sleep 30
fi

if [ "$1" == "IN-040R_History" ] || [ $RUNALL == 1 ] 
then
	python3 /home/admin/CODE/IN040RDigest/Live.py &
	y=`echo $!`
	x="$x\n$y --IN-040R_History"
	sleep 30
fi

if [ "$1" == "IN-039L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3860862" "[IN-039L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-01"> /home/admin/Logs/LogsIN039LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-039L_History"
	sleep 30
fi


if [ "$1" == "IN-041L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835524" "[IN-041L]" "Asia/Kolkata" "Asia/Calcutta" "2019-11-01"> /home/admin/Logs/LogsIN041LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-041L_History"
	sleep 30
fi

if [ "$1" == "IN-042L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3777348" "[IN-042L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN042LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-042L_History"
	sleep 30
fi

if [ "$1" == "IN-043L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3777349" "[IN-043L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN043LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-043L_History"
	sleep 30
fi

if [ "$1" == "IN-044L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3897225" "[IN-044L]" "Asia/Kolkata" "Asia/Calcutta" "2021-01-01"> /home/admin/Logs/LogsIN044LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-044L_History"
	sleep 30
fi

if [ "$1" == "IN-045L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3794812" "[IN-045L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN045LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-045L_History"
	sleep 30
fi

if [ "$1" == "IN-046L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3900485" "[IN-046L]" "Asia/Kolkata" "Asia/Calcutta" "2021-02-01"> /home/admin/Logs/LogsIN046LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-046L_History"
	sleep 30
fi



if [ "$1" == "IN-047L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3794653" "[IN-047L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN047LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-047L_History"
	sleep 30
fi

if [ "$1" == "IN-048L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN048LDigest/Live.py  > /home/admin/Logs/LogsIN048LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-048L_History"
	sleep 30
fi

if [ "$1" == "IN-049L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3794253" "[IN-049L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN049LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-049L_History"
	sleep 30
fi

if [ "$1" == "IN-050L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1IN050.py "3864840" "[IN-050L]" "Asia/Kolkata" "Asia/Calcutta" "2020-08-04"> /home/admin/Logs/LogsIN050LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-050L_History"
	sleep 30
fi

if [ "$1" == "IN-051L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3787245" "[IN-051L]" "Asia/Kolkata" "Asia/Calcutta" "2019-07-01"> /home/admin/Logs/LogsIN051LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-051L_History"
	sleep 30
fi

if [ "$1" == "IN-052L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN052LDigest/Live.py > /home/admin/Logs/LogsIN052LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-052L_History"
	sleep 30
fi

if [ "$1" == "IN-053L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN053LDigest/Live.py > /home/admin/Logs/LogsIN053LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-053L_History"
	sleep 30
fi

if [ "$1" == "IN-054L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV6.py "3864876" "[IN-054L]" "Asia/Kolkata" "Asia/Calcutta" "2020-08-04"> /home/admin/Logs/LogsIN054LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-054L_History"
	sleep 30
fi

if [ "$1" == "IN-055L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3795317" "[IN-055L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN055LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-055L_History"
	sleep 30
fi


if [ "$1" == "IN-056L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN056LDigest/Live.py > /home/admin/Logs/LogsIN056LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-056L_History"
	sleep 30
fi


if [ "$1" == "IN-057L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3869097" "[IN-057L]" "Asia/Kolkata" "Asia/Calcutta" "2020-09-08"> /home/admin/Logs/LogsIN057LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-057L_History"
	sleep 30
fi


if [ "$1" == "IN-058L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN058LDigest/Live.py > /home/admin/Logs/LogsIN058LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-058L_History"
	sleep 30
fi


if [ "$1" == "IN-058L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN058LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-058L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-059L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3800132" "[IN-059L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN059LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-059L_History"
	sleep 30
fi

if [ "$1" == "IN-060L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN060LDigest/Live.py > /home/admin/Logs/LogsIN060LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-060L_History"
	sleep 30
fi

if [ "$1" == "IN-061L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3800671" "[IN-061L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN061LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-061L_History"
	sleep 30
fi

if [ "$1" == "IN-062L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3800635" "[IN-062L]" "Asia/Kolkata" "Asia/Calcutta" "2021-01-31"> /home/admin/Logs/LogsIN062LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-062L_History"
	sleep 30
fi

if [ "$1" == "IN-063L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3802488" "[IN-063L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN063LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-063L_History"
	sleep 30
fi

if [ "$1" == "IN-064L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN064LDigest/Live.py > /home/admin/Logs/LogsIN064LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-064L_History"
	sleep 30
fi

if [ "$1" == "IN-065L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3814767" "[IN-065L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN065LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-065L_History"
	sleep 30
fi

if [ "$1" == "IN-066L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN066LDigest/Live.py > /home/admin/Logs/LogsIN066LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-066L_History"
	sleep 30
fi

if [ "$1" == "IN-067L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3804923" "[IN-067L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN067LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-067L_History"
	sleep 30
fi

if [ "$1" == "IN-068L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN068LDigest/Live.py > /home/admin/Logs/LogsIN068LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-068L_History"
	sleep 30
fi

if [ "$1" == "IN-069L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN069LDigest/Live.py > /home/admin/Logs/LogsIN069LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-069L_History"
	sleep 30
fi

if [ "$1" == "IN-070L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3864775" "[IN-070L]" "Asia/Kolkata" "Asia/Calcutta" "2020-08-01"> /home/admin/Logs/LogsIN070LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-070L_History"
	sleep 10
fi

if [ "$1" == "IN-071L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3804130" "[IN-071L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN071LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-071L_History"
	sleep 30
fi

if [ "$1" == "IN-072L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3805118" "[IN-072L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN072LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-072L_History"
	sleep 30
fi

if [ "$1" == "IN-073L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3805120" "[IN-073L]" "Asia/Kolkata" "Asia/Calcutta" "2019-09-01" > /home/admin/Logs/LogsIN073LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-073L_History"
	sleep 30
fi

if [ "$1" == "IN-074L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3820177" "[IN-074L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN074LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-074L_History"
	sleep 30
fi

if [ "$1" == "IN-075L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN075LDigest/Live.py > /home/admin/Logs/LogsIN075LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-075L_History"
	sleep 30
fi
 
if [ "$1" == "IN-076L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3822707" "[IN-076L]" "Asia/Kolkata" "Asia/Calcutta" "2020-02-10"> /home/admin/Logs/LogsIN076LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-076L_History"
	sleep 30
fi

if [ "$1" == "IN-077L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3823815" "[IN-077L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN077LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-077L_History"
	sleep 30
fi

if [ "$1" == "IN-078L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3821964" "[IN-078L]" "Asia/Kolkata" "Asia/Calcutta" > /home/admin/Logs/LogsIN078LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-078L_History"
	sleep 30
fi

if [ "$1" == "IN-079L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3829586" "[IN-079L]" "Asia/Kolkata" "Asia/Calcutta" "2019-09-01"> /home/admin/Logs/LogsIN079LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-079L_History"
	sleep 30
fi

if [ "$1" == "IN-080L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN080LDigest/Live.py  > /home/admin/Logs/LogsIN080LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-080L_History"
	sleep 30
fi

if [ "$1" == "IN-081L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3836445" "[IN-081L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN081LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-081L_History"
	sleep 30
fi

if [ "$1" == "IN-082L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3835833" "[IN-082L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN082LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-082L_History"
	sleep 30
fi

if [ "$1" == "IN-083L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3839350" "[IN-083L]" "Asia/Kolkata" "Asia/Calcutta" "2019-12-01"> /home/admin/Logs/LogsIN083LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-083L_History"
	sleep 30
fi

if [ "$1" == "IN-084L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV6.py "3895542" "[IN-084L]" "Asia/Kolkata" "Asia/Calcutta" "2020-12-10"> /home/admin/Logs/LogsIN084LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-084L_History"
	sleep 30
fi


if [ "$1" == "IN-085L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3849158" "[IN-085L]" "Asia/Kolkata" "Asia/Calcutta" "2020-02-10"> /home/admin/Logs/LogsIN085LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-085L_History"
	sleep 30
fi

if [ "$1" == "IN-086L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3850328" "[IN-086L]" "Asia/Kolkata" "Asia/Calcutta" "2020-02-10"> /home/admin/Logs/LogsIN086LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-086L_History"
	sleep 30
fi

if [ "$1" == "IN-087L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3850906" "[IN-087L]" "Asia/Kolkata" "Asia/Calcutta" "2020-03-01"> /home/admin/Logs/LogsIN087LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-087L_History"
	sleep 30
fi

if [ "$1" == "IN-088L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3862554" "[IN-088L]" "Asia/Kolkata" "Asia/Calcutta" "2020-06-10"> /home/admin/Logs/LogsIN088LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-088L_History"
	sleep 30
fi

if [ "$1" == "IN-089L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3851158" "[IN-089L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-01"> /home/admin/Logs/LogsIN089LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-089L_History"
	sleep 30
fi

if [ "$1" == "IN-090L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3864358" "[IN-090L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-21"> /home/admin/Logs/LogsIN090LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-090L_History"
	sleep 30
fi

if [ "$1" == "IN-091L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3878562" "[IN-091L]" "Asia/Kolkata" "Asia/Calcutta" "2020-10-14"> /home/admin/Logs/LogsIN091LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-091L_History"
	sleep 30
fi

if [ "$1" == "IN-092L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3886200" "[IN-092L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-21"> /home/admin/Logs/LogsIN092LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-092L_History"
	sleep 30
fi

if [ "$1" == "IN-093L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3892115" "[IN-093L]" "Asia/Kolkata" "Asia/Calcutta" "2020-11-01"> /home/admin/Logs/LogsIN093LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-093L_History"
	sleep 30
fi

if [ "$1" == "IN-094L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3888886" "[IN-094L]" "Asia/Kolkata" "Asia/Calcutta" "2020-11-01"> /home/admin/Logs/LogsIN094LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-094L_History"
	sleep 30
fi

if [ "$1" == "IN-095L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898116" "[IN-095L]" "Asia/Kolkata" "Asia/Calcutta" "2021-01-01"> /home/admin/Logs/LogsIN095LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-095L_History"
	sleep 30
fi



if [ "$1" == "IN-096L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3892890" "[IN-096L]" "Asia/Kolkata" "Asia/Calcutta" "2020-11-01"> /home/admin/Logs/LogsIN096LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-096L_History"
	sleep 30
fi

if [ "$1" == "IN-097L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898862" "[IN-097L]" "Asia/Kolkata" "Asia/Calcutta" "2021-01-01"> /home/admin/Logs/LogsIN097LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-097L_History"
	sleep 30
fi

if [ "$1" == "IN-098L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3900526" "[IN-098L]" "Asia/Kolkata" "Asia/Calcutta" "2021-01-01"> /home/admin/Logs/LogsIN098LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-098L_History"
	sleep 30
fi


if [ "$1" == "IN-099L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3902198" "[IN-099L]" "Asia/Kolkata" "Asia/Calcutta" "2021-03-01"> /home/admin/Logs/LogsIN099LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-099L_History"
	sleep 30
fi



if [ "$1" == "IN-100L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3907030" "[IN-100L]" "Asia/Kolkata" "Asia/Calcutta" "2021-03-01"> /home/admin/Logs/LogsIN100LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-100L_History"
	sleep 30
fi


if [ "$1" == "IN-101L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3908273" "[IN-101L]" "Asia/Kolkata" "Asia/Calcutta" "2021-04-01"> /home/admin/Logs/LogsIN101LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-101L_History"
	sleep 30
fi



if [ "$1" == "IN-301L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live1min.py "3862993" "[IN-301L]" "Asia/Kolkata" "Asia/Calcutta" "2020-07-01"> /home/admin/Logs/LogsIN301LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-301L_History"
	sleep 30
fi

if [ "$1" == "IN-302L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV6.py "3866833" "[IN-302L]" "Asia/Kolkata" "Asia/Calcutta" "2020-09-01"> /home/admin/Logs/LogsIN302LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-302L_History"
	sleep 30
fi


if [ "$1" == "IN-303L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV6.py "3869138" "[IN-303L]" "Asia/Kolkata" "Asia/Calcutta" "2020-12-01"> /home/admin/Logs/LogsIN303LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-303L_History"
	sleep 30
fi

if [ "$1" == "IN-304L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV6.py "3884281" "[IN-304L]" "Asia/Kolkata" "Asia/Calcutta" "2020-10-10"> /home/admin/Logs/LogsIN304LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-304L_History"
	sleep 30
fi


if [ "$1" == "IN-305L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV6.py "3893692" "[IN-305L]" "Asia/Kolkata" "Asia/Calcutta" "2020-11-25"> /home/admin/Logs/LogsIN305LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-305L_History"
	sleep 30
fi


if [ "$1" == "IN-306L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN306LDigest/Live.py "3904683" "[IN-306L]" "Asia/Kolkata" "Asia/Calcutta" "2021-04-01"> /home/admin/Logs/LogsIN306LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-306L_History"
	sleep 30
fi

if [ "$1" == "IN-307L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN307LDigest/Live.py "3904683" "[IN-307L]" "Asia/Kolkata" "Asia/Calcutta" "2021-04-01"> /home/admin/Logs/LogsIN307LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-307L_History"
	sleep 30
fi

if [ "$1" == "IN-308L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN308LDigest/Live.py "3904683" "[IN-308L]" "Asia/Kolkata" "Asia/Calcutta" "2021-04-01"> /home/admin/Logs/LogsIN308LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-308L_History"
	sleep 30
fi



if [ "$1" == "IN-309L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN309LDigest/Live.py "3904683" "[IN-309L]" "Asia/Kolkata" "Asia/Calcutta" "2021-05-01"> /home/admin/Logs/LogsIN309LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-309L_History"
	sleep 30
fi


if [ "$1" == "KH-009L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3848700" "[KH-009L]" "Asia/Phnom_Penh" "Asia/Phnom_Penh" "2020-02-01"> /home/admin/Logs/LogsKH009LHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-009L_History"
	sleep 30
fi

if [ "$1" == "MY-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3862535" "[MY-002L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-06-15"> /home/admin/Logs/LogsMY002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-002L_History"
	sleep 30
fi

if [ "$1" == "MY-005L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3819590" "[MY-005L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2019-06-01"> /home/admin/Logs/LogsMY005LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-005L_History"
	sleep 30
fi	

if [ "$1" == "MY-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3827749" "[MY-007L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2019-12-01"> /home/admin/Logs/LogsMY007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-007L_History"
	sleep 30
fi

if [ "$1" == "MY-008L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3824826" "[MY-008L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2019-12-01"> /home/admin/Logs/LogsMY008LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-008L_History"
	sleep 30
fi

if [ "$1" == "MY-009L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3836443" "[MY-009L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2019-12-01"> /home/admin/Logs/LogsMY009LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-009L_History"
	sleep 30
fi

if [ "$1" == "MY-010L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3849442" "[MY-010L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-02-01"> /home/admin/Logs/LogsMY010LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-010L_History"
	sleep 30
fi

if [ "$1" == "MY-011L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3871876" "[MY-011L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-20"> /home/admin/Logs/LogsMY011LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-011L_History"
	sleep 30
fi

if [ "$1" == "MY-012L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/MY012LDigest/Live.py "3878380" "[MY-012L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-22"> /home/admin/Logs/LogsMY012LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-012L_History"
	sleep 30
fi

if [ "$1" == "MY-013L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3886295" "[MY-013L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-10"> /home/admin/Logs/LogsMY013LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-013L_History"
	sleep 30
fi

if [ "$1" == "MY-014L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3895797" "[MY-014L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-10"> /home/admin/Logs/LogsMY014LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-014L_History"
	sleep 30
fi

if [ "$1" == "MY-015L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3900786" "[MY-015L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY015LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-015L_History"
	sleep 30
fi



if [ "$1" == "MY-016L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3908296" "[MY-016L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY016LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-016L_History"
	sleep 30
fi





if [ "$1" == "IN-015L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3899851" "[IN-015L]" "Asia/Kolkata" "Asia/Calcutta" "2021-02-01"> /home/admin/Logs/LogsIN015LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-015L_History"
	sleep 30
fi




if [ "$1" == "IN-016L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3897240" "[IN-016L]" "Asia/Kolkata" "Asia/Calcutta" "2021-01-01"> /home/admin/Logs/LogsIN016LHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-016L_History"
	sleep 30
fi




if [ "$1" == "MY-401L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3845432" "[MY-401L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY401LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-401L_History"
	sleep 30
fi

if [ "$1" == "MY-402L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3845631" "[MY-402L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY402LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-402L_History"
	sleep 30
fi

if [ "$1" == "MY-403L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3845864" "[MY-403L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY403LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-403L_History"
	sleep 30
fi

if [ "$1" == "MY-404L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3846888" "[MY-404L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-02-01"> /home/admin/Logs/LogsMY404LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-404L_History"
	sleep 30
fi

if [ "$1" == "MY-405L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3847585" "[MY-405L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-01-01"> /home/admin/Logs/LogsMY405LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-405L_History"
	sleep 30
fi

if [ "$1" == "MY-406L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3848698" "[MY-406L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-02-01"> /home/admin/Logs/LogsMY406LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-406L_History"
	sleep 30
fi

if [ "$1" == "MY-407L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865271" "[MY-407L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY407LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-407L_History"
	sleep 30
fi

if [ "$1" == "MY-408L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865052" "[MY-408L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY408LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-408L_History"
	sleep 30
fi

if [ "$1" == "MY-409L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865316" "[MY-409L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY409LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-409L_History"
	sleep 30
fi

if [ "$1" == "MY-410L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865451" "[MY-410L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY410LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-410L_History"
	sleep 30
fi

if [ "$1" == "MY-411L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865502" "[MY-411L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY411LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-411L_History"
	sleep 30
fi

if [ "$1" == "MY-412L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865515" "[MY-412L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY412LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-412L_History"
	sleep 30
fi


if [ "$1" == "MY-413L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865526" "[MY-413L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY413LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-413L_History"
	sleep 30
fi

if [ "$1" == "MY-414L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3865896" "[MY-414L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-21"> /home/admin/Logs/LogsMY414LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-414L_History"
	sleep 30
fi

if [ "$1" == "MY-415L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3866594" "[MY-415L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-25"> /home/admin/Logs/LogsMY415LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-415L_History"
	sleep 30
fi

if [ "$1" == "MY-416L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3866705" "[MY-416L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-25"> /home/admin/Logs/LogsMY416LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-416L_History"
	sleep 30
fi

if [ "$1" == "MY-417L_History" ] || [ $RUNALL == 1 ]  
then 
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3866777" "[MY-417L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-08-27"> /home/admin/Logs/LogsMY417LHistory.txt & 
	y=`echo $!` 
	x="$x\n$y --MY-417L_History" 
	sleep 30 
fi

if [ "$1" == "MY-418L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3866866" "[MY-418L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-05"> /home/admin/Logs/LogsMY418LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-418L_History"
	sleep 30
fi


if [ "$1" == "MY-419L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869062" "[MY-419L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-05"> /home/admin/Logs/LogsMY419LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-419L_History"
	sleep 30
fi

if [ "$1" == "MY-420L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869136" "[MY-420L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY420LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-420L_History"
	sleep 30
fi

if [ "$1" == "MY-421L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869677" "[MY-421L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY421LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-421L_History"
	sleep 30
fi

if [ "$1" == "MY-422L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869173" "[MY-422L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY422LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-422L_History"
	sleep 30
fi

if [ "$1" == "MY-423L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869288" "[MY-423L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY423LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-423L_History"
	sleep 30
fi

if [ "$1" == "MY-424L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869291" "[MY-424L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY424LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-424L_History"
	sleep 30
fi

if [ "$1" == "MY-425L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3869096" "[MY-425L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-20"> /home/admin/Logs/LogsMY425LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-425L_History"
	sleep 30
fi
	
if [ "$1" == "MY-426L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3870203" "[MY-426L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY426LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-426L_History"
	sleep 30
fi

if [ "$1" == "MY-427L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3871874" "[MY-427L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-16"> /home/admin/Logs/LogsMY427LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-427L_History"
	sleep 30
fi

if [ "$1" == "MY-428L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3873918" "[MY-428L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-08"> /home/admin/Logs/LogsMY428LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-428L_History"
	sleep 30
fi

if [ "$1" == "MY-429L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3878377" "[MY-429L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-20"> /home/admin/Logs/LogsMY429LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-429L_History"
	sleep 30
fi

if [ "$1" == "MY-430L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3878379" "[MY-430L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-20"> /home/admin/Logs/LogsMY430LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-430L_History"
	sleep 30
fi

if [ "$1" == "MY-431L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3878388" "[MY-431L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-22"> /home/admin/Logs/LogsMY431LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-431L_History"
	sleep 30
fi

if [ "$1" == "MY-432L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3883755" "[MY-432L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-25"> /home/admin/Logs/LogsMY432LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-432L_History"
	sleep 30
fi

if [ "$1" == "MY-433L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3883756" "[MY-433L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-09-25"> /home/admin/Logs/LogsMY433LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-433L_History"
	sleep 30
fi

if [ "$1" == "MY-434L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3883824" "[MY-434L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-01"> /home/admin/Logs/LogsMY434LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-434L_History"
	sleep 30
fi

if [ "$1" == "MY-435L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3884278" "[MY-435L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-01"> /home/admin/Logs/LogsMY435LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-435L_History"
	sleep 30
fi

if [ "$1" == "MY-436L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3884280" "[MY-436L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-01"> /home/admin/Logs/LogsMY436LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-436L_History"
	sleep 30
fi

if [ "$1" == "MY-437L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3884282" "[MY-437L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-01"> /home/admin/Logs/LogsMY437LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-437L_History"
	sleep 30
fi

if [ "$1" == "MY-438L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3886199" "[MY-438L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-01"> /home/admin/Logs/LogsMY438LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-438L_History"
	sleep 30
fi

if [ "$1" == "MY-439L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3886302" "[MY-439L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-10"> /home/admin/Logs/LogsMY439LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-439L_History"
	sleep 30
fi

if [ "$1" == "MY-440L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3886301" "[MY-440L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-10"> /home/admin/Logs/LogsMY440LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-440L_History"
	sleep 30
fi


if [ "$1" == "MY-441L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3886304" "[MY-441L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-10"> /home/admin/Logs/LogsMY441LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-441L_History"
	sleep 30
fi

if [ "$1" == "MY-442L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3888334" "[MY-442L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-10"> /home/admin/Logs/LogsMY442LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-442L_History"
	sleep 30
fi


if [ "$1" == "MY-443L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3888413" "[MY-443L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-10"> /home/admin/Logs/LogsMY443LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-443L_History"
	sleep 30
fi


if [ "$1" == "MY-444L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3888382" "[MY-444L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-10"> /home/admin/Logs/LogsMY444LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-444L_History"
	sleep 30
fi

if [ "$1" == "MY-445L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3888543" "[MY-445L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-18"> /home/admin/Logs/LogsMY445LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-445L_History"
	sleep 30
fi

if [ "$1" == "MY-446L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3888412" "[MY-446L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-10"> /home/admin/Logs/LogsMY446LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-446L_History"
	sleep 30
fi

if [ "$1" == "MY-447L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3888542" "[MY-447L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-18"> /home/admin/Logs/LogsMY447LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-447L_History"
	sleep 30
fi


if [ "$1" == "MY-448L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3892041" "[MY-448L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-20"> /home/admin/Logs/LogsMY448LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-448L_History"
	sleep 30
fi

if [ "$1" == "MY-449L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3888890" "[MY-449L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-20"> /home/admin/Logs/LogsMY449LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-449L_History"
	sleep 30
fi


if [ "$1" == "MY-450L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3892268" "[MY-450L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-10-28"> /home/admin/Logs/LogsMY450LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-450L_History"
	sleep 30
fi

if [ "$1" == "MY-451L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3892420" "[MY-451L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-01"> /home/admin/Logs/LogsMY451LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-451L_History"
	sleep 30
fi


if [ "$1" == "MY-452L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3892287" "[MY-452L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-01"> /home/admin/Logs/LogsMY452LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-452L_History"
	sleep 30
fi


if [ "$1" == "MY-453L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3892385" "[MY-453L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-01"> /home/admin/Logs/LogsMY453LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-453L_History"
	sleep 30
fi


if [ "$1" == "MY-454L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3892387" "[MY-454L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-01"> /home/admin/Logs/LogsMY454LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-454L_History"
	sleep 30
fi

if [ "$1" == "MY-455L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3892417" "[MY-455L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-01"> /home/admin/Logs/LogsMY455LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-455L_History"
	sleep 30
fi

if [ "$1" == "MY-456L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3892690" "[MY-456L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-01"> /home/admin/Logs/LogsMY456LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-456L_History"
	sleep 30
fi

if [ "$1" == "MY-458L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3893396" "[MY-458L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY458LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-458L_History"
	sleep 10
fi



if [ "$1" == "MY-457L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3892686" "[MY-457L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-01"> /home/admin/Logs/LogsMY457LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-457L_History"
	sleep 10
fi


if [ "$1" == "MY-459L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3893694" "[MY-459L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY459LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-459L_History"
	sleep 10
fi


if [ "$1" == "MY-460L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3892687" "[MY-460L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY460LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-460L_History"
	sleep 10
fi


if [ "$1" == "MY-461L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3893401" "[MY-461L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY461LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-461L_History"
	sleep 10
fi

if [ "$1" == "MY-462L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3893393" "[MY-462L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-01"> /home/admin/Logs/LogsMY462LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-462L_History"
	sleep 10
fi

if [ "$1" == "MY-463L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3893400" "[MY-463L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY463LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-463L_History"
	sleep 10
fi

if [ "$1" == "MY-464L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3893643" "[MY-464L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY464LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-464L_History"
	sleep 10
fi


if [ "$1" == "MY-465L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3893716" "[MY-465L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY465LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-465L_History"
	sleep 10
fi

if [ "$1" == "MY-466L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3893693" "[MY-466L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY466LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-466L_History"
	sleep 10
fi

if [ "$1" == "MY-467L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3893740" "[MY-467L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY467LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-467L_History"
	sleep 10
fi

if [ "$1" == "MY-468L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3893739" "[MY-468L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY468LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-468L_History"
	sleep 10
fi


if [ "$1" == "MY-469L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898993" "[MY-469L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY469LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-469L_History"
	sleep 10
fi



if [ "$1" == "MY-470L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3893953" "[MY-470L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY470LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-470L_History"
	sleep 10
fi

if [ "$1" == "MY-471L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3894588" "[MY-471L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY471LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-471L_History"
	sleep 10
fi

if [ "$1" == "MY-472L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3894589" "[MY-472L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY472LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-472L_History"
	sleep 10
fi

if [ "$1" == "MY-473L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3894586" "[MY-473L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY473LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-473L_History"
	sleep 10
fi

if [ "$1" == "MY-474L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3894604" "[MY-474L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY474LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-474L_History"
	sleep 10
fi



if [ "$1" == "MY-475L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3899559" "[MY-475L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY475LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-475L_History"
	sleep 10
fi



if [ "$1" == "MY-476L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898861" "[MY-476L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY476LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-476L_History"
	sleep 10
fi


if [ "$1" == "MY-477L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3902091" "[MY-477L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY477LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-477L_History"
	sleep 10
fi


if [ "$1" == "MY-478L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3894603" "[MY-478L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY478LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-478L_History"
	sleep 10
fi

if [ "$1" == "MY-479L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3894865" "[MY-479L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY478LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-479L_History"
	sleep 10
fi

if [ "$1" == "MY-480L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3894866" "[MY-480L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-11-10"> /home/admin/Logs/LogsMY480LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-480L_History"
	sleep 10
fi


if [ "$1" == "MY-481L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3895123" "[MY-481L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-01"> /home/admin/Logs/LogsMY481LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-481L_History"
	sleep 10
fi



if [ "$1" == "MY-482L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3895121" "[MY-482L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-01"> /home/admin/Logs/LogsMY482LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-482L_History"
	sleep 10
fi


if [ "$1" == "MY-483L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3895122" "[MY-483L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-01"> /home/admin/Logs/LogsMY483LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-483L_History"
	sleep 10
fi


if [ "$1" == "MY-484L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3895278" "[MY-484L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-01"> /home/admin/Logs/LogsMY484LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-484L_History"
	sleep 10
fi

if [ "$1" == "MY-485L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3895731" "[MY-485L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-01"> /home/admin/Logs/LogsMY485LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-485L_History"
	sleep 10
fi

if [ "$1" == "MY-486L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3895732" "[MY-486L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-01"> /home/admin/Logs/LogsMY486LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-486L_History"
	sleep 10
fi

if [ "$1" == "MY-487L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3895954" "[MY-487L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-01"> /home/admin/Logs/LogsMY487LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-487L_History"
	sleep 10
fi

if [ "$1" == "MY-488L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3895977" "[MY-488L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-20"> /home/admin/Logs/LogsMY488LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-488L_History"
	sleep 10
fi

if [ "$1" == "MY-489L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3895975" "[MY-489L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-20"> /home/admin/Logs/LogsMY489LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-489L_History"
	sleep 10
fi

if [ "$1" == "MY-490L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3895976" "[MY-490L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-20"> /home/admin/Logs/LogsMY490LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-490L_History"
	sleep 10
fi


if [ "$1" == "MY-491L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3897223" "[MY-491L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-20"> /home/admin/Logs/LogsMY491LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-491L_History"
	sleep 10
fi


if [ "$1" == "MY-492L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898073" "[MY-492L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-25"> /home/admin/Logs/LogsMY492LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-492L_History"
	sleep 10
fi



if [ "$1" == "MY-493L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898068" "[MY-493L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2020-12-25"> /home/admin/Logs/LogsMY493LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-493L_History"
	sleep 10
fi



if [ "$1" == "MY-494L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898435" "[MY-494L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY494LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-494L_History"
	sleep 10
fi



if [ "$1" == "MY-495L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3904681" "[MY-495L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY495LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-495L_History"
	sleep 10
fi



if [ "$1" == "MY-496L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3899533" "[MY-496L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY496LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-496L_History"
	sleep 10
fi


if [ "$1" == "MY-497L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3899532" "[MY-497L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY497LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-497L_History"
	sleep 10
fi


if [ "$1" == "MY-498L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898135" "[MY-498L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY498LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-498L_History"
	sleep 10
fi


if [ "$1" == "MY-499L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898134" "[MY-499L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY499LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-499L_History"
	sleep 10
fi

if [ "$1" == "MY-500L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898446" "[MY-500L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY500LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-500L_History"
	sleep 10
fi



if [ "$1" == "MY-501L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898463" "[MY-501L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY501LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-501L_History"
	sleep 10
fi


if [ "$1" == "MY-502L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898464" "[MY-502L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY502LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-502L_History"
	sleep 10
fi

if [ "$1" == "MY-503L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898992" "[MY-503L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY503LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-503L_History"
	sleep 10
fi


if [ "$1" == "MY-504L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898995" "[MY-504L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY504LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-504L_History"
	sleep 10
fi


if [ "$1" == "MY-505L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3898994" "[MY-505L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY505LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-505L_History"
	sleep 10
fi


if [ "$1" == "MY-506L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3899684" "[MY-506L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY506LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-506L_History"
	sleep 10
fi

if [ "$1" == "MY-507L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3899685" "[MY-507L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY507LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-507L_History"
	sleep 10
fi



if [ "$1" == "MY-508L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3899846" "[MY-508L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY508LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-508L_History"
	sleep 10
fi



if [ "$1" == "MY-509L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3900484" "[MY-509L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY509LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-509L_History"
	sleep 10
fi


if [ "$1" == "MY-510L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3899847" "[MY-510L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY510LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-510L_History"
	sleep 10
fi


if [ "$1" == "MY-511L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3899848" "[MY-511L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY511LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-511L_History"
	sleep 10
fi



if [ "$1" == "MY-512L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3900208" "[MY-512L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY512LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-512L_History"
	sleep 10
fi


if [ "$1" == "MY-513L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3900471" "[MY-513L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-01-01"> /home/admin/Logs/LogsMY513LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-513L_History"
	sleep 10
fi


if [ "$1" == "MY-514L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3900709" "[MY-514L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY514LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-514L_History"
	sleep 10
fi



if [ "$1" == "MY-515L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3900707" "[MY-515L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY515LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-515L_History"
	sleep 10
fi


if [ "$1" == "MY-516L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3900708" "[MY-516L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY516LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-516L_History"
	sleep 10
fi


if [ "$1" == "MY-517L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3900785" "[MY-517L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY517LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-517L_History"
	sleep 10
fi


if [ "$1" == "MY-518L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3900806" "[MY-518L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY518LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-518L_History"
	sleep 10
fi



if [ "$1" == "MY-519L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3902197" "[MY-519L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY519LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-519L_History"
	sleep 10
fi



if [ "$1" == "MY-520L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3902222" "[MY-520L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY520LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-520L_History"
	sleep 10
fi


if [ "$1" == "MY-521L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3902223" "[MY-521L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY521LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-521L_History"
	sleep 10
fi


if [ "$1" == "MY-522L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3902275" "[MY-522L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY522LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-522L_History"
	sleep 10
fi



if [ "$1" == "MY-523L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3902274" "[MY-523L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY523LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-523L_History"
	sleep 10
fi



if [ "$1" == "MY-524L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3902286" "[MY-524L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-02-01"> /home/admin/Logs/LogsMY524LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-524L_History"
	sleep 10
fi

if [ "$1" == "MY-525L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3902471" "[MY-525L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY525LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-525L_History"
	sleep 10
fi


if [ "$1" == "MY-526L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3902472" "[MY-526L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY526LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-526L_History"
	sleep 10
fi



if [ "$1" == "MY-528L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3904684" "[MY-528L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY528LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-528L_History"
	sleep 10
fi



if [ "$1" == "MY-529L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3907028" "[MY-529L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY529LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-529L_History"
	sleep 10
fi


if [ "$1" == "MY-530L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3907302" "[MY-530L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY530LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-530L_History"
	sleep 10
fi


if [ "$1" == "MY-531L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3907301" "[MY-531L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY531LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-531L_History"
	sleep 10
fi


if [ "$1" == "MY-532L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3907303" "[MY-532L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY532LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-532L_History"
	sleep 10
fi


if [ "$1" == "MY-533L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3907587" "[MY-533L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY533LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-533L_History"
	sleep 10
fi



if [ "$1" == "MY-534L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3907606" "[MY-534L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY534LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-534L_History"
	sleep 10
fi

if [ "$1" == "MY-535L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3907607" "[MY-535L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY535LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-535L_History"
	sleep 10
fi


if [ "$1" == "MY-536L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3907885" "[MY-536L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY536LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-536L_History"
	sleep 10
fi


if [ "$1" == "MY-537L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3908225" "[MY-537L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY537LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-537L_History"
	sleep 10
fi


if [ "$1" == "MY-538L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3908227" "[MY-538L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY538LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-538L_History"
	sleep 10
fi



if [ "$1" == "MY-539L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3908292" "[MY-539L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY539LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-539L_History"
	sleep 10
fi



if [ "$1" == "MY-540L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3908293" "[MY-540L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY540LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-540L_History"
	sleep 10
fi



if [ "$1" == "MY-541L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3909181" "[MY-541L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY541LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-541L_History"
	sleep 10
fi



if [ "$1" == "MY-544L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3909179" "[MY-544L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY544LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-544L_History"
	sleep 10
fi



if [ "$1" == "MY-542L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3909170" "[MY-542L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY542LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-542L_History"
	sleep 10
fi



if [ "$1" == "MY-543L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3909171" "[MY-543L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-25"> /home/admin/Logs/LogsMY543LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-543L_History"
	sleep 10
fi




if [ "$1" == "MY-545L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3909185" "[MY-545L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY545LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-545L_History"
	sleep 10
fi


if [ "$1" == "MY-546L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3909215" "[MY-546L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-04-01"> /home/admin/Logs/LogsMY546LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-546L_History"
	sleep 10
fi



if [ "$1" == "MY-547L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3909587" "[MY-547L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-04-01"> /home/admin/Logs/LogsMY547LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-547L_History"
	sleep 10
fi



if [ "$1" == "MY-548L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3909588" "[MY-548L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-04-01"> /home/admin/Logs/LogsMY548LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-548L_History"
	sleep 10
fi




if [ "$1" == "MY-549L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3909567" "[MY-549L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-04-01"> /home/admin/Logs/LogsMY549LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-549L_History"
	sleep 10
fi



if [ "$1" == "MY-550L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3914578" "[MY-550L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-04-01"> /home/admin/Logs/LogsMY550LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-550L_History"
	sleep 10
fi


if [ "$1" == "MY-551L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3914577" "[MY-551L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-04-01"> /home/admin/Logs/LogsMY551LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-551L_History"
	sleep 10
fi


if [ "$1" == "MY-552L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3922088" "[MY-552L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-04-01"> /home/admin/Logs/LogsMY552LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-552L_History"
	sleep 10
fi


if [ "$1" == "MY-553L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3922108" "[MY-553L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-04-01"> /home/admin/Logs/LogsMY553LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-553L_History"
	sleep 10
fi


if [ "$1" == "MY-554L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3922247" "[MY-554L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-05-01"> /home/admin/Logs/LogsMY554LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-554L_History"
	sleep 10
fi


if [ "$1" == "MY-555L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3922123" "[MY-555L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-04-01"> /home/admin/Logs/LogsMY555LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-555L_History"
	sleep 10
fi


if [ "$1" == "MY-556L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3922248" "[MY-556L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-05-01"> /home/admin/Logs/LogsMY556LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-556L_History"
	sleep 10
fi


if [ "$1" == "MY-557L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3922249" "[MY-557L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-05-01"> /home/admin/Logs/LogsMY557LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-557L_History"
	sleep 10
fi



if [ "$1" == "MY-558L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3922686" "[MY-558L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-05-01"> /home/admin/Logs/LogsMY558LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-558L_History"
	sleep 10
fi



if [ "$1" == "MY-560L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3926246" "[MY-560L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-05-01"> /home/admin/Logs/LogsMY560LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-560L_History"
	sleep 10
fi




if [ "$1" == "MY-561L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3926265" "[MY-561L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-05-01"> /home/admin/Logs/LogsMY561LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-561L_History"
	sleep 10
fi




if [ "$1" == "MY-562L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3926264" "[MY-562L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-05-01"> /home/admin/Logs/LogsMY562LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-562L_History"
	sleep 10
fi



if [ "$1" == "MY-563L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3926378" "[MY-563L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-05-01"> /home/admin/Logs/LogsMY563LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-563L_History"
	sleep 10
fi


if [ "$1" == "MY-559L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3925039" "[MY-559L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-05-01"> /home/admin/Logs/LogsMY559LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-559L_History"
	sleep 10
fi


if [ "$1" == "MY-564L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3926473" "[MY-564L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-05-01"> /home/admin/Logs/LogsMY564LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-564L_History"
	sleep 10
fi


if [ "$1" == "PH-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3830382" "[PH-002L]" "Asia/Manila" "Asia/Manila" "2019-07-01"> /home/admin/Logs/LogsPH002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --PH-002L_History"
	sleep 30
fi

if [ "$1" == "SG-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3795604" "[SG-007L]" "Asia/Singapore" "Asia/Singapore" > /home/admin/Logs/LogsSG007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-007L_History"
	sleep 30
fi

if [ "$1" == "SG-008L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3836631" "[SG-008L]" "Asia/Singapore" "Asia/Singapore" "2019-12-01" > /home/admin/Logs/LogsSG008LHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-008L_History"
	sleep 30
fi


if [ "$1" == "TH-401L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3897212" "[TH-401L]" "Asia/Bangkok" "Asia/Bangkok" "2020-12-20"> /home/admin/Logs/LogsTH401LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-401L_History"
	sleep 30
fi


if [ "$1" == "TH-402L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3908294" "[TH-402L]" "Asia/Bangkok" "Asia/Bangkok" "2021-04-01"> /home/admin/Logs/LogsTH402LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-402L_History"
	sleep 30
fi


if [ "$1" == "TH-403L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3908295" "[TH-403L]" "Asia/Bangkok" "Asia/Bangkok" "2021-04-01"> /home/admin/Logs/LogsTH403LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-403L_History"
	sleep 30
fi



if [ "$1" == "TH-404L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3907608" "[TH-404L]" "Asia/Bangkok" "Asia/Bangkok" "2021-03-01"> /home/admin/Logs/LogsTH404LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-404L_History"
	sleep 30
fi



if [ "$1" == "TH-901L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3907709" "[TH-901L]" "Asia/Bangkok" "Asia/Bangkok" "2021-03-01"> /home/admin/Logs/LogsTH901LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-901L_History"
	sleep 30
fi


if [ "$1" == "TH-001L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3865479" "[TH-001L]" "Asia/Bangkok" "Asia/Bangkok" "2020-08-17"> /home/admin/Logs/LogsTH001LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-001L_History"
	sleep 30
fi


if [ "$1" == "TH-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3862423" "[TH-002L]" "Asia/Bangkok" "Asia/Bangkok" "2020-06-15"> /home/admin/Logs/LogsTH002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-002L_History"
	sleep 30
fi

if [ "$1" == "TH-004L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835526" "[TH-004L]" "Asia/Bangkok" "Asia/Bangkok" "2019-11-01" > /home/admin/Logs/LogsTH004LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-004L_History"
	sleep 30
fi

if [ "$1" == "TH-005L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV2.py "3835213" "[TH-005L]" "Asia/Bangkok" "Asia/Bangkok" "2019-11-01"> /home/admin/Logs/LogsTH005LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-005L_History"
	sleep 30
fi

if [ "$1" == "TH-006L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV4.py "3864365" "[TH-006L]" "Asia/Bangkok" "Asia/Bangkok" "2020-07-25"> /home/admin/Logs/LogsTH006LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-006L_History"
	sleep 30
fi

if [ "$1" == "TH-007L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3846845" "[TH-007L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-01"> /home/admin/Logs/LogsTH007LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-007L_History"
	sleep 30
fi

if [ "$1" == "TH-008L_History" ] || [ $RUNALL == 1 ]  
then 
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3871877" "[TH-008L]" "Asia/Bangkok" "Asia/Bangkok" "2020-09-14"> /home/admin/Logs/LogsTH008LHistory.txt & 
	y=`echo $!` 
	x="$x\n$y --TH-008L_History"
	sleep 30
fi


if [ "$1" == "MY-527L_History" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveMY.py "3902473" "[MY-527L]" "Asia/Kuala_Lumpur" "Asia/Kuala_Lumpur" "2021-03-01"> /home/admin/Logs/LogsMY527LHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-527L_History"
	sleep 10
fi


if [ "$1" == "TH-009L_History" ] || [ $RUNALL == 1 ]  
then 
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3895798" "[TH-009L]" "Asia/Bangkok" "Asia/Bangkok" "2020-12-20"> /home/admin/Logs/LogsTH009LHistory.txt & 
	y=`echo $!` 
	x="$x\n$y --TH-009L_History"
	sleep 30
fi


if [ "$1" == "TH-010L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3839347" "[TH-010L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH010LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-010L_History"
	sleep 30
fi	

if [ "$1" == "TH-011L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3839348" "[TH-011L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH011LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-011L_History"
	sleep 30
fi

if [ "$1" == "TH-012L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3840208" "[TH-012L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH012LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-012L_History"
	sleep 30
fi

if [ "$1" == "TH-013L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3839597" "[TH-013L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH013LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-013L_History"
	sleep 30
fi

if [ "$1" == "TH-014L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3844885" "[TH-014L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH014LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-014L_History"
	sleep 30
fi

if [ "$1" == "TH-015L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3840207" "[TH-015L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH015LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-015L_History"
	sleep 30
fi	

if [ "$1" == "TH-016L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3840369" "[TH-016L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH016LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-016L_History"
	sleep 30
fi

if [ "$1" == "TH-017L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3839502" "[TH-017L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH017LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-017L_History"
	sleep 30
fi	

if [ "$1" == "TH-018L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3841478" "[TH-018L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH018LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-018L_History"
	sleep 30
fi	

if [ "$1" == "TH-019L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3841479" "[TH-019L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH019LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-019L_History"
	sleep 30
fi	

if [ "$1" == "TH-020L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3841476" "[TH-020L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH020LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-020L_History"
	sleep 30
fi	

if [ "$1" == "TH-021L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3839596" "[TH-021L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH021LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-021L_History"
	sleep 30
fi

if [ "$1" == "TH-022L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3840166" "[TH-022L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH022LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-022L_History"
	sleep 30
fi		

if [ "$1" == "TH-023L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3840169" "[TH-023L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH023LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-023L_History"
	sleep 30
fi	

if [ "$1" == "TH-024L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3845630" "[TH-024L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH024LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-024L_History"
	sleep 30
fi

if [ "$1" == "TH-025L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3840406" "[TH-025L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH025LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-025L_History"
	sleep 30
fi

if [ "$1" == "TH-026L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3840480" "[TH-026L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH026LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-026L_History"
	sleep 30
fi

if [ "$1" == "TH-027L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3840205" "[TH-027L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH027LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-027L_History"
	sleep 30
fi	

if [ "$1" == "TH-028L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3844886" "[TH-028L]" "Asia/Bangkok" "Asia/Bangkok" "2019-12-01"> /home/admin/Logs/LogsTH028LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-028L_History"
	sleep 30
fi



if [ "$1" == "TH-029L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3909268" "[TH-029L]" "Asia/Bangkok" "Asia/Bangkok" "2021-04-01"> /home/admin/Logs/LogsTH029LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-029L_History"
	sleep 30
fi	


if [ "$1" == "TH-030L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3900784" "[TH-030L]" "Asia/Bangkok" "Asia/Bangkok" "2021-02-01"> /home/admin/Logs/LogsTH030LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-030L_History"
	sleep 30
fi



if [ "$1" == "TH-031L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898374" "[TH-031L]" "Asia/Bangkok" "Asia/Bangkok" "2021-01-01"> /home/admin/Logs/LogsTH031LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-031L_History"
	sleep 30
fi


if [ "$1" == "TH-032L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898416" "[TH-032L]" "Asia/Bangkok" "Asia/Bangkok" "2021-01-01"> /home/admin/Logs/LogsTH032LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-032L_History"
	sleep 30
fi




if [ "$1" == "TH-033L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898093" "[TH-033L]" "Asia/Bangkok" "Asia/Bangkok" "2020-12-25"> /home/admin/Logs/LogsTH033LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-033L_History"
	sleep 30
fi


if [ "$1" == "TH-034L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898962" "[TH-034L]" "Asia/Bangkok" "Asia/Bangkok" "2021-01-01"> /home/admin/Logs/LogsTH034LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-034L_History"
	sleep 30
fi



if [ "$1" == "TH-035L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898070" "[TH-035L]" "Asia/Bangkok" "Asia/Bangkok" "2020-12-25"> /home/admin/Logs/LogsTH035LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-035L_History"
	sleep 30
fi

if [ "$1" == "TH-036L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898072" "[TH-036L]" "Asia/Bangkok" "Asia/Bangkok" "2020-12-25"> /home/admin/Logs/LogsTH036LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-036L_History"
	sleep 30
fi


if [ "$1" == "TH-037L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898071" "[TH-037L]" "Asia/Bangkok" "Asia/Bangkok" "2020-12-25"> /home/admin/Logs/LogsTH037LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-037L_History"
	sleep 30
fi


if [ "$1" == "TH-038L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898964" "[TH-038L]" "Asia/Bangkok" "Asia/Bangkok" "2021-01-01"> /home/admin/Logs/LogsTH038LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-038L_History"
	sleep 30
fi


if [ "$1" == "TH-039L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898447" "[TH-039L]" "Asia/Bangkok" "Asia/Bangkok" "2021-01-01"> /home/admin/Logs/LogsTH039LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-039L_History"
	sleep 30
fi


if [ "$1" == "TH-040L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898433" "[TH-040L]" "Asia/Bangkok" "Asia/Bangkok" "2021-01-01"> /home/admin/Logs/LogsTH040LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-040L_History"
	sleep 30
fi


if [ "$1" == "TH-041L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898950" "[TH-041L]" "Asia/Bangkok" "Asia/Bangkok" "2021-01-01"> /home/admin/Logs/LogsTH041LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-041L_History"
	sleep 30
fi

if [ "$1" == "TH-042L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898415" "[TH-042L]" "Asia/Bangkok" "Asia/Bangkok" "2021-01-01"> /home/admin/Logs/LogsTH042LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-042L_History"
	sleep 30
fi



if [ "$1" == "TH-043L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898434" "[TH-043L]" "Asia/Bangkok" "Asia/Bangkok" "2021-01-01"> /home/admin/Logs/LogsTH043LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-043L_History"
	sleep 30
fi


if [ "$1" == "TH-044L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3898449" "[TH-044L]" "Asia/Bangkok" "Asia/Bangkok" "2021-01-01"> /home/admin/Logs/LogsTH044LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-044L_History"
	sleep 30
fi


if [ "$1" == "TH-045L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV5.py "3902185" "[TH-045L]" "Asia/Bangkok" "Asia/Bangkok" "2021-02-01"> /home/admin/Logs/LogsTH045LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-045L_History"
	sleep 30
fi


if [ "$1" == "TH-046L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3857794" "[TH-046L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-25"> /home/admin/Logs/LogsTH046LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-046L_History"
	sleep 30
fi

if [ "$1" == "TH-047L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3850754" "[TH-047L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-25"> /home/admin/Logs/LogsTH047LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-047L_History"
	sleep 30
fi

if [ "$1" == "TH-048L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3850298" "[TH-048L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-25"> /home/admin/Logs/LogsTH048LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-048L_History"
	sleep 30
fi

if [ "$1" == "TH-049L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3849157" "[TH-049L]" "Asia/Bangkok" "Asia/Bangkok" "2020-02-25"> /home/admin/Logs/LogsTH049LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-049L_History"
	sleep 30
fi	



if [ "$1" == "TH-050L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3899710" "[TH-050L]" "Asia/Bangkok" "Asia/Bangkok" "2021-01-01"> /home/admin/Logs/LogsTH050LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-050L_History"
	sleep 30
fi	

if [ "$1" == "TH-051L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3900473" "[TH-051L]" "Asia/Bangkok" "Asia/Bangkok" "2021-02-01"> /home/admin/Logs/LogsTH051LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-051L_History"
	sleep 30
fi	

if [ "$1" == "TH-052L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3899747" "[TH-052L]" "Asia/Bangkok" "Asia/Bangkok" "2021-02-01"> /home/admin/Logs/LogsTH052LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-052L_History"
	sleep 30
fi	


if [ "$1" == "TH-056L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3925648" "[TH-056L]" "Asia/Bangkok" "Asia/Bangkok" "2021-05-01"> /home/admin/Logs/LogsTH056LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-056L_History"
	sleep 30
fi	


if [ "$1" == "TH-057L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3925716" "[TH-057L]" "Asia/Bangkok" "Asia/Bangkok" "2021-05-01"> /home/admin/Logs/LogsTH057LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-057L_History"
	sleep 30
fi	

if [ "$1" == "TH-060L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3921281" "[TH-060L]" "Asia/Bangkok" "Asia/Bangkok" "2021-04-01"> /home/admin/Logs/LogsTH060LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-060L_History"
	sleep 30
fi	




if [ "$1" == "TH-061L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3915943" "[TH-061L]" "Asia/Bangkok" "Asia/Bangkok" "2021-04-01"> /home/admin/Logs/LogsTH061LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-061L_History"
	sleep 30
fi	


if [ "$1" == "TH-062L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3921279" "[TH-062L]" "Asia/Bangkok" "Asia/Bangkok" "2021-04-01"> /home/admin/Logs/LogsTH062LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-062L_History"
	sleep 30
fi	


if [ "$1" == "TH-063L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3921400" "[TH-063L]" "Asia/Bangkok" "Asia/Bangkok" "2021-04-01"> /home/admin/Logs/LogsTH063LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-063L_History"
	sleep 30
fi	



if [ "$1" == "TH-064L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3921284" "[TH-064L]" "Asia/Bangkok" "Asia/Bangkok" "2021-04-01"> /home/admin/Logs/LogsTH064LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-064L_History"
	sleep 30
fi	



if [ "$1" == "TH-065L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3915944" "[TH-065L]" "Asia/Bangkok" "Asia/Bangkok" "2021-04-01"> /home/admin/Logs/LogsTH065LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-065L_History"
	sleep 30
fi	



if [ "$1" == "TH-066L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3921401" "[TH-066L]" "Asia/Bangkok" "Asia/Bangkok" "2021-04-01"> /home/admin/Logs/LogsTH066LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-066L_History"
	sleep 30
fi	



if [ "$1" == "TH-068L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1LiveV3.py "3922109" "[TH-068L]" "Asia/Bangkok" "Asia/Bangkok" "2021-04-01"> /home/admin/Logs/LogsTH068LHistory.txt &
	y=`echo $!`
	x="$x\n$y --TH-068L_History"
	sleep 30
fi	



if [ "$1" == "VN-002L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3818352" "[VN-002L]" "Asia/Saigon" "Asia/Saigon" > /home/admin/Logs/LogsVN002LHistory.txt &
	y=`echo $!`
	x="$x\n$y --VN-002L_History"
	sleep 30
fi

if [ "$1" == "VN-003L_History" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/LocusMaster/Gen1Live.py "3822066" "[VN-003L]" "Asia/Saigon" "Asia/Saigon" > /home/admin/Logs/LogsVN003LHistory.txt &
	y=`echo $!`
	x="$x\n$y --VN-003L_History"
	sleep 30
fi

if [ "$1" == "SG-003S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/716Live/Live.py > /home/admin/Logs/Logs716History.txt &
	y=`echo $!`
	x="$x\n$y --SG-003S_Live"
	sleep 10
fi

if [ "$1" == "SG-003S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/716Live/Live_Azure_4g.py > /home/admin/Logs/Logs716AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-003S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "SG-004S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/717Live/Live.py > /home/admin/Logs/Logs717History.txt &
	y=`echo $!`
	x="$x\n$y --SG-004S_Live"
	sleep 10
fi

if [ "$1" == "SG-004S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/717Live/Live_Azure_4g.py > /home/admin/Logs/Logs717AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-004S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "SG-005S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/725Live/Live.py > /home/admin/Logs/Logs725History.txt &
	y=`echo $!`
	x="$x\n$y --SG-005S_Live"
	sleep 10
fi

if [ "$1" == "SG-005S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/725Live/Live_Azure_4g.py > /home/admin/Logs/Logs725AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-005S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "KH-001S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/714Live/Live.py > /home/admin/Logs/Logs714History.txt &
	y=`echo $!`
	x="$x\n$y --KH-001S_Live"
	sleep 10
fi

if [ "$1" == "KH-008S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/728Live/Live.py > /home/admin/Logs/Logs728History.txt &
	y=`echo $!`
	x="$x\n$y --KH-008S_Live"
	sleep 10
fi

if [ "$1" == "KH-008S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/728Live/Live_Azure_4g.py > /home/admin/Logs/Logs728AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-008S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "KH-003S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/715Live/Live.py > /home/admin/Logs/Logs715History.txt &
	y=`echo $!`
	x="$x\n$y --KH-003S_Live"
	sleep 10
fi

if [ "$1" == "KH-003S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/715Live/Live_Azure_4g.py > /home/admin/Logs/Logs715AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --KH-003S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "MY-004S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/720Live/Live.py > /home/admin/Logs/Logs720History.txt &
	y=`echo $!`
	x="$x\n$y --MY-004S_Live"
	sleep 10
fi
if [ "$1" == "MY-006S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/729Live/Live.py > /home/admin/Logs/Logs729History.txt &
	y=`echo $!`
	x="$x\n$y --MY-006S_Live"
	sleep 10
fi

if [ "$1" == "MY-006S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/729Live/Live_Azure_4g.py > /home/admin/Logs/Logs729AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --MY-006S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "IN-015S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/712Live/Live.py > /home/admin/Logs/Logs712History.txt &
	y=`echo $!`
	x="$x\n$y --IN-015S_Live"
	sleep 10
fi

if [ "$1" == "IN-015S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/712Live/Live_Azure_4g.py > /home/admin/Logs/Logs712AzureHistory.txt &	
	y=`echo $!`
	x="$x\n$y --IN-015S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "IN-036S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/718Live/Live.py > /home/admin/Logs/Logs718History.txt &
	y=`echo $!`
	x="$x\n$y --IN-036S_Live"
	sleep 10
fi

if [ "$1" == "IN-036S_Live_Azure_4g" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/718Live/Live_Azure_4g.py > /home/admin/Logs/Logs718AzureHistory.txt &
	y=`echo $!`
	x="$x\n$y --IN-036S_Live_Azure_4g"
	sleep 10
fi

if [ "$1" == "VN-001S_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/722Live/Live.py > /home/admin/Logs/Logs722History.txt &
	y=`echo $!`
	x="$x\n$y --VN-001S_Live"
	sleep 10
fi


if [ "$1" == "SerisLifetimeMaster" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/SerisLifetimeMaster.py > /home/admin/Logs/LogsSerisLifetimeMaster.txt &
	y=`echo $!`
	x="$x\n$y --SerisLifetimeMaster"
	sleep 20
fi

if [ "$1" == "LocusLifetimeMaster" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/LocusLifetimeMaster.py > /home/admin/Logs/LogsLocusLifetimeMaster.txt &
	y=`echo $!`
	x="$x\n$y --LocusLifetimeMaster"
	sleep 20
fi


if [ "$1" == "Lifetime_Gen2" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/Lifetime_Gen2.py > /home/admin/Logs/LogsLifetimeGen_2.txt &
	y=`echo $!`
	x="$x\n$y --Lifetime_Gen2"
	sleep 10
fi

if [ "$1" == "System_Uptime" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Azure/System_Uptime.py > /home/admin/Logs/LogsSystem_Uptime.txt &
	y=`echo $!`
	x="$x\n$y --System_Uptime"
	sleep 10
fi

if [ "$1" == "Solar_Percentage" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Azure/Solar_Percentage.py > /home/admin/Logs/LogsSolar_Percentage.txt &
	y=`echo $!`
	x="$x\n$y --Solar_Percentage"
	sleep 10
fi

if [ "$1" == "Azure_GIS_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Azure/Azure_GIS_Live.py > /home/admin/Logs/LogsAzure_GIS_Live.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --Azure_GIS_Live"
	sleep 10
fi

if [ "$1" == "Azure_Met_Stations" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Azure/Azure_Met_Stations.py "L" > /home/admin/Logs/LogsAzure_Met_Stations.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --Azure_Met_Stations"
	sleep 10
fi

if [ "$1" == "Web_Interface" ] || [ $RUNALL == 1 ] 
then
	nohup python3  /home/admin/CODE/WebInterface/Interface_V2.py > /home/admin/Logs/LogsWebInterface.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --Web_Interface"
	sleep 10
fi

#Added as a Cronjob
#if [ "$1" == "Invoice_View" ] || [ $RUNALL == 1 ] 
#then
#	stdbuf -oL python /home/admin/CODE/Invoicing/View.py > /home/admin/Logs/LogsInvoiceHistory.txt &
#	y=`echo $!`
#	x="$x\n$y --Invoice_View"
#	sleep 10
#fi

if [ "$1" == "KH008_Curtailment_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Alarms/KH008_Curtailment_Alarm.py > /home/admin/Logs/LogsKH008CurtailmentAlarm.txt &
	y=`echo $!`
	x="$x\n$y --KH008_Curtailment_Alarm"
	sleep 10
fi

if [ "$1" == "Tesco_Server_Push" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Tesco/tesco_server_push.py > /home/admin/Logs/LogsTescoServerPushHistory.txt &
	y=`echo $!`
	x="$x\n$y --Tesco_Server_Push"
	sleep 10
fi

if [ "$1" == "IN-721S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/721Digest/MailDigest721.R &
	y=`echo $!`
	x="$x\n$y --IN-721S_Mail"
	sleep 10
fi

if [ "$1" == "SG-724S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG724Digest/MailDigest724.R &
	y=`echo $!`
	x="$x\n$y --SG-724S_Mail"
	sleep 20 
fi

if [ "$1" == "IN-003W_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN003WDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-003W_Mail"
	sleep 10 
fi

if [ "$1" == "IN-004W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN004WDigest/Digest.py "Mail" "2020-12-01"> /home/admin/Logs/LogsIN004WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-004W_Mail"
    sleep 10
fi


if [ "$1" == "IN-004W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN004WDigest/Client_Digest.py "Mail" "2020-12-01"> /home/admin/Logs/LogsIN004WClientMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-004W_Mail"
    sleep 10
fi


if [ "$1" == "IN-005W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN005WDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsIN005WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-005W_Mail"
    sleep 10
fi

if [ "$1" == "IN-006W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN006WDigest/Digest.py "Mail" "2021-01-28"> /home/admin/Logs/LogsIN006WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-006W_Mail"
    sleep 10
fi

if [ "$1" == "IN-007W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN007WDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsIN007WMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-007W_Mail"
    sleep 10
fi

if [ "$1" == "IN-009W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN009WDigest/Digest.py "Mail" "2020-11-08"> /home/admin/Logs/LogsIN009WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-009W_Mail"
    sleep 10
fi

if [ "$1" == "IN-013W_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN013WDigest/Digest.py "Mail" "2020-09-30"> /home/admin/Logs/LogsIN013WMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-013W_Mail"
    sleep 10
fi

# if [ "$1" == "IN-014L_Mail" ] || [ $RUNALL == 1 ] 
# then
# 	stdbuf -oL python /home/admin/CODE/LocusMaster/Digest_Generic.py "Mail" "2020-06-10"> /home/admin/Logs/LogsIN014LMail.txt &
# 	y=`echo $!`
# 	x="$x\n$y --IN-014L_Mail"
# 	sleep 10 
# fi

if [ "$1" == "IN-014L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN014LDigest/Digest.py "Mail" "2020-10-16"> /home/admin/Logs/LogsIN014LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-014L_Mail"
	sleep 10 
fi


if [ "$1" == "IN-015L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN015LDigest/Digest.py "Mail" "2021-02-10"> /home/admin/Logs/LogsIN015LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-015L_Mail"
	sleep 10 
fi



if [ "$1" == "IN-016L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN016LDigest/Digest.py "Mail" "2021-01-07"> /home/admin/Logs/LogsIN016LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-016L_Mail"
	sleep 10 
fi

if [ "$1" == "IN-017A_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN017ADigest/Digest.py "Mail" "2020-09-10"> /home/admin/Logs/LogsIN017AMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-017A_Mail"
    sleep 10
fi

if [ "$1" == "IN-018L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN018LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-018L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-019L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN019LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-019L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-021L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN021LDigest/Digest.py "Mail" "2020-11-16"> /home/admin/Logs/LogsIN021LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-021L_Mail"
	sleep 10 
fi

if [ "$1" == "IN-022L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN022LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-022L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-023L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN023LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-023L_Mail"
	sleep 10
fi

if [ "$1" == "IN-025L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN025LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-025L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-026L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN026LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-026L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-027L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN027LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-027L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-028L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN028LDigest/Digest.py "Mail" "2020-10-01"> /home/admin/Logs/LogsIN028LMail.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --IN-028L_Mail"
	sleep 10 
fi

if [ "$1" == "IN-029W_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN029WDigest/Digest.py "Mail" "2020-10-01"> /home/admin/Logs/LogsIN029WMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-029W_Mail"
	sleep 10 
fi

if [ "$1" == "IN-030L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN030LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-030L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-031L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN031LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-031L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-032W_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN032WDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsIN032WMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-032W_Mail"
	sleep 10 
fi


if [ "$1" == "IN-033L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN033LDigest/Digest.py "Mail" "2020-09-10"> /home/admin/Logs/LogsIN033LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-033L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-034L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN034LDigest/Digest.py "Mail" "2021-01-01"> /home/admin/Logs/LogsIN034LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-034L_Mail"
    sleep 10
fi

if [ "$1" == "IN-035L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN035LDigest/Digest.py "Mail" "2021-01-07"> /home/admin/Logs/LogsIN035LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-035L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-036A_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN036ADigest/Digest.py "Mail" "2020-09-10"> /home/admin/Logs/LogsIN036AMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-036A_Mail"
    sleep 10
fi

if [ "$1" == "IN-037L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN037LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-037L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-038L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN038LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-038L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-039L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN039LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-039L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-040R_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN040RDigest/Digest.py "Mail" "2020-10-04"> /home/admin/Logs/LogsIN040RMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-040R_Mail"
	sleep 20 
fi

if [ "$1" == "IN-041L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN041LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-041L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-042L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN042LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-042L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-043L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN043LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-043L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-044L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN044LDigest/Digest.py "Mail" "2021-01-20"> /home/admin/Logs/LogsIN044LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-044L_Mail"
    sleep 10
fi

if [ "$1" == "IN-045L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN045LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-045L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-046L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN046LDigest/Digest.py "Mail" "2021-01-01"> /home/admin/Logs/LogsIN046LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-046L_Mail"
    sleep 10
fi


if [ "$1" == "IN-047L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN047LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-047L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-048L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN048LDigest/Digest.py "Mail" "2020-09-10"> /home/admin/Logs/LogsIN048LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-048L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-049L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN049LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-049L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-050L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN050LDigest/Digest.py "Mail" "2020-09-10"> /home/admin/Logs/LogsIN050LMail.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --IN-050L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-051L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN051LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-051L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-052L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN052LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-052L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-053L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN053LDigest/Digest.py "Mail" "2019-02-01"> /home/admin/Logs/LogsIN053LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-053L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-054L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN054LDigest/Digest.py "Mail" "2020-10-20"> /home/admin/Logs/LogsIN054LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-054L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-055L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN055LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-055L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-056L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN056LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-056L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-057L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN057LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-057L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-059L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN059LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-059L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-060L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN060LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-060L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-061L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN061LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-061L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-062L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN062LDigest/Digest.py "Mail" "2021-01-27"> /home/admin/Logs/LogsIN062LMail.txt &
    y=`echo $!`
    x="$x\n$y --IN-062L_Mail"
    sleep 10
fi

if [ "$1" == "IN-063L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN063LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-063L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-064L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN064LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-064L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-065L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN065LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-065L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-066L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN066LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-066L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-067L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN067LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-067L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-068L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN068LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-068L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-068L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN068LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-068L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-069L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN069LDigest/Client_Digest.py "Mail" "2020-12-02"> /home/admin/Logs/LogsIN9069LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-069L_Mail"
    sleep 10
fi

if [ "$1" == "IN-069L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN069LDigest/Digest.py "Mail" "2020-04-22"> /home/admin/Logs/LogsIN069LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-069L_Mail"
    sleep 10
fi

if [ "$1" == "IN-071L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN071LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-071L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-072L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN072LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-072L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-073L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN073LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-073L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-074L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN074LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-074L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-075L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN075LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-075L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-080L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN080LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-080L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-076L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN076LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-076L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-077L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN077LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-077L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-078L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN078LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-078L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-079L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN079LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-079L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-081L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN081LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-081L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-082L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN082LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-082L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-083L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN083LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-083L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-084L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN084LDigest/Digest.py "Mail" "2020-12-14"> /home/admin/Logs/LogsIN084LMail.txt 2>&1 &
   	y=`echo $!`
    x="$x\n$y --IN-084L_Mail"
    sleep 10
fi

if [ "$1" == "IN-085L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN085LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-085L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-086L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN086LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-086L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-086L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN086LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-086L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-087L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN087LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-087L_Mail"
	sleep 40 
fi

if [ "$1" == "IN-088L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN088LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-088_Mail"
	sleep 20 
fi

if [ "$1" == "IN-088L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN088LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-088L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-087L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN089LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-087L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-087L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN087LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-087L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-089L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN089LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-089L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-090L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN090LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-090L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-091L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN091LDigest/Digest.py "Mail" "2020-10-14"> /home/admin/Logs/LogsIN091LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-091L_Mail"
	sleep 20 
fi

if [ "$1" == "IN-092L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN092LDigest/Digest.py "Mail" "2020-10-10"> /home/admin/Logs/LogsIN092LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-092L_Mail"
    sleep 10
fi

if [ "$1" == "IN-092L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN092LDigest/Client_Digest.py "Mail" "2020-12-02"> /home/admin/Logs/LogsIN9092LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-092L_Mail"
    sleep 10
fi

if [ "$1" == "IN-093L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN093LDigest/Digest.py "Mail" "2020-10-31"> /home/admin/Logs/LogsIN093LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-093L_Mail"
    sleep 10
fi

if [ "$1" == "IN-094L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN094LDigest/Digest.py "Mail" "2020-11-04"> /home/admin/Logs/LogsIN094LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-094L_Mail"
    sleep 10
fi

if [ "$1" == "IN-095L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN095LDigest/Digest.py "Mail" "2021-01-01"> /home/admin/Logs/LogsIN095LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-095L_Mail"
    sleep 10
fi


if [ "$1" == "IN-096L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN096LDigest/Digest.py "Mail" "2020-11-25"> /home/admin/Logs/LogsIN096LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-096L_Mail"
    sleep 10
fi


if [ "$1" == "IN-097L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN097LDigest/Digest.py "Mail" "2021-01-18"> /home/admin/Logs/LogsIN097LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-097L_Mail"
    sleep 10
fi


if [ "$1" == "IN-098L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN098LDigest/Digest.py "Mail" "2021-02-01"> /home/admin/Logs/LogsIN098LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-098L_Mail"
    sleep 10
fi


if [ "$1" == "IN-098L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN098LDigest/Client_Digest.py "Mail" "2020-12-02"> /home/admin/Logs/LogsIN9098LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-098L_Mail"
    sleep 10
fi

if [ "$1" == "IN-099L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN099LDigest/Digest.py "Mail" "2021-03-16"> /home/admin/Logs/LogsIN099LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-099L_Mail"
    sleep 10
fi


if [ "$1" == "IN-099L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN099LDigest/Client_Digest.py "Mail" "2020-03-16"> /home/admin/Logs/LogsIN9099LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-099L_Mail"
    sleep 10
fi

if [ "$1" == "IN-100L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN100LDigest/Digest.py "Mail" "2021-03-20"> /home/admin/Logs/LogsIN100LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-100L_Mail"
    sleep 10
fi


if [ "$1" == "IN-101L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/IN101LDigest/Digest.py "Mail" "2021-04-07"> /home/admin/Logs/LogsIN101LMail.txt &
   	y=`echo $!`
    x="$x\n$y --IN-101L_Mail"
    sleep 10
fi


if [ "$1" == "MY-008L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY008LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-008L_Mail"
fi

if [ "$1" == "MY-009L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY009LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-009L_Mail"
fi


if [ "$1" == "IN-301L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN301LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-301L_Mail"
	sleep 20 
fi


if [ "$1" == "IN-303L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN303LDigest/Digest.py "Mail" "2020-12-02"> /home/admin/Logs/LogsIN303LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-303L_Mail"
	sleep 10
fi

if [ "$1" == "IN-304L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN304LDigest/Digest.py "Mail" "2020-09-01"> /home/admin/Logs/LogsIN304LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-304L_Mail"
	sleep 10
fi

if [ "$1" == "SG-725S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG725Digest/MailDigest725.R &
	y=`echo $!`
	x="$x\n$y --SG-725S_Mail"
	sleep 20 
fi

if [ "$1" == "VN-002L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/VN002LDigest/Digest.py "Mail" "2021-01-01"> /home/admin/Logs/LogsVN002LMail.txt &
	y=`echo $!`
	x="$x\n$y --VN-002L_Mail"
	sleep 10
fi

if [ "$1" == "VN-003L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/VN003LDigest/Digest.py "Mail" "2021-01-01"> /home/admin/Logs/LogsVN003LMail.txt &
	y=`echo $!`
	x="$x\n$y --VN-003L_Mail"
	sleep 10
fi

if [ "$1" == "SG-726S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG726Digest/MailDigest726.R &
	y=`echo $!`
	x="$x\n$y --SG-726S_Mail"
	sleep 20 
fi


if [ "$1" == "SG-727S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG727Digest/MailDigest727.R &
	y=`echo $!`
	x="$x\n$y --SG-727S_Mail"
	sleep 20 
fi

if [ "$1" == "SG-005S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG005SDigest/MailDigest724.R &
	y=`echo $!`
	x="$x\n$y --SG-005S_Mail"
	sleep 10
fi

if [ "$1" == "VN-001S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN001SDigest/MailDigest722.R &
	y=`echo $!`
	x="$x\n$y --VN-001S_Mail"
	sleep 10
fi

if [ "$1" == "VN-722S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN722Digest/MailDigest722.R &
	y=`echo $!`
	x="$x\n$y --VN-722S_Mail"
	sleep 10
fi

if [ "$1" == "SG-999S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/Customer/MailDigest724.R &
	y=`echo $!`
	x="$x\n$y --SG-999S_Mail"
	sleep 10
fi

if [ "$1" == "MY-004S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY004SDigest/MailDigest720.R &
	y=`echo $!`
	x="$x\n$y --MY-004S_Mail"
	sleep 10
fi

if [ "$1" == "MY-720S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY720Digest/MailDigest720.R &
	y=`echo $!`
	x="$x\n$y --MY-720S_Mail"
	sleep 10
fi

if [ "$1" == "MY-006S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY006SDigest/MailDigest729.R &
	y=`echo $!`
	x="$x\n$y --MY-006S_Mail"
	sleep 10
fi

if [ "$1" == "KH-008S_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH008SDigest/MailDigestKH008.R &
	y=`echo $!`
	x="$x\n$y --KH-008S_Mail"
	sleep 10
fi

if [ "$1" == "IN-008X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN008Digest/LalruCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --IN-008X_History"
	sleep 10
fi

if [ "$1" == "IN-010X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN010Digest/DelhiCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --IN-010X_History"
	sleep 10
fi

if [ "$1" == "SG-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG001Digest/SG001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-001X_History"
	sleep 10
fi

if [ "$1" == "SG-002X_History" ] || [ $RUNALL == 1 ]
then
	Rscript /home/admin/CODE/SGDigest/SG002Digest/SG002CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-002X_History"
	sleep 10
fi

if [ "$1" == "SG-004X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG004XDigest/SG004CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-004X_History"
	sleep 10
fi

if [ "$1" == "SG-006X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG006XDigest/SG006CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-006X_History"
	sleep 10
fi


if [ "$1" == "SG-003EF_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Final/SG003EFCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-003EF_History"
	sleep 10
fi

if [ "$1" == "SG-003EP_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Prelim/SG003EPCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-003EP_History"
	sleep 10
fi

if [ "$1" == "SG-007EP_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Prelim/SG007EPCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-007EP_History"
	sleep 10
fi

if [ "$1" == "SG-007EF_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Final/SG007EFCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-007EF_History"
	sleep 10
fi

if [ "$1" == "SG-008EF_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Final/SG008EFCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-008EF_History"
	sleep 10
fi

if [ "$1" == "SG-008EP_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Prelim/SG008EPCleanHistory.R &
	y=`echo $!`
	x="$x\n$y --SG-008EP_History"
	sleep 10
fi

if [ "$1" == "KH-002X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH002Digest/KH002CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-002X_History"
	sleep 10
fi

if [ "$1" == "KH-003X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH003Digest/KH003CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-003X_History"
	sleep 10
fi

if [ "$1" == "KH-004X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH004Digest/KH004CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-004X_History"
	sleep 10
fi

if [ "$1" == "KH-005X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH005XDigest/KH005CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-005X_History"
	sleep 10
fi

if [ "$1" == "KH-006X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH006XDigest/KH006CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-006X_History"
	sleep 10
fi

if [ "$1" == "KH-007X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH007XDigest/KH007CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-007X_History"
	sleep 10
fi

if [ "$1" == "KH-008X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH008Digest/KH008CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --KH-008X_History"
	sleep 10
fi



if [ "$1" == "TH-003X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH003Digest/TH003CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --TH-003X_History"
	sleep 10
fi

if [ "$1" == "MY-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY001Digest/MY001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --MY-001X_History"
	sleep 10
fi

if [ "$1" == "MY-003X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY003Digest/MY003CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --MY-003X_History"
	sleep 10
fi

if [ "$1" == "VN-001X_History" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN001XDigest/VN001CleanHistory.R &
	y=`echo $!`
	x="$x\n$y --VN-001X_History"
	sleep 10
fi

if [ "$1" == "IN-012L_Mail" ] || [ $RUNALL == 1 ]
then
	Rscript /home/admin/CODE/IN012LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-012L_Mail"
	sleep 10
fi


if [ "$1" == "IN-070L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN070LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-070L_Mail"
	sleep 10
fi

if [ "$1" == "MY-002L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY002LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-002L_Mail"
	sleep 10
fi

if [ "$1" == "TH-002L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH002LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-002L_Mail"
	sleep 10
fi

if [ "$1" == "MY-002L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY002LDigest/Client_Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-002L_Mail"
	sleep 10
fi

if [ "$1" == "MY-011L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY011LDigest/Digest.py "Mail" "2020-09-25"> /home/admin/Logs/LogsMY011LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-011L_Mail"
    sleep 10
fi

if [ "$1" == "MY-012L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY012LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-012L_Mail"
	sleep 10
fi


if [ "$1" == "MY-013L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY013LDigest/Digest.py "Mail" "2020-10-10"> /home/admin/Logs/LogsMY013LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-013L_Mail"
    sleep 10
fi

if [ "$1" == "MY-014L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY014LDigest/Digest.py "Mail" "2020-12-16"> /home/admin/Logs/LogsMY014LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-014L_Mail"
    sleep 10
fi

if [ "$1" == "MY-015L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY015LDigest/Digest.py "Mail" "2021-02-01"> /home/admin/Logs/LogsMY015LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-015L_Mail"
    sleep 10
fi

if [ "$1" == "MY-016L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY016LDigest/Digest.py "Mail" "2021-03-24"> /home/admin/Logs/LogsMY016LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-016L_Mail"
    sleep 10
fi


if [ "$1" == "IN-301L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN301LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --IN-301L_Mail"
	sleep 10
fi


if [ "$1" == "IN-302L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN302LDigest/Digest.py "Mail" "2020-09-01"> /home/admin/Logs/LogsIN302LMail.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --IN-302L_Mail"
	sleep 10
fi

if [ "$1" == "IN-305L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN305LDigest/Digest.py "Mail" "2020-11-28"> /home/admin/Logs/LogsIN305LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-305L_Mail"
	sleep 10
fi

if [ "$1" == "IN-306L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN306LDigest/Digest.py "Mail" "2021-04-03"> /home/admin/Logs/LogsIN306LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-306L_Mail"
	sleep 10
fi

if [ "$1" == "IN-307L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN307LDigest/Digest.py "Mail" "2021-04-04"> /home/admin/Logs/LogsIN307LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-307L_Mail"
	sleep 10
fi

if [ "$1" == "IN-308L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN308LDigest/Digest.py "Mail" "2021-04-04"> /home/admin/Logs/LogsIN308LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-308L_Mail"
	sleep 10
fi


if [ "$1" == "IN-309L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/IN309LDigest/Digest.py "Mail" "2021-05-18"> /home/admin/Logs/LogsIN309LMail.txt &
	y=`echo $!`
	x="$x\n$y --IN-309L_Mail"
	sleep 10
fi

if [ "$1" == "IN-008X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN008Digest/MailDigestLalru.R &
	y=`echo $!`
	x="$x\n$y --IN-008X_Mail"
	sleep 10
fi

if [ "$1" == "IN-010X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN010Digest/MailDigestDelhi.R &
	y=`echo $!`
	x="$x\n$y --IN-010X_Mail"
	sleep 10
fi

if [ "$1" == "KH-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH001Digest/MailDigestKH001.R &
	y=`echo $!`
	x="$x\n$y --KH-001X_Mail"
	sleep 10
fi

if [ "$1" == "KH-002X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH002Digest/MailDigestKH002.R &
	y=`echo $!`
	x="$x\n$y --KH-002X_Mail"
	sleep 10
fi

if [ "$1" == "SG-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG001Digest/MailDigestSG001.R &
	y=`echo $!`
	x="$x\n$y --SG-001X_Mail"
	sleep 10
fi

if [ "$1" == "SG-002X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG002Digest/MailDigestSG002.R &
	y=`echo $!`
	x="$x\n$y --SG-002X_Mail"
	sleep 10
fi

if [ "$1" == "SG-003EP_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Prelim/MailDigestSG003EP.R &
	y=`echo $!`
	x="$x\n$y --SG-003EP_Mail"
	sleep 10
fi

if [ "$1" == "SG-003EF_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG003EDigest/Final/MailDigestSG003EF.R &
	y=`echo $!`
	x="$x\n$y --SG-003EF_Mail"
	sleep 10
fi

if [ "$1" == "SG-007EP_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Prelim/MailDigestSG007EP.R &
	y=`echo $!`
	x="$x\n$y --SG-007EP_Mail"
	sleep 10
fi

if [ "$1" == "SG-007EF_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG007EDigest/Final/MailDigestSG007EF.R &
	y=`echo $!`
	x="$x\n$y --SG-007EF_Mail"
	sleep 10
fi

if [ "$1" == "TH-001L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH001LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-001L_Mail"
	sleep 10
fi

if [ "$1" == "SG-008EP_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Prelim/MailDigestSG008EP.R &
	y=`echo $!`
	x="$x\n$y --SG-008EP_Mail"
	sleep 10
fi

if [ "$1" == "SG-008EF_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG008EDigest/Final/MailDigestSG008EF.R &
	y=`echo $!`
	x="$x\n$y --SG-008EF_Mail"
	sleep 10
fi

if [ "$1" == "SG-004X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG004XDigest/MailDigestSG004.R &
	y=`echo $!`
	x="$x\n$y --SG-004X_Mail"
	sleep 10
fi

if [ "$1" == "SG-006X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SGDigest/SG006XDigest/MailDigestSG006.R &
	y=`echo $!`
	x="$x\n$y --SG-006X_Mail"
	sleep 10
fi


if [ "$1" == "IN-018X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/IN018Digest/MailDigestIN018.R &
	y=`echo $!`
	x="$x\n$y --IN-018X_Mail"
	sleep 10
fi

if [ "$1" == "SG-006_Lifetime" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Lifetime/SG006Lifetime.py > /home/admin/Logs/LogsSG006LifetimeHistory.txt &
	y=`echo $!`
	x="$x\n$y --SG-006_Lifetime"
	sleep 10
fi

if [ "$1" == "KH-003X_Mail" ] || [ $RUNALL == 1 ]
then
	Rscript /home/admin/CODE/KH003Digest/MailDigestKH003.R &
	y=`echo $!`
	x="$x\n$y --KH-003X_Mail"
	sleep 10
fi

if [ "$1" == "KH-004X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH004Digest/MailDigestKH004.R &
	y=`echo $!`
	x="$x\n$y --KH-004X_Mail"
	sleep 10
fi

if [ "$1" == "KH-005X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH005XDigest/MailDigestKH005.R &
	y=`echo $!`
	x="$x\n$y --KH-005X_Mail"
	sleep 10
fi

if [ "$1" == "KH-006X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH006XDigest/MailDigestKH006.R &
	y=`echo $!`
	x="$x\n$y --KH-006X_Mail"
	sleep 10
fi

if [ "$1" == "KH-007X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH007XDigest/MailDigestKH007.R &
	y=`echo $!`
	x="$x\n$y --KH-007X_Mail"
	sleep 10
fi

if [ "$1" == "KH-008X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/KH008Digest/MailDigestKH008.R &
	y=`echo $!`
	x="$x\n$y --KH-008X_Mail"
	sleep 10
fi



if [ "$1" == "TH-003X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH003Digest/MailDigestTH003.R &
	y=`echo $!`
	x="$x\n$y --TH-003X_Mail"
	sleep 10
fi

if [ "$1" == "MY-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY001Digest/MailDigestMY001.R &
	y=`echo $!`
	x="$x\n$y --MY-001X_Mail"
	sleep 10
fi

if [ "$1" == "MY-003X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY003Digest/MailDigestMY003.R &
	y=`echo $!`
	x="$x\n$y --MY-003X_Mail"
	sleep 10
fi


if [ "$1" == "TH-004L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH004LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-004L_Mail"
fi


if [ "$1" == "TH-005L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH005LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-005L_Mail"
fi

if [ "$1" == "TH-006L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH006LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-006L_Mail"
fi


if [ "$1" == "TH-007L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH007LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-007L_Mail"
fi

if [ "$1" == "TH-008L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH008LDigest/Digest.py "Mail" "2020-09-10"> /home/admin/Logs/LogsTH008LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-008L_Mail"
fi

if [ "$1" == "TH-009L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH009LDigest/Digest.py "Mail" "2020-12-16"> /home/admin/Logs/LogsTH009LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-009L_Mail"
fi

if [ "$1" == "TH-009L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH009LDigest/Client_Digest.py "Mail" "2020-12-20"> /home/admin/Logs/LogsTH9009LMail.txt 2>&1 &
	y=`echo $!`
	x="$x\n$y --TH-009L_Mail"
fi

if [ "$1" == "TH-010L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH010LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-010L_Mail"
fi

if [ "$1" == "TH-011L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH011LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-011L_Mail"
fi


if [ "$1" == "TH-012L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH012LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-012L_Mail"
fi


if [ "$1" == "TH-013L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH013LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-013L_Mail"
fi

if [ "$1" == "TH-014L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH014LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-014L_Mail"
fi

if [ "$1" == "TH-015L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH015LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-015L_Mail"
fi

if [ "$1" == "TH-016L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH016LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-016L_Mail"
fi

if [ "$1" == "TH-017L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH017LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-017L_Mail"
fi

if [ "$1" == "TH-018L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH018LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-018L_Mail"
fi

if [ "$1" == "TH-019L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH019LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-019L_Mail"
fi

if [ "$1" == "TH-020L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH020LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-020L_Mail"
fi

if [ "$1" == "TH-021L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH021LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-021L_Mail"
fi

if [ "$1" == "TH-022L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH022LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-022L_Mail"
fi

if [ "$1" == "TH-023L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH023LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-023L_Mail"
fi

if [ "$1" == "TH-024L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH024LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-024L_Mail"
fi

if [ "$1" == "TH-025L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH025LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-025L_Mail"
fi

if [ "$1" == "TH-026L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH026LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-026L_Mail"
fi

if [ "$1" == "TH-027L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH027LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-027L_Mail"
fi

if [ "$1" == "TH-028L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH028LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --TH-028L_Mail"
fi



if [ "$1" == "TH-029L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH029LDigest/Digest.py "Mail" "2021-04-01"> /home/admin/Logs/LogsTH029LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-029L_Mail"
fi




if [ "$1" == "TH-030L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH030LDigest/Digest.py "Mail" "2021-02-01"> /home/admin/Logs/LogsTH030LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-030L_Mail"
fi


if [ "$1" == "TH-031L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH031LDigest/Digest.py "Mail" "2021-01-05"> /home/admin/Logs/LogsTH031LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-031L_Mail"
fi


if [ "$1" == "TH-032L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH032LDigest/Digest.py "Mail" "2021-01-05"> /home/admin/Logs/LogsTH032LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-032L_Mail"
fi


if [ "$1" == "TH-033L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH033LDigest/Digest.py "Mail" "2020-12-29"> /home/admin/Logs/LogsTH033LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-033L_Mail"
fi


if [ "$1" == "TH-034L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH034LDigest/Digest.py "Mail" "2021-01-01"> /home/admin/Logs/LogsTH034LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-034L_Mail"
fi


if [ "$1" == "TH-035L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH035LDigest/Digest.py "Mail" "2020-12-28"> /home/admin/Logs/LogsTH035LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-035L_Mail"
fi


if [ "$1" == "TH-036L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH036LDigest/Digest.py "Mail" "2020-12-29"> /home/admin/Logs/LogsTH036LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-036L_Mail"
fi


if [ "$1" == "TH-037L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH037LDigest/Digest.py "Mail" "2020-12-29"> /home/admin/Logs/LogsTH037LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-037L_Mail"
fi


if [ "$1" == "TH-038L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH038LDigest/Digest.py "Mail" "2021-01-01"> /home/admin/Logs/LogsTH038LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-038L_Mail"
fi


if [ "$1" == "TH-039L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH039LDigest/Digest.py "Mail" "2020-12-29"> /home/admin/Logs/LogsTH039LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-039L_Mail"
fi


if [ "$1" == "TH-040L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH040LDigest/Digest.py "Mail" "2020-12-29"> /home/admin/Logs/LogsTH040LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-040L_Mail"
fi

if [ "$1" == "TH-041L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH041LDigest/Digest.py "Mail" "2021-01-01"> /home/admin/Logs/LogsTH041LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-041L_Mail"
fi


if [ "$1" == "TH-042L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH042LDigest/Digest.py "Mail" "2020-12-29"> /home/admin/Logs/LogsTH042LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-042L_Mail"
fi


if [ "$1" == "TH-043L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH043LDigest/Digest.py "Mail" "2020-12-29"> /home/admin/Logs/LogsTH043LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-043L_Mail"
fi


if [ "$1" == "TH-044L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH044LDigest/Digest.py "Mail" "2020-12-29"> /home/admin/Logs/LogsTH044LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-044L_Mail"
fi


if [ "$1" == "TH-045L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH045LDigest/Digest.py "Mail" "2021-02-01"> /home/admin/Logs/LogsTH045LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-045L_Mail"
fi


if [ "$1" == "TH-0P2_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Master_Digest/TESCO_P2_Digest.py > /home/admin/Logs/LogsTH0P2_Mail.txt &
	y=`echo $!`
	x="$x\n$y --TH-0P2_Mail"
fi

if [ "$1" == "TH-0P1_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Master_Digest/TESCO_P1_Digest.py > /home/admin/Logs/LogsTH0P1_Mail.txt &
	y=`echo $!`
	x="$x\n$y --TH-0P1_Mail"
fi


if [ "$1" == "TH-046L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/TH046LDigest/Digest.py "Mail" "2021-01-20"> /home/admin/Logs/LogsTH046LMail.txt &
   	y=`echo $!`
    x="$x\n$y --TH-046L_Mail"
    sleep 10
fi


if [ "$1" == "TH-047L_Mail" ] || [ $RUNALL == 1 ] 
then
    Rscript /home/admin/CODE/TH047LDigest/Mail.R &
   	y=`echo $!`
    x="$x\n$y --TH-047L_Mail"
    sleep 10
fi


if [ "$1" == "TH-048L_Mail" ] || [ $RUNALL == 1 ] 
then
    Rscript /home/admin/CODE/TH048LDigest/Mail.R &
   	y=`echo $!`
    x="$x\n$y --TH-048L_Mail"
    sleep 10
fi


if [ "$1" == "TH-049L_Mail" ] || [ $RUNALL == 1 ] 
then
    Rscript /home/admin/CODE/TH049LDigest/Mail.R &
   	y=`echo $!`
    x="$x\n$y --TH-049L_Mail"
    sleep 10
fi


if [ "$1" == "TH-050L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH050LDigest/Digest.py "Mail" "2021-01-01"> /home/admin/Logs/LogsTH050LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-050L_Mail"
fi


if [ "$1" == "TH-051L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH051LDigest/Digest.py "Mail" "2021-02-01"> /home/admin/Logs/LogsTH051LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-051L_Mail"
fi


if [ "$1" == "TH-052L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH052LDigest/Digest.py "Mail" "2021-02-01"> /home/admin/Logs/LogsTH052LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-052L_Mail"
fi


if [ "$1" == "TH-056L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH056LDigest/Digest.py "Mail" "2021-05-01"> /home/admin/Logs/LogsTH056LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-056L_Mail"
fi


if [ "$1" == "TH-057L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH057LDigest/Digest.py "Mail" "2021-05-01"> /home/admin/Logs/LogsTH057LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-057L_Mail"
fi


if [ "$1" == "TH-060L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH060LDigest/Digest.py "Mail" "2021-04-20"> /home/admin/Logs/LogsTH060LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-060L_Mail"
fi

if [ "$1" == "TH-061L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH061LDigest/Digest.py "Mail" "2021-04-19"> /home/admin/Logs/LogsTH061LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-061L_Mail"
fi

if [ "$1" == "TH-062L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH062LDigest/Digest.py "Mail" "2021-04-20"> /home/admin/Logs/LogsTH062LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-062L_Mail"
fi


if [ "$1" == "TH-063L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH063LDigest/Digest.py "Mail" "2021-04-20"> /home/admin/Logs/LogsTH063LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-063L_Mail"
fi


if [ "$1" == "TH-064L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH064LDigest/Digest.py "Mail" "2021-04-20"> /home/admin/Logs/LogsTH064LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-064L_Mail"
fi

if [ "$1" == "TH-065L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH065LDigest/Digest.py "Mail" "2021-04-19"> /home/admin/Logs/LogsTH065LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-065L_Mail"
fi

if [ "$1" == "TH-066L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH066LDigest/Digest.py "Mail" "2021-04-20"> /home/admin/Logs/LogsTH066LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-066L_Mail"
fi

if [ "$1" == "TH-068L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH068LDigest/Digest.py "Mail" "2021-04-20"> /home/admin/Logs/LogsTH068LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-068L_Mail"
fi

if [ "$1" == "TH-401L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH401LDigest/Digest.py "Mail" "2020-12-22"> /home/admin/Logs/LogsTH401LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-401L_Mail"
fi



if [ "$1" == "TH-402L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH402LDigest/Digest.py "Mail" "2021-04-01"> /home/admin/Logs/LogsTH402LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-402L_Mail"
fi



if [ "$1" == "TH-403L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH403LDigest/Digest.py "Mail" "2021-05-05"> /home/admin/Logs/LogsTH403LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-403L_Mail"
fi



if [ "$1" == "TH-404L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH404LDigest/Digest.py "Mail" "2021-03-01"> /home/admin/Logs/LogsTH404LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-404L_Mail"
fi


if [ "$1" == "TH-901L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/TH901LDigest/Digest.py "Mail" "2021-03-01"> /home/admin/Logs/LogsTH901LMail.txt &
	y=`echo $!`
	x="$x\n$y --TH-901L_Mail"
fi



if [ "$1" == "MY-005L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY005LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-005L_Mail"
fi

if [ "$1" == "MY-007L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY007LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-007L_Mail"
fi

if [ "$1" == "MY-010L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY010LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-010L_Mail"
fi

if [ "$1" == "MY-401L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY401LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-401L_Mail"
fi


if [ "$1" == "MY-402L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY402LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-402L_Mail"
fi


if [ "$1" == "MY-403L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY403LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-403L_Mail"
fi


if [ "$1" == "MY-404L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY404LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-404L_Mail"
fi

if [ "$1" == "MY-405L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY405LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-405L_Mail"
fi

if [ "$1" == "MY-406L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY406LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-406L_Mail"
fi


if [ "$1" == "MY-408L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY408LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-408L_Mail"
fi

if [ "$1" == "MY-407L_Mail" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/MY407LDigest/Digest.py "Mail" "2020-10-01"> /home/admin/Logs/LogsMY407LMail.txt &
	y=`echo $!`
	x="$x\n$y --MY-407L_Mail"
fi

if [ "$1" == "MY-409L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY409LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-409L_Mail"
fi

if [ "$1" == "MY-410L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY410LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-410L_Mail"
fi


if [ "$1" == "MY-411L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY411LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-411L_Mail"
fi

if [ "$1" == "MY-412L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY412LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-412L_Mail"
fi

if [ "$1" == "MY-413L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY413LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-413L_Mail"
fi

if [ "$1" == "MY-414L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY414LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-414L_Mail"
fi

if [ "$1" == "MY-415L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY415LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-415L_Mail"
fi

if [ "$1" == "MY-416L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY416LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-416L_Mail"
fi

if [ "$1" == "MY-417L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY417LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-417L_Mail"
fi


if [ "$1" == "MY-418L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY418LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY418LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-418L_Mail"
    sleep 10
fi

if [ "$1" == "MY-419L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY419LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-419L_Mail"
fi


if [ "$1" == "MY-420L_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MY420LDigest/Mail.R &
	y=`echo $!`
	x="$x\n$y --MY-420L_Mail"
fi

if [ "$1" == "MY-421L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY421LDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsMY421LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-421L_Mail"
    sleep 10
fi

if [ "$1" == "MY-422L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY422LDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsMY422LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-422L_Mail"
    sleep 10
fi

if [ "$1" == "MY-423L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY423LDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsMY423LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-423L_Mail"
    sleep 10
fi

if [ "$1" == "MY-424L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY424LDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsMY424LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-424L_Mail"
    sleep 10
fi

if [ "$1" == "MY-425L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY425LDigest/Digest.py "Mail" "2020-09-20"> /home/admin/Logs/LogsMY425LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-425L_Mail"
    sleep 10
fi

if [ "$1" == "MY-426L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY426LDigest/Digest.py "Mail" "2020-09-06"> /home/admin/Logs/LogsMY426LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-426L_Mail"
    sleep 10
fi

if [ "$1" == "MY-427L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY427LDigest/Digest.py "Mail" "2020-09-16"> /home/admin/Logs/LogsMY427LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-427L_Mail"
    sleep 10
fi

if [ "$1" == "MY-428L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY428LDigest/Digest.py "Mail" "2020-09-08"> /home/admin/Logs/LogsMY428LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-428L_Mail"
    sleep 10
fi

if [ "$1" == "MY-429L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY429LDigest/Digest.py "Mail" "2020-09-22"> /home/admin/Logs/LogsMY429LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-429L_Mail"
    sleep 10
fi

if [ "$1" == "MY-430L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY430LDigest/Digest.py "Mail" "2020-09-22"> /home/admin/Logs/LogsMY430LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-430L_Mail"
    sleep 10
fi

if [ "$1" == "MY-430L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY431LDigest/Digest.py "Mail" "2020-09-25"> /home/admin/Logs/LogsMY431LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-431L_Mail"
    sleep 10
fi

if [ "$1" == "MY-432L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY432LDigest/Digest.py "Mail" "2020-09-25"> /home/admin/Logs/LogsMY432LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-432L_Mail"
    sleep 10
fi

if [ "$1" == "MY-433L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY433LDigest/Digest.py "Mail" "2020-09-25"> /home/admin/Logs/LogsMY433LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-433L_Mail"
    sleep 10
fi

if [ "$1" == "MY-434L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY434LDigest/Digest.py "Mail" "2020-10-01"> /home/admin/Logs/LogsMY434LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-434L_Mail"
    sleep 10
fi

if [ "$1" == "MY-435L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY435LDigest/Digest.py "Mail" "2020-10-01"> /home/admin/Logs/LogsMY435LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-435L_Mail"
    sleep 10
fi

if [ "$1" == "MY-436L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY436LDigest/Digest.py "Mail" "2020-10-01"> /home/admin/Logs/LogsMY436LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-436L_Mail"
    sleep 10
fi

if [ "$1" == "MY-437L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY437LDigest/Digest.py "Mail" "2020-10-01"> /home/admin/Logs/LogsMY437LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-437L_Mail"
    sleep 10
fi


if [ "$1" == "MY-438L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY438LDigest/Digest.py "Mail" "2020-10-01"> /home/admin/Logs/LogsMY438LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-438L_Mail"
    sleep 10
fi


if [ "$1" == "MY-439L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY439LDigest/Digest.py "Mail" "2020-10-10"> /home/admin/Logs/LogsMY439LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-439L_Mail"
    sleep 10
fi


if [ "$1" == "MY-440L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY440LDigest/Digest.py "Mail" "2020-10-10"> /home/admin/Logs/LogsMY440LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-440L_Mail"
    sleep 10
fi

if [ "$1" == "MY-441L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY441LDigest/Digest.py "Mail" "2020-10-10"> /home/admin/Logs/LogsMY441LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-441L_Mail"
    sleep 10
fi

if [ "$1" == "MY-442L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY442LDigest/Digest.py "Mail" "2020-10-10"> /home/admin/Logs/LogsMY442LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-442L_Mail"
    sleep 10
fi

if [ "$1" == "MY-443L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY443LDigest/Digest.py "Mail" "2020-10-15"> /home/admin/Logs/LogsMY443LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-443L_Mail"
    sleep 10
fi


if [ "$1" == "MY-444L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY444LDigest/Digest.py "Mail" "2020-10-15"> /home/admin/Logs/LogsMY444LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-444L_Mail"
    sleep 10
fi

if [ "$1" == "MY-445L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY445LDigest/Digest.py "Mail" "2020-10-18"> /home/admin/Logs/LogsMY445LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-445L_Mail"
    sleep 10
fi

if [ "$1" == "MY-446L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY446LDigest/Digest.py "Mail" "2020-10-15"> /home/admin/Logs/LogsMY446LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-446L_Mail"
    sleep 10
fi

if [ "$1" == "MY-447L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY447LDigest/Digest.py "Mail" "2020-10-20"> /home/admin/Logs/LogsMY447LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-447L_Mail"
    sleep 10
fi


if [ "$1" == "MY-448L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY448LDigest/Digest.py "Mail" "2020-10-20"> /home/admin/Logs/LogsMY448LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-448L_Mail"
    sleep 10
fi


if [ "$1" == "MY-449L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY449LDigest/Digest.py "Mail" "2020-10-20"> /home/admin/Logs/LogsMY449LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-449L_Mail"
    sleep 10
fi

if [ "$1" == "MY-450L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY450LDigest/Digest.py "Mail" "2020-10-29"> /home/admin/Logs/LogsMY450LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-450L_Mail"
    sleep 10
fi

if [ "$1" == "MY-451L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY451LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY451LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-451L_Mail"
    sleep 10
fi

if [ "$1" == "MY-452L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY452LDigest/Digest.py "Mail" "2020-10-31"> /home/admin/Logs/LogsMY452LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-452L_Mail"
    sleep 10
fi

if [ "$1" == "MY-455L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY455LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY455LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-455L_Mail"
    sleep 10
fi

if [ "$1" == "MY-453L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY453LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY453LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-453L_Mail"
    sleep 10
fi

if [ "$1" == "MY-454L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY454LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY454LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-454L_Mail"
    sleep 10
fi

if [ "$1" == "MY-456L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY456LDigest/Digest.py "Mail" "2020-11-08"> /home/admin/Logs/LogsMY456LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-456L_Mail"
    sleep 10
fi

if [ "$1" == "MY-457L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY457LDigest/Digest.py "Mail" "2020-11-08"> /home/admin/Logs/LogsMY457LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-457L_Mail"
    sleep 10
fi

if [ "$1" == "MY-458L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY458LDigest/Digest.py "Mail" "2020-11-16"> /home/admin/Logs/LogsMY458LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-458L_Mail"
    sleep 10
fi

if [ "$1" == "MY-459L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY459LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY459LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-459L_Mail"
    sleep 10
fi

if [ "$1" == "MY-460L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY460LDigest/Digest.py "Mail" "2020-11-08"> /home/admin/Logs/LogsMY460LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-460L_Mail"
    sleep 10
fi

if [ "$1" == "MY-461L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY461LDigest/Digest.py "Mail" "2020-11-16"> /home/admin/Logs/LogsMY461LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-461L_Mail"
    sleep 10
fi

if [ "$1" == "MY-462L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY462LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY462LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-462L_Mail"
    sleep 10
fi

if [ "$1" == "MY-463L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY463LDigest/Digest.py "Mail" "2020-11-16"> /home/admin/Logs/LogsMY463LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-463L_Mail"
    sleep 10
fi

if [ "$1" == "MY-464L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY464LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY464LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-464L_Mail"
    sleep 10
fi

if [ "$1" == "MY-465L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY465LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY465LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-465L_Mail"
    sleep 10
fi



if [ "$1" == "MY-466L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY466LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY466LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-466L_Mail"
    sleep 10
fi

if [ "$1" == "MY-467L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY467LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY467LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-467L_Mail"
    sleep 10
fi

if [ "$1" == "MY-468L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY468LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY468LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-468L_Mail"
    sleep 10
fi

if [ "$1" == "MY-469L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY469LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY469LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-469L_Mail"
    sleep 10
fi

if [ "$1" == "MY-470L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY470LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY470LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-470L_Mail"
    sleep 10
fi

if [ "$1" == "MY-471L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY471LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY471LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-471L_Mail"
    sleep 10
fi


if [ "$1" == "MY-472L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY472LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY472LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-472L_Mail"
    sleep 10
fi

if [ "$1" == "MY-473L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY473LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY473LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-473L_Mail"
    sleep 10
fi

if [ "$1" == "MY-474L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY474LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY474LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-474L_Mail"
    sleep 10
fi

if [ "$1" == "MY-475L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY475LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY475LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-475L_Mail"
    sleep 10
fi

if [ "$1" == "MY-476L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY476LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY476LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-476L_Mail"
    sleep 10
fi

if [ "$1" == "MY-477L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY477LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY477LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-477L_Mail"
    sleep 10
fi

if [ "$1" == "MY-478L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY478LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY478LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-478L_Mail"
    sleep 10
fi

if [ "$1" == "MY-479L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY479LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY479LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-479L_Mail"
    sleep 10
fi

if [ "$1" == "MY-480L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY480LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY480LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-480L_Mail"
    sleep 10
fi


if [ "$1" == "MY-481L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY481LDigest/Digest.py "Mail" "2020-12-01"> /home/admin/Logs/LogsMY481LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-481L_Mail"
    sleep 10
fi


if [ "$1" == "MY-482L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY482LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY482LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-482L_Mail"
    sleep 10
fi

if [ "$1" == "MY-483L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY483LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY483LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-483L_Mail"
    sleep 10
fi

if [ "$1" == "MY-484L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY484LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY484LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-484L_Mail"
    sleep 10
fi

if [ "$1" == "MY-485L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY485LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY485LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-485L_Mail"
    sleep 10
fi

if [ "$1" == "MY-486L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY486LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY486LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-486L_Mail"
    sleep 10
fi

if [ "$1" == "MY-487L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY487LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY487LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-487L_Mail"
    sleep 10
fi

if [ "$1" == "MY-488L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY488LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY488LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-488L_Mail"
    sleep 10
fi

if [ "$1" == "MY-489L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY489LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY489LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-489L_Mail"
    sleep 10
fi

if [ "$1" == "MY-490L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY490LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY490LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-490L_Mail"
    sleep 10
fi

if [ "$1" == "MY-491L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY491LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY491LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-491L_Mail"
    sleep 10
fi

if [ "$1" == "MY-492L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY492LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY492LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-492L_Mail"
    sleep 10
fi

if [ "$1" == "MY-493L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY493LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY493LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-493L_Mail"
    sleep 10
fi

if [ "$1" == "MY-494L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY494LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY494LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-494L_Mail"
    sleep 10
fi

if [ "$1" == "MY-495L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY495LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY495LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-495L_Mail"
    sleep 10
fi

if [ "$1" == "MY-496L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY496LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY496LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-496L_Mail"
    sleep 10
fi

if [ "$1" == "MY-497L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY497LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY497LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-497L_Mail"
    sleep 10
fi

if [ "$1" == "MY-498L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY498LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY498LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-498L_Mail"
    sleep 10
fi

if [ "$1" == "MY-499L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY499LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY499LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-499L_Mail"
    sleep 10
fi

if [ "$1" == "MY-500L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY500LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY500LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-500L_Mail"
    sleep 10
fi

if [ "$1" == "MY-501L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY501LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY501LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-501L_Mail"
    sleep 10
fi

if [ "$1" == "MY-502L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY502LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY502LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-502L_Mail"
    sleep 10
fi

if [ "$1" == "MY-503L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY503LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY503LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-503L_Mail"
    sleep 10
fi

if [ "$1" == "MY-504L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY504LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY504LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-504L_Mail"
    sleep 10
fi

if [ "$1" == "MY-505L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY505LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY505LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-505L_Mail"
    sleep 10
fi

if [ "$1" == "MY-506L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY506LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsMY506LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-506L_Mail"
    sleep 10
fi

if [ "$1" == "MY-507L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY507LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY507LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-507L_Mail"
    sleep 10
fi

if [ "$1" == "MY-508L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY508LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY508LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-508L_Mail"
    sleep 10
fi

if [ "$1" == "MY-509L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY509LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY509LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-509L_Mail"
    sleep 10
fi

if [ "$1" == "MY-510L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY510LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY510LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-510L_Mail"
    sleep 10
fi

if [ "$1" == "MY-511L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY511LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY511LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-511L_Mail"
    sleep 10
fi

if [ "$1" == "MY-512L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY512LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY512LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-512L_Mail"
    sleep 10
fi

if [ "$1" == "MY-513L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY513LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY513LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-513L_Mail"
    sleep 10
fi

if [ "$1" == "MY-514L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY514LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY514LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-514L_Mail"
    sleep 10
fi

if [ "$1" == "MY-515L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY515LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY515LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-515L_Mail"
    sleep 10
fi

if [ "$1" == "MY-516L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY516LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY516LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-516L_Mail"
    sleep 10
fi

if [ "$1" == "MY-517L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY517LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY517LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-517L_Mail"
    sleep 10
fi

if [ "$1" == "MY-518L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY518LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY518LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-518L_Mail"
    sleep 10
fi

if [ "$1" == "MY-519L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY519LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY519LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-519L_Mail"
    sleep 10
fi

if [ "$1" == "MY-520L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY520LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY520LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-520L_Mail"
    sleep 10
fi

if [ "$1" == "MY-521L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY521LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY521LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-521L_Mail"
    sleep 10
fi

if [ "$1" == "MY-522L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY522LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY522LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-522L_Mail"
    sleep 10
fi

if [ "$1" == "MY-523L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY523LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY523LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-523L_Mail"
    sleep 10
fi

if [ "$1" == "MY-524L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY524LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY524LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-524L_Mail"
    sleep 10
fi

if [ "$1" == "MY-525L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY525LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY525LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-525L_Mail"
    sleep 10
fi

if [ "$1" == "MY-526L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY526LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY526LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-526L_Mail"
    sleep 10
fi

if [ "$1" == "MY-527L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY527LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY527LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-527L_Mail"
    sleep 10
fi

if [ "$1" == "MY-528L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY528LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY528LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-528L_Mail"
    sleep 10
fi

if [ "$1" == "MY-529L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY529LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY529LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-529L_Mail"
    sleep 10
fi

if [ "$1" == "MY-530L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY530LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY530LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-530L_Mail"
    sleep 10
fi

if [ "$1" == "MY-531L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY531LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY531LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-531L_Mail"
    sleep 10
fi

if [ "$1" == "MY-532L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY532LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY532LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-532L_Mail"
    sleep 10
fi

if [ "$1" == "MY-533L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY533LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY533LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-533L_Mail"
    sleep 10
fi

if [ "$1" == "MY-534L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY534LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY534LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-534L_Mail"
    sleep 10
fi

if [ "$1" == "MY-535L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY535LDigest/Digest.py "Mail" "2020-12-25"> /home/admin/Logs/LogsMY535LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-535L_Mail"
    sleep 10
fi

if [ "$1" == "MY-536L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY536LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY536LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-536L_Mail"
    sleep 10
fi

if [ "$1" == "MY-537L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY537LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY537LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-537L_Mail"
    sleep 10
fi

if [ "$1" == "MY-538L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY538LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY538LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-538L_Mail"
    sleep 10
fi

if [ "$1" == "MY-539L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY539LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY539LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-539L_Mail"
    sleep 10
fi

if [ "$1" == "MY-540L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY540LDigest/Digest.py "Mail" "2021-03-24"> /home/admin/Logs/LogsMY540LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-540L_Mail"
    sleep 10
fi

if [ "$1" == "MY-541L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY541LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY541LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-541L_Mail"
    sleep 10
fi

if [ "$1" == "MY-542L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY542LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY542LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-542L_Mail"
    sleep 10
fi

if [ "$1" == "MY-543L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY543LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY543LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-543L_Mail"
    sleep 10
fi

if [ "$1" == "MY-544L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY544LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY544LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-544L_Mail"
    sleep 10
fi

if [ "$1" == "MY-545L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY545LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY545LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-545L_Mail"
    sleep 10
fi

if [ "$1" == "MY-546L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY546LDigest/Digest.py "Mail" "2021-04-07"> /home/admin/Logs/LogsMY545LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-546L_Mail"
    sleep 10
fi


if [ "$1" == "MY-547L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY547LDigest/Digest.py "Mail" "2021-04-07"> /home/admin/Logs/LogsMY547LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-547L_Mail"
    sleep 10
fi


if [ "$1" == "MY-548L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY548LDigest/Digest.py "Mail" "2021-04-07"> /home/admin/Logs/LogsMY545LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-548L_Mail"
    sleep 10
fi

if [ "$1" == "MY-549L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY549LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY549LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-549L_Mail"
    sleep 10
fi

if [ "$1" == "MY-550L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY550LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY550LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-550L_Mail"
    sleep 10
fi

if [ "$1" == "MY-551L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY551LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY551LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-551L_Mail"
    sleep 10
fi

if [ "$1" == "MY-552L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY552LDigest/Digest.py "Mail" "2021-03-21"> /home/admin/Logs/LogsMY552LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-552L_Mail"
    sleep 10
fi

if [ "$1" == "MY-553L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY553LDigest/Digest.py "Mail" "2021-05-01"> /home/admin/Logs/LogsMY553LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-553L_Mail"
    sleep 10
fi

if [ "$1" == "MY-554L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY554LDigest/Digest.py "Mail" "2021-05-01"> /home/admin/Logs/LogsMY554LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-554L_Mail"
    sleep 10
fi

if [ "$1" == "MY-555L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY555LDigest/Digest.py "Mail" "2021-04-01"> /home/admin/Logs/LogsMY555LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-555L_Mail"
    sleep 10
fi

if [ "$1" == "MY-556L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY556LDigest/Digest.py "Mail" "2021-04-01"> /home/admin/Logs/LogsMY556LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-556L_Mail"
    sleep 10
fi

if [ "$1" == "MY-557L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY557LDigest/Digest.py "Mail" "2021-04-01"> /home/admin/Logs/LogsMY557LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-557L_Mail"
    sleep 10
fi

if [ "$1" == "MY-558L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY558LDigest/Digest.py "Mail" "2021-05-01"> /home/admin/Logs/LogsMY558LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-558L_Mail"
    sleep 10
fi

if [ "$1" == "MY-559L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY559LDigest/Digest.py "Mail" "2021-05-01"> /home/admin/Logs/LogsMY559LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-559L_Mail"
    sleep 10
fi

if [ "$1" == "MY-560L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY560LDigest/Digest.py "Mail" "2021-05-01"> /home/admin/Logs/LogsMY560LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-560L_Mail"
    sleep 10
fi

if [ "$1" == "MY-561L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY561LDigest/Digest.py "Mail" "2021-05-01"> /home/admin/Logs/LogsMY561LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-561L_Mail"
    sleep 10
fi

if [ "$1" == "MY-562L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY562LDigest/Digest.py "Mail" "2021-05-01"> /home/admin/Logs/LogsMY562LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-562L_Mail"
    sleep 10
fi

if [ "$1" == "MY-563L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY563LDigest/Digest.py "Mail" "2021-05-01"> /home/admin/Logs/LogsMY563LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-563L_Mail"
    sleep 10
fi

if [ "$1" == "MY-564L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/MY564LDigest/Digest.py "Mail" "2021-05-01"> /home/admin/Logs/LogsMY564LMail.txt &
    y=`echo $!`
    x="$x\n$y --MY-564L_Mail"
    sleep 10
fi

if [ "$1" == "MY-000L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python3 /home/admin/CODE/Master_Digest/Shell_Master_Digest_Azure.py > /home/admin/Logs/LogsMY000LMail.txt &
   	y=`echo $!`
    x="$x\n$y --MY-000L_Mail"
    sleep 10
fi

if [ "$1" == "PH-002L_Mail" ] || [ $RUNALL == 1 ] 
then
    stdbuf -oL python /home/admin/CODE/PH002LDigest/Digest.py "Mail" "2020-11-01"> /home/admin/Logs/LogsPH002LMail.txt &
   	y=`echo $!`
    x="$x\n$y --PH-002L_Mail"
    sleep 10
fi

if [ "$1" == "VN-001X_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/VN001XDigest/MailDigestVN001.R &
	y=`echo $!`
	x="$x\n$y --VN-001X_Mail"
	sleep 10
fi

if [ "$1" == "MasterMail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/MasterMail/MasterMail.R &
	y=`echo $!`
	x="$x\n$y --MasterMail"
	sleep 10
fi

if [ "$1" == "SERIS3G_Mail" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/SERIS_3G/SERIS_3G.R &
	y=`echo $!`
	x="$x\n$y --SERIS3G"
	sleep 10
fi

if [ "$1" == "MY-001X_Azure4G" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/MY001X/MY-001X_azure_4g.py > /home/admin/Logs/LogsMY001X_Azure_4G.txt &
	y=`echo $!`
	x="$x\n$y --MY-001X_Azure4G"
	sleep 10
fi

if [ "$1" == "MY-003X_Azure4G" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/MY003X/MY-003X_azure_4g.py > /home/admin/Logs/LogsMY003X_Azure_4G.txt &
	y=`echo $!`
	x="$x\n$y --MY-003X_Azure4G"
	sleep 10
fi

if [ "$1" == "SG-003E_AzureAll" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/SG003E/SG-003E_azure_all.py > /home/admin/Logs/LogsSG003E_Azure_All.txt &
	y=`echo $!`
	x="$x\n$y --SG-003E_AzureAll"
	sleep 10
fi

if [ "$1" == "SG-007E_AzureAll" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/SG007E/SG-007E_azure_all.py > /home/admin/Logs/LogsSG007E_Azure_All.txt &
	y=`echo $!`
	x="$x\n$y --SG-007E_AzureAll"
	sleep 10
fi

if [ "$1" == "GIS_MASTER_ALL" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/GIS_Master_All/gis_master_all.py > /home/admin/Logs/LogsGISMasterAll.txt &	
	y=`echo $!`
	x="$x\n$y --GIS_MASTER_ALL"
	sleep 10
fi

if [ "$1" == "GIS_SUM_AHME" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/GIS_SUM/gis_sum_ahmedabad.py > /home/admin/Logs/LogsGIS_sum_ahme.txt &
	y=`echo $!`
	x="$x\n$y --GIS_SUM_AHME"
	sleep 10
fi

if [ "$1" == "GIS_SUM_NEWD" ] || [ $RUNALL == 1 ] 
then
	python2 /home/admin/CODE/DatabaseCreation/GIS_SUM/gis_sum_newd.py > /home/admin/Logs/LogsGIS_sum_newd.txt &
	y=`echo $!`
	x="$x\n$y --GIS_SUM_NEWD"
	sleep 10
fi

if [ "$1" == "TH-003X_Live" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/TH003XDigest/MailDigestTH003.R &
	y=`echo $!`
	x="$x\n$y --TH-003X_Live"
	sleep 10
fi

if [ "$1" == "KH003_Merge_Live" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Azure/KH003_Merge_Live.py > /home/admin/Logs/LogsKH003MergeLive.txt &
	y=`echo $!`
	x="$x\n$y --KH003_Merge_Live"
	sleep 10
fi

if [ "$1" == "Seris_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Alarms/Seris_Alarm.py > /home/admin/Logs/LogsSeris_Alarm.txt &
	y=`echo $!`
	x="$x\n$y --Seris_Alarm"
	sleep 10
fi

if [ "$1" == "Locus_Alarm" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python3 /home/admin/CODE/Alarms/Locus_Alarm.py > /home/admin/Logs/LogsLocus_Alarm.txt &
	y=`echo $!`
	x="$x\n$y --Locus_Alarm"
	sleep 10
fi

if [ "$1" == "Inverter_Alarm" ] || [ $RUNALL == 1 ] 
then
	sudo stdbuf -oL python3 /home/admin/CODE/Alarms/Inverter_Alarm.py > /home/admin/Logs/LogsInverter_Alarm.txt &
	y=`echo $!`
	x="$x\n$y --Inverter_Alarm"
	sleep 10
fi

if [ "$1" == "Bot_Status" ] || [ $RUNALL == 1 ] 
then
	stdbuf -oL python /home/admin/CODE/Alarms/Bot_Status.py > /home/admin/Logs/LogsBotStatus.txt &
	y=`echo $!`
	x="$x\n$y --Bot_Status"
	sleep 10
fi

fi # RunRONLY

if [ $RUNPYTHONONLY -eq 0 ] 
then
if [ "$1" == "FaultAlert" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/Misc/FaultAlert.R &
	y=`echo $!`
	x="$x\n$y --FaultAlert"
	sleep 10
fi

if [ "$1" == "DropboxCheck" ] || [ $RUNALL == 1 ] 
then
	Rscript /home/admin/CODE/Misc/dropbox.R &
	y=`echo $!`
	x="$x\n$y --DropboxCheck"
	sleep 10
fi

if [ "$1" == "MemoryProfile" ] || [ $RUNALL == 1 ] 
then
	sh /home/admin/Usage.sh &
	sleep 1
fi
fi #RUNPYTHONONLY

if [ $# == 0 ] 
then 
	echo -e "$x" > "/home/admin/psIds.txt"
else 
	y1="$(echo -e "${x}" | sed -e ':a;N;$!ba;s/\n/ /g')"
	y2="$(echo -e "${y1}" | sed -e 's/^[[:space:]]*//')"
	sed -i "/$1/c$y2" "/home/admin/psIds.txt"
	#echo -e "$x" >> "/home/admin/psIds.txt"
fi

