import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats
from matplotlib.ticker import MaxNLocator

date=sys.argv[1]
path_write='/home/admin/Graphs/'

df_pr=pd.read_csv('/home/admin/Graphs/Graph_Extract/IN-301/[IN-301] Graph '+date+ " - PR Evolution.txt",sep='\t')
df_cpr=pd.read_csv('/home/admin/Graphs/Graph_Extract/IN-301/[IN-301] Graph '+date+ " - Temperature Corrected PR Evolution.txt",sep='\t')

df_pr=pd.merge(df_pr,df_cpr,on='Date',how="left")
df_pr=df_pr[df_pr['Date']<=date]
df_pr=df_pr.loc[(df_pr['GHI_x']<9),:]
df_pr['PR_Diff']=df_pr['PR_y']-df_pr['PR_x']
no_points=len(df_pr)
avg_cpr=df_pr['PR_y'].mean()
avg_pr=df_pr['PR_x'].mean()
df_pr['Date']=pd.to_datetime(df_pr['Date'])
plt.rcParams.update({'font.size': 28})
colors=['red','darkorange','green']
#Plot 1
fig = plt.figure(num=None, figsize=(45  , 30))
ax = fig.add_subplot(111)
x=df_pr['Date'].tolist()
y=df_pr['PR_Diff'].tolist()
z=df_pr['GHI_x'].tolist()
for index,i in enumerate(y):
    if(z[index]<3):
        ln1=ax.scatter(x[index], i, marker='D',color=colors[2],s=150, alpha=0.75, zorder=3)
    elif(z[index]>=3 and z[index]<=6):
        ln2=ax.scatter(x[index], i, marker='o',color=colors[1],s=150, alpha=0.75, zorder=3)
    elif(z[index]>6):
        ln3=ax.scatter(x[index], i, marker='^',color=colors[0],s=150, alpha=0.75, zorder=3)
myFmt = mdates.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_formatter(myFmt)
ax.set_ylabel('Performance Ratio Difference [%]', fontsize=32)
df_pr = df_pr[~df_pr.isin([np.nan, np.inf, -np.inf]).any(1)]
print(max(df_pr['PR_Diff'].tolist()))
ax.set_ylim([0,max(df_pr['PR_Diff'].tolist())+3])
ttl=fig.suptitle("Temperature Corrected PR vs PR",fontsize=45)
ttl.set_position([.5, 0.95])
ax.legend((ln1, ln2, ln3),('GHI<3', '3<GHI<6', 'GHI>6'),scatterpoints=1,fontsize=32,loc=(0.1,.9),frameon=False,ncol=3)
#ax.annotate('No. of Points: '+str(no_points)+' days\n\nAverage PR using Pyranometer [%]: '+str(round(avg_pyr,1))+'\n\nAverage PR using Silicon Sensor [%]: '+str(round(avg_si,1)), (.7, .1),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='left', bbox=dict(boxstyle="square", fc="none", ec="black",lw=5), va='bottom',size=25)
fig.savefig(path_write+"Graph_Output/IN-301/[IN-301] Graph "+date+" - PR Difference.pdf", bbox_inches='tight')

#Plot 2
fig2 = plt.figure(num=None, figsize=(45  , 30))
ax2 = fig2.add_subplot(111)
x=df_pr['GHI_x'].tolist()
y=df_pr['PR_Diff'].tolist()
for index,i in enumerate(y):
    if(i<3):
        ln1=ax2.scatter(x[index], i, marker='D',color=colors[2],s=150, alpha=0.75, zorder=3)
    elif(i>=3 and i<=6):
        ln2=ax2.scatter(x[index], i, marker='o',color=colors[1],s=150, alpha=0.75, zorder=3)
    elif(i>6):
        ln3=ax2.scatter(x[index], i, marker='^',color=colors[0],s=150, alpha=0.75, zorder=3)
ax2.set_ylabel('Performance Ratio Difference [%]', fontsize=32)
ax2.set_xlabel('Global Horizontal Irradiance [W/m$^2$]', fontsize=32)
print(max(df_pr['PR_Diff'].tolist()))

ttl=fig2.suptitle("PR Difference vs GHI",fontsize=45)
ttl.set_position([.5, 0.95])
ax2.legend((ln1, ln2, ln3),('GHI<3', '3<GHI<6', 'GHI>6'),scatterpoints=1,fontsize=32,loc=(0.1,.9),frameon=False,ncol=3)
#ax2.annotate('No. of Points: '+str(no_points)+' days\n\nAverage PR using Pyranometer [%]: '+str(round(avg_pyr,1)), (.68, .1),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='left', bbox=dict(boxstyle="square", fc="none", ec="black",lw=5), va='bottom',size=25)
#plt.legend([a],['PR Calculated using Pyronameter'],scatterpoints=1,loc=(0.02,.9),ncol=1,fontsize=25)
plt.gca().xaxis.set_major_locator(MaxNLocator(prune='lower'))
fig2.savefig(path_write+"Graph_Output/IN-301/[IN-301] Graph "+date+" - PR Difference vs GHI Pyranometer.pdf", bbox_inches='tight')

