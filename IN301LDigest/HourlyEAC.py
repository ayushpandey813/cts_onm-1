import pandas as pd
import numpy as np
import os
import pytz
import datetime
from functools import cmp_to_key
import xlsxwriter
import shutil


tz = pytz.timezone('Asia/Calcutta')
date = datetime.datetime.now(tz)
date_yesterday = date - datetime.timedelta(days=1)
yesterday = date_yesterday.strftime('%Y-%m-%d')
date_yesterday = datetime.datetime.strptime(yesterday, '%Y-%m-%d')

SITE_CODE = 'IN-301L'
MFM_NAME = 'MFM_2_MCR TVM 1 TRAFO'
BASE_PATH = '/home/admin/Dropbox/Gen 1 Data/[{SITE_CODE}]/'.format(SITE_CODE=SITE_CODE)
EXCEL_PATH_WRITE = 'YearlyHourAggregation2020.xlsx'
INTERMEDIATE_FOLDER = './INTERMEDIATE_FILES_2020'



def col_cmp(col1, col2):
    '''
    Comparator function to sort day wise data in dataframes
    '''
    col1day = list(map(int, col1.split('-')[-1]))[0]
    col2day = list(map(int, col2.split('-')[-1]))[0]
    if col1day < col2day:
        return -1
    return 1

def get_month_values(month_path):
    '''
    retrieve daywise gen1 data for a month 
    store in a dataframe and return the df
    '''
    mfm_month_path = month_path + '/{mfm_name}'.format(mfm_name=MFM_NAME)
    month_df = None
    daily_file_list = [f.path for f in os.scandir(mfm_month_path) if f.is_dir]
    for daily_file in daily_file_list:
        date = str(daily_file)[-14:-4]
        month = int(date[5:7])
        if datetime.datetime.strptime(date, '%Y-%m-%d') > date_yesterday:
            continue
        daily_df = pd.read_csv(daily_file, sep='\t').loc[:, ['ts', 'TotWhExp_max']]
        daily_df['ts'] = pd.to_datetime(daily_df['ts'])
        daily_df.set_index('ts', inplace=True)
        daily_df_grouped = daily_df.groupby(daily_df.index.hour)
        daily_df = pd.DataFrame(index=daily_df_grouped.groups.keys(), columns=[date])
        daily_df.columns = [date]
        for hour in daily_df_grouped.groups.keys():
            hdf = daily_df_grouped.get_group(hour)['TotWhExp_max']
            # THIS DID NOT WORK DUE TO NANS, AND SAMPLING FREQUENCY DIFFERENCES, USED THE NEXT SNIPPET INSTEAD
            # daily_df.iloc[hour] = abs((hdf.iloc[-1]-hdf.iloc[0])/1000) if int(date[5:7]) not in [7,8] else abs(hdf.iloc[0]/1000)
            # THE FOLLOWING SNIPPET USES THE FIRST  AND LAST AVAILABLE (NOT NAN) VALUE TO CALCULATE HOURLY DATA
            idxl = hdf.last_valid_index()
            idxf = hdf.first_valid_index()
            
            try:
                v1 = hdf.loc[idxl]
            except KeyError:
                v1 = hdf.iloc[-1]
            try:
                v2 = hdf.loc[idxf]
            except KeyError:
                v2 = hdf.iloc[0]
            daily_df.iloc[hour] = abs((v1-v2)/1000)

        if month_df is None:
            month_df = daily_df
        else:
            daily_df.index = month_df.index
            month_df = pd.concat([month_df, daily_df], axis = 1)
    col_list = month_df.columns
    col_list = sorted(col_list, key = cmp_to_key(col_cmp))
    month_df.columns = col_list
    month_df.replace(np.nan, 0, inplace=True)
    return month_df


def month_mapper(month):
    return datetime.datetime.strptime(str(month), "%m").strftime("%b")

def get_year_values_monthwise(year):
    '''
    aggregates monthwise values into a dataframe based off of the year,
    calls the get_month_vales to get it's data monthwise
    '''
    year_path = '/home/admin/Dropbox/Gen 1 Data/[{SITE_CODE}]/{year}'.format(SITE_CODE=SITE_CODE, year=year)
    month_list = [f.path for f in os.scandir(year_path) if f.is_dir]
    year_df = None
    if os.path.isdir(INTERMEDIATE_FOLDER):
        shutil.rmtree(INTERMEDIATE_FOLDER)
    os.mkdir(INTERMEDIATE_FOLDER)
    for month_path in month_list:
        month_df = get_month_values(month_path)
        month = int(month_path.split('-')[-1])
        day_list = list(month_df.columns)
        month_df['HourSum'] = 0
        month_df['HourMean'] = 0
        month_df['HourSum'] = round(month_df[day_list].sum(axis=1),2)
        month_df['HourMean'] =round( month_df[day_list].mean(axis=1),2)
        save_path = INTERMEDIATE_FOLDER+'/{yr}-{month}.csv'.format(yr=year, month=month)
        month_df.to_csv(save_path, sep = '\t')
        month_df = month_df[['HourSum', 'HourMean']]
        month_df.rename(columns={
            'HourSum': '{month}_{year}.Sum'.format(month=month_mapper(month), year=year),
            'HourMean': '{month}_{year}.Mean'.format(month=month_mapper(month), year=year)
            }, inplace=True)
        
        if year_df is None:
            year_df = month_df
        else:
            month_df.index = year_df.index
            year_df = pd.concat([year_df, month_df], axis = 1)
    return year_df

def excel_style(col):
    """ Convert given row and column number to an Excel-style cell name. """
    LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    col+=1
    result = []
    while col:
        col, rem = divmod(col-1, 26)
        result[:0] = LETTERS[rem]
    return ''.join(result)

def gen_year_excel(year):
    '''
    Generates the excel off of the get_year_values_monthwise dataframe
    '''
    year_df = get_year_values_monthwise(year)
    year_df.index = ['{x}:00'.format(x=str(x).zfill(2)) for x in list(year_df.index)]
    
    # SET UP EXCEL META INFO AND HEADER DATA
    FIRST_ROW_HEADERS = (year_df.columns)
    FIRST_ROW_HEADERS = list([x.split('.')[0] for x in FIRST_ROW_HEADERS])
    FIRST_ROW_HEADERS = pd.Series(FIRST_ROW_HEADERS).drop_duplicates().tolist()

    workbook = xlsxwriter.Workbook(EXCEL_PATH_WRITE)
    worksheet = workbook.add_worksheet()

    bold = workbook.add_format({'bold': True,
        'align': 'center'
    })
    center_only = workbook.add_format({'align': 'center'})
    num_format =workbook.add_format({'num_format': '#,###0.00'})

    # ROW 1 MERGE CELLS AND ADD HEADERS
    #   set column widths 
    worksheet.set_column('A1:{ec}1'.format(ec =excel_style(year_df.shape[1])),  15)
    #   write column data
    row, col = 0, 0
    
    col+=1
    for header in FIRST_ROW_HEADERS:
        range_str = "{fc}{r}:{tc}{r}".format(fc=excel_style(col), r=row+1, tc=excel_style(col+1))
        worksheet.merge_range(range_str, header, bold)
        col += 2
    

    row+=1 
    col = 0


    # ROW 2 - ADD UNITS WRT TO COLUMNS
    worksheet.write("A2", "Time", bold)
    col += 1
    for col in range(1, year_df.shape[1]+1):
        worksheet.write("{c}{r}".format(c=excel_style(col),r=row+1), 'Sum [kWh]' if col%2 == 1 else 'Mean [kWh]', center_only)



    #ROW3 ONWARDS -- iterate over every row and column of df to enter values to the excel
    col = 0
    row += 1
    for i in range(year_df.shape[0]):
        worksheet.write('{c}{r}'.format(c=excel_style(col),r=row+1), str(year_df.index[i]))
        col+=1
        for j in range(year_df.shape[1]):
            pos = '{c}{r}'.format(c=excel_style(col), r = row+1)
            col+=1
            worksheet.write(pos,year_df.iloc[i, j], num_format)
        col = 0
        row+=1



    workbook.close()


if __name__ == '__main__':
    gen_year_excel(2020)
    # get_year_values_monthwise(2021)






































# def retrieve_data():

#     ''''
#     Given the station this function will retrieve gen-1 data for it and collate it into an appropriate dataframe
#     '''
#     data_path = '/home/admin/Dropbox/Gen 1 Data/{station}/'.format(station = stn) + date[:4] + '/' + date[:7]
#     # get inverter data for all inverters 
#     # get list of all folders in directory
#     folder_list_unfiltered = [f.path for f in os.scandir(data_path) if f.is_dir()]
#     #generate and store inverter data

#     master_df_inverter = None
#     folder_list_inverter = []
#     for folder in folder_list_unfiltered:
#         if "INVERTER" in folder:
#             inverter_name = folder.split('/')[-1]
#             if '22' not in inverter_name and '23' not in inverter_name and '24' not in inverter_name:
#                 folder_list_inverter.append(folder)
#     for folder in folder_list_inverter:
#         inverter_name = folder.split('/')[-1]
#         # go to this path and find file with the current date in name this is the file we query for data
#         files_in_folder = [f.path for f in os.scandir(folder) if f.is_dir]
#         file_with_date_in_name = list(filter(lambda x : date in x, files_in_folder))[0]
#         inverter_df = pd.read_csv(file_with_date_in_name, sep='\t').loc[:,['ts','W_avg','Wh_sum']]
#         inverter_df['ts'] = pd.to_datetime(inverter_df['ts'])
#         inverter_df_wavg = pd.DataFrame(inverter_df.resample('H', on='ts').W_avg.mean())
#         inverter_df_whsum = pd.DataFrame(inverter_df.resample('H', on='ts').Wh_sum.sum())
#         inverter_df_wavg.columns = [inverter_name+'_wavg']    
#         inverter_df_whsum.columns = [inverter_name+'_whsum']      
#         inverter_df_wavg.index = inverter_df_whsum.index
#         inverter_df = pd.concat([inverter_df_wavg, inverter_df_whsum], axis = 1) 
#         if master_df_inverter is None:
#             master_df_inverter = inverter_df
#         else:
#             inverter_df.index = master_df_inverter.index
#             master_df_inverter = pd.concat([master_df_inverter, inverter_df], axis = 1)

#     master_df_mfm = pd.DataFrame(index=master_df_inverter.index)
#     folder_list_mfm = []
#     for folder in folder_list_unfiltered:
#         if 'MFM' in folder:
#             mfm_name = folder.split('/')[-1]
#             for num in ['1', '2', '3', '7', '9']:
#                 if num in mfm_name: 
#                     folder_list_mfm.append(folder)
#     for folder in folder_list_mfm:
#         mfm_name = folder.split('/')[-1]
#         files_in_folder = [f.path for f in os.scandir(folder) if f.is_dir]
#         file_with_date_in_name = list(filter(lambda x: date in x, files_in_folder))[0]
#         mfm_df = pd.read_csv(file_with_date_in_name, sep='\t')
#         mfm_df = pd.DataFrame(mfm_df.loc[:,['ts','TotWhExp_max']])
#         mfm_df['ts'] = pd.to_datetime(mfm_df['ts'])
#         mfm_df.set_index('ts', inplace=True)
#         mfm_df_grouped = mfm_df.groupby(mfm_df.index.hour)
#         mfm_df = pd.DataFrame(index=master_df_mfm.index, columns=[mfm_name])

#         for hour in mfm_df_grouped.groups.keys():            
#             hour_df = mfm_df_grouped.get_group(hour)['TotWhExp_max']
#             mfm_df.iloc[hour] = hour_df.iloc[-1]-hour_df.iloc[0]
#         master_df_mfm = pd.concat([master_df_mfm, mfm_df], axis = 1)

#     master_df = pd.concat([master_df_inverter, master_df_mfm], axis = 1)
#     master_df = pd.DataFrame(master_df.iloc[5:(7+12)+1])
#     master_df.replace(np.nan, 0, inplace=True)
#     return master_df






