import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

end_date=sys.argv[1]


path='/home/admin/Dropbox/Third Gen/[SG-006X]/'
path_write='/home/admin/Graphs/'
df=pd.DataFrame()
for i in sorted(os.listdir(path)):
    for j in sorted(os.listdir(path+'/'+i)):
        df_temp=pd.read_csv(path+'/'+i+'/'+j,sep='\t')
        df=df.append(df_temp)


df_Yld=df[['Date','CovYld']]
df_Yld.columns=['Date','COV']

df=df_Yld
no_points=len(df)
df=df[df['Date']<=end_date].dropna()
df=df.replace(0, np.nan)
df=df.drop_duplicates()
last_7=df.tail(7)
last_30=df.tail(30)
last_60=df.tail(60).copy()
avg_std_7=last_7['COV'].mean()
avg_std_30=last_30['COV'].mean()
avg_std_60=last_60['COV'].mean()
avg_std_lifetime=df['COV'].mean()
plt.rcParams.update({'font.size': 28})
plt.rcParams['axes.facecolor'] = 'gainsboro'
fig = plt.figure(num=None, figsize=(55  , 30))
ax = fig.add_subplot(111)
df_outlier=last_60[(np.abs(stats.zscore(last_60['COV'])) > 3)]
outlier_dates=df_outlier['Date'].astype('datetime64[s]').tolist()
outlier_values=df_outlier['COV'].values.tolist()
last_60['Date']=pd.to_datetime(last_60['Date'])
last_date=last_60['Date'].tail(1).values[0].astype('datetime64[s]')
first_date=last_60['Date'].head(1).values[0].astype('datetime64[s]')
dates=last_60['Date'].astype('datetime64[s]').tolist()
last_60.index=last_60['Date']
upsampled=last_60.resample('2H')
interpolated = upsampled.interpolate(method='cubic')
plt.plot(interpolated['COV'],color='black',linewidth=3)
cov_values=last_60['COV'].values.tolist()
#plt.plot(last_60['Date'].tolist(),last_60['COV'].tolist(),color='black',linewidth=3)
plt.xticks(last_60['Date'].tolist(),rotation=90)
plt.xlim([first_date,last_date])
plt.ylim([0,10])
myFmt = mdates.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_formatter(myFmt)
ttl = ax.set_title('From '+str(first_date)[0:10]+' to '+str(last_date)[0:10], fontdict={'fontsize': 45, 'fontweight': 'medium'})
ttl.set_position([.485, 1.02])
ttl_main=fig.suptitle("SG-006"+' Daily Meter CoV',fontsize=60)
ax.annotate('No. of Points: '+str(no_points)+' days\nLast 7-day CoV average [%]: '+str(round(avg_std_7,1))+'\nLast 30-day CoV average [%]: '+str(round(avg_std_30,1))+'\nLast 60-day CoV average [%]: '+str(round(avg_std_60,1))+'\nLifetime CoV average [%]: '+str(round(avg_std_lifetime,1)), (.01, .98),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='white',backgroundcolor='k',ha='left', va='top',size=25)
plt.axhline(y=5, color='green',linewidth=8)
plt.axhline(y=7, color='orange',linewidth=8)
ax.set_ylabel('CoV [%]', fontsize=32)
ln1=None
ln2=None
ln3=None
for index,i in enumerate(cov_values):
    if(i<=5):
        ln1=plt.scatter(dates[index], i, marker='D',color='green',s=300)
    elif(i>5 and i<8):
        ln2=plt.scatter(dates[index], i, marker='s',color='orange',s=300)
    elif(i>=8):
        ln3=plt.scatter(dates[index], i, marker='^',color='red',s=300)
print(ln1,ln2,ln3)
if(ln1!=None and ln2!=None and ln3!=None):
    plt.legend([ln1, ln2, ln3],
            ['Cov [%] <= 5', 'Cov [%] > 5 and CoV [%] < 7', 'Cov [%] >= 7'],
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
elif(ln1!=None and ln2!=None):
    plt.legend([ln1, ln2],
            ['Cov [%] <= 5', 'Cov [%] > 5 and CoV [%] < 7'],
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
elif(ln1==None and ln2!=None and ln3!=None):
    plt.legend([ln2, ln3],
            [ 'Cov [%] > 5 and CoV [%] < 7', 'Cov [%] >= 7'],
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
else:
    plt.legend([ln1],
            ['Cov [%] <= 5'],
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
for i, label in enumerate(outlier_dates):
    if(outlier_values[i]>10):
        pass
    else:
        ax.annotate(label, (label, outlier_values[i]+.25),size=25,color='red')
chkdir(path_write+"Graph_Output/"+"SG-006")
chkdir(path_write+"Graph_Extract/"+"SG-006")
fig.savefig(path_write+"Graph_Output/"+"SG-006"+'/['+"SG-006"+"] Graph "+end_date+" - Meter CoV.pdf",bbox_inches='tight')
last_60.to_csv(path_write+"Graph_Extract/"+"SG-006"+'/['+"SG-006"+"] Graph "+end_date+" - Meter CoV.txt",sep='\t',index=False)