rm(list = ls())
errHandle = file('/home/admin/Logs/LogsSG007EFHistory.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/SGDigest/SG007EDigest/Final/FTPSG007EFdump.R')
LTCUTOFF = .001
FIREERRATA = 1
FIRETWILIONA = 0
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

checkValidFile = function(path,day)
{
  data = readLines(paste(path,day,sep="/"))
  if(grepl("CLEANTECH CHIA TAI",data[4]))                                
    return (1)
  return (0)
}

stitchFile = function(path,day,pathwrite)
{
  if(!checkValidFile(path,day))
    return ()
	splitvals = unlist(strsplit(day,"_"))
	yr = substr(as.character(splitvals[3]),5,8)
	mon = paste(yr,substr(as.character(splitvals[3]),3,4),sep="-")
	dayac = paste(mon,substr(as.character(splitvals[3]),1,2),sep="-")
	filename = paste(stnname," ",dayac,".txt",sep="")
	print(paste('Full filename is',filename))
	pathwrite = paste(pathwrite,yr,sep="/")
	checkdir(pathwrite)
	pathwrite = paste(pathwrite,mon,sep="/")
	checkdir(pathwrite)
	pathwrite = paste(pathwrite,filename,sep="/")
	dataread = read.csv(paste(path,day,sep="/"),header = T,sep = ",",skip=6,
	stringsAsFactors = F)
	col2 = as.character(dataread[,2])
	idxrows = which(col2 %in% col2[grepl('IGS',col2)])
	idxrows2 = idxrows+1
	datareadcp = dataread
	dataread = dataread[idxrows,]
	dataread = dataread[,-2]
	dataread = dataread[,-c(5,6,10)]
	dataread[,c(5,7)] = datareadcp[idxrows2,c(8,10)]
	colnew = round((as.numeric(dataread[,2]) - as.numeric(dataread[,5])),2)
	colnew2 = round((as.numeric(dataread[,4]) + as.numeric(dataread[,7])),2)
	colnam = c(colnames(dataread),"IEQ-WEQ","NET")
	dataread[,(ncol(dataread)+1)] = colnew
	dataread[,(ncol(dataread)+1)] = colnew2
	colnames(dataread) = colnam
	write.table(dataread,file=pathwrite,row.names=F,col.names=T,sep="\t",append=F)
	recordTimeMaster("SG-007EFinal","Gen1",as.character(dayac))
}

path = '/home/admin/Dropbox/EMC-Raw/[SG-007E]/Final-Settlements'
checkdir(path)
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[SG-007E]'
checkdir(pathwrite)
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[SG-007E]/Final-Settlements'
checkdir(pathwrite)
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'SG007EF.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
idxtostart = 1
{
	if(!file.exists(pathdatelastread))
	{
	  print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[SG-007E]/Final-Settlements"')
		idxtostart = 1
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		lastrecordeddate = as.character(lastrecordeddate[1])
		idxtostart = match(lastrecordeddate,days)
		print(paste('Date read is',lastrecordeddate,'and idxtostart is',idxtostart))
	}
}
checkdir(pathwrite)

stnname =  "[SG-007E-F]"
{
	if(idxtostart <= length(days))
	{
		print(paste('idxtostart is',idxtostart,'while length of days is',length(days)))
		for(x in idxtostart : length(days))
		{
			stitchFile(path,days[x],pathwrite)
		}
		lastrecordeddate = as.character(days[x])
	}
	else
	{
		print('No historical backlog')
	}
}

print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
write(lastrecordeddate,file =pathdatelastread)

while(1)
{
	print('Checking FTP for new data')
 	filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(10800)
		next
	}
	print('FTP Check complete')
  daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	if(length(daysnew) == length(days))
	{
		if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(10800)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
		print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite)
		print(paste('Done',daysnew[x]))
	}
	write(as.character(daysnew[x]),pathdatelastread)
	gc()
}

print('Exit Loop')
sink()
