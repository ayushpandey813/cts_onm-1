import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc
import pymsteams
from twilio.rest import Client

def get_inverter_names(site,temp_list=None):
    inverters={}
    if(site=='[IN-301L]' or site=='[IN-304L]'):
        for i in os.listdir(path+site+'/'+date_now[0:4]+'/'+date_now[0:7]):
            if('INV' in i):
                inverters[i]=[datetime.datetime.now(tz),0]
    else:
        for i in temp_list:
            inverters[i]=[datetime.datetime.now(tz),0]
    return inverters

in302_mapping = [['INVERTER_24_LT Panel-01 Inverter-01', 'INVERTER_25_LT Panel-01 Inverter-02', 'INVERTER_26_LT Panel-01 Inverter-03', 'INVERTER_27_LT Panel-01 Inverter-04','INVERTER_28_LT Panel-01 Inverter-05','INVERTER_29_LT Panel-01 Inverter-06', 'INVERTER_30_LT Panel-01 Inverter-07', 
 'INVERTER_31_LT Panel-02 Inverter-8',
 'INVERTER_32_LT Panel-02 Inverter-9',
 'INVERTER_33_LT Panel-02 Inverter-10',
 'INVERTER_34_LT Panel-02 Inverter-11',
 'INVERTER_35_LT Panel-02 Inverter-12',
 'INVERTER_36_LT Panel-02 Inverter-13', 
 'INVERTER_37_LT Panel-03 Inverter-14',
 'INVERTER_38_LT Panel-03 Inverter-15',
 'INVERTER_39_LT Panel-03 Inverter-16', 
 'INVERTER_40_LT Panel-03 Inverter-17',
 'INVERTER_41_LT Panel-03 Inverter-18',
 'INVERTER_42_LT Panel-03 Inverter-19',
 'INVERTER_43_LT Panel-03 Inverter-20',
 'INVERTER_44_LT Panel-04 Inverter-21',
 'INVERTER_45_LT Panel-04 Inverter-22',
 'INVERTER_46_LT Panel-04 Inverter-23', 
 'INVERTER_47_LT Panel-04 Inverter-24',
 'INVERTER_48_LT Panel-04 Inverter-25', 
 'INVERTER_49_LT Panel-04 Inverter-26'],
 ['INVERTER_1_LT Panel-01 Inverter-27',
 'INVERTER_2_LT Panel-01 Inverter-28',
 'INVERTER_3_LT Panel-01 Inverter-29',
 'INVERTER_4_LT Panel-01 Inverter-30',
 'INVERTER_5_LT Panel-01 Inverter-31',
 'INVERTER_6_LT Panel-01 Inverter-32',
 'INVERTER_7_LT Panel-02 Inverter-33', 
 'INVERTER_13_LT Panel-03 Inverter-34',
 'INVERTER_14_LT Panel-03 Inverter-35', 
 'INVERTER_50_LT Panel-01 Inverter-36', 
 'INVERTER_18_LT Panel-04 Inverter-37',
 'INVERTER_19_LT Panel-04 Inverter-38', 
 'INVERTER_20_LT Panel-04 Inverter-39',
 'INVERTER_15_LT Panel-03 Inverter-40', 
 'INVERTER_16_LT Panel-03 Inverter-41',
 'INVERTER_17_LT Panel-03 Inverter-42',
 'INVERTER_21_LT Panel-04 Inverter-43', 
 'INVERTER_22_LT Panel-04 Inverter-44',
 'INVERTER_23_LT Panel-04 Inverter-45', 
 'INVERTER_8_LT Panel-02 Inverter-46',
 'INVERTER_9_LT Panel-02 Inverter-47', 
 'INVERTER_10_LT Panel-02 Inverter-48',
 'INVERTER_11_LT Panel-02 Inverter-49', 
 'INVERTER_12_LT Panel-02 Inverter-50']]

in303_mapping = [['INVERTER_1_Inverter 01', 'INVERTER_2_Inverter 02', 'INVERTER_3_Inverter 03', 'INVERTER_4_Inverter 04', 'INVERTER_5_Inverter 05', 'INVERTER_6_Inverter 06', 'INVERTER_7_Inverter 07', 'INVERTER_8_Inverter 08', 'INVERTER_9_Inverter 09', 'INVERTER_10_Inverter 10', 'INVERTER_11_Inverter 11', 'INVERTER_12_Inverter 12', 'INVERTER_13_Inverter 13', 'INVERTER_14_Inverter 14', 'INVERTER_15_Inverter 15', 'INVERTER_16_Inverter 16', 'INVERTER_47_Inverter-17', 'INVERTER_48_Inverter-18', 'INVERTER_49_Inverter-19', 'INVERTER_50_Inverter-20', 'INVERTER_51_Inverter-21', 'INVERTER_52_Inverter-22', 'INVERTER_53_Inverter-23', 'INVERTER_54_Inverter-24', 'INVERTER_55_Inverter-25', 'INVERTER_56_Inverter-26', 'INVERTER_57_Inverter-27', 'INVERTER_58_Inverter-28', 'INVERTER_59_Inverter-29', 'INVERTER_60_Inverter-30', 'INVERTER_61_Inverter-31'],
['INVERTER_17_Inverter 32','INVERTER_18_Inverter 33','INVERTER_19_Inverter 34',
'INVERTER_20_Inverter 35',
'INVERTER_21_Inverter 36',
'INVERTER_22_Inverter 37',
'INVERTER_23_Inverter 38',
'INVERTER_24_Inverter 39',
'INVERTER_25_Inverter 40',
'INVERTER_26_Inverter 41',
 'INVERTER_27_Inverter 42',
 'INVERTER_28_Inverter 43',
 'INVERTER_29_Inverter 44',
 'INVERTER_30_Inverter 45',
 'INVERTER_31_Inverter 46',
 'INVERTER_32_Inverter 47',
 'INVERTER_62_Inverter-48',
 'INVERTER_63_Inverter-49',
 'INVERTER_64_Inverter-50',
 'INVERTER_65_Inverter-51',
 'INVERTER_66_Inverter-52',
 'INVERTER_67_Inverter-53',
 'INVERTER_68_Inverter-54',
 'INVERTER_69_Inverter-55',
 'INVERTER_93_Inverter 56',
 'INVERTER_70_Inverter-57',
 'INVERTER_90_Inverter 58',
 'INVERTER_71_Inverter-59',
 'INVERTER_72_Inverter-60',
 'INVERTER_73_Inverter-61',
 'INVERTER_74_Inverter-62']
,['INVERTER_91_Inverter 63',
  'INVERTER_33_Inverter 64',
 'INVERTER_34_Inverter 65',
 'INVERTER_35_Inverter 66',
 'INVERTER_36_Inverter 67',
 'INVERTER_37_Inverter 69',
 'INVERTER_38_Inverter 70',
 'INVERTER_39_Inverter 71',
 'INVERTER_40_Inverter 72',
 'INVERTER_41_Inverter 73',
 'INVERTER_42_Inverter 74',
 'INVERTER_43_Inverter 75',
 'INVERTER_44_Inverter 76',
 'INVERTER_45_Inverter 77',
 'INVERTER_46_Inverter 78',
 'INVERTER_75_Inverter-79',
 'INVERTER_76_Inverter-80',
 'INVERTER_77_Inverter-81',
 'INVERTER_78_Inverter-82',
 'INVERTER_79_Inverter-83',
 'INVERTER_80_Inverter-84',
 'INVERTER_81_Inverter-85',
 'INVERTER_82_Inverter-86',
 'INVERTER_83_Inverter-87',
 'INVERTER_84_Inverter-88',
 'INVERTER_85_Inverter-89','INVERTER_86_Inverter-90','INVERTER_87_Inverter-91','INVERTER_88_Inverter-92','INVERTER_89_Inverter-93']]

in305_mapping = [['INVERTER_1_INV 01', 'INVERTER_2_INV 02', 'INVERTER_3_INV 03', 'INVERTER_4_INV 04', 'INVERTER_32_INV 05', 'INVERTER_5_INV 06', 'INVERTER_7_INV 07', 'INVERTER_33_INV 08', 'INVERTER_34_INV 09', 'INVERTER_8_INV 10', 'INVERTER_9_INV 11', 'INVERTER_10_INV 12', 'INVERTER_11_INV 13', 'INVERTER_12_INV 14', 'INVERTER_13_INV 15', 'INVERTER_14_INV 16', 'INVERTER_15_INV 17', 'INVERTER_16_INV 18', 'INVERTER_17_INV 19', 'INVERTER_18_INV 20', 'INVERTER_19_INV 21', 'INVERTER_20_INV 22', 'INVERTER_21_INV 23', 'INVERTER_22_INV 24', 'INVERTER_23_INV 25', 'INVERTER_24_INV 26', 'INVERTER_25_INV 27', 'INVERTER_26_INV 28', 'INVERTER_27_INV 29', 'INVERTER_28_INV 30','INVERTER_29_INV 31', 'INVERTER_30_INV 32', 'INVERTER_31_INV 33'],['INVERTER_64_INV 34', 'INVERTER_49_INV 35', 'INVERTER_45_INV 36', 'INVERTER_50_INV 37', 'INVERTER_55_INV 38', 'INVERTER_65_INV 39', 'INVERTER_36_INV 40', 'INVERTER_63_INV 41', 'INVERTER_67_INV 42', 'INVERTER_56_INV 43', 'INVERTER_66_INV 44', 'INVERTER_61_INV 45', 'INVERTER_35_INV 46', 'INVERTER_53_INV 47', 'INVERTER_40_INV 48', 'INVERTER_48_INV 49', 'INVERTER_43_INV 50', 'INVERTER_59_INV 51', 'INVERTER_42_INV 52', 'INVERTER_46_INV 53', 'INVERTER_58_INV 54', 'INVERTER_62_INV 55', 'INVERTER_54_INV 56', 'INVERTER_57_INV 57', 'INVERTER_41_INV 58', 'INVERTER_39_INV 59', 'INVERTER_60_INV 60', 'INVERTER_44_INV 61', 'INVERTER_37_INV 62', 'INVERTER_6_INV 63', 'INVERTER_47_INV 64', 'INVERTER_51_INV 65', 'INVERTER_52_INV 66', 'INVERTER_38_INV 67']]

in069_mapping = [['INVERTER_17', 'INVERTER_18'],['INVERTER_1', 'INVERTER_2','INVERTER_3', 'INVERTER_4', 'INVERTER_5', 'INVERTER_6', 'INVERTER_7', 'INVERTER_8', 'INVERTER_9', 'INVERTER_10', 'INVERTER_11', 'INVERTER_12', 'INVERTER_13', 'INVERTER_14', 'INVERTER_15', 'INVERTER_16'],['INVERTER_19', 'INVERTER_20'],['INVERTER_23'],['INVERTER_24']]

path = '/home/admin/Dropbox/Gen 1 Data/'
tz=pytz.timezone('Asia/Calcutta')
date_now=datetime.datetime.now(tz).strftime('%Y-%m-%d')

mapping = {  
'[IN-302L]':{'MFM_2_ICR-1 Main MFM':get_inverter_names('[IN-302L]',in302_mapping[0]),'MFM_3_ICR-2 Main MFM':get_inverter_names('[IN-302L]',in302_mapping[1]),'WMS_1_WMS-1':[]},
'[IN-303L]':{'MFM_2_ICR 01 HT MFM':get_inverter_names('[IN-303L]',in303_mapping[0]),'MFM_3_ICR 02 HT MFM':get_inverter_names('[IN-303L]',in303_mapping[1]),'MFM_4_ICR 03 HT MFM':get_inverter_names('[IN-303L]',in303_mapping[2]),'WMS_1_K&Z RT1':[]},
'[IN-305L]':{'MFM_4_Incomer MFM 01':get_inverter_names('[IN-305L]',in305_mapping[0]),'MFM_3_Incomer MFM 02':get_inverter_names('[IN-305L]',in305_mapping[1]),'WMS_1_Pyranometer':[]}

}


rec ={
 '[IN-069L]':['sai.pranav@cleantechsolar.com']
,'[IN-301L]':['sai.pranav@cleantechsolar.com']
,'[IN-302L]':['sai.pranav@cleantechsolar.com']
,'[IN-303L]':['sai.pranav@cleantechsolar.com']
,'[IN-304L]':['sai.pranav@cleantechsolar.com']
,'[IN-305L]':['sai.pranav@cleantechsolar.com']
}

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

SQL_Query = pd.read_sql_query('SELECT TOP (1000) [O&M_Code],[Webhook] FROM [dbo].[Station_Limits]', connStr)
df_webhook= pd.DataFrame(SQL_Query, columns=['O&M_Code','Webhook'])

connStr.close()

def sendmail(station,data,rec):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    recipients = rec# must be a list
    cleantech_rec=[]
    for i in recipients:
        if(i.split('@')[1]=='cleantechsolar.com' or i.split('@')[1]=='comin.com.kh'):
            cleantech_rec.append(i)
    TO=", ".join(cleantech_rec)
    SUBJECT = station+' String Alarm'
    text2='Last Timestamp Read: '+str(data[0]) +'\n\n Last Current Reading: '+str(data[2])+'\n\n Inverter-String  Name: '+str(data[1])
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    """ try:
        teams_alert(station,data)
    except:
        print('Teams Alert Failed') """
    server.quit()

def teams_alert(station,data):
    try:
        webhook=df_webhook.loc[df_webhook['O&M_Code'].str.strip()==station[1:-2],'Webhook'].values[0].strip()
        myTeamsMessage = pymsteams.connectorcard(webhook)
        myTeamsMessage.title(station[1:-2]+" String Alarm")
        myTeamsMessage.text("<pre>Last Timestamp Read: "+str(data[0])+"<br>Last Current Reading: "+str(round(data[2],1))+"<br>Inverter-String Name: "+str(data[1])+"</pre>")
        myTeamsMessage.send()
        time.sleep(5)
    except:
        pass

while(1):
    for i in mapping:
        print("###########################")
        print(i,datetime.datetime.now())
        print("###########################")
        for j in sorted(mapping[i],reverse=True):
            if('WMS' in j):
                wms_df=pd.read_csv(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+j+'/'+i+'-WMS'+j.split('_')[1]+'-'+date_now+'.txt',sep='\t')
                if(i=='[IN-301L]'):
                    irr_sum=wms_df['POAI_avg'].tail(10).sum()
                elif(i=='[IN-305L]' or i=='[IN-069L]'):
                    irr_sum=wms_df['OTI_avg'].tail(6).sum()
                else:
                    irr_sum=wms_df['POAI_avg'].tail(6).sum()
            else:
                mfm_df=pd.read_csv(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+j+'/'+i+'-MFM'+j.split('_')[1]+'-'+date_now+'.txt',sep='\t')
                mfm_df['ts']=pd.to_datetime(mfm_df['ts'])
                df_power=mfm_df[['W_avg']]
                if(i=='[IN-301L]'):
                    mfm_pow_sum=df_power.tail(10).sum()
                else:
                    mfm_pow_sum=df_power.tail(6).sum()
                if(irr_sum>200):                       #Irradiance filter
                    if(mfm_pow_sum[0]<=0):                   #MFM Filter
                        print('MFM Down!')
                    else: 
                        
                        for k in mapping[i][j]: 
                            
                            inv_name = k
                            if(os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+inv_name+'/'+i+'-I'+inv_name.split('_')[1]+'-'+date_now+'.txt')):
                                print(inv_name)
                                inv_df=pd.read_csv(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+inv_name+'/'+i+'-I'+inv_name.split('_')[1]+'-'+date_now+'.txt',sep='\t')
                                df_power=inv_df[['W_avg']].dropna()
                                inv_pow_sum=df_power.tail(10).sum()
                                if(inv_pow_sum[0]>0):
                                    temp_cols = []
                                    for c in inv_df.columns.tolist():
                                        if 'InDCA' in c and c.split('_')[1]=='avg':
                                            temp_cols.append(c) 
 
                                    df_current=inv_df[temp_cols].dropna()
                                    string_cur = {}
                                    string_flag = 0 
                                    for c in temp_cols:
                                        if(df_current[df_current[c]>0.5][c].sum()>10):
                                            string_flag = 1
                                            string_cur[c] = df_current[df_current[c]>0.5][c].sum()
                                    
                                    if(len(list(string_cur.values()))>0):
                                        max_cur = max(list(string_cur.values()))
                                        for key in string_cur:
                                            string_cur[key] = string_cur[key]/max_cur
                                        print(string_cur,k,)
                                            
                                        
                                   
    
                else:
                    print('No Irr!')

    print('sleeping')
    time.sleep(600)