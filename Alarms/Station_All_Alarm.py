import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import math
import sys
import numpy as np
import pyodbc
import pymsteams
import logging

tz = pytz.timezone('Asia/Calcutta')
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
path='/home/admin/Dropbox/Second Gen/'
date=str(datetime.datetime.now(tz).date()+datetime.timedelta(days=-1))

def create_template(date,stn,energy='NULL',yld='NULL',pr='NULL',ga='NULL',pa='NULL',cov_mfm='NULL',cov_inv='NULL',pr_alarm='NULL',mfm_cov_alarm='NULL',inv_cov_alarm='NULL',status='NULL',ghi='NULL',da='NULL'):
    df_template = pd.DataFrame({'Date':[date],'O&M_Code':[stn],'Energy':[energy],'Yield':[yld],'GHI':[ghi],'PR':[pr],'DA':[da],'GA':[ga],'PA':[pa],'CoV_MFM':[cov_mfm],'CoV_INV':[cov_inv],'PR_Alarm':[pr_alarm],'MFM_CoV_Alarm':[mfm_cov_alarm],'INV_CoV_Alarm':[inv_cov_alarm],'Status':[status]},columns =['Date','O&M_Code','Energy','Yield','GHI','PR','DA','GA','PA','CoV_MFM','CoV_INV','PR_Alarm','MFM_CoV_Alarm','INV_CoV_Alarm','Status'])
    return df_template

def seris_calc(stn,df,date):
    if(stn=='IN-015'):
        vals=df[['SolEnergyDelHVSide','HvYld','PRHV','Gsi','DA']].values.tolist()[0]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],ghi=vals[3],da=vals[4])
    elif(stn=='IN-036'):
        vals=df[['siClean','Eac2Str','Eac2Ctr','grid_avl','CoV','DA']].values.tolist()[0]
        Eac=vals[1]+vals[2]
        Yld=round(float((vals[1]+vals[2])/4511),2)
        PR=round(float(Yld/vals[0])*100,1)
        vals=[Eac]+[Yld]+[PR]+[vals[3],'NULL',vals[4],vals[5],vals[0]]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],cov_mfm=vals[5],ghi=vals[7],da=vals[6])
    elif(stn=='KH-001'):
        vals=df[['Eac2SystGen','DailySpecificYield2','PR2','DA']].values.tolist()[0]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],da=vals[3])
    elif(stn=='KH-003'):
        vals=df[['EacCokeMeth2','Yld2Coke','PR2Coke','Irradiance','DA']].values.tolist()[0]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],ghi=vals[3],da=vals[4])
    elif(stn=='KH-008'):#Cov
        vals=df[['Eac2Full','Yld2Full','PR2Full','Gsi02','DA']].values.tolist()[0]
        cov_vals=df[['Yld2Pond','Yld2R02','Yld2R03','Yld2R06','Yld2R11']].values.tolist()[0]
        cov=round(np.nanstd(cov_vals)*100/np.nanmean(cov_vals),1)
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],cov_mfm=cov,ghi=vals[3],da=vals[4])
    elif(stn=='MY-004'):#Cov
        vals=df[['Eac21','FullSiteYld2','FullSitePR2Si','GSi','DA']].values.tolist()[0]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],ghi=vals[3],da=vals[4])
    elif(stn=='MY-006'):#Cov
        vals=df[['FillSite2','FullSiteYld2','FullSitePR2Si','GSi','DA']].values.tolist()[0]
        caps=[1615.6, 1579.5, 858.2]
        eac_vals=df[['Eac21','Eac22','Eac23']].values.tolist()[0]
        ylds=[]
        for index,i in enumerate(eac_vals):
            ylds.append(i/caps[index])
        cov=round(np.nanstd(ylds)*100/np.nanmean(ylds),1)
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],cov_mfm=cov,ghi=vals[3],da=vals[4])
    elif(stn=='SG-003'):
        vals=df[['FullSiteProd','CovYlds','Gsi00','W_G','W_P','Gsi00','DA']].values.tolist()[0]
        yld=float(vals[0]/1621.8)
        pr=float(yld/vals[2])*100
        return create_template(date,stn,energy=vals[0],yld=yld,pr=pr,cov_mfm=vals[1],ga=vals[3],pa=vals[4],ghi=vals[5],da=vals[6])
    elif(stn=='SG-004'):
        vals=df[['FullSiteProd','FullSiteYld','Gsi00','CovYlds','DA']].values.tolist()[0]
        PR=round(float(vals[1]/vals[2])*100,1)
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=PR,cov_mfm=vals[3],ghi=vals[2],da=vals[4])
    elif(stn=='SG-005'):
        vals=df[['FullSite2','FullSiteYld2','FullSitePRSi2','CovYld2','GSi','DA']].values.tolist()[0]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],cov_mfm=vals[3],ghi=vals[4],da=vals[5])
    elif(stn=='VN-001'):
        if(os.path.exists(path+'[VN-001X]/'+date[0:4]+'/'+date[0:7]+'/[VN-001X] '+date+'.txt')):
            df_cov=pd.read_csv(path+'[VN-001X]/'+date[0:4]+'/'+date[0:7]+'/[VN-001X] '+date+'.txt',sep='\t')
            cov=df_cov['CovYld'].values[0]
            vals=df[['FullSite','FullSiteYld2','FullSitePRSi2','GSi','DA']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2],cov_mfm=0,cov_inv=cov,ghi=vals[3],da=vals[4])

def ebx_calc(stn,date,capacities):
    if(stn=='IN-008'):
        if(os.path.exists(path+'[IN-008X]/'+date[0:4]+'/'+date[0:7]+'/Meter 1/[IN-008X-M1] '+date+'.txt') and os.path.exists(path+'[IN-008X]/'+date[0:4]+'/'+date[0:7]+'/Meter 2/[IN-008X-M2] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[IN-008X]/'+date[0:4]+'/'+date[0:7]+'/Meter 1/[IN-008X-M1] '+date+'.txt',sep='\t')
            df_m2=pd.read_csv(path+'[IN-008X]/'+date[0:4]+'/'+date[0:7]+'/Meter 2/[IN-008X-M2] '+date+'.txt',sep='\t')
            vals_m1=df_m1[['Eac2']].values.tolist()[0]
            vals_m2=df_m2[['Eac2']].values.tolist()[0]
            eac=vals_m1[0]+vals_m2[0]
            yld=float(eac)/sum(capacities)
            return create_template(date,stn,energy=eac,yld=yld)
    elif(stn=='IN-010'):
        if(os.path.exists(path+'[IN-010X]/'+date[0:4]+'/'+date[0:7]+'/[IN-010X] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[IN-010X]/'+date[0:4]+'/'+date[0:7]+'/[IN-010X] '+date+'.txt',sep='\t')
            vals_m1=df[['Eac2','DailySpecYield2']].values.tolist()[0]
            return create_template(date,stn,energy=vals_m1[0],yld=vals_m1[1])
    elif(stn=='KH-002'):
        if(os.path.exists(path+'[KH-002X]/'+date[0:4]+'/'+date[0:7]+'/Solar/[KH-002X-M1] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[KH-002X]/'+date[0:4]+'/'+date[0:7]+'/Solar/[KH-002X-M1] '+date+'.txt',sep='\t')
            vals=df_m1[['LoadConsumed','DailySpecYield1','PR']].values.tolist()[0]
        return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='KH-004'):
        if(os.path.exists(path+'[KH-004X]/'+date[0:4]+'/'+date[0:7]+'/MSB-01 Solar/[KH-004X-M3] '+date+'.txt') and os.path.exists(path+'[KH-004X]/'+date[0:4]+'/'+date[0:7]+'/MSB-02 Solar/[KH-004X-M6] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[KH-004X]/'+date[0:4]+'/'+date[0:7]+'/MSB-01 Solar/[KH-004X-M3] '+date+'.txt',sep='\t')
            df_m2=pd.read_csv(path+'[KH-004X]/'+date[0:4]+'/'+date[0:7]+'/MSB-02 Solar/[KH-004X-M6] '+date+'.txt',sep='\t')
            vals_m1=df_m1[['SolarPowerMeth2','PR2']].values.tolist()[0]
            vals_m2=df_m2[['SolarPowerMeth2','PR2']].values.tolist()[0]
            eac=vals_m1[0]+vals_m2[0]
            yld=float(eac)/sum(capacities)
            pr=(vals_m1[1]*capacities[0]+vals_m2[1]*capacities[1])/(capacities[0]+capacities[1])
            return create_template(date,stn,energy=eac,yld=yld,pr=pr)
    elif(stn=='KH-005'):#Cov
        if(os.path.exists(path+'[KH-005X]/'+date[0:4]+'/'+date[0:7]+'/SCS/[KH-005X-M3] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[KH-005X]/'+date[0:4]+'/'+date[0:7]+'/SCS/[KH-005X-M3] '+date+'.txt',sep='\t')
            vals=df_m1[['SolEac2','Yld','PR2']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='KH-006'):#Cov
        if(os.path.exists(path+'[KH-006X]/'+date[0:4]+'/'+date[0:7]+'/MSB-01-Solar/[KH-006X-M2] '+date+'.txt') and os.path.exists(path+'[KH-006X]/'+date[0:4]+'/'+date[0:7]+'/MSB-02-Solar/[KH-006X-M4] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[KH-006X]/'+date[0:4]+'/'+date[0:7]+'/MSB-01-Solar/[KH-006X-M2] '+date+'.txt',sep='\t')
            df_m2=pd.read_csv(path+'[KH-006X]/'+date[0:4]+'/'+date[0:7]+'/MSB-02-Solar/[KH-006X-M4] '+date+'.txt',sep='\t')
            vals_m1=df_m1[['SolarPowerMeth2','PR2']].values.tolist()[0]
            vals_m2=df_m2[['SolarPowerMeth2','PR2']].values.tolist()[0]
            eac=vals_m1[0]+vals_m2[0]
            yld=float(eac)/sum(capacities)
            pr=(vals_m1[1]*capacities[0]+vals_m2[1]*capacities[1])/(capacities[0]+capacities[1])
            return create_template(date,stn,energy=eac,yld=yld,pr=pr)
    elif(stn=='KH-007'):#Cov
        if(os.path.exists(path+'[KH-007X]/'+date[0:4]+'/'+date[0:7]+'/[KH-007X] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[KH-007X]/'+date[0:4]+'/'+date[0:7]+'/[KH-007X] '+date+'.txt',sep='\t')
            vals=df_m1[['Eac2','Yld2','PR2']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='MY-001'):
        if(os.path.exists(path+'[MY-001X]/'+date[0:4]+'/'+date[0:7]+'/[MY-001X] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[MY-001X]/'+date[0:4]+'/'+date[0:7]+'/[MY-001X] '+date+'.txt',sep='\t')
            vals=df_m1[['Eac2','DailySpecYield2','PR2']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='MY-003'):
        if(os.path.exists(path+'[MY-003X]/'+date[0:4]+'/'+date[0:7]+'/GF-1/[MY-003X-M1] '+date+'.txt') and os.path.exists(path+'[MY-003X]/'+date[0:4]+'/'+date[0:7]+'/GF-2/[MY-003X-M2] '+date+'.txt') and os.path.exists(path+'[MY-003X]/'+date[0:4]+'/'+date[0:7]+'/GF-3/[MY-003X-M3] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[MY-003X]/'+date[0:4]+'/'+date[0:7]+'/GF-1/[MY-003X-M1] '+date+'.txt',sep='\t')
            df_m2=pd.read_csv(path+'[MY-003X]/'+date[0:4]+'/'+date[0:7]+'/GF-1/[MY-003X-M2] '+date+'.txt',sep='\t')
            df_m3=pd.read_csv(path+'[MY-003X]/'+date[0:4]+'/'+date[0:7]+'/GF-3/[MY-003X-M3] '+date+'.txt',sep='\t')
            vals_m1=df_m1[['SolarPowerMeth2','PR2']].values.tolist()[0]
            vals_m2=df_m2[['SolarPowerMeth2','PR2']].values.tolist()[0]
            vals_m3=df_m3[['SolarPowerMeth2','PR2']].values.tolist()[0]
            eac=vals_m1[0]+vals_m2[0]+vals_m3[0]
            yld=float(eac)/sum(capacities)
            pr=(vals_m1[1]*capacities[0]+vals_m2[1]*capacities[1]+vals_m3[1]*capacities[2])/sum(capacities)
            return create_template(date,stn,energy=eac,yld=yld,pr=pr)
    elif(stn=='SG-001'):
        if(os.path.exists(path+'[SG-001X]/'+date[0:4]+'/'+date[0:7]+'/[SG-001X] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[SG-001X]/'+date[0:4]+'/'+date[0:7]+'/[SG-001X] '+date+'.txt',sep='\t')
            vals=df_m1[['Eac2','DSPY2','PR2']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='SG-002'):
        if(os.path.exists(path+'[SG-002X]/'+date[0:4]+'/'+date[0:7]+'/[SG-002X] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[SG-002X]/'+date[0:4]+'/'+date[0:7]+'/[SG-002X] '+date+'.txt',sep='\t')
            vals=df_m1[['Eac2','DailySpecYield2','PR2']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='SG-006'):
        if(os.path.exists(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Admin/[SG-006X-M1] '+date+'.txt') and os.path.exists(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Canteen/[SG-006X-M2] '+date+'.txt') and os.path.exists(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/EM/[SG-006X-M3] '+date+'.txt') and os.path.exists(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Mill/[SG-006X-M4] '+date+'.txt') and os.path.exists(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Warehouse/[SG-006X-M5] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Admin/[SG-006X-M1] '+date+'.txt',sep='\t')
            df_m2=pd.read_csv(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Canteen/[SG-006X-M2] '+date+'.txt',sep='\t')
            df_m3=pd.read_csv(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/EM/[SG-006X-M3] '+date+'.txt',sep='\t')
            df_m4=pd.read_csv(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Mill/[SG-006X-M4] '+date+'.txt',sep='\t')
            df_m5=pd.read_csv(path+'[SG-006X]/'+date[0:4]+'/'+date[0:7]+'/Warehouse/[SG-006X-M5] '+date+'.txt',sep='\t')
            vals_m1=df_m1[['SolarPowerMeth2','PR2','Yield2']].values.tolist()[0]
            vals_m2=df_m2[['SolarPowerMeth2','PR2','Yield2']].values.tolist()[0]
            vals_m3=df_m3[['SolarPowerMeth2','PR2','Yield2']].values.tolist()[0]
            vals_m4=df_m4[['SolarPowerMeth2','PR2','Yield2']].values.tolist()[0]
            vals_m5=df_m5[['SolarPowerMeth2','PR2','Yield2']].values.tolist()[0]
            eac=vals_m1[0]+vals_m2[0]+vals_m3[0]+vals_m4[0]+vals_m5[0]
            yld=float(eac)/sum(capacities)
            ylds=[vals_m1[2],vals_m2[2],vals_m3[2],vals_m4[2],vals_m5[2]]
            pr=(vals_m1[1]*capacities[0]+vals_m2[1]*capacities[1]+vals_m3[1]*capacities[2]+vals_m4[1]*capacities[3]+vals_m5[1]*capacities[4])/sum(capacities)
            if(pr>85):
                pr=85
            cov=round(np.nanstd(ylds)*100/np.nanmean(ylds),1)
            return create_template(date,stn,energy=eac,yld=yld,pr=pr,cov_mfm=cov)
    elif(stn=='TH-002'):
        if(os.path.exists(path+'[TH-002X]/'+date[0:4]+'/'+date[0:7]+'/Invoice/[TH-002X-M2] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[TH-002X]/'+date[0:4]+'/'+date[0:7]+'/Invoice/[TH-002X-M2] '+date+'.txt',sep='\t')
            vals=df_m1[['SolEac2','Yld2','PR2']].values.tolist()[0]
            return create_template(date,stn,energy=vals[0],yld=vals[1],pr=vals[2])
    elif(stn=='TH-003'):
        if(os.path.exists(path+'[TH-003X]/'+date[0:4]+'/'+date[0:7]+'/MDB-01 Invoice/[TH-003X-M2] '+date+'.txt') and os.path.exists(path+'[TH-003X]/'+date[0:4]+'/'+date[0:7]+'/MDB-02 Invoice/[TH-003X-M4] '+date+'.txt')):
            df_m1=pd.read_csv(path+'[TH-003X]/'+date[0:4]+'/'+date[0:7]+'/MDB-01 Invoice/[TH-003X-M2] '+date+'.txt',sep='\t')
            df_m2=pd.read_csv(path+'[TH-003X]/'+date[0:4]+'/'+date[0:7]+'/MDB-02 Invoice/[TH-003X-M4] '+date+'.txt',sep='\t')
            vals_m1=df_m1[['Eac2','PR2']].values.tolist()[0]
            vals_m2=df_m2[['Eac2','PR2']].values.tolist()[0]
            eac=vals_m1[0]+vals_m2[0]
            yld=float(eac)/sum(capacities)
            pr=(vals_m1[1]*capacities[0]+vals_m2[1]*capacities[1])/(capacities[0]+capacities[1])
            return create_template(date,stn,energy=eac,yld=yld,pr=pr)



def locus_webdyn_calc(stn,df,date,capacities,provider):
    cols=df.columns.tolist()
    temp_invcols=[]
    temp_mfmcols=[]
    temp_pr=[]
    temp_ga=[]
    temp_pa=[]
    temp_meterdata=[]
    temp_wms=[]
    temp_da=[]
    if(provider=='Locus' or provider=='Webdyn' or provider=='Avisolar' or provider=='Resync'):
        for i in cols:
            if(i[-4:]=='Yld2' and i[0:3]!='MFM'):
                temp_invcols.append(i)
            elif(i[-4:]=='Yld2' and i[0:3]=='MFM'):
                temp_mfmcols.append(i)
            elif(i[-3:]=='PR2' and i[0:3]=='MFM'):
                temp_pr.append(i)
            elif(i[-4:]=='Eac2' and i[0:3]=='MFM'):
                temp_meterdata.append(i)
            elif(i[-2:]=='GA' and ('inverter' not in i.lower())):
                temp_ga.append(i)
            elif(i[-2:]=='PA' and ('inverter' not in i.lower())):
                temp_pa.append(i)
            elif(('GTI' in i) or ('GHI' in i)):
                temp_wms.append(i)
            elif('DA' in i and 'MFM' in i):
                temp_da.append(i)  
    if(stn=='IN-035'):
        capacities=[3510]
    elif(stn=='IN-065'):
        temp_pr = ['MFM_3.PR2GMod']   
    elif(stn=='IN-009'):
        temp_pr = ['MFM_1.PR22']     
    elif(stn=='IN-303'):
        temp_pr = ['MFM_2_ICR 01 HT MFM.PR2_GTI','MFM_3_ICR 02 HT MFM.PR2_GTI','MFM_4_ICR 03 HT MFM.PR2_GTI'] 
    elif(stn=='IN-006'):
        temp_pr = ['MFM_1_ABS.PR2', 'MFM_2_EMR.PR2', 'MFM_3_ER&D.PR2', 'MFM_4_SS.PR2', 'MFM_5_Unit21.PR2', 'MFM_6_Unit24.PR2']
    elif(stn=='IN-305'):
        temp_meterdata = ['MFM_5_Outgoing MFM.Eac2']
    cols_temp={'INV':temp_invcols,'MFM':temp_mfmcols,'PR':temp_pr,'MD':temp_meterdata,'GA':temp_ga,'PA':temp_pa,'WMS':temp_wms,'DA':temp_da}
    template_df=create_template(date,stn)
    for i in cols_temp:
        df_Yld=df[['Date']+cols_temp[i]].fillna(np.nan).copy()
        df_Subset=df_Yld.loc[df_Yld['Date']==date,].fillna(np.nan)
        vals=[]
        if(df_Subset.empty):
            print('Data not present for the Date!')
            return template_df
        else:
            vals=list(df_Subset.values[0][1:])
            if(i=='INV' or i=='MFM'):
                if((stn=='IN-087' or stn=='IN-080') and i=='INV'):#SMB only
                    vals=vals[1:]
                if(len(vals)==0):
                    template_df.loc[:,'CoV_'+i]=0
                else:
                    cov=round(np.nanstd(vals)*100/np.nanmean(vals),1)
                    template_df.loc[:,'CoV_'+i]=cov
            elif(i=='PR'):
                temp2=0
                temp_cap=[]
                for i in range(len(capacities)):
                    if(stn=='IN-012'):
                        if(vals[i]==0 or vals[i]==np.nan or math.isnan(vals[i])):
                            print(vals)
                            continue   
                    if(vals[i]==np.nan or math.isnan(vals[i])):
                        continue
                    temp2=temp2+capacities[i]*vals[i]
                    temp_cap.append(capacities[i])
                if((stn=='IN-033' and len(temp_cap)==0) or (stn=='IN-057' and len(temp_cap)==0)): 
                    PR = 0
                elif(len(temp_cap)==0):
                    PR='NULL'
                else:
                    PR=temp2/sum(temp_cap)
                print(stn,PR)
                template_df.loc[:,'PR']=PR
            elif(i=='MD'):
                if(stn=='TH-001'):
                    template_df.loc[:,'Energy']=(np.nansum(vals))/2
                else:
                    template_df.loc[:,'Energy']=np.nansum(vals)
                template_df.loc[:,'Yield']=round(np.nansum(vals)/sum(capacities),2)
            elif(i=='WMS'):
                if(len(vals)!=0):
                    template_df.loc[:,'GHI']=vals[0]
            elif(i=='DA'):
                temp2=0
                for i in range(len(capacities)):
                    if(vals[i]==np.nan):
                        continue
                    temp2=temp2+capacities[i]*vals[i]
                DA=temp2/sum(capacities)
                if(DA>100):
                    DA=100
                template_df.loc[:,'DA']=DA
            elif(i=='GA' or i=='PA'):
                if(len(temp_ga)==0 or len(temp_pa)==0):
                    pass
                else:
                    avl=0
                    for j in range(len(capacities)):
                        avl=avl+capacities[j]*vals[j]
                    template_df.loc[:,i]=avl/sum(capacities)
    return template_df


def get_status(data,mfm_limit,inv_limit,pr_limit,irr_stn):
    data=data.fillna('NULL')
    if(data['O&M_Code'][0][0:2]!='IN' and irr_stn!='NULL' and data['PR'][0]!='NULL' and data['PR'][0]>90):
        data['PR']=85
    elif(data['O&M_Code'][0][0:2]!='IN' and irr_stn!='NULL' and  data['PR'][0]!='NULL' and data['PR'][0]<70 and data['PR'][0]>60):
        print('in')
        print(data)
        data['PR']=70
    if(data['PR'][0]=='NULL'):
        data['PR_Alarm']='NULL'
    elif(data['PR'][0]>95 and data['PR'][0]!='NULL'):
        data['PR_Alarm']=2
    elif(data['PR'][0]<pr_limit and data['PR'][0]!='NULL'):
        data['PR_Alarm']=1
    else:
        data['PR_Alarm']=0
    if(data['CoV_MFM'][0]=='NULL'):
        data['MFM_CoV_Alarm']='NULL'
    elif(((data['CoV_MFM'][0])>mfm_limit and data['CoV_MFM'][0]!='NULL')):
        data['MFM_CoV_Alarm']=1
    else:
        data['MFM_CoV_Alarm']=0
    if(data['CoV_INV'][0]=='NULL'):
        data['INV_CoV_Alarm']='NULL'
    elif((data['CoV_INV'][0]>inv_limit and data['CoV_INV'][0]!='NULL')):
        data['INV_CoV_Alarm']=1
    else:
        data['INV_CoV_Alarm']=0
    if(data['PR_Alarm'][0]=='NULL'):
        data['Status']='NULL'
    elif(data['PR_Alarm'][0]==0 and (data['INV_CoV_Alarm'][0]==1 or data['MFM_CoV_Alarm'][0]==1)):
        data['Status']=1
    elif(data['PR_Alarm'][0]==2):
        data['Status']=2
    elif(data['PR_Alarm'][0]==1):
        data['Status']=3
    elif(data['PR_Alarm'][0]==0):
        data['Status']=0
    data=data.replace(np.inf, np.nan)
    return data


    

def azure_push(data):
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = connStr.cursor()
    data=data.fillna('NULL')
    data=data.replace(to_replace ="NULL", value =np.nan) 
    data = data.replace({np.nan: None})
    for index,row in data.iterrows():
        try:
            with cursor.execute("INSERT INTO dbo.Station_Alerts([Date],[O&M_Code],[Energy],[Yield],[GHI],[PR],[DA],[GA],[PA],[CoV_MFM],[CoV_INV],[PR_Alarm],[MFM_CoV_Alarm],[INV_CoV_Alarm],[Status]) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['O&M_Code'], row['Energy'], row['Yield'] ,row['GHI'],row['PR'],row['DA'],row['GA'],row['PA'], row['CoV_MFM'],row['CoV_INV'],row['PR_Alarm'],row['MFM_CoV_Alarm'],row['INV_CoV_Alarm'],row['Status']):
                pass
            connStr.commit()
        except Exception as e:
            pass
    cursor.close()
    connStr.close()

def teams_alert(data,webhook):
    if((data['Status'][0]==3 or data['Status'][0]==2 or data['Status'][0]==1) and webhook!='NA'):
        data=data.fillna('NA')
        myTeamsMessage = pymsteams.connectorcard(webhook)
        #myTeamsMessage = pymsteams.connectorcard("https://outlook.office.com/webhook/2d5e48a9-107c-41d1-be0b-d482a5f12de0@8ec327b2-e844-412c-8463-3e633202b00f/IncomingWebhook/486b64fd0d59476ba0056e7d883d79d6/b91f3ec7-71fb-437f-8eab-f5905ff0c963")
        if(data['Status'][0]==3 or data['Status'][0]==2):
            myTeamsMessage.title(data['O&M_Code'][0]+" PR ALERT")
        elif(data['Status'][0]==1):
            myTeamsMessage.title(data['O&M_Code'][0]+" CoV ALERT")
        myTeamsMessage.text("<pre>Date: "+str(data['Date'][0])+"<br>PR: "+str(round(data['PR'][0],1))+"<br>Energy: "+str(round(data['Energy'][0],1))+"<br>MFM CoV: "+str(data['CoV_MFM'][0])+"<br>INV CoV: "+str(data['CoV_INV'][0])+"</pre>")
        myTeamsMessage.send()
        time.sleep(5)
    else:
        return 



while(1):
    if(datetime.datetime.now(tz).hour==1):
        connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
        limits = pd.read_sql_query('''  SELECT  * FROM [dbo].[Station_Limits] ''', connStr)
        df_limits = pd.DataFrame(limits, columns=['O&M_Code','MFM_limit','INV_limit','PR_limit','Webhook'])
        stations = pd.read_sql_query('''  SELECT  * FROM [dbo].[Stations] ''', connStr)
        df_stations = pd.DataFrame(stations, columns=['Station_Name','Station_Id','Provider','Station_Irradiation_Center'])
        meters = pd.read_sql_query('''  SELECT  * FROM [dbo].[Meters] ''', connStr)
        df_meters = pd.DataFrame(meters, columns=['Meter_id','Station_Id','Capacity'])
        df_stations.columns=['O&M_Code','Station_Id','Provider','Station_Irradiation_Center']
        connStr.close()

        df_merge=pd.merge(df_limits,df_stations,on='O&M_Code',how="left")
        df_merge=pd.merge(df_merge,df_meters,on='Station_Id',how="left")

        df_merge['O&M_Code']=df_merge['O&M_Code'].str.strip()
        df_merge['Provider']=df_merge['Provider'].str.strip()
        df_merge['Webhook']=df_merge['Webhook'].str.strip()
        df_merge['Station_Irradiation_Center']=df_merge['Station_Irradiation_Center'].str.strip()
        df_merge['Station_Irradiation_Center']=df_merge['Station_Irradiation_Center'].fillna('NULL')

        df_locus=df_merge.loc[df_merge['Provider']=='Locus',['O&M_Code','MFM_limit','INV_limit','PR_limit','Webhook','Capacity','Station_Irradiation_Center']].groupby('O&M_Code')
        df_webdyn=df_merge.loc[df_merge['Provider']=='Webdyn',['O&M_Code','MFM_limit','INV_limit','PR_limit','Webhook','Capacity','Station_Irradiation_Center']].groupby('O&M_Code')
        df_seris=df_merge.loc[df_merge['Provider']=='Seris',['O&M_Code','MFM_limit','INV_limit','PR_limit','Webhook','Capacity','Station_Irradiation_Center']].groupby('O&M_Code')
        df_ebx=df_merge.loc[df_merge['Provider']=='Ebx',['O&M_Code','MFM_limit','INV_limit','PR_limit','Webhook','Capacity','Station_Irradiation_Center']].groupby('O&M_Code')
        df_avisolar=df_merge.loc[df_merge['Provider']=='Avisolar',['O&M_Code','MFM_limit','INV_limit','PR_limit','Webhook','Capacity','Station_Irradiation_Center']].groupby('O&M_Code')
        df_resync=df_merge.loc[df_merge['Provider']=='Resync',['O&M_Code','MFM_limit','INV_limit','PR_limit','Webhook','Capacity','Station_Irradiation_Center']].groupby('O&M_Code')

        df_list=[df_seris,df_locus,df_ebx,df_webdyn,df_avisolar,df_resync]
        stns=[]
        capacities=[]
        mfm_limit=[]
        inv_limit=[]
        pr_limit=[]
        webhook=[]
        irr_stn=[]
        for n in df_list:
            temp_code=[]
            temp_capacity=[]
            temp_mfm_limit=[]
            temp_inv_limit=[]
            temp_pr_limit=[]
            temp_webhook=[]
            temp_irr_stn=[]
            for key, item in n:

                temp_code.append(key)
                temp_capacity.append(n.get_group(key)['Capacity'].tolist())
                temp_mfm_limit.append(n.get_group(key)['MFM_limit'].values[0])
                temp_inv_limit.append(n.get_group(key)['INV_limit'].values[0])
                temp_pr_limit.append(n.get_group(key)['PR_limit'].values[0])
                temp_webhook.append(n.get_group(key)['Webhook'].values[0])
                temp_irr_stn.append(n.get_group(key)['Station_Irradiation_Center'].values[0])
            stns.append(temp_code)
            capacities.append(temp_capacity)
            mfm_limit.append(temp_mfm_limit)
            inv_limit.append(temp_inv_limit)
            pr_limit.append(temp_pr_limit)
            webhook.append(temp_webhook)
            irr_stn.append(temp_irr_stn)

        stns[0].append('IN-036')
        capacities[0].append([2255.5,2255.5])
        mfm_limit[0].append(2)
        inv_limit[0].append(2)
        pr_limit[0].append(77)
        webhook[0].append('https://outlook.office.com/webhook/2d5e48a9-107c-41d1-be0b-d482a5f12de0@8ec327b2-e844-412c-8463-3e633202b00f/IncomingWebhook/844aa0ea981947cd978888508b376509/b91f3ec7-71fb-437f-8eab-f5905ff0c963')                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
        irr_stn[0].append('NULL')

        date=str(datetime.datetime.now(tz).date()+datetime.timedelta(days=-1))
        for provider_index,n in enumerate((stns)):
            for index,i in enumerate(n):
                try:
                    print(i)
                    if(provider_index==0):
                        if(os.path.exists(path+'['+i+'S]/'+date[0:4]+'/'+date[0:7]+'/['+i+'S] '+date+'.txt')):
                            df=pd.read_csv(path+'['+i+'S]/'+date[0:4]+'/'+date[0:7]+'/['+i+'S] '+date+'.txt',sep='\t')
                            data=seris_calc(i,df,date)
                            data=get_status(data,mfm_limit[provider_index][index],inv_limit[provider_index][index],pr_limit[provider_index][index],irr_stn[provider_index][index])
                            if(data['Energy'][0]!='NULL' and (data['Energy'][0]<0 or data['Energy'][0]>10000000)):
                                data=create_template(date,n)
                            azure_push(data)
                            teams_alert(data,webhook[provider_index][index])
                        else:
                            print('Not there')
                            data=create_template(date,i)
                            azure_push(data)
                    elif(provider_index==1):
                        if(os.path.exists('/home/admin/Dropbox/Fourth_Gen/['+i+'L]/['+i+'L]-lifetime.txt')):
                            df=pd.read_csv('/home/admin/Dropbox/Fourth_Gen/['+i+'L]/['+i+'L]-lifetime.txt',sep='\t')
                            data=locus_webdyn_calc(i,df,date,capacities[provider_index][index],'Locus')
                            data=get_status(data,mfm_limit[provider_index][index],inv_limit[provider_index][index],pr_limit[provider_index][index],irr_stn[provider_index][index])
                            azure_push(data)
                            teams_alert(data,webhook[provider_index][index])
                    elif(provider_index==4):
                        if(os.path.exists('/home/admin/Dropbox/Fourth_Gen/['+i+'A]/['+i+'A]-lifetime.txt')):
                            df=pd.read_csv('/home/admin/Dropbox/Fourth_Gen/['+i+'A]/['+i+'A]-lifetime.txt',sep='\t')
                            data=locus_webdyn_calc(i,df,date,capacities[provider_index][index],'Avisolar')
                            data=get_status(data,mfm_limit[provider_index][index],inv_limit[provider_index][index],pr_limit[provider_index][index],irr_stn[provider_index][index])
                            azure_push(data)
                            teams_alert(data,webhook[provider_index][index])
                    elif(provider_index==5):
                        if(os.path.exists('/home/admin/Dropbox/Fourth_Gen/['+i+'R]/['+i+'R]-lifetime.txt')):
                            df=pd.read_csv('/home/admin/Dropbox/Fourth_Gen/['+i+'R]/['+i+'R]-lifetime.txt',sep='\t')
                            data=locus_webdyn_calc(i,df,date,capacities[provider_index][index],'Avisolar')
                            data=get_status(data,mfm_limit[provider_index][index],inv_limit[provider_index][index],pr_limit[provider_index][index],irr_stn[provider_index][index])
                            azure_push(data)
                            teams_alert(data,webhook[provider_index][index])
                    elif(provider_index==2):
                        data=ebx_calc(i,date,capacities[provider_index][index])
                        data=get_status(data,mfm_limit[provider_index][index],inv_limit[provider_index][index],pr_limit[provider_index][index],irr_stn[provider_index][index])
                        azure_push(data)
                        teams_alert(data,webhook[provider_index][index])
                    else:
                        if(os.path.exists('/home/admin/Dropbox/Fourth_Gen/['+i+'W]/['+i+'W]-lifetime.txt')):
                            df=pd.read_csv('/home/admin/Dropbox/Fourth_Gen/['+i+'W]/['+i+'W]-lifetime.txt',sep='\t')
                            data=locus_webdyn_calc(i,df,date,capacities[provider_index][index],'Webdyn')
                            
                            data=get_status(data,mfm_limit[provider_index][index],inv_limit[provider_index][index],pr_limit[provider_index][index],irr_stn[provider_index][index])
                            azure_push(data)
                            teams_alert(data,webhook[provider_index][index])
                except Exception as e:
                    print(e)
                    logging.exception("message")
    time.sleep(3600)
