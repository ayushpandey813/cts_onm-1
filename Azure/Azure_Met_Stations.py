import requests, json
import requests.auth
import pandas as pd
import datetime 
import os
import re 
import time
import pyodbc
import pytz
import sys
import numpy as np

sitecodes = ['KH-714','IN-711','IN-713','IN-721','MY-720','SG-724','VN-722']
bot_type = sys.argv[1]

tz = pytz.timezone('Asia/Calcutta')
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+'; PORT=1433; SERVER='+server+'; PORT=1443; DATABASE='+database+'; UID='+username+'; PWD='+password)


#Fast Upsert Function
def fast_upsert_processed(df):
    final_query='DECLARE @Date datetime, @Value float, @ParameterId int, @ComponentId int, @SiteId int\nSET @Date=?\nSET @Value=?\nSET @ParameterId=?\nSET @ComponentId=?\nSET @SiteId=?\nUPDATE [Portfolio].[ProcessedData] SET [Date]=@Date, [Value]=@Value, [ParameterId]=@ParameterId, [ComponentId]=@ComponentId, [SiteId]=@SiteId WHERE [Date]=@Date AND [SiteId]=@SiteId AND [ParameterId]=@ParameterId\nif @@ROWCOUNT = 0\nINSERT INTO [Portfolio].[ProcessedData] ([Date], [Value], [ParameterId], [ComponentId], [SiteId]) VALUES(@Date, @Value, @ParameterId, @ComponentId, @SiteId)'
    cursor = connStr.cursor()
    cursor.fast_executemany = True
    cursor.executemany(final_query, df.values.tolist())
    connStr.commit()


def addsite(sitecode):
  #Adding site to [Portfolio].[Site]
  siteid = pd.read_sql_query("SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='" + sitecode + "'", connStr)
  if siteid.empty == True:
    print("Site not present in [Portfolio].[Site], adding site!!")
    cursor = connStr.cursor()
    cursor.execute("INSERT INTO [Portfolio].[Site] ([SiteCode], [MonitoringProvider]) VALUES('" + sitecode + "', 'Seris')")
    connStr.commit()
  else:
    print("Site Already Present in [Portfolio].[Site]")
    
  #Adding component to [Portfolio].[Component]
  siteid = pd.read_sql_query("SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='" + sitecode + "'", connStr)
  siteid = siteid.values[0][0]
  
  ComponentId = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[Component] WHERE [SiteId]=" + str(siteid), connStr)
  if ComponentId.empty == True:
    print("Component not present in [Portfolio].[Component], adding component!!")
    cursor = connStr.cursor()
    cursor.execute("INSERT INTO [Portfolio].[Component] ([SiteId], [ComponentName], [ComponentType]) VALUES(" + str(siteid) + ", 'WMS', 'WMSIRR')")
    connStr.commit()
  else:
    print("Component Already Present in [Portfolio].[Component]")
    
  #Adding data to [Portfolio].[CleantechMetadata]
  ComponentId = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[Component] WHERE [SiteId]=" + str(siteid), connStr)
  ComponentId = ComponentId.values[0][0]
  
  cursor = connStr.cursor()
  for parameter in ['DA', 'GHI', 'Avg Tamb', 'Avg Hamb']:
    parameterid = pd.read_sql_query("SELECT [ParameterId] FROM [Portfolio].[Parameter] where CSName='" + parameter + "' and ParameterType= 'Processed'", connStr)
    parameterid = parameterid.values[0][0]
    cursor.execute('UPDATE [Portfolio].[CleantechMetadata] SET [SiteId]=' + str(siteid) + ', [ComponentId]=' + str(ComponentId) + ', [ParameterId]=' + str(parameterid) + ' WHERE [SiteId]=' + str(siteid) + ' AND [ParameterId]=' + str(parameterid) + ' if @@ROWCOUNT=0 INSERT INTO [Portfolio].[CleantechMetadata] VALUES (' + str(siteid) + ', ' + str(ComponentId) + ', ' + str(parameterid) + ')')
    connStr.commit()
  
  print("$$Site Added Successfully$$")


def historical(sitecode):
  print("Processing Historical for " + str(sitecode))
  mapping = {'DA':'DA', 'Gsi':'GHI', 'Tamb':'Avg Tamb', 'Hamb':'Avg Hamb'}
  years = sorted(os.listdir('/home/admin/Dropbox/Second Gen/[' + sitecode + 'S]/'))
  
  siteid = pd.read_sql_query("SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='" + sitecode + "'", connStr).values[0][0]
  ComponentId = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[Component] WHERE [SiteId]=" + str(siteid), connStr).values[0][0]
  
  for year in years:
    months = sorted(os.listdir('/home/admin/Dropbox/Second Gen/[' + sitecode + 'S]/' + year + '/'))
    for month in months:
      days = sorted(os.listdir('/home/admin/Dropbox/Second Gen/[' + sitecode + 'S]/' + year + '/' + month + '/'))
      for path in days:
        read = '/home/admin/Dropbox/Second Gen/[' + sitecode + 'S]/' + year + '/' + month + '/' + path
        if len(path)>20:
          print(read)
          if sitecode == 'KH-714':
            df = pd.read_csv(read, sep='\t')
            if 'DA' in df.columns:
              df = pd.read_csv(read, sep='\t', usecols = ['Date', 'DA', 'Gsi', 'Tamb', 'Hamb'])[['Date', 'DA', 'Gsi', 'Tamb', 'Hamb']]
            else:
              continue
          else:
            df = pd.read_csv(read, sep='\t')
            if 'DA' in df.columns:
              df = pd.read_csv(read, sep='\t', usecols = ['Date', 'DA', 'Gsi01', 'Tamb', 'Hamb'])[['Date', 'DA', 'Gsi01', 'Tamb', 'Hamb']]
            else:
              continue
            
          df.columns = ['Date', 'DA', 'Gsi', 'Tamb', 'Hamb']
          for cols in df.columns[1:]:
            export = pd.DataFrame()
            export['Date'] = df['Date']
            export['Value'] = df[cols]
            export['ParameterId'] = pd.read_sql_query("SELECT [ParameterId] FROM [Portfolio].[Parameter] where CSName='"+mapping[cols]+"' and ParameterType='Processed'", connStr).values[0][0]
            export['ComponentId'] = ComponentId
            export['SiteId'] = siteid
            fast_upsert_processed(export)
        
  print("Historical Processed!!")
  

if bot_type == "H":
  for sitecode in sitecodes: 
    connStr = pyodbc.connect('DRIVER='+driver+'; PORT=1433; SERVER='+server+'; PORT=1443; DATABASE='+database+'; UID='+username+'; PWD='+password)
    addsite(sitecode)
    historical(sitecode)

elif bot_type == "L":
  print("Going Live...")
  while(1):
    timenow = datetime.datetime.now(tz)
    if timenow.hour == 10:
      for sitecode in sitecodes:
        date_yesterday = (timenow + datetime.timedelta(days = -1)).strftime('%Y-%m-%d')
        print("Processing", sitecode)
        mapping = {'DA':'DA', 'Gsi':'GHI', 'Tamb':'Avg Tamb', 'Hamb':'Avg Hamb'}
        connStr = pyodbc.connect('DRIVER='+driver+'; PORT=1433; SERVER='+server+'; PORT=1443; DATABASE='+database+'; UID='+username+'; PWD='+password)
        siteid = pd.read_sql_query("SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='" + sitecode + "'", connStr).values[0][0]
        ComponentId = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[Component] WHERE [SiteId]=" + str(siteid), connStr).values[0][0]
      
        read = '/home/admin/Dropbox/Second Gen/[' + sitecode + 'S]/' + date_yesterday[:4] + '/' + date_yesterday[:7] + '/[' + sitecode + 'S] ' + date_yesterday + '.txt'
        
        if sitecode == 'KH-714':
          df = pd.read_csv(read, sep='\t', usecols = ['Date', 'DA', 'Gsi', 'Tamb', 'Hamb'])[['Date', 'DA', 'Gsi', 'Tamb', 'Hamb']]
        else:
          df = pd.read_csv(read, sep='\t', usecols = ['Date', 'DA', 'Gsi01', 'Tamb', 'Hamb'])[['Date', 'DA', 'Gsi01', 'Tamb', 'Hamb']]
        
        df.columns = ['Date', 'DA', 'Gsi', 'Tamb', 'Hamb']
        for cols in df.columns[1:]:
          export = pd.DataFrame()
          export['Date'] = df['Date']
          export['Value'] = df[cols]
          export['ParameterId'] = pd.read_sql_query("SELECT [ParameterId] FROM [Portfolio].[Parameter] where CSName='"+mapping[cols]+"' and ParameterType='Processed'", connStr).values[0][0]
          export['ComponentId'] = ComponentId
          export['SiteId'] = siteid
          fast_upsert_processed(export)
      print("Sleeping for an hour...")
      time.sleep(3600)
    else:
      print("Sleeping for five minutes...")
      time.sleep(300)      
  