import requests, json
import requests.auth
import pandas as pd
import datetime 
import os
import re 
import time
import pyodbc
import pytz
import sys
import numpy as np

stn=['IN-005','IN-007','IN-012','IN-013','IN-052']
#'IN-005','IN-007','IN-012','IN-013','IN-052'

path='/home/admin/Dropbox/Gen 1 Data/'
tz = pytz.timezone('Asia/Kuala_Lumpur')
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

def calc_ga_pa_da_sh(mfm_df_sh,date,stn_id,meter_id,country_id,freq_column,power_column,ts):
    freq_timestamps=mfm_df_sh.loc[mfm_df_sh[freq_column]>40,ts]
    power_timestamps=mfm_df_sh.loc[mfm_df_sh[power_column]>2,ts]
    pa_common=list(set(freq_timestamps) & set(power_timestamps))
    GA=(float(len(freq_timestamps))/float(len(mfm_df_sh[ts])))*100
    PA=(float(len(pa_common))/float(len(freq_timestamps)))*100
    DA=(float(len(mfm_df_sh[mfm_df_sh[power_column].notnull()]))/120)*100
    SA=round((GA*PA)/100,2)
    if(DA>100):
        DA=100
    azure_push(date,GA,PA,DA,SA,stn_id,meter_id,country_id)

def calc_ga_pa_da(date, stn, stn_suffix, irr_stn, irr_suffix, mfm_name, wms_name, stn_id, meter_id, country_id):
    #Determing MFM file name
    if len(mfm_name.split('_'))>1:
        mfm_no = mfm_name.split('_')[1]
    else:
        mfm_no = ''
    
    #Determing WMS file name
    if(wms_name == ''):
        wms_no = '1'
    else:
        if len(wms_name.split('_'))>1:
            wms_no = wms_name.split('_')[1]
        else:
            wms_no = ''
    
    mfm_file=path+'['+stn+stn_suffix+']/'+date[0:4]+'/'+date[0:7]+'/'+mfm_name+'/['+stn+stn_suffix+']-MFM'+mfm_no+'-'+date+'.txt'
    
    if(irr_stn=='NULL'):
        wms_file=path+'['+stn+stn_suffix+']/'+date[0:4]+'/'+date[0:7]+'/'+wms_name+'/['+stn+stn_suffix+']-WMS'+wms_no+'-'+date+'.txt'
    else:
        wms_file=path+'['+irr_stn+irr_suffix+']/'+date[0:4]+'/'+date[0:7]+'/'+wms_name+'/['+irr_stn+irr_suffix+']-WMS'+wms_no+'-'+date+'.txt'
    
    if(os.path.exists(mfm_file)):
        mfm_df=pd.read_csv(mfm_file,sep='\t')
        if len(mfm_df)>300:
            mfm_df = mfm_df[::5]
        if stn in ['IN-005', 'IN-007', 'IN-013']:            #####Check MFM Column Name here######
            freq_column = 'Frequency'
            power_column = 'Total Power AC'
        else:
            freq_column = 'Hz_avg'
            power_column = 'W_avg'
        ts=list(mfm_df.columns)[0]
        mfm_df=mfm_df[[ts, freq_column,power_column]]
        mfm_df[ts]=pd.to_datetime(mfm_df[ts])
        mfm_df_sh=mfm_df[(mfm_df[ts].dt.hour > 6) & (mfm_df[ts].dt.hour < 18)]
        freq_timestamps=mfm_df.loc[mfm_df[freq_column]>40,ts]
        freq_timestamps_sh=mfm_df_sh.loc[mfm_df_sh[freq_column]>40,ts]
        if(len(freq_timestamps)>0 and len(freq_timestamps_sh)>0):
            if(os.path.exists(wms_file)):
                wms_df=pd.read_csv(wms_file,sep='\t')
                wms_df[ts]=pd.to_datetime(wms_df[ts])
                if len(wms_df)>300:
                    wms_df = wms_df[::5]
                ts = list(wms_df.columns)[0]
                if stn == 'IN-005':                          #####Check WMS Column Name here######
                    irr_column = 'Irradiation	Record_Id_2'
                else:
                    irr_column = 'POAI_avg'
                if(len(wms_df)>0 and irr_column in list(wms_df.columns)):
                    irr_df=wms_df[[ts,irr_column]]
                    irr_df=irr_df[irr_df[irr_column].notnull()]
                    irr_timestamps=irr_df.loc[irr_df[irr_column]>20,ts]
                    freq_timestamps=mfm_df.loc[mfm_df[freq_column]>40,ts]
                    power_timestamps=mfm_df.loc[mfm_df[power_column]>2,ts]
                    ga_common=list(set(freq_timestamps) & set(irr_timestamps))
                    pa_common=list(set(freq_timestamps) & set(irr_timestamps)  & set(power_timestamps))
                    if((len(irr_timestamps)>0) and (len(ga_common)>0)):
                        GA=(float(len(ga_common))/float(len(irr_timestamps)))*100
                        PA=(float(len(pa_common))/float(len(ga_common)))*100
                        DA=(float(len(mfm_df[mfm_df['W_avg'].notnull()]))/288)*100
                        SA=round((GA*PA)/100,2)
                        if(DA>100):
                            DA=100
                        azure_push(date,GA,PA,DA,SA,stn_id,meter_id,country_id)
                    else:
                        calc_ga_pa_da_sh(mfm_df_sh,date,stn_id,meter_id,country_id,freq_column,power_column,ts)
                else:
                    calc_ga_pa_da_sh(mfm_df_sh,date,stn_id,meter_id,country_id,freq_column,power_column,ts)
            else:
                calc_ga_pa_da_sh(mfm_df_sh,date,stn_id,meter_id,country_id,freq_column,power_column,ts)
        else: 
            azure_push(date,"NULL","NULL",0,"NULL",stn_id,meter_id,country_id)
    else: 
        print('no file')
        azure_push(date,"NULL","NULL",0,"NULL",stn_id,meter_id,country_id)

def azure_push(date,GA,PA,DA,SA,stn_id,meter_id,country_id):
    print(date,GA,PA,DA,SA,stn_id,meter_id,country_id)
    '''cursor = connStr.cursor()
    with cursor.execute("UPDATE [dbo].[System_Uptime] SET [GA] = {},[PA] = {},[DA] = {},[SA] = {} where [Date] = '{}' AND [Meter_Id]={} if @@ROWCOUNT = 0 INSERT into [dbo].[System_Uptime](Date, GA, PA, DA, SA, Station_Id, Meter_Id, Country_Id, [Master-Flag]) values('{}', {}, {}, {}, {}, {}, {}, {}, {}) ".format(str(GA),str(PA),str(DA),str(SA), str(date),str(meter_id), str(date) ,str(GA) ,str(PA) ,str(DA) ,str(SA) ,str(stn_id) ,str(meter_id) ,str(country_id) ,str(0))):
        pass
    connStr.commit()'''
        
def find_first_date(stn, mfm):
    for year in sorted(os.listdir(path+'['+stn+']/')):
        for yearmonth in sorted(os.listdir(path+'['+stn+']/' + year)):
            for files in sorted(os.listdir(path+'['+stn+']/' + year + '/' + yearmonth + '/' + mfm)):
                date = files[-14:-4]
                break
            break
        break
    return date

#Hisorical
connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
df_stations = pd.read_sql_query('''SELECT [Station_Id],[Station_Name],[Station_Columns],[Station_Irradiation_Center],[Station_No_Meters],[Provider] FROM [dbo].[Stations]''', connStr)
df_meters = pd.read_sql_query('''SELECT [Meter_id],[Station_Id],[Country_Id] FROM [dbo].[Meters]''', connStr)

date_today = datetime.datetime.now(tz).date()

for current_station in stn:
    print(current_station)
    df_current_station = df_stations.loc[df_stations['Station_Name'].str.strip()==current_station]
    current_no_meters = df_current_station['Station_No_Meters'].values[0]
    current_columns = df_current_station['Station_Columns'].values[0].strip()
    current_irr_stn = df_current_station['Station_Irradiation_Center'].fillna('NULL').values[0].strip()
    current_stn_id = df_current_station['Station_Id'].values[0]
    current_meter_ids = df_meters.loc[(df_meters['Station_Id'] == current_stn_id), 'Meter_id'].tolist()
    current_country_id = df_meters.loc[(df_meters['Station_Id'] == current_stn_id), 'Country_Id'].tolist()
    current_provider = df_current_station['Provider'].values[0]
    provider_suffix = current_provider[0]
    
    if(current_irr_stn!='NULL'):
        if current_station in ['IN-007']:            #####Check Irradiance Provider Suffix here######
            irr_provider_suffix = ''
        else:
            df = pd.read_sql_query('''SELECT [Provider] FROM [dbo].[Stations] where [Station_Name]=''' + "'" + current_irr_stn + "'", connStr)
            irr_provider_suffix = df['Provider'].values[0]
    else:
        irr_provider_suffix = ''
    
    lifetime = pd.read_sql_query('SELECT TOP (1) [Station_Id] FROM [dbo].[System_Uptime] Where Station_Id=' + str(current_stn_id), connStr)
    if(lifetime.empty):
        components=[]
        for index, j in enumerate(current_columns.strip().split(',')):
            if(current_station=='IN-070'):                    #####HardCode MFM Names here######
                components=['','MFM_3_ESS-02 Main_MFM', 'MFM_1_ESS-03 Main_MFM', 'MFM_7_ESS-10 Main_MFM']
            elif(current_station=='MY-010'):
                components=['','MFM_5_LG1-1 MFM', 'MFM_6_LG1-2 MFM', 'MFM_7_L3B MFM', 'MFM_8_L3A MFM', 'MFM_9_LG1B MFM', 'MFM_11_LG1A MFM']
            elif(current_station=='IN-037'):
                components=['WMS_1_WMS','MFM_1_Plant Meter-Tooling Shed Block', 'MFM_2_Plant Meter-Corporate Office']
            elif(current_station=='TH-024'):
                components=['WMS_1_Pyranometer','MFM_1_PV MFM-1', 'MFM_2_PV MFM-2']
            else:
                if(index==1):
                    temp=(j.replace('"','').strip().split('.')[:-1])
                    components.append(' '.join(temp))
                if(index>1 and index<(2+current_no_meters)):     #Skip date and include meters only
                    temp=(j.replace('"','').strip().split('.')[:-1])
                    components.append(' '.join(temp))
        wms_name=components[0]
        mfm_name=components[1:]
        print(components)
        
        for index, mfm in enumerate(mfm_name):
            if current_station == 'IN-007':                  #####HardCode Firstdate here######
                start_date = datetime.datetime.strptime('2020-10-15', "%Y-%m-%d").date()
            elif current_station == 'IN-013':
                start_date = datetime.datetime.strptime('2020-10-01', "%Y-%m-%d").date()
            else:
                start_date = datetime.datetime.strptime(find_first_date(current_station+provider_suffix, mfm), "%Y-%m-%d").date()
            print(start_date)
            while(start_date<date_today):
                calc_ga_pa_da(str(start_date),current_station,provider_suffix,current_irr_stn,irr_provider_suffix,mfm,wms_name,current_stn_id,current_meter_ids[index],current_country_id[index])
                start_date = start_date + datetime.timedelta(days=1)
                
#Live
while(1):
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    df_stations = pd.read_sql_query('''SELECT [Station_Id],[Station_Name],[Station_Columns],[Station_Irradiation_Center],[Station_No_Meters],[Provider] FROM [dbo].[Stations]''', connStr)
    df_meters = pd.read_sql_query('''SELECT [Meter_id],[Station_Id],[Country_Id] FROM [dbo].[Meters]''', connStr)
    for i in range(7,-1,-1):
        date=(datetime.datetime.now(tz)+datetime.timedelta(days=-i)).strftime('%Y-%m-%d')
        print(date)
        for current_station in stn:
            print(current_station)
            df_current_station = df_stations.loc[df_stations['Station_Name'].str.strip()==current_station]
            current_no_meters = df_current_station['Station_No_Meters'].values[0]
            current_columns = df_current_station['Station_Columns'].values[0].strip()
            current_irr_stn = df_current_station['Station_Irradiation_Center'].fillna('NULL').values[0].strip()
            current_stn_id = df_current_station['Station_Id'].values[0]
            current_meter_ids = df_meters.loc[(df_meters['Station_Id'] == current_stn_id), 'Meter_id'].tolist()
            current_country_id = df_meters.loc[(df_meters['Station_Id'] == current_stn_id), 'Country_Id'].tolist()
            current_provider = df_current_station['Provider'].values[0]
            provider_suffix = current_provider[0]
            
            if(current_irr_stn!='NULL'):
                if current_station in ['IN-007']:            #####Check Irradiance Provider Suffix here######
                    irr_provider_suffix = ''
                else:
                    df = pd.read_sql_query('''SELECT [Provider] FROM [dbo].[Stations] where [Station_Name]=''' + "'" + current_irr_stn + "'", connStr)
                    irr_provider_suffix = df['Provider'].values[0]
            else:
                irr_provider_suffix = ''
            
            components=[]
            for index, j in enumerate(current_columns.strip().split(',')):
                if(current_station=='IN-070'):                    #####HardCode MFM Names here######
                    components=['','MFM_3_ESS-02 Main_MFM', 'MFM_1_ESS-03 Main_MFM', 'MFM_7_ESS-10 Main_MFM']
                elif(current_station=='MY-010'):
                    components=['','MFM_5_LG1-1 MFM', 'MFM_6_LG1-2 MFM', 'MFM_7_L3B MFM', 'MFM_8_L3A MFM', 'MFM_9_LG1B MFM', 'MFM_11_LG1A MFM']
                elif(current_station=='IN-037'):
                    components=['WMS_1_WMS','MFM_1_Plant Meter-Tooling Shed Block', 'MFM_2_Plant Meter-Corporate Office']
                elif(current_station=='TH-024'):
                    components=['WMS_1_Pyranometer','MFM_1_PV MFM-1', 'MFM_2_PV MFM-2']
                else:
                    if(index==1):
                        temp=(j.replace('"','').strip().split('.')[:-1])
                        components.append(' '.join(temp))
                    if(index>1 and index<(2+current_no_meters)):     #Skip date and include meters only
                        temp=(j.replace('"','').strip().split('.')[:-1])
                        components.append(' '.join(temp))
            wms_name=components[0]
            mfm_name=components[1:]
            print(components)
        
            for index,mfm in enumerate(mfm_name):
                calc_ga_pa_da(str(start_date),current_station,provider_suffix,current_irr_stn,irr_provider_suffix,mfm,wms_name,current_stn_id,current_meter_ids[index],current_country_id[index])
    connStr.close()
    time.sleep(3600)