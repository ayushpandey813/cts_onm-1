import os
import pandas as pd
import pyodbc
import datetime

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+'; PORT=1433; SERVER='+server+'; PORT=1443; DATABASE='+database+'; UID='+username+'; PWD='+password)
folder = '/home/admin/Dropbox/GIS_API3/'
sites = sorted(os.listdir(folder))

#Fast Upsert Function
def fast_upsert_processed(df):
    final_query='DECLARE @Date datetime, @GISId int, @value float\nSET @Date=?\nSET @GISId=?\nSET @value=?\nUPDATE [Portfolio].[GISData] SET [Date]=@Date, [GISId]=@GISId, [Value]=@value WHERE [Date]=@Date AND [GISId]=@GISId\nif @@ROWCOUNT = 0\nINSERT INTO [Portfolio].[GISData]([Date], [GISId], [Value]) VALUES(@Date, @GISId, @value)'
    cursor = connStr.cursor()
    cursor.fast_executemany = True
    cursor.executemany(final_query, df.values.tolist())
    connStr.commit()

def datefixer(x):
  try:
    datetime.datetime.strptime(x, "%d-%m-%Y")
    return(datetime.datetime.strptime(x, "%d-%m-%Y").strftime("%Y-%m-%d"))
  except:
    datetime.datetime.strptime(x, "%Y-%m-%d")
    return(datetime.datetime.strptime(x, "%Y-%m-%d").strftime("%Y-%m-%d"))

for i in sites:
  export = pd.DataFrame(columns = ['Date', 'GISId', 'Value'])
  if os.path.exists(folder + i + '/' + i + '_AGGREGATE.txt'):
    print("Processing: " + i)  
    if i == 'RAYONG':
      read = folder + i + '/' + i + '_AGGREGATE.txt'
      df = pd.read_csv(read, sep='\t', error_bad_lines=False)
      
      df = df.dropna(subset=['Date'])
      df = df.reset_index(drop=True)
      df['Date'] = df['Date'].apply(datefixer)
      
      for rows in range(len(df)):
        export = export.append({'Date'        : df['Date'][rows],
                                'GISId'       : 119,
                                'Value'       : df['GHI'][rows]}, ignore_index = True)
        
      for rows in range(len(df)):
        export = export.append({'Date'        : df['Date'][rows],
                                'GISId'       : 79,
                                'Value'       : df['GTI_180az'][rows]}, ignore_index = True)
      
      read = folder + i + '/' + i + '_0az_AGGREGATE.txt'
      df = pd.read_csv(read, sep='\t', error_bad_lines=False)
      
      df = df.dropna(subset=['Date'])
      df = df.reset_index(drop=True)
      df['Date'] = df['Date'].apply(datefixer)
      
      for rows in range(len(df)):
        export = export.append({'Date'        : df['Date'][rows],
                                'GISId'       : 80,
                                'Value'       : df['GTI_0az'][rows]}, ignore_index = True)
    
    else:
      read = folder + i + '/' + i + '_AGGREGATE.txt'
      df = pd.read_csv(read, sep='\t', error_bad_lines=False)
      
      if 'Date' not in df.columns:
        if len(df.columns) == 2:
          df.columns = ['Date', 'GHI']
        elif len(df.columns) == 3:
          df.columns = ['Date', 'GHI', 'GTI']
          
      df = df.dropna(subset=['Date'])
      df = df.reset_index(drop=True)
      df['Date'] = df['Date'].apply(datefixer)
      
      if 'GTI' not in df.columns:
        ghi_id= pd.read_sql_query("SELECT Id FROM [Portfolio].[GIS] WHERE GISName='" + i + "'", connStr)
        for rows in range(len(df)):
          export = export.append({'Date'        : df['Date'][rows],
                                  'GISId'       : ghi_id.values[0][0],
                                  'Value'       : df['GHI'][rows]}, ignore_index = True)
                                                               
      if 'GTI' in df.columns:
        ghi_id= pd.read_sql_query("SELECT Id FROM [Portfolio].[GIS] WHERE GISName='" + i + "' and Type='GHI'", connStr)
        gti_id= pd.read_sql_query("SELECT Id FROM [Portfolio].[GIS] WHERE GISName='" + i + "' and Type='GTI'", connStr)
        for rows in range(len(df)):
          export = export.append({'Date'        : df['Date'][rows],
                                  'GISId'       : ghi_id.values[0][0],
                                  'Value'       : df['GHI'][rows]}, ignore_index = True)
        try:
          for rows in range(len(df)):
            export = export.append({'Date'        : df['Date'][rows],
                                    'GISId'       : gti_id.values[0][0],
                                    'Value'       : df['GTI'][rows]}, ignore_index = True)
        except:
          print(i + " GTI Missing in Portfolio")
    
    export = export.dropna(subset=['Value'])
    export = export.reset_index(drop=True)
    export['Date']= pd.to_datetime(export['Date'])
    fast_upsert_processed(export)