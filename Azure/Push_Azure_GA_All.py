#Author: Tangcueco Vicente III Tiu
#Date Created: 14 June 2020
#Date Last Modified: 15 June 2020

#This code is used to push new PA, GA, DA, DA_SH data to Azure
#This code can only be run in the server
import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import numpy as np
import pyodbc

print(datetime.datetime.now(pytz.timezone("Asia/Singapore")).hour)

#Database settings
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

EXCEL_PATH = '/home/vicente/code/GA_List.csv'

site_details = pd.read_csv(EXCEL_PATH)

while (True):
    print("Entering loop")
    if(datetime.datetime.now(pytz.timezone("Asia/Singapore")).hour == 3):
        for i in range(len(site_details["Site"])):
            #Gen 1 data parameters
            SITE = str(site_details["Site"][i])
            root_path = str(site_details["Root Path"][i])
            save_path = str(site_details["Save Path"][i])
            MFM_name = str(site_details["MFM Name"][i])
            MFM_filename = str(site_details["MFM Filename"][i])
            WMS_rootpath = str(site_details["WMS Root Path"][i])
            WMS_name = str(site_details["WMS Name"][i])
            WMS_filename = str(site_details["WMS Filename"][i])
            irr_column_name = str(site_details["Irr Column Name"][i])
            timestamp_column_name = str(site_details["Timestamp Column Name"][i])

            print("Inserting: ", SITE)

            #Connect to database
            connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
            cursor = connStr.cursor()

            #Get last date in database
            cursor.execute("SELECT dbo.[Grid_Availability].Date FROM dbo.[Grid_Availability] WHERE Reference = ?", SITE)
            data = cursor.fetchall()

            #Generate list of dates not in Grid Availability
            dates = pd.date_range(data[-1][0], datetime.datetime.today()).to_pydatetime().tolist()
            dates = dates[1:]

            #Iterate through missing dates from database
            for date in dates:
                year = date.strftime("%Y")
                year_month = date.strftime("%Y-%m")
                day = date.strftime("%Y-%m-%d")
                path_name = root_path + year + "/" + year_month + "/" + MFM_name + "/" + MFM_filename + "-" + day + ".txt"
                path_name_irr =  WMS_rootpath + year + "/" + year_month + "/" + WMS_name + "/" + WMS_filename + "-" + day + ".txt"
                print(path_name)

                mfm_df = pd.read_csv(path_name, sep="\t")
                print(path_name_irr)

                if(os.path.exists(path_name_irr)):
                    wms_df = pd.read_csv(path_name_irr, sep="\t")

                    #Get rows from irradiation sensor data where irradiation is not blank
                    irr_df = wms_df[[timestamp_column_name, irr_column_name]]
                    irr_df = irr_df[irr_df[irr_column_name].notnull()]

                    if(len(irr_df>0)):
                        #Filter rows where irradiation > 20
                        irr_timestamps=irr_df.loc[irr_df[irr_column_name]>20,timestamp_column_name]
                        
                        #Store rows where Freq > 40 and Power > 2 separately
                        mfm_df=mfm_df[['ts','Hz_avg','W_avg']]
                        freq_timestamps=mfm_df.loc[mfm_df['Hz_avg']>40,'ts']
                        power_timestamps=mfm_df.loc[mfm_df['W_avg']>2,'ts']
                        ga_common=list(set(freq_timestamps) & set(irr_timestamps))
                        pa_common=list(set(freq_timestamps) & set(irr_timestamps)  & set(power_timestamps))

                        if(len(irr_timestamps)>0):
                            GA=(float(len(ga_common))/float(len(irr_timestamps)))*100
                            if(float(len(list(set(freq_timestamps) & set(irr_timestamps)))) == 0):
                                PA=0
                            else:
                                PA=(float(len(pa_common))/float(len(list(set(freq_timestamps) & set(irr_timestamps)))))*100

                            #set ts as index for mfm_df
                            mfm_df['ts'] = pd.to_datetime(mfm_df['ts'])
                            mfm_dt = mfm_df.set_index('ts')

                            #remove duplicated timestamps
                            mfm_dt = mfm_dt.loc[~mfm_dt.index.duplicated(keep='first')]
                            irr_timestamps = pd.to_datetime(irr_timestamps)
                            mask = list(set(mfm_dt.index).intersection(irr_timestamps))
                            DA=(float(len(mfm_dt[mfm_dt['W_avg'].notnull()]))/288)*100
                            DA_SH=(float(len(mfm_dt.loc[mask][mfm_dt['W_avg'].notnull()])/float(len(irr_timestamps))))*100.0

                            df_ga=pd.DataFrame({"Date":day,'GA':[GA],'PA':[PA],'DA':[DA], 'DA_SH':[DA_SH]},columns=['Date', 'GA', 'PA', 'DA', 'DA_SH'])
                            print((df_ga))

                            for index,row in df_ga.iterrows():
                                if(datetime.datetime.strptime(row["Date"], "%Y-%m-%d") <= data[-1][0]):
                                    print(row["Date"], " already in database")
                                else:
                                    print("INSERT INTO dbo.[Grid_Availability]([Date], [GA], [DA], [Reference], [DA_SH], [PA]) values (?, ?, ?, ?, ?, ?)", row['Date'], row['GA'], row['DA'], SITE, row['DA_SH'], row['PA'])
                                    df_ga.to_csv(save_path,sep='\t',mode='a',header=False,index=False)
                                    with cursor.execute("INSERT INTO dbo.[Grid_Availability]([Date], [GA], [DA], [Reference], [DA_SH], [PA]) values (?, ?, ?, ?, ?, ?)", row['Date'], row['GA'], row['DA'], SITE, row['DA_SH'], row['PA']):
                                        pass
            connStr.commit()
            cursor.close()
            connStr.close()
            print("Inserting ", SITE, ": Success!")

        print("Sleeping now")
        time.sleep(216000)
    else:
        print("I am sleeping")
        time.sleep(216000)