import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc
#SQL Initialisation

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
tz = pytz.timezone('Asia/Phnom_Penh')

path1='/home/admin/Dropbox/SerisData/1min/[715]/'
path2='/home/admin/Dropbox/Gen 1 Data/[KH-003X]/'



while(1):
    if(datetime.datetime.now(tz).hour==3):
        date=str(datetime.datetime.now(tz).date()+datetime.timedelta(days=-1))
        try:
            df1=pd.read_csv(path1+date[0:4]+'/'+date[0:7]+'/[715] '+date+'.txt',sep='\t')
            df2=pd.read_csv('/home/admin/Dropbox/Gen 1 Data/[KH-003X]/'+date[0:4]+'/'+date[0:7]+'/CKL/[KH-003X-M3] '+date+'.txt',sep='\t')
            df_merge=pd.merge(df1, df2, left_on=['Tm'],right_on=['Local.Time.Stamp'],sort=False,how='left')
            a=df_merge[['Tm','AvgGsi00','Act_Pwr-Tot_CK','Active.Power..W.']].copy()
            connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
            cursor = connStr.cursor()
            a=a.fillna('NULL')
            for index,row in a.iterrows():
                print("INSERT INTO [dbo].[KH-003S-L-2]([Date],[AvgGsi00],[EAC Solar],[EAC Grid]) VALUES ('"+row['Tm']+"',"+str(row['AvgGsi00'])+','+str(row['Act_Pwr-Tot_CK'])+','+str(row['Active.Power..W.'])+")")
                try:
                    with cursor.execute("INSERT INTO [dbo].[KH-003S-L-2]([Date],[AvgGsi00],[EAC Solar],[EAC Grid]) VALUES ('"+row['Tm']+"',"+str(row['AvgGsi00'])+','+str(row['Act_Pwr-Tot_CK'])+','+str(row['Active.Power..W.'])+")"):
                        pass
                    connStr.commit()
                except Exception as e:
                    print(e)
            cursor.close()
            date2=df_merge['Tm'].head(1).values
            perc=df_merge['Act_Pwr-Tot_CK'].sum()/(df_merge['Act_Pwr-Tot_CK'].sum()+(df_merge['Active.Power..W.'].sum()/1000))
            if(perc>=1):
                perc='NULL'
            cursor = connStr.cursor()
            try:
                with cursor.execute("INSERT INTO [dbo].[KH-003S-L-3] ([Date],[Solar Percentage]) VALUES ('"+str(date2[0])+"',"+str(perc)+")"):
                    pass
                connStr.commit()
            except Exception as e:
                print(e)
            cursor.close()
            connStr.close()
        except Exception as e:
            print(e)
            pass
    else:
        print('Sleeping')
    time.sleep(3600)
