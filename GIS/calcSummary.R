require("lubridate")
calcSummary = function(files)
{
  y = 1
  city = unlist(strsplit(files[y],"_"))
  idx = match(city[5],cities)
	writebigfile = 1
	idxGTI = 5
	if(!is.finite(idx))
	{
	  cities[(length(cities)+1)]<<- city[5]
		idx = length(cities)
		createdir(pathwrite,cities[idx])
		tilts <<- vector('list',length(cities))
		tilts <<- filltilts(cities,path,tilts)
		tables[[idx]] <<- vector('list')
    tables[[idx]][1] <<- vector('list',1)
    tables[[idx]][2] <<- vector('list',1)
		cumnames[idx] <<- paste(pathwrite,"/",cities[idx],"/",toupper(substr(cities[idx],1,9))," Aggregate.txt",sep="")
    if(length(tilts[[idx]]) < 1)
    {
      tables[[idx]][3] = vector('list',1)
    }
		if(length(tilts[[idx]]) > 0){
    for(z in 1 : length(tilts[[idx]]))
    {
      tables[[idx]][(z+2)] = vector('list',1)
    }}
	}
  if(toupper(substr(cities[idx],1,9))=='BRATISLAV' ||
  toupper(substr(cities[idx],1,9))=='HYDERABAD' || 
	toupper(substr(cities[idx],1,9))=='AHMEDABAD' || 
	toupper(substr(cities[idx],1,7))=='NELLORE' || 
	toupper(substr(cities[idx],1,9))=='AURANGABA')
		idxGTI = 4
  dataread = try(read.table(files[y],header = T,sep = ";"),silent=T)
	if(class(dataread)=='try-error')
		return()
  if(nrow(dataread) < 1)
	return()
	days = unique(as.character(dataread[,1]))
  uniqmonths = unique(substr(days,4,11))
  idxmonths = match(uniqmonths,substr(as.character(dataread[,1]),4,11))
  outer = 1
  for(outer in  1: length(idxmonths))
  { 
    sum = 0
    sum2 = 0
    tot = NULL
    tot2 = NULL
    x = 1
    dataread2 = dataread
    {
      if(outer == length(idxmonths))
      {
        dataread = dataread[idxmonths[outer]:length(dataread[,1]),]
      }
      else
      {
        dataread = dataread[idxmonths[outer]:(idxmonths[(outer+1)]-1),]
      }
    }
    days = unique(as.character(dataread[,1]))
    for(x in 1 : length(days))
    {
      subdata = as.numeric(dataread[as.character(dataread[,1]) == days[x],3])
      subdata = subdata[complete.cases(subdata)]
      val = as.numeric(format(round(sum(subdata)/1000,2),nsmall=2))
			tot = unlist(tot)
      tot = c(tot,as.character(val))
      sum = sum + val
      subdata = as.numeric(dataread[as.character(dataread[,1]) == days[x],idxGTI])
      subdata = subdata[complete.cases(subdata)]
      val = as.numeric(format(round(sum(subdata)/1000,2),nsmall=2))
			if(!(colnames(dataread)[idxGTI]=='GTI' || colnames(dataread)[idxGTI]=='DIF'))
				val = NA
			tot2 = unlist(tot2)
			tot2 = c(tot2,as.character(val))
      sum2 = sum2 + val
    }
    df = data.frame(Date = as.Date(days,"%d.%m.%Y"), GHITot = tot,GTot = tot2)
    monthnm=paste(substr(uniqmonths[outer],4,7),substr(uniqmonths[outer],1,2),sep="-")
  {
    if(length(tilts[[idx]]) < 1)
    {
    filename = paste(pathwrite,"/",cities[idx],"/",toupper(substr(cities[idx],1,9)),
                     " ",monthnm,".txt",sep="")
    }
    else
    {
      filename = paste(pathwrite,"/",cities[idx],"/",toupper(substr(cities[idx],1,9)),
            " ",monthnm," ",city[7]," ",city[8],".txt",sep="")
      if(is.finite(as.numeric(city[8])))
      {
        filename = paste(pathwrite,"/",cities[idx],"/",toupper(substr(cities[idx],1,9)),
            " ",monthnm," ",city[7],".txt",sep="")
      }
    }
  }
    {
		if(!file.exists(filename))
		{
		write.table(df,file = filename,sep = "\t", row.names = F, col.names = T,append = F)
		}
		else
		{
		write.table(df,file = filename,sep = "\t", row.names = F, col.names = F,append = T)
		datareadredund = read.table(filename,sep = "\t",header = T)
		#pick last occurance
		datareadredund = datareadredund[(length(datareadredund[,1]) - match(unique(as.character(datareadredund[,1])),rev(as.character(datareadredund[,1])))+1),]
		write.table(datareadredund,file = filename,row.names = F,col.names = T,append = F,sep="\t")
		}
		}
    {
      if((length(tilts[[idx]]) < 1))
      {
       tables[[idx]][[1]] <<- c(unlist(tables[[idx]][[1]]),as.character(as.Date(days,"%d.%m.%Y")))
        tables[[idx]][[2]] <<- c(unlist(tables[[idx]][[2]]),tot)
        tables[[idx]][[3]] <<- c(unlist(tables[[idx]][[3]]),tot2)
       df = data.frame(c1=tables[[idx]][[1]],c2=tables[[idx]][[2]],c3=tables[[idx]][[3]])
#				df = data.frame(tables[[idx]])
			 if(writebigfile)
			 {
       colnames(df) = c("Date","GHI","GTI")
			 }
			 df = df[(length(df[,1])-match(unique(as.character(df[,1])),rev(as.character(df[,1])))+1),] #pick last occurance
			 df = df[order(df[,1]),]
        write.table(df,file = cumnames[idx],sep = "\t", row.names = F, col.names = T,append = F)
     }
      else
      {
			  {
			  if(length(city)==10)
				  strmatch = paste(city[7],city[8])
				else
					strmatch = city[7]
				}
        tiltmatch = (match(strmatch,tilts[[idx]]) + 2)
				if(!is.finite(tiltmatch))
					return()
        if(tiltmatch == 3)
        {
          tables[[idx]][[1]] <<- c(unlist(tables[[idx]][[1]]),as.character(as.Date(days,"%d.%m.%Y")))
          tables[[idx]][[2]] <<- c(unlist(tables[[idx]][[2]]),tot)
        }
        tables[[idx]][[tiltmatch]] <<- c(unlist(tables[[idx]][[tiltmatch]]),tot2)
        t = tables[[idx]]
        listlen = c()
        for( n in 1 : length(t))
        {
          listlen[n] = length(tables[[idx]][[n]])
        }
        {
				if(length(unique(listlen)) == 1)
        {
          df = data.frame(tables[[idx]])
					if(writebigfile)
					{
          colnames(df) = c("Date","GHI",paste("GTI",tilts[[idx]]))
					}
			 df = df[(length(df[,1])-match(unique(as.character(df[,1])),rev(as.character(df[,1])))+1),]
					df = df[order(df[,1]),]
					print('writing cumnames')
          write.table(df,file = cumnames[idx],sep = "\t", row.names = F, col.names = T,append = F)
        }
        #else
        #{
         #print(paste("not writing cumnames", cumnames[idx], "due to missmatch"))
         #print(listlen)
         #print(tilts[[idx]])
        #}
				}
      }
    }
    dataread = dataread2
  }
return(filename)
}

checkAggregate = function(path)
{
	pathwrite = path
	for(x in 1 : length(cities))
	{
		tab = tables[[x]]
		tz = as.character(tables[[x]][[1]])
		listlen = c()
    for( n in 1 : length(tab))
    {
      listlen[n] = length(tables[[x]][[n]])
    }
		if(!file.exists(cumnames[x]) || length(unique(listlen)) != 1)
		{
			print(paste('---------------',cumnames[x],'doesnt exist-------------'))
			tab = tables[[x]]
			tz = as.character(tables[[x]][[1]])
			listlen = c()
      for( n in 1 : length(tab))
      {
        listlen[n] = length(tables[[x]][[n]])
      }
			print(listlen)
			print("#####################")
			print(tilts[[x]])
			maxlen = max(listlen)
      for( n in 1 : length(tab))
      {
        if(listlen[n] != maxlen)
				{
					print(paste('Missmatch found for',tilts[[x]][[(n-2)]]))
					replaceCol = c()
					idx = 1
					siteName = as.character(tilts[[x]][[(n-2)]])
					cityName = toupper(substr(cities[x],1,9))
					for(t in 1 : length(tz))
					{
						mon = substr(tz[t],1,7)
						pathprobe = paste(pathwrite,"/",cities[x],"/",cityName," ",mon,
						" ",siteName,".txt",sep="")
						{
						if(!file.exists(paste(pathprobe)))
						{
							replaceCol[idx] = NA
						}
						else
						{
							dataread = read.table(pathprobe,header = T,sep = "\t")
							tm = as.character(dataread[,1])
							match = match(tz[t],tm)
							{
								if(!is.finite(match))
								{
									replaceCol[idx] = NA
								}
								else
									replaceCol[idx] = as.numeric(dataread[match,3])
							}
						}
						}
						idx = idx + 1
					}
					tables[[x]][[n]] <<- replaceCol
				}
      }
			 print('rewrote column')
       df = data.frame(tables[[x]])
       colnames(df) = c("Date","GHI",paste("GTI",tilts[[x]]))
			 df = df[(length(df[,1])-match(unique(as.character(df[,1])), rev(as.character(df[,1]))) + 1),]
			 df = df[order(as.character(df[,1])),]
       write.table(df,file = cumnames[x],sep = "\t", row.names = F, 
			 col.names = T,append = F)
			 print('Wrote aggregate file')
		}
	}
}

findCountry = function(x)
{
	pathProbe = "/home/admin/Dropbox/GIS/Raw Files"
	files = dir(pathProbe)
	idxmtch = grepl(x,files)
	country = "IN"
	if(is.finite(idxmtch))
	{
		files = files[idxmtch]
		{
			if(grepl("Malaysia",files))
				country = "MY"
			else if(grepl("Cambodia",files))
				country = "KH"
			else if(grepl("Thailand",files))
				country = "TH"
			else if(grepl("Phillipines",files))
				country = "PH"
			else if(grepl("Singapore",files))
				country = "SG"
			else if(grepl("Vietnam",files))
				country = "VN"
		}
	}
	return(country)
}

findLatLong = function(x)
{
	pathProbe = "/home/admin/Dropbox/GIS/Raw Files"
	files = dir(pathProbe)
	x = paste("_",x,"_",sep="")
  idxmtch = files[grepl(x,files)]
	idxmtch = match(idxmtch[1], files)
	lat = long = NA
	if(is.finite(idxmtch))
	{
		ptuse = paste(pathProbe,files[idxmtch],sep="/")
		print(ptuse)
		data = readLines(ptuse)
		for(t in 1 : length(data))
		{
			if(grepl("Latitude",data[t]))
			{
				val = unlist(strsplit(data[t],":"))
				lat = trimws(val[2],"b")
				lat = as.numeric(lat)
			}
			if(grepl("Longitude",data[t]))
			{
				val = unlist(strsplit(data[t],":"))
				long = trimws(val[2],"b")
				long = as.numeric(long)
			}
			if(is.finite(lat) && is.finite(long))
				break
		}
	}
	return(c(lat,long))
}

extrapolateInfo = function(date, currMonIrr)
{
	dateCurr = strptime(date,format="%Y-%m-%d")
	daysofar = as.numeric(substr(date,9,10))
	nodays = as.numeric(days_in_month(dateCurr))
	datelast = paste(substr(date,1,8),nodays,sep="")
	percsofar = round(daysofar/nodays,5)
	extrap = round(currMonIrr/percsofar,2)
	return(c(percsofar,extrap))
}

getAggInfo = function(x)
{
	pathProbe = "/home/admin/Dropbox/GIS/Summary"
	lastd = ma30 = last=sum365 = NA
	finalPath = paste(pathProbe,"/",x,"/",toupper(substr(x,1,9))," Aggregate.txt",sep="")
	if(file.exists(finalPath))
	{
		print("Agg file present for Master")
		dataread = read.table(finalPath,header=T,sep="\t",stringsAsFactors=F)
		lastd = as.character(dataread[nrow(dataread),1])
		last = as.character(dataread[nrow(dataread),2])
		v1 = as.numeric(dataread[,2])
		ma30 = as.character(round(mean(tail(v1,30)),2))
		sum365 = as.character(round(mean(tail(v1,365))*365,2))
	}
	return(c(lastd,last,ma30,sum365))
}
computeMaster = function()
{
	path = "/home/admin/Dropbox/GIS/Summary"
	pathwrite = "/home/admin/Dropbox/GIS/Master"

	stns = dir(path)
	idx = match("Bratislava",stns)
	stns = stns[-idx]
	idx = match("Tay-Ninh",stns)
	stns = stns[-idx]
	c1max = 0
	colnames = c()
	for(x in 1 : length(stns))
	{
		c1 = c2 = c3 = c()
		pathwritefinal = paste(pathwrite,"/",stns[x],"_Master.txt",sep="")
		pathsumm = paste(path,stns[x],sep="/")
		files = dir(pathsumm)
		files = files[grepl("Aggregate",files)]
		dataread = read.table(paste(pathsumm,files[1],sep="/"),header =T,sep="\t")
		mons = substr(as.character(dataread[,1]),1,7)
		mons = unique(mons)
		if(length(mons) > c1max)
		{
			c1max = length(mons)
			colnames = mons
		}
		for(x in 1: length(mons))
		{
			c1[x] = mons[x]
			temp = as.numeric(dataread[grepl(mons[x],as.character(dataread[,1])),2])
			temp = temp[complete.cases(temp)]
			c3[x] = 0
			c2[x]=NA
			if(length(temp))
			{
				c2[x] = sum(as.numeric(temp))
				c3[x] = length(temp)
			}
		}
		df = data.frame(Month=c1,GHITot=c2,NumDaysRecorded=c3)
		write.table(df,file=pathwritefinal,sep="\t",row.names=F,col.names=T,append=F)
	}
	colnames = paste("GHITot",colnames,sep="_")
	colnames = c("City","Country","Lat","Long","LastDayRecord","LastDayValue","Last30MA","Last365DayTot","FirstMonth","TimePassedMonth","ExtrapolatedIrradiationMonth","SummaryName",colnames)
	rowtemplate = unlist(rep(NA,(c1max+12)))
	mastercities = dir(pathwrite)
	for(x in 1 : length(mastercities))
	{
		rowtemplate = unlist(rep(NA,(c1max+12)))
		dataread = read.table(paste(pathwrite,mastercities[x],sep="/"),header=T,sep="\t")
		for(y in 1 : nrow(dataread))
		{
			idxmtch = match(paste("GHITot",as.character(dataread[y,1]),sep="_"),colnames)
			rowtemplate[idxmtch] = as.character(dataread[y,2])
		}
		rowtemplate[1] = stns[x]
		rowtemplate[2] = findCountry(stns[x])
		vals = findLatLong(stns[x])
		rowtemplate[3] = as.character(vals[1])
		rowtemplate[4] = as.character(vals[2])
		rowtemplate[9] = as.character(dataread[1,1])
		aggInfo = getAggInfo(stns[x])
		rowtemplate[5] = as.character(aggInfo[1])
		rowtemplate[6] = as.character(aggInfo[2])
		rowtemplate[7] = as.character(aggInfo[3])
		rowtemplate[8] = as.character(aggInfo[4])
		extrapInfo = extrapolateInfo(as.character(rowtemplate[5]),as.numeric(rowtemplate[length(rowtemplate)]))
		rowtemplate[10] = as.character(extrapInfo[1])
		rowtemplate[11] = as.character(extrapInfo[2])
		rowtemplate[12] = toupper(substr(as.character(stns[x]),1,9))
		df = data.frame(rbind(rowtemplate))
		colnames(df) = colnames
		if(x == 1)
		{
			write.table(df,file=paste(pathwrite,"Z_AllSites_Master.txt",sep="/"),row.names=F,col.names=T,sep="\t",append=F)
			next
		}
		write.table(df,file=paste(pathwrite,"Z_AllSites_Master.txt",sep="/"),row.names=F,col.names=F,sep="\t",append=T)
	}
  df = read.table(paste(pathwrite,"Z_AllSites_Master.txt",sep="/"), header=T,sep="\t",stringsAsFactors=F)
  c1 = as.character(df[,1])
  idx1 = which(c1 %in% NA)
  if(length(idx1))
  {
    df = df[-idx1,]
  }
  idx1 = which(c1 %in% "NA")
  if(length(idx1))
  {
    df = df[-idx1,]
  }
			write.table(df,file=paste(pathwrite,"Z_AllSites_Master.txt",sep="/"),row.names=F,col.names=T,sep="\t",append=F)
}
