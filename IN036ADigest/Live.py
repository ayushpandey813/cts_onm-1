import requests, json
import requests.auth
import pandas as pd
import time
import pytz
import datetime
import os
import shutil

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

tz = pytz.timezone('Asia/Calcutta')
path='/home/admin/Dropbox/Gen 1 Data/[IN-036A]/'
start_path="/home/admin/Start/IN036A.txt"

components={'WMS_1': ['Weather Station', ['1-1'], 'WMS1'], 'MFM_1': ['MFM', ['1-1'], 'MFM1'], 'MFM_2': ['MFM', ['1-2'], 'MFM2'], 'MFM_3': ['MFM', ['1-3'], 'MFM3'], 'MFM_4': ['MFM', ['1-4'], 'MFM4'], 'Transformer_1': ['Transformer', ['1-1-1'], 'WMS1'], 'Transformer_2': ['Transformer', ['1-1-2'], 'WMS2'], 'INVERTER_1': ['Inverter', ['1-1-1'], 'I1'], 'INVERTER_2': ['Inverter', ['1-1-2'], 'I2'], 'INVERTER_3': ['Inverter', ['1-1-3'], 'I3'], 'INVERTER_4': ['Inverter', ['1-1-4'], 'I4'], 'INVERTER_5': ['Inverter', ['1-1-5'], 'I5'], 'INVERTER_6': ['Inverter', ['1-1-6'], 'I6'], 'INVERTER_7': ['Inverter', ['1-1-7'], 'I7'], 'INVERTER_8': ['Inverter', ['1-1-8'], 'I8'], 'INVERTER_9': ['Inverter', ['1-1-9'], 'I9'], 'INVERTER_10': ['Inverter', ['1-1-10'], 'I10'], 'INVERTER_11': ['Inverter', ['1-1-11'], 'I11'], 'INVERTER_12': ['Inverter', ['1-1-12'], 'I12'], 'INVERTER_13': ['Inverter', ['1-1-13'], 'I13'], 'INVERTER_14': ['Inverter', ['1-1-14'], 'I14'], 'INVERTER_15': ['Inverter', ['1-1-15'], 'I15'], 'INVERTER_16': ['Inverter', ['1-1-16'], 'I16'], 'INVERTER_17': ['Inverter', ['1-1-17'], 'I17'], 'INVERTER_18': ['Inverter', ['1-1-18'], 'I18'], 'INVERTER_19': ['Inverter', ['1-1-19'], 'I19'], 'INVERTER_20': ['Inverter', ['1-1-20'], 'I20'], 'INVERTER_21': ['Inverter', ['1-1-21'], 'I21'], 'INVERTER_22': ['Inverter', ['1-1-22'], 'I22'], 'INVERTER_23': ['Inverter', ['1-1-23'], 'I23'], 'INVERTER_24': ['Inverter', ['1-1-24'], 'I24'], 'INVERTER_25': ['Inverter', ['1-2-1'], 'I25'], 'INVERTER_26': ['Inverter', ['1-2-2'], 'I26'], 'INVERTER_27': ['Inverter', ['1-2-3'], 'I27'], 'INVERTER_28': ['Inverter', ['1-2-4'], 'I28'], 'INVERTER_29': ['Inverter', ['1-2-5'], 'I29'], 'INVERTER_30': ['Inverter', ['1-2-6'], 'I30'], 'INVERTER_31': ['Inverter', ['1-2-7'], 'I31'], 'INVERTER_32': ['Inverter', ['1-2-8'], 'I32'], 'INVERTER_33': ['Inverter', ['1-2-9'], 'I33'], 'INVERTER_34': ['Inverter', ['1-2-10'], 'I34'], 'INVERTER_35': ['Inverter', ['1-2-11'], 'I35'], 'INVERTER_36': ['Inverter', ['1-2-12'], 'I36'], 'INVERTER_37': ['Inverter', ['1-2-13'], 'I37'], 'INVERTER_38': ['Inverter', ['1-2-14'], 'I38'], 'INVERTER_39': ['Inverter', ['1-2-15'], 'I39'], 'INVERTER_40': ['Inverter', ['1-2-16'], 'I40'], 'INVERTER_41': ['Inverter', ['1-2-17'], 'I41'], 'INVERTER_42': ['Inverter', ['1-2-18'], 'I42'], 'INVERTER_43': ['Inverter', ['1-2-19'], 'I43'], 'INVERTER_44': ['Inverter', ['1-2-20'], 'I44'], 'INVERTER_45': ['Inverter', ['1-2-21'], 'I45'], 'INVERTER_46': ['Inverter', ['1-2-22'], 'I46'], 'INVERTER_47': ['Inverter', ['1-2-23'], 'I47'], 'INVERTER_48': ['Inverter', ['2-1-1'], 'I48'], 'INVERTER_49': ['Inverter', ['2-1-2'], 'I49']}


if(os.path.exists(start_path)):
    pass
else:
    try:
        shutil.rmtree(path,ignore_errors=True)
    except Exception as e:
        print(e)
    with open(start_path, "w") as file:
        file.write("2021-01-04\n00:00:00")   
with open(start_path) as f:
    startdate = f.readline(11)[:-1]
    starttime = f.readline(10)

start=datetime.datetime.strptime(startdate, "%Y-%m-%d")

data = {"email":"operations@cleantechsolar.com","password":"Operations@123"}	
r = requests.post("https://apis.eaglesunscada.com/api/v1.0/getToken", data=data)
token_json = r.json()  
auth_token=token_json['token']

while(start.date()<datetime.datetime.now(tz).date()):
    date=start.strftime("%Y-%m-%d")
    endpoint = "https://apis.eaglesunscada.com/api/v1.0/report"
    for i in components:
        data_params = {"plantId":"3429","dateFrom":date,"dateTo":date,"idList":components[i][1],"interval":"1","module":"report","device":components[i][0],"role":"user"}
        headers = {"Authorization": "Bearer "+auth_token, 'Content-type': 'application/json'}
        data=requests.post(endpoint, data=json.dumps(data_params), headers=headers).json()
        df=pd.DataFrame(data['message'])
        print(df.head())
        if(df.empty):
            pass
        else:
            path_temp=path+date[0:4]+'/'+date[0:7]
            chkdir(path_temp+'/'+i+'/')
            cols = df.columns.tolist()
            cols.insert(0, cols.pop(cols.index('Date')))
            df = df.reindex(columns= cols)
            df=df.sort_values(by='Date')
            print(path_temp+'/'+i+'/'+'[IN-036A]-'+components[i][2]+'-'+date+'.txt')
            df.to_csv(path_temp+'/'+i+'/'+'[IN-036A]-'+components[i][2]+'-'+date+'.txt',sep='\t',index=False,mode='w')
        time.sleep(100)
    start=start+datetime.timedelta(days=+1)


while(1):
    try:
        print('Live')
        date=(datetime.datetime.now(tz)).strftime("%Y-%m-%d")
        date_time=datetime.datetime.now(tz)
        data = {"email":"operations@cleantechsolar.com","password":"Operations@123"}	
        r = requests.post("https://apis.eaglesunscada.com/api/v1.0/getToken", data=data)
        token_json = r.json()  
        auth_token=token_json['token']
        endpoint = "https://apis.eaglesunscada.com/api/v1.0/report"
        for i in components:
            data_params = {"plantId":"3429","dateFrom":date,"dateTo":date,"idList":components[i][1],"interval":"1","module":"report","device":components[i][0],"role":"user"}
            headers = {"Authorization": "Bearer "+auth_token, 'Content-type': 'application/json'}
            data=requests.post(endpoint, data=json.dumps(data_params), headers=headers).json()
            try:
                df=pd.DataFrame(data['message'])
                path_temp=path+date[0:4]+'/'+date[0:7]
                chkdir(path_temp+'/'+i+'/')
                cols = df.columns.tolist()
                cols.insert(0, cols.pop(cols.index('Date')))
                df = df.reindex(columns= cols)
                df=df.sort_values(by='Date')
                df.to_csv(path_temp+'/'+i+'/'+'[IN-036A]-'+components[i][2]+'-'+date+'.txt',sep='\t',index=False,mode='w')
            except:
                print('No data')
            time.sleep(100)
        with open(start_path, "w") as file:
            file.write(str(date)+"\n"+str(date_time.time().replace(microsecond=0)))
    except:
        print('json error')
    time.sleep(600)

