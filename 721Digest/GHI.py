import datetime
import os
import pandas as pd
from matplotlib.pyplot import figure
import matplotlib.pyplot as plt
import matplotlib.colors as clrs
import matplotlib.ticker as ticker
from datetime import datetime
import matplotlib.dates as mdates
from datetime import datetime as dt
from datetime import timedelta 
import numpy as np

date = []
date_new = []
DA = []
DA_new = []
current_DA = []
prev_DA = []
prevMonth = 12

yesterday = datetime.now() - timedelta(days=1)
yesterday  = str(yesterday)
yesterday = yesterday[0:11]
currentMonth = int(datetime.now().month)
currentYear = str(datetime.now().year)
print(currentMonth)
print(currentYear)
if(currentMonth == 1):
  prevMonth == 12
else:
  prevMonth = currentMonth-1

if(currentMonth == 1 or currentMonth == 2 or currentMonth == 3 or currentMonth == 4 or currentMonth == 5 or currentMonth == 6 or currentMonth == 7 or currentMonth == 8 or currentMonth == 9):
  currentMonth = str(currentMonth)
  currentMonth = "0"+currentMonth
prevMonth = str(prevMonth)
currentMonth = str(currentMonth)
if(prevMonth!= '12' and prevMonth!= '11' and prevMonth != '10'):
    prevMonth = "0"+prevMonth
if(currentMonth == "01"):
  prevyrmon = "2020"+"-"+prevMonth
else:
  prevyrmon = currentYear+"-"+prevMonth
yrmon = currentYear+"-"+currentMonth
print(yrmon)

  
print(prevMonth)
path = '/home/admin/Dropbox/Third Gen/SERIS_SITES/[IN-721S]/[IN-721S]_lifetime.txt'
with open(path) as file:
  df1 = pd.read_csv(file,sep='\t',header=0,skipinitialspace=True)
for i in df1['Date']:
  date.append(i)
for i in df1['Irradiance']:
  DA.append(i)
for i in DA: 
  i = float(i)
  DA_new.append(i)
  
for i in date:
   i = datetime.strptime(i, '%Y-%m-%d')
   date_new.append(i)
   
#CALCULATION:
avg = round(np.mean(DA_new),2)
print(avg)


for i,j in zip(date_new,DA_new):
  i = str(i)
  if("2020" in i):
    prev_DA.append(j)

for i,j in zip(date_new,DA_new):
  i = str(i)
  if(yrmon in i):
    current_DA.append(j)
       
DA_new = DA_new[-90:]
date_new = date_new[-90:]
 
df = pd.DataFrame(list(zip(date_new, DA_new)),columns=['Date','DA_new'])

#df.to_csv('/home/anusha/GRAPHS/Graph [SG_724S] GHI - '+yesterday+'.csv')
df.to_csv('/home/admin/Graphs/Graph_Extract/IN-721/Graph [IN_721S] GHI - '+yesterday+'.csv')
fig, ax = plt.subplots(figsize=(13.2  , 8.8))
tick_spacing  = 30
y = df['DA_new']
plt.rcParams.update({'font.size': 11})
x = df['Date']
  
ax.plot(x,y,  color = 'green', linewidth = '0.8',marker='o') 

prev_DA_avg = round(np.sum(prev_DA)/365,2)
print(prev_DA_avg)
current_DA_avg = round(np.mean(current_DA),2)
plt.rcParams.update({'font.size': 12})
plt.axhline(y=prev_DA_avg, color='r', linestyle='--')
ax.set_xlim(x[0], x.iloc[-1])
ax.set_ylim(0, 7)
ax.set_ylabel('GHI [kWh/m2]', fontsize = 12)
first_day = str(date_new[0])
first_day = first_day[0:11]

ax.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))
ax.text(0.78, 0.065, 'Lifetime GHI [kWh/m2]: '+str(avg), va="top",transform=ax.transAxes,weight='bold',size = 10)
ax.text(0.82, 0.68, 'TMY [kWh/m2]: '+str(prev_DA_avg), va="top",transform=ax.transAxes,weight='bold',size = 10, color = 'r')
#ax.text(0.72, 0.135, 'Previous Month DA [%]: '+str(prev_DA_avg), va="top",transform=ax.transAxes,weight='bold')
#ax.text(0.72, 0.10, 'Current Month DA [%]: '+str(current_DA_avg), va="top",transform=ax.transAxes,weight='bold')
plt.suptitle('Global Horizontal Irradiation - IN-721', x=0.51, y=.95, horizontalalignment='center', verticalalignment='top', fontsize = 15)
plt.title('From '+ str(first_day) +'to ' +yesterday , fontsize = 13)
ax.grid(axis='y')
ax.tick_params(axis='both', which='major', labelsize=11)
plt.xticks(rotation=90)

plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%b-%y'))
#fig.savefig('/home/anusha/GRAPHS/Graph [SG_724S] GHI - '+yesterday+'.pdf', dpi=fig.dpi,bbox_inches='tight')
fig.savefig('/home/admin/Graphs/Graph_Output/IN-721/Graph [IN_721S] GHI - '+yesterday+'.pdf', dpi=fig.dpi,bbox_inches='tight')
plt.show()
