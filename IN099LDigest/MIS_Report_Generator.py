import pandas as pd
import numpy as np
import os
import pytz
import datetime
import xlsxwriter
import numpy as np
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import sys
from functools import cmp_to_key





tz = pytz.timezone('Asia/Calcutta')
date = datetime.datetime.now(tz)
date_yesterday = date - datetime.timedelta(days=1)
date = date_yesterday.strftime('%Y-%m-%d')
timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
stn = '[IN-099L]'
recipients = ['operationsWestIN@cleantechsolar.com','om-it-digest@cleantechsolar.com','om.prakash@prismjohnson.in','pcl.energy@prismjohnson.in','jabir.khan@prismjohnson.in','manish.singh@prismjohnson.in','kumar.akash@hrjohnsonindia.com','narendra.singhai@prismjohnson.in','gaurav.khatri@prismjohnson.in','sanjeeva@prismjohnson.in','ashok.singh@prismjohnson.in','anik.mukherjee@prismjohnson.in','sanjay.singh@prismjohnson.in','shivdayal.singh@prismjohnson.in']
# recipients = ['amogh.diwan@cleantechsolar.com']
excel_path_write = '/home/admin/Dropbox/Customer/[IN-9099L]/MIS Report.xlsx'
# excel_path_write = 'MIS Report.xlsx'

def col_cmp(col1, col2):
    c1v = int(col1.split('_')[1])
    c2v = int(col2.split('_')[1])
    if c1v < c2v:
        return -1
    elif c1v > c2v:
        return 1
    return 0



def retrieve_data():
    ''''
    Given the station this function will retrieve gen-1 data for it and collate it into an appropriate dataframe
    '''
    data_path = '/home/admin/Dropbox/Gen 1 Data/{station}/'.format(station = stn) + date[:4] + '/' + date[:7]
    
    # get inverter data for all inverters 
    
    # get list of all folders in directory
    folder_list_unfiltered = [f.path for f in os.scandir(data_path) if f.is_dir()]


    #generate and store inverter data
    master_df_inverter = None
    folder_list_inverter = []
    for folder in folder_list_unfiltered:
        if "INVERTER" in folder:
            folder_list_inverter.append(folder)
    for folder in folder_list_inverter:
        inverter_name = folder.split('/')[-1]
        # go to this path and find file with the current date in name this is the file we query for data
        files_in_folder = [f.path for f in os.scandir(folder) if f.is_dir]
        file_with_date_in_name = list(filter(lambda x : date in x, files_in_folder))[0]
        inverter_df = pd.read_csv(file_with_date_in_name, sep='\t').loc[:,['ts','W_avg','Wh_sum']]
        inverter_df['ts'] = pd.to_datetime(inverter_df['ts'])
        inverter_df_wavg = pd.DataFrame(inverter_df.resample('H', on='ts').W_avg.mean())
        inverter_df_whsum = pd.DataFrame(inverter_df.resample('H', on='ts').Wh_sum.sum())
        inverter_df_wavg.columns = [inverter_name+'_wavg']    
        inverter_df_whsum.columns = [inverter_name+'_whsum']      
        inverter_df_wavg.index = inverter_df_whsum.index
        inverter_df = pd.concat([inverter_df_wavg, inverter_df_whsum], axis = 1) 
        if master_df_inverter is None:
            master_df_inverter = inverter_df
        else:
            inverter_df.index = master_df_inverter.index
            master_df_inverter = pd.concat([master_df_inverter, inverter_df], axis = 1)

    inverter_columns_sorted = sorted(list(master_df_inverter), key=cmp_to_key(col_cmp))
    master_df_inverter = master_df_inverter[inverter_columns_sorted]

    master_df_mfm = pd.DataFrame(index=master_df_inverter.index)
    folder_list_mfm = []
    for folder in folder_list_unfiltered:
        if 'MFM' in folder:
            mfm_name = folder.split('/')[-1]
            if "Billing" not in mfm_name:
                continue
            folder_list_mfm.append(folder)
    
    for folder in folder_list_mfm:
        mfm_name = folder.split('/')[-1]
        files_in_folder = [f.path for f in os.scandir(folder) if f.is_dir]
        file_with_date_in_name = list(filter(lambda x: date in x, files_in_folder))[0]
        mfm_df = pd.read_csv(file_with_date_in_name, sep='\t')
        mfm_df = pd.DataFrame(mfm_df.loc[:,['ts','TotWhImp_max']])
        mfm_df['ts'] = pd.to_datetime(mfm_df['ts'])
        mfm_df.set_index('ts', inplace=True)
        mfm_df_grouped = mfm_df.groupby(mfm_df.index.hour)
        mfm_df = pd.DataFrame(index=master_df_mfm.index, columns=[mfm_name])

        for hour in mfm_df_grouped.groups.keys():            
            hour_df = mfm_df_grouped.get_group(hour)['TotWhImp_max']
            mfm_df.iloc[hour] = hour_df.iloc[-1]-hour_df.iloc[0]
        master_df_mfm = pd.concat([master_df_mfm, mfm_df], axis = 1)
    master_df = pd.concat([master_df_inverter, master_df_mfm], axis = 1)
    master_df = pd.DataFrame(master_df.iloc[5:(8+12)+1])
    master_df.replace(np.nan, 0, inplace=True)
    return master_df






def excel_style(col):
    """ Convert given row and column number to an Excel-style cell name. """
    LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    col+=1
    result = []
    while col:
        col, rem = divmod(col-1, 26)
        result[:0] = LETTERS[rem]
    return ''.join(result)

def create_excel_sheet():
    master_df = retrieve_data()
    master_df.index = master_df.index.strftime('%H:%M')
    COLUMN_NAMES_HEADER_EXCEL_INVERTER = ["INVERTER {i}".format(i=i.split('_')[1]) for i in list(master_df.columns)[:-1]]
    COLUMN_NAMES_HEADER_EXCEL_INVERTER = list(dict.fromkeys(COLUMN_NAMES_HEADER_EXCEL_INVERTER))
    # hard coding of the flip of inverter 17 and 18 in the columns
    COLUMN_NAMES_HEADER_EXCEL_MFM = ['Billing MFM']


    # create workbook and worksheet
    workbook = xlsxwriter.Workbook(excel_path_write)
    worksheet = workbook.add_worksheet()

    

    #SET UP FRAME FOR SHEET
        # SET UP FORMATS
    bold = workbook.add_format({'bold': True,
        'align': 'center'
    })
    num_format =workbook.add_format({'num_format': '#,###0.00'})
    # -------------------------------------- ROW 1----------------------------------------------
    # writing the headers
    worksheet.set_column('A:{end_col}'.format(end_col=excel_style(2*len(COLUMN_NAMES_HEADER_EXCEL_INVERTER)+1)),15)
        # WRITE COLUMN NAMES
    row, col = 0,0 
    worksheet.write("A1", "Date", bold)
    col += 1
    #writing inverter name headers and merging two columns for it
    for name in COLUMN_NAMES_HEADER_EXCEL_INVERTER:
        range_str = "{fc}{r}:{tc}{r}".format(fc=excel_style(col), r=row+1, tc=excel_style(col+1))
        worksheet.merge_range(range_str, name, bold)
        col += 2
    
    # worksheet.set_column('{c}:{ce}'.format(c=excel_style(col), ce = excel_style(col)), 20)
    for name in COLUMN_NAMES_HEADER_EXCEL_MFM:
        worksheet.write("{c}{r}".format(r=row+1,c=excel_style(col)), name, bold)
        col += 1
    
    
    # -------------------------------------- ROW 2 ------------------------------------------
    for column in master_df.columns:
        master_df[column] = master_df[column].apply(lambda x: round(x/1000, 2))
        # master_df[column] = master_df[column].apply(lambda x: "{:,}".format(x))
    row += 1
    col = 0
    worksheet.write("A2", date)
    col += 1
    value_format = workbook.add_format({'align': 'center'})
    for col in range(1,len(COLUMN_NAMES_HEADER_EXCEL_INVERTER)*2):
        worksheet.write("{c}{r}".format(c=excel_style(col),r=row+1), "PAC (kW)" if col%2==1 else "ENERGY (kWh)", value_format)
    start = col+1
    for col in range(start, start+2):
        worksheet.write("{c}{r}".format(c=excel_style(col),r=row+1), "ENERGY (kWh)", value_format)
    #feed values by looping over the data frame
    # #------------------ ROW 3 ONWARDS, FILLING IN THE DATA --------------------------------------- 
    col = 0
    row += 1
    for i in range(master_df.shape[0]):
        worksheet.write('{c}{r}'.format(c=excel_style(col),r=row+1), master_df.index[i])
        col+=1
        for j in range(master_df.shape[1]):
            pos = '{c}{r}'.format(c=excel_style(col), r = row+1)
            col+=1
            worksheet.write(pos,master_df.iloc[i, j])
        col = 0
        row+=1
    for i in range(master_df.shape[1]+1):
        if i == 0:
            worksheet.write(row, i, "Total", value_format)
        else:
            worksheet.write(row, i, master_df.iloc[:, i-1].sum(), num_format)


    workbook.close()

def send_mail(recipients):
     server = smtplib.SMTP("smtp.office365.com")
     server.starttls()
     server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
     msg = MIMEMultipart()
     sender = 'operations@cleantechsolar.com'
     print(date)
     dates = pd.to_datetime(date).strftime("%Y-%m-%d")
     msg['Subject'] = "Station [IN-9099L] MIS Report "+ str(dates)
     #if sender is not None:
     msg['From'] = sender
     msg['To'] = ", ".join(recipients)
     email_body = "Dear Sir,\n\nPlease find the MIS Report for "+str(dates)+".\n\nRegards,\nTeam Cleantech\n "
     attachment_path_list = [excel_path_write]
     if attachment_path_list is not None:
         for each_file_path in attachment_path_list:
             # try:
             file_name = each_file_path.split("/")[-1]
             part = MIMEBase('application', "octet-stream")
             part.set_payload(open(each_file_path, "rb").read())
             encoders.encode_base64(part)
             part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
             msg.attach(part)
         #   except:
                 # print("could not attache file")

     msg.attach(MIMEText(email_body))
     server.sendmail(sender, recipients, msg.as_string())

if __name__ == "__main__":
    create_excel_sheet()
    send_mail(recipients)
