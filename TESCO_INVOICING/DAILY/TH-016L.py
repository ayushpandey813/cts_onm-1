import os
import glob
import csv
import pandas as pd
import numpy as np
from numpy import *
import statistics
import math
import smtplib
import email.message
from datetime import date
from datetime import timedelta 
import time

tod = date.today()
stn = '[TH-016L]'
filename =  stn +'_'+ (tod - timedelta(days = 1)).strftime("%Y%m%d")+ '.csv'

yesterday = (tod - timedelta(days = 1)).strftime("%Y-%m-%d")
year = (tod - timedelta(days = 1)).year
month = (tod - timedelta(days = 1)).strftime("%m")
loc = str(year)+'/'+str(year)+'-'+str(month)

path_MFM1='/home/admin/Dropbox/Gen 1 Data/'+stn+'/'+loc+'/MFM_1_PV Meter/'+stn+'-MFM1-'+yesterday+'.txt'
df_MFM1=pd.DataFrame(columns=['ts','TotWh_max','TotWhExp_max','TotWhImp_max','TotWhNet_max'])
df=pd.read_csv(path_MFM1,sep='\t')
df['TotWh_max'] = df['TotWh_max']/1000
df['TotWhExp_max'] = df['TotWhExp_max']/1000
df['TotWhImp_max'] = df['TotWhImp_max']/1000
df['TotWhNet_max'] = df['TotWhNet_max']/1000
df_MFM1=df_MFM1.append(df[['ts','TotWh_max','TotWhExp_max','TotWhImp_max','TotWhNet_max']],sort=False)
df_MFM1.rename(columns={'ts':'Date','TotWh_max': 'Kamphaeng Saen - PV Meter - Measured AC Total Real Energy','TotWhExp_max': 'Kamphaeng Saen - PV Meter - Measured AC Total Exported Real Energy', 'TotWhImp_max': 'Kamphaeng Saen - PV Meter - Measured AC Total Imported Real Energy', 'TotWhNet_max' : 'Kamphaeng Saen - PV Meter - Measured AC Total Net Energy'}, inplace=True)


path_MFM2='/home/admin/Dropbox/Gen 1 Data/'+stn+'/'+loc+'/MFM_2_Grid MFM/'+stn+'-MFM2-'+yesterday+'.txt'
df_MFM2=pd.DataFrame(columns=['TotWhExp_max','TotWhImp_max','TotWhNet_max'])
df=pd.read_csv(path_MFM2,sep='\t')
df['TotWhExp_max'] = df['TotWhExp_max']/1000
df['TotWhImp_max'] = df['TotWhImp_max']/1000
df['TotWhNet_max'] = df['TotWhNet_max']/1000
df_MFM2=df_MFM2.append(df[['TotWhExp_max','TotWhImp_max','TotWhNet_max']],sort=False)
df_MFM2.rename(columns={'TotWhExp_max': 'Kamphaeng Saen - Grid meter - Measured AC Total Exported Real Energy', 'TotWhImp_max': 'Kamphaeng Saen - Grid meter - Measured AC Total Imported Real Energy', 'TotWhNet_max' : 'Kamphaeng Saen - Grid meter - Measured AC Total Net Energy'}, inplace=True)


path_WMS1='/home/admin/Dropbox/Gen 1 Data/'+stn+'/'+loc+'/WMS_1_Reference Cell 01/'+stn+'-WMS1-'+yesterday+'.txt'
df_WMS1=pd.DataFrame(columns=['TmpBOM_avg','POAI_avg'])
df=pd.read_csv(path_WMS1,sep='\t')
df_WMS1=df_WMS1.append(df[['TmpBOM_avg','POAI_avg']],sort=False)
df_WMS1.rename(columns={'TmpBOM_avg': 'Kamphaeng Saen - Reference Cell 01 - Measured Temperature Panel', 'POAI_avg': 'Kamphaeng Saen - Reference Cell 01 - Measured Irradiance Plane of Array'}, inplace=True)


path_WMS2='/home/admin/Dropbox/Gen 1 Data/'+stn+'/'+loc+'/WMS_2_Reference Cell 02/'+stn+'-WMS2-'+yesterday+'.txt'
df_WMS2=pd.DataFrame(columns=['TmpBOM_avg','POAI_avg'])
df=pd.read_csv(path_WMS2,sep='\t')
df_WMS2=df_WMS2.append(df[['TmpBOM_avg','POAI_avg']],sort=False)
df_WMS2.rename(columns={'TmpBOM_avg': 'Kamphaeng Saen - Reference Cell 02 - Measured Temperature Panel', 'POAI_avg': 'Kamphaeng Saen - Reference Cell 02 - Measured Irradiance Plane of Array'}, inplace=True)


path_WMS3='/home/admin/Dropbox/Gen 1 Data/'+stn+'/'+loc+'/WMS_5_Pyranometer/'+stn+'-WMS5-'+yesterday+'.txt'
df_WMS3=pd.DataFrame(columns=['OTI_avg'])
df=pd.read_csv(path_WMS3,sep='\t')
df_WMS3=df_WMS3.append(df[['OTI_avg']],sort=False)
df_WMS3.rename(columns={'OTI_avg': 'Kamphaeng Saen - Pyranometer - Measured Irradiance Other'}, inplace=True)

path_WMS4='/home/admin/Dropbox/Gen 1 Data/'+stn+'/'+loc+'/WMS_3_Temperature Sensor 01/'+stn+'-WMS3-'+yesterday+'.txt'
df_WMS4=pd.DataFrame(columns=['TmpBOM_avg'])
df=pd.read_csv(path_WMS4,sep='\t')
df_WMS4=df_WMS4.append(df[['TmpBOM_avg']],sort=False)
df_WMS4.rename(columns={'TmpBOM_avg': 'Kamphaeng Saen - Temperature Sensor 01 - Measured Temperature Panel'}, inplace=True)


path_WMS5='/home/admin/Dropbox/Gen 1 Data/'+stn+'/'+loc+'/WMS_4_Temperature Sensor 02/'+stn+'-WMS4-'+yesterday+'.txt'
df_WMS5=pd.DataFrame(columns=['TmpBOM_avg'])
df=pd.read_csv(path_WMS5,sep='\t')
df_WMS5=df_WMS5.append(df[['TmpBOM_avg']],sort=False)
df_WMS5.rename(columns={'TmpBOM_avg': 'Kamphaeng Saen - Temperature Sensor 02 - Measured Temperature Panel'}, inplace=True)


result = pd.concat([df_MFM1, df_MFM2, df_WMS1, df_WMS2, df_WMS3, df_WMS4, df_WMS5], axis=1, sort=False)
result.to_csv('/home/admin/Dropbox/Customer/TESCO_Invoicing/DAILY/'+stn+'/'+str(year)+'/'+filename, index=False)
print(result.columns)