import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
import math
import numpy as np
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import logging
import pyodbc
import sharepy
tz = pytz.timezone('Asia/Calcutta')


user = 'admin'
start = "2021-04-23"
recipients = ['operationsMY@cleantechsolar.com','om-interns@cleantechsolar.com','arnaud.ayral@cleantechsolar.com','lucas.ferrand@cleantechsolar.com','om-it-digest@cleantechsolar.com']

logging.basicConfig(level=logging.INFO, format='%(message)s')
logger = logging.getLogger()
logger.addHandler(logging.FileHandler('/home/' + user + '/Logs/LogsMY000LMaill.txt', 'w'))
print = logger.info


def send_mail(date, info, recipients, attachment_path_list=None):
    server = smtplib.SMTP("smtp.office365.com")
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    msg = MIMEMultipart()
    sender = 'operations@cleantechsolar.com'
    msg['Subject'] = "Shell Malaysia - MASTER DIGEST - " + str(date)
    #if sender is not None:
    msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
            try:
                file_name = each_file_path.split("/")[-1]
                part = MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())
                Encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
            except:
                print("could not attache file")
    msg.attach(MIMEText(info,'plain/text'))
    server.sendmail(sender, recipients, msg.as_string())


#Digest Start
startpath = '/home/' + user + '/Start/MasterMail/'
if(os.path.exists(startpath + 'MY-000L' + "_Mail.txt")):
    print('Exists')
else:
    with open(startpath + 'MY-000L' + "_Mail.txt", "w") as file:
        timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
        file.write(timenow + "\n" + start)
    
with open(startpath + 'MY-000L' + "_Mail.txt") as f:
    startdate = f.readlines()[1].strip()
startdate = datetime.datetime.strptime(startdate, "%Y-%m-%d")


def process(date):
  #Authentication means for SP
  username = 'test-account@cleantechsolar.com'
  password = 'Cleantechsolarpv@123'
  base_path = 'https://cleantechenergycorp.sharepoint.com'
  auth = sharepy.connect(base_path, username = username, password = password)
  
  #Authentication means for Azure
  server = 'cleantechsolar.database.windows.net'
  database = 'Cleantech Meter Readings'
  username = 'RohanKN'
  password = 'R@h@nKN1'
  driver= '{ODBC Driver 17 for SQL Server}'
  connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
  ALL_SITES = pd.read_sql_query("SELECT * FROM [dbo].[Stations] where (Station_Name LIKE 'MY-4%' OR Station_Name LIKE 'MY-5%') ORDER BY Station_Name", connStr)
  
  #Fetching Site O&M Codes
  sites = list(ALL_SITES['Station_Name'])
  for i in range(len(sites)):
     sites[i] = sites[i].strip() + 'L'
  
  #Fetching MFM Folder Name
  temp = list(ALL_SITES['Station_Columns'])
  sites_mfm = []
  for i in temp:
    temp_name = ' '.join(i.strip().split(',')[2].replace('"', '').split('.')[:-1])
    sites_mfm.append(temp_name)
    
  #Fetching Irradiation Source
  sites_irr_source = list(ALL_SITES['Station_Irradiation_Center'])
  for i in range(len(sites_irr_source)):
     if sites_irr_source[i] is None:
       sites_irr_source[i] = sites[i]
     else:
       sites_irr_source[i] = sites_irr_source[i].strip() + 'L'
     
  #Fetching WMS Folder Name
  temp = list(ALL_SITES['Station_Columns'])
  sites_wms = []
  for i in temp:
    temp_name = ' '.join(i.strip().split(',')[1].replace('"', '').split('.')[:-1])
    sites_wms.append(temp_name)
  
  #Fetching Sites Capacity
  ALL_SITES_CAP = pd.read_sql_query("SELECT [Capacity] FROM [dbo].[Meters] where Reference LIKE 'MY-4%' OR Reference LIKE 'MY-5%' ORDER BY Reference", connStr)
  sites_cap = list(ALL_SITES_CAP['Capacity'])
  
  #Fetching Site Names from Shell Tracker Excel
  filedownload1 = 'https://cleantechenergycorp.sharepoint.com/sites/CSDC-OMMY/Shared%20Documents/MY-400%20Shell-MY%20Retail/Shell MY-Retail Details.xlsx'
  fileread1 = '/home/' + user + '/Dropbox/Customer/Shell_Invoicing/Shell MY-Retail Details_Raw.xlsx'
  response1 = auth.getfile(filedownload1, filename = fileread1)
  temp = pd.read_excel(fileread1,engine='openpyxl')
  temp = temp[['System Code O&M', 'Name ']]
  temp = temp.dropna(subset=['System Code O&M'])
  temp = temp.dropna(subset=['Name '])
  
  sites_name=[]
  for i in range(len(temp['System Code O&M'])):
    if str(temp['System Code O&M'][i]) + 'L' in sites:
      sites_name.append(temp['Name '][i].strip())

  yields = []
  irradiations = []
  prs = []
  totalgeneration = 0
  nogeneration = 0
  body = ""
  
  for i in range(len(sites)):
      mfm_filename = '[' + sites[i] +']-'+ sites_mfm[i][:3] + sites_mfm[i][4] + '-'+ str(date) + '.txt'
      wms_filename = '[' + sites_irr_source[i] +']-'+ sites_wms[i][:3] + sites_wms[i][4] + '-'+ str(date) + '.txt'
      mfm_path = '/home/admin/Dropbox/Second Gen/[' + sites[i] + ']/' + str(date[:4]) + '/' + str(date[:7]) + '/' + sites_mfm[i] + '/' + mfm_filename
      wms_path = '/home/admin/Dropbox/Second Gen/[' + sites_irr_source[i] + ']/' + str(date[:4]) + '/' + str(date[:7]) + '/' + sites_wms[i] + '/' + wms_filename
      data = [float('nan')]*6
      if os.path.exists(mfm_path):
        df = pd.read_csv(mfm_path,sep='\t')
        data[0] = df['Eac2'][0]
        data[1] = df['Yld2'][0]
        data[2] = df['PR2'][0]
        data[3] = df['LastRead'][0]
        data[5] = df['LastTime'][0]
      else:
        print(sites[i] + " MFM File Missing")
      
      if os.path.exists(wms_path):
        df = pd.read_csv(wms_path,sep='\t')
        try:
            data[4] = df['GTI'][0]
        except:
            data[4] = df['GHI'][0]
      else:
        print(sites[i] + " WMS File Missing")
      
      if math.isnan(data[0]) == False:
        totalgeneration += data[0]
        yields.append(data[1])
        if math.isnan(data[4]) == False:
            irradiations.append(data[4])
        if math.isnan(data[2]) == False:
              prs.append(data[2])
        else:
            prs.append(0)
      else:
          nogeneration += 1
          
      body += "______________________________________________\n\n" + str(sites[i]) + " - " + sites_name[i] + "\n______________________________________________\n\n"
      body += "Installed Capacity [kWp]: " + str(sites_cap[i]) + "\n\n"
      body += "Energy Generated [kWh]: " + str(data[0])+ "\n\n"
      body += "Yield [kWh/kWp]: " + str(data[1]) + "\n\n"
      body += "Performance Ratio [%]: " + str(data[2]) + "\n\n"
      if math.isnan(data[3]) == False:
          body += "Last Meter Reading [kWh]: "+ str(round(data[3], 1))+"\n\n"
      else:
          body += "Last Meter Reading [kWh]: "+ str(data[3])+"\n\n"
      body += "Last Recorded Time: "+ str(data[5])+"\n\n"
      body += "Irradiation [kWh/m2]: " + str(data[4]) + "\n\n"
    
  temp_body = ''
  temp_body += "SHELL Malaysia Master Digest \n______________________________________________\n\n"
  temp_body += "Total Installed Capacity [kWp]: " + str(round(sum(sites_cap),2)) + "\n\n"
  temp_body += "Total Sites Running: " + str(len(sites)) + "\n\n"
  temp_body += "Number of Sites with Irradiance Sensor: " + str(len(set(sites_irr_source))) + '\n\n'
  temp_body += "Total Energy Generated [kWh]: " + str(round(totalgeneration,1)) + '\n\n'
  temp_body += "Total Yield [kWh/kWp]: " + str(round(totalgeneration/sum(sites_cap),2)) + "\n\n"
  std=np.std(yields)
  cov=(std*100/np.mean(yields))
  temp_body += "CoV Yields [%]: " + str(round(cov,1)) +"\n\n"
  temp_body += "Average Irradiation at sites with Sensors [kWh/m2]: " + str(round(np.mean(irradiations),1)) + '\n\n'
  std=np.std(irradiations)
  cov=(std*100/np.mean(irradiations))
  temp_body += "CoV Irradiation [%]: " + str(round(cov,1)) + "\n\n"
  temp_body += "Estimated Portfolio PR [%]: " + str(round((totalgeneration*100/sum(sites_cap))/np.mean(irradiations),1)) + '\n\n'
  temp_body += "Number of Sites with PR > 80 %: " + str(sum(1 for i in prs if i > 80)) + '\n\n'
  temp_body += "Number of Sites with 70 < PR < 80 %: " + str(sum(1 for i in prs if i <= 80 and i > 70)) + '\n\n'
  temp_body += "Number of Sites with PR < 70 %: " + str(sum(1 for i in prs if i <= 70)) + '\n\n'
  temp_body += "Number of Sites with No Generation: " + str(nogeneration) + '\n\n'
  
  body = temp_body + body
  
  return body
  
#Historical
print('Historical Started!')
startdate = startdate + datetime.timedelta(days = 1)
while((datetime.datetime.now(tz).date() - startdate.date()).days>0):
  print('Processing Date: ' + str(startdate)[:10])
  processed_data = process(str(startdate)[:10])
  send_mail(str(startdate)[:10], processed_data, recipients)
  startdate = startdate + datetime.timedelta(days = 1)
startdate = startdate + datetime.timedelta(days = -1)
with open(startpath + "MY-000L_Mail.txt", "w") as file:
  timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S') 
  file.write(timenow + "\n" + (startdate).strftime('%Y-%m-%d'))  

while(1):
  try:
    date = (datetime.datetime.now(tz)).strftime('%Y-%m-%d')
    timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    with open(startpath + 'MY-000L_Bot.txt', "w") as file:
      file.write(timenow)  
    processed_data = process(date)
    print('Done Processing')
    if(datetime.datetime.now(tz).hour == 2 and (datetime.datetime.now(tz).date()-startdate.date()).days>1):
      print('Sending')
      date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
      processed_data = process(date_yesterday)
      send_mail(date_yesterday, processed_data, recipients)
      with open(startpath + "MY-000L_Mail.txt", "w") as file:
          file.write(timenow + "\n" + date_yesterday)   
      startdate= datetime.datetime.strptime(date_yesterday,'%Y-%m-%d')
    print('Sleeping')
  except:
    print('error')
    logging.exception('msg')
  time.sleep(300)