import pandas as pd
import sharepy
import datetime
import pytz
import pyodbc
import os
import time
import math
import pymsteams
from os import listdir
from os.path import isfile, join

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

def get_sp_files(auth):
    #Pushing file to sharepoint
    link = base_path + site_name + "_api/web/GetFolderByServerRelativeUrl('" + doc_libary + "')?$expand=Folders,Files"
    sp_data = auth.get(link)
    
    data = sp_data.json()
    df = pd.DataFrame(sp_data.json())
    folders  = df.loc[df.index=='Folders','d'].values[0]
    sp_files  = []
    for i in folders['results']:
        link = base_path + site_name + "_api/web/GetFolderByServerRelativeUrl('" + doc_libary +'/'+i['Name']+"')?$expand=Folders,Files"
        sp_data = auth.get(link)
        
        data = sp_data.json()
        df = pd.DataFrame(sp_data.json())
        folders_month  = df.loc[df.index=='Folders','d'].values[0]
        for j in folders_month['results']:
            link = base_path + site_name + "_api/web/GetFolderByServerRelativeUrl('" + doc_libary+'/'+i['Name'] +'/'+j['Name']+"')?$expand=Folders,Files"
            sp_data = auth.get(link)

            data = sp_data.json()
            df = pd.DataFrame(sp_data.json())
            files = df.loc[df.index=='Files','d'].values[0]
            temp_files = [k['Name'] for k in files['results']]
            sp_files = sp_files + temp_files
    return sp_files
            
def get_server_files():
    server_files = []
    for i in os.listdir(write_path):
        for j in os.listdir(write_path+'/'+i):
            temp_files = [f for f in listdir(write_path+'/'+i+'/'+j) if isfile(join(write_path+'/'+i+'/'+j, f))]
            server_files = server_files+temp_files
    return server_files

def download_file(auth,file_name):
    #file_link = base_path + site_name + "_api/web/GetFolderByServerRelativeUrl('" + doc_libary+'/'+file_name[6:10] +'/'+file_name[6:13]+"')?$expand=Folders,Files"
    #file_link = 'https://cleantechenergycorp.sharepoint.com/OM/SERIS%20Raw%20Data/1min/%5B729%5D/2021/2021-04/%5B729%5D%202021-04-03.txt'
    file_link = base_path + site_name + "_api/web/GetFolderByServerRelativeUrl('" + doc_libary+'/'+file_name[6:10] +'/'+file_name[6:13]+"')/Files('"+file_name+"')/$value"
    print(file_link)
    file_write = write_path+'/'+file_name[6:10] +'/'+file_name[6:13]+'/'+file_name 
    chkdir('/'.join(file_write.split('/')[:-1]))
    print(write_path)
    files_data = auth.getfile(file_link, filename = file_write)
    print(files_data.status_code)
 
seris_sites = ['711','712','713','714','715','716','717','718','719','720','721','722','723','724','725','726','727','728','729']

#Initializing variables
username = 'test-account@cleantechsolar.com'
password = 'Cleantechsolarpv@123'
site_name = "/OM/"
base_path = 'https://cleantechenergycorp.sharepoint.com'

def recheck(site,auth):
    for i in range(7):
        date = datetime.datetime.now().date()+datetime.timedelta(days=-i)
        file_name = '['+site+'] '+str(date)+'.txt'
        download_file(auth,file_name)



while(1):
    auth = sharepy.connect(base_path, username = username, password = password)
    for site in seris_sites:
        try:
            doc_libary = "SERIS Raw Data/1min/["+site+"]"
            write_path = '/home/admin/Dropbox/SerisData/1min/['+site+']'   
            server_files = get_server_files()
            sp_files = get_sp_files(auth)
            new_file = list(set(sp_files) - set(server_files))
            print(new_file)
            for i in new_file:
                download_file(auth,i)
            recheck(site,auth)
        except:
            pass
    time.sleep(3600)
