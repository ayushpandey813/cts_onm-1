rm(list = ls(all = T))

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
prepareSumm = function(dataread)
{
  da = nrow(dataread)
  thresh = 5
  gsi1 = sum(dataread[complete.cases(dataread[,3]),3])/60000
  gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
  gsismp = sum(dataread[complete.cases(dataread[,5]),5])/60000
  subdata = dataread[complete.cases(dataread[,3]),]
  subdata = subdata[as.numeric(subdata[,3]) > thresh,]
  tamb = mean(dataread[complete.cases(dataread[,10]),10])
  tambst = mean(subdata[,10])
  
  hamb = mean(dataread[complete.cases(dataread[,6]),6])
  hambst = mean(subdata[,6])
  
  tambmx = max(dataread[complete.cases(dataread[,10]),10])
  tambstmx = max(subdata[,10])
  
  hambmx = max(dataread[complete.cases(dataread[,6]),6])
  hambstmx = max(subdata[,6])
  
  tambmn = min(dataread[complete.cases(dataread[,10]),10])
  tambstmn = min(subdata[,10])
  
  hambmn = min(dataread[complete.cases(dataread[,6]),6])
  hambstmn = min(subdata[,6])
  
  tsi1 = mean(dataread[complete.cases(dataread[,3]),8])
  tsi2 = mean(dataread[complete.cases(dataread[,3]),9])
  
  gsirat = gsi1 / gsi2
  smprat = gsismp / gsi2
  daPerc = da/14.40

  datawrite = data.frame(Date = substr(dataread[1,1],1,10),PtsRec = rf(da),Gsi01 = rf(gsi1), Gsi02 = rf(gsi2),Smp = rf(gsismp),
                         Tamb = rf(tamb), TambSH = rf(tambst),TambMx = rf(tambmx), TambMn = rf(tambmn),
                         TambSHmx = rf(tambstmx), TambSHmn = rf(tambstmn), Hamb = rf(hamb), HambSH = rf(hambst),
                         HambMx = rf(hambmx), HambMn = rf(hambmn), HambSHmx = rf(hambstmx), HambSHmn = rf(hambstmn),
                         Tsi01 = rf(tsi1), Tsi02= rf(tsi2), GsiRat = rf(gsirat), SpmRat = rf(smprat),DA=rf(daPerc))
  datawrite
}
rewriteSumm = function(datawrite)
{
  
  df = data.frame(Date = as.character(datawrite[1,1]),Gsi01 = as.character(datawrite[1,3]),Gsi02 = as.character(datawrite[1,4]),Smp = as.character(datawrite[1,5]),
             Tamb = as.character(datawrite[1,6]),TambSH = as.character(datawrite[1,7]),Hamb = as.character(datawrite[1,12]),HambSH = as.character(datawrite[,13]),
						 DA=as.character(datawrite[1,21]),stringsAsFactors=F)
  df
}

path = "/home/admin/Dropbox/SerisData/1min/[711]"
pathwrite = "/home/admin/Dropbox/Second Gen/[IN-711S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[IN-711S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread)
      datasum = rewriteSumm(datawrite)
			currdayw = gsub("711","IN-711S",days[z])
			
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
