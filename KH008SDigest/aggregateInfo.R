source('/home/admin/CODE/common/aggregate.R')

registerMeterList("KH-008S",c("Pond","R02","R03","R06","R11","InSameFile"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 35 #Column no for DA
aggColTemplate[3] = 42 #column for LastRead
aggColTemplate[4] = 43 #column for LastTime
aggColTemplate[5] = 36 #column for Eac-1
aggColTemplate[6] = 37 #column for Eac-2
aggColTemplate[7] = 38 #column for Yld-1
aggColTemplate[8] = 39 #column for Yld-2
aggColTemplate[9] = 40 #column for PR-1
aggColTemplate[10] = 41 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("KH-008S","Pond",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 35 #Column no for DA
aggColTemplate[3] = 50 #column for LastRead
aggColTemplate[4] = 51 #column for LastTime
aggColTemplate[5] = 44 #column for Eac-1
aggColTemplate[6] = 45 #column for Eac-2
aggColTemplate[7] = 46 #column for Yld-1
aggColTemplate[8] = 47 #column for Yld-2
aggColTemplate[9] = 48 #column for PR-1
aggColTemplate[10] = 49 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("KH-008S","R02",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 35 #Column no for DA
aggColTemplate[3] = 58 #column for LastRead
aggColTemplate[4] = 59 #column for LastTime
aggColTemplate[5] = 52 #column for Eac-1
aggColTemplate[6] = 43 #column for Eac-2
aggColTemplate[7] = 54 #column for Yld-1
aggColTemplate[8] = 55 #column for Yld-2
aggColTemplate[9] = 56 #column for PR-1
aggColTemplate[10] = 57 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("KH-008S","R03",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 35 #Column no for DA
aggColTemplate[3] = 66 #column for LastRead
aggColTemplate[4] = 67 #column for LastTime
aggColTemplate[5] = 60 #column for Eac-1
aggColTemplate[6] = 61 #column for Eac-2
aggColTemplate[7] = 62 #column for Yld-1
aggColTemplate[8] = 63 #column for Yld-2
aggColTemplate[9] = 64 #column for PR-1
aggColTemplate[10] = 65 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("KH-008S","R06",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 35 #Column no for DA
aggColTemplate[3] = 74 #column for LastRead
aggColTemplate[4] = 75 #column for LastTime
aggColTemplate[5] = 68 #column for Eac-1
aggColTemplate[6] = 69 #column for Eac-2
aggColTemplate[7] = 70 #column for Yld-1
aggColTemplate[8] = 71 #column for Yld-2
aggColTemplate[9] = 72 #column for PR-1
aggColTemplate[10] = 73 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("KH-008S","R11",aggNameTemplate,aggColTemplate)

