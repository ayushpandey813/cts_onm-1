rm(list=ls())
require(lubridate)
require(ggplot2)
require(scales)
require(dplyr)
library(zoo)
###################declare constants######################
#file path
ROOT_PATH = "/home/admin/Dropbox/Second Gen/[KH-008S]"
SAVE_PATH = "/home/admin/Jason/cec intern/results/KH008S"
SITE_NAME = "KH-008S"

#site details
POND_SIZE = 2835.32
R02_SIZE = 2249.98
R03_SIZE = 2854.80
R06_SIZE = 495.95
R11_SIZE = 1397.50
TOTAL_SIZE =  POND_SIZE + R02_SIZE + R03_SIZE + R06_SIZE + R11_SIZE
BUSINESS_PLAN_PR = 78.0

#graph items position constants
XPOS_BIZ_PR = 1
YPOS_BIZ_PR = 100
XPOS_METER = 150
YPOS_METER = 17.5
XPOS_AVE = 250
YPOS_AVE = 15
XPOS_LEGEND = 50
YPOS_LEGEND = 20

###########data cleaning + summarisation##################
#get file from all directory
yearFiles = list.files(path = ROOT_PATH, all.files = F, full.names = T, recursive = T)
remove_files = sapply(yearFiles, function(i) nchar(i) == (nchar(ROOT_PATH)+32))
yearFiles = yearFiles[remove_files]

data = read.csv(yearFiles[1], header=T, sep="\t", stringsAsFactors = F)

#store all daily GSI, PR, and EAC values in final_data
for (i in yearFiles[2:length(yearFiles)]) {
  temp_data = read.csv(i, header=T, sep="\t", stringsAsFactors = F)
  data = rbind(data, temp_data)
}

data$Date = as.Date(data$Date)





#removing bad PR values
bad_data = filter(data, (Gsi01 <= 0)  | (PR2R02 > 100) | (PR2R03 > 100) | (PR2R06 > 100) | (PR2R11 > 100)  | (PR2R02 <= 0) | (PR2R03 <= 0) | (PR2R06 <= 0) | (PR2R11 <= 0))
good_data = filter(data, (Gsi01 > 0) &   (PR2R02 < 100) & (PR2R03 < 100) & (PR2R06 < 100) & (PR2R11 < 100)   & (PR2R02 > 0) & (PR2R03 > 0) & (PR2R06 > 0) & (PR2R11 > 0))

bad_data1 = filter(data, (Gsi01 <= 0) | (PR2Pond > 100) | (PR2R02 > 100) | (PR2R03 > 100) | (PR2R06 > 100) | (PR2R11 > 100) | (PR2Pond <= 0) | (PR2R02 <= 0) | (PR2R03 <= 0) | (PR2R06 <= 0) | (PR2R11 <= 0))
good_data1 = filter(data, (Gsi01 > 0) & (PR2Pond < 100) & (PR2R02 < 100) & (PR2R03 < 100) & (PR2R06 < 100) & (PR2R11 < 100) & (PR2Pond > 0) & (PR2R02 > 0) & (PR2R03 > 0) & (PR2R06 > 0) & (PR2R11 > 0))
#good_data$PR2Full[is.na(good_data$PR2Full)] <- 0
for(i in 415:485)
{
good_data$PR2Full[i] = (good_data$PR2R02[i] + good_data$PR2R03[i] + good_data$PR2R06[i] + good_data$PR2R11[i])/5
#print(good_data$PR2Full[i])
}
test = good_data[1:length(good_data$PR2Full),]
test1 = good_data1[1:length(good_data1$PR2Full),]



test$PR2Pond[is.na(test$PR2Pond)] <- 0

options(scipen = 999)
MA <- round(rollmean(test$PR2Pond, k = 30, fill = NA),2)
cbind(test, MA)

PR2Pond_ave = mean(test$PR2Pond)

PR2R02_ave = mean(test$PR2R02)
PR2R03_ave = mean(test$PR2R03)
PR2R06_ave = mean(test$PR2R06)
PR2R11_ave = mean(test$PR2R11)
n = nrow(test)
for(i in 1:278)
{
test$PRB[i] = 77.9
}
for(i in 278:521)
{
test$PRB[i] = 77.3
}
count = 0
for(i in 1:521)
if(test$PR2Pond[i] > test$PRB[i])
{
count = count+1
}


ave_30_day = (sum(test$PR2Pond[(length(test$PR2Pond)-29):length(test$PR2Pond)]))/30
ave_60_day = (sum(test$PR2Pond[(length(test$PR2Pond)-59):length(test$PR2Pond)]))/60
ave_90_day = (sum(test$PR2Pond[(length(test$PR2Pond)-89):length(test$PR2Pond)]))/90
ave_lifetime = (sum(test$PR2Pond[1:length(test$PR2Pond)]))/(length(test$PR2Pond))

#text for plots
data_duration = paste("From", test$Date[1],"to", test$Date[length(test$Date)])

R03_size_text = paste("R03: ", R03_SIZE, "kWp [", sprintf("%0.1f", round((100*R03_SIZE/TOTAL_SIZE), digits=1)), "%]", sep="")
Pond_size_text = paste("Pond: ", POND_SIZE, "kWp [", sprintf("%0.1f", round((100*POND_SIZE/TOTAL_SIZE), digits=1)), "%]", sep="")
R02_size_text = paste("R02: ", R02_SIZE, "kWp [", sprintf("%0.1f", round((100*R02_SIZE/TOTAL_SIZE), digits=1)), "%]", sep="")
R11_size_text = paste("R11: ", R11_SIZE, "kWp [", sprintf("%0.1f", round((100*R11_SIZE/TOTAL_SIZE), digits=1)), "%]", sep="")
R06_size_text = paste("R06: ", R06_SIZE, "kWp [", sprintf("%0.1f", round((100*R06_SIZE/TOTAL_SIZE), digits=1)), "%]", sep="")
Total_size_text = paste("Total: ", TOTAL_SIZE, "kWp\t[", sprintf("%0.1f", round((100*TOTAL_SIZE/TOTAL_SIZE), digits=1)), "%]", sep="")

ave_30_text = paste("30-day Average:   ", sprintf("%0.1f", round(ave_30_day, digits=1)), "%", sep="")
ave_60_text = paste("60-day Average:   ", sprintf("%0.1f", round(ave_60_day, digits=1)), "%", sep="")
ave_90_text = paste("90-day Average:   ", sprintf("%0.1f", round(ave_90_day, digits=1)), "%", sep="")
ave_lt_text = paste("Lifetime Average:  ", sprintf("%0.1f", round(ave_lifetime, digits=1)), "%", sep="")
#PR_budget =  paste("Number of Points above PR budget line:  ", sprintf(round(count, digits=1)), "%", sep="")

Pond_legend = paste("Pond [Average = ", sprintf("%0.1f", round(PR2Pond_ave, digits=1)), "%]", sep="")
R02_legend = paste("R02 [Average = ", sprintf("%0.1f", round(PR2R02_ave, digits=1)), "%]", sep="")
R03_legend = paste("R03 [Average = ", sprintf("%0.1f", round(PR2R03_ave, digits=1)), "%]", sep="")
R06_legend = paste("R06 [Average = ", sprintf("%0.1f", round(PR2R06_ave, digits=1)), "%]", sep="")
R11_legend = paste("R11 [Average = ", sprintf("%0.1f", round(PR2R11_ave, digits=1)), "%]", sep="")

#highlight latest total PR value
highlight_point = filter(test, Date==test$Date[length(test$Date)])

#points for legend
legend_Pond = data.frame(xname = test$Date[length(test$Date)-XPOS_LEGEND-10], ypos = YPOS_LEGEND)
legend_R02 = data.frame(xname = test$Date[length(test$Date)-XPOS_LEGEND-10], ypos = YPOS_LEGEND-4)
legend_R03 = data.frame(xname = test$Date[length(test$Date)-XPOS_LEGEND-10], ypos = YPOS_LEGEND-8)
legend_R06 = data.frame(xname = test$Date[length(test$Date)-XPOS_LEGEND-10], ypos = YPOS_LEGEND-12)
legend_R11 = data.frame(xname = test$Date[length(test$Date)-XPOS_LEGEND-10], ypos = YPOS_LEGEND-16)

################create the graph#################

min <- as.Date("2019-04-01")
max <- NA
test$PR2Pond[is.na(test$PR2Pond)] <- 0
#data sorting for visualisation
test$colour[test[,2] < 2] <- 'blue'
test$colour[test[,2] >= 2 & test[,2] <= 4] <- 'deepskyblue1'
test$colour[test[,2] >= 4 & test[,2] <= 6] <- 'orange'
test$colour[test[,2] > 6] <- 'darkorange3'
#test$colour = factor(test$colour, levels = c("< 2", "2 ~ 4", "4 ~ 6", "> 6"), labels =c('< 2', '2 ~ 4', '4 ~ 6', '> 6')) #to fix legend arrangement
print(test$colour)

graph = ggplot(data=test) + 

  geom_point(aes(x=Date, y=PR2Pond), colour=test$colour, shape=15) + 
  scale_colour_manual('Daily Irradiation [kWh/m2]', values = c("blue", "deepskyblue1", "orange","darkorange3"), labels = c('< 2','2 ~ 4','4 ~ 6','> 6'),                                 guide = guide_legend(title.position = "left", ncol = 4, nrow=1)) +
  geom_line(aes(x=Date, y=MA), colour="#00008b", shape=15, size=1) + 
  geom_line(aes(x=Date, y=PRB), colour="darkgreen", shape=15, size=1) + 
  #geom_line(aes(x=Date, y=prthresh), colour="#darkgreen", shape=15, size=1) + 
  #geom_vline(xintercept=seq(7.08, 7.14, by=0.01),colour="#00008b")+
  #geom_hline(yintercept=seq(50.93, 50.96, by=0.01),colour="#00008b")+
  #geom_point(aes(x=Date, y=PR2R02), colour="#ff0000", shape=16) + 
  #geom_point(aes(x=Date, y=PR2R03), colour="#ffa500", shape=17) + 
  #geom_point(aes(x=Date, y=PR2R06), colour="#000000", shape=18) + 
  #geom_point(aes(x=Date, y=PR2R11), colour="#a020f0", shape=19) + 
  #geom_line(aes(x=Date, y=(sum(test$PR2Pond[(length(test$PR2Pond)-29):length(test$PR2Pond)]))/30), colour="#00008b", size=1) +
  #geom_point(data=highlight_point, aes(x=Date, y=PR2Full), colour="#00008b", shape=4) + 
  #geom_text(data=scale_colour_manual, aes(x=Date, y=PR2Pond, label=sprintf("%0.1f", round(PR2Pond, digits=1))), nudge_x=7, size=3, colour="#00008b") +
  #geom_hline(yintercept=PR2Pond_ave, colour="#63b8ff") + 
  #geom_hline(yintercept=PR2R02_ave, colour="#ff0000") + 
  #geom_hline(yintercept=PR2R03_ave, colour="#ffa500") + 
  #geom_hline(yintercept=PR2R06_ave, colour="#000000") + 
  #geom_hline(yintercept=PR2R11_ave, colour="#a020f0") + 
  #geom_hline(yintercept=BUSINESS_PLAN_PR, colour="#008000", linetype="dashed") +
  theme_classic() + 
  ylim(0, 100) + 
  theme(legend.position = c(35, 850), legend.justification = c(35, 850), axis.title.x=element_blank(),axis.title=element_text(size=15),axis.text=element_text(size=15), plot.title = element_text(hjust = 0.5,face="bold",size=16), plot.subtitle = element_text(hjust = 0.5, size=15), panel.grid.major = element_line(colour = "grey",size=0.5),panel.border=element_rect(color="black", fill=NA, size=1)) + 
  labs(y="Performance Ratio [%]", size = 15) + 
  scale_x_date(date_breaks = "3 months",limits = c(min, max), labels = date_format("%b/%y")) + 
  ggtitle(label=paste("[", SITE_NAME, "] Performance Ratios - POND", sep= ""), subtitle=data_duration) + 
 
  annotate('text', label = "Daily Irradiation [kWh/m2] Mapping", y = 35, x = as.Date(test$Date[length(test$Date)-XPOS_AVE-170]), colour = "black",fontface =2,hjust = 0)+
  annotate(geom="segment", x=test$Date[length(test$Date)-XPOS_AVE-170], xend=test$Date[length(test$Date)-XPOS_AVE-167], y=30, yend=30, colour="blue", size=1.5)+
  annotate(geom="segment", x=test$Date[length(test$Date)-XPOS_AVE-170], xend=test$Date[length(test$Date)-XPOS_AVE-167], y=25, yend=25, colour="deepskyblue1", size=1.5)+
  annotate(geom="segment", x=test$Date[length(test$Date)-XPOS_AVE-170], xend=test$Date[length(test$Date)-XPOS_AVE-167], y=20, yend=20, colour="orange", size=1.5)+
  annotate(geom="segment", x=test$Date[length(test$Date)-XPOS_AVE-170], xend=test$Date[length(test$Date)-XPOS_AVE-167], y=15, yend=15, colour="darkorange3", size=1.5)+
  annotate('text', label = "<2", y = 30, x = as.Date(test$Date[length(test$Date)-XPOS_AVE-165]), colour = "black",fontface =2,hjust = 0)+
  annotate('text', label = "2 ~ 4", y = 25, x = as.Date(test$Date[length(test$Date)-XPOS_AVE-165]), colour = "black",fontface =2,hjust = 0)+
  annotate('text', label = "4 ~ 6", y = 20, x = as.Date(test$Date[length(test$Date)-XPOS_AVE-165]), colour = "black",fontface =2,hjust = 0)+
  annotate('text', label = ">6", y = 15, x = as.Date(test$Date[length(test$Date)-XPOS_AVE-165]), colour = "black",fontface =2,hjust = 0)+
  #annotate("Daily Irradiation [kWh/m2]", values = c("blue", "deepskyblue1", "orange","darkorange3"), labels = c('< 2','2 ~ 4','4 ~ 6','> 6'), x = 4, y = 25, label = "Some text")+
  annotate(geom="segment", x=test$Date[length(test$Date)-XPOS_LEGEND-230], xend=test$Date[length(test$Date)-XPOS_LEGEND-210], y=45, yend=45, colour="darkgreen", size=1.5)+
  annotate('text', label = "Budget PR Line", y = 45, x = as.Date(test$Date[length(test$Date)-XPOS_AVE]), colour = "darkgreen",fontface =2,hjust = 0)+
  annotate(geom="segment", x=test$Date[length(test$Date)-XPOS_LEGEND-230], xend=test$Date[length(test$Date)-XPOS_LEGEND-210], y=35, yend=35, colour="blue", size=1.5)+
  annotate('text', label = "30-d moving average of PR", y = 35, x = as.Date(test$Date[length(test$Date)-XPOS_AVE]), colour = "#00008b",fontface =2,hjust = 0)+
  annotate(geom="text", x=as.Date(test$Date[XPOS_BIZ_PR]), y=YPOS_BIZ_PR, label=paste("Performance Ratio = ", BUSINESS_PLAN_PR, "% (business plan)", sep=""), hjust=0, color="#3A8A0C") +
  annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_AVE]), y=YPOS_AVE, label=ave_30_text, hjust=0, color="#0C3A8A") +
  annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_AVE]), y=YPOS_AVE-2.5, label=ave_60_text, hjust=0, color="#0C3A8A") +
  annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_AVE]), y=YPOS_AVE-5, label=ave_90_text, hjust=0, color="#0C3A8A") +
  annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_AVE]), y=YPOS_AVE-7.5, label=ave_lt_text, hjust=0, color="#0C3A8A") +
  #annotate("text", x = 10, y=20, label = "ship")+
  #annotate("segment", x = as.Date(test[nrow(test$Date)* 0.25,1]), xend = as.Date(test[nrow(test$Date)* 0.28,1]), y=50, yend=50, colour="darkgreen", size=1.5)+
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_METER]), y=YPOS_METER, label="Size of Meters", hjust=0) + 
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_METER]), y=YPOS_METER-2.5, label=R03_size_text, hjust=0) +
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_METER]), y=YPOS_METER-5, label=Pond_size_text, hjust=0) +
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_METER]), y=YPOS_METER-7.5, label=R02_size_text, hjust=0) +
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_METER]), y=YPOS_METER-10, label=R11_size_text, hjust=0) +
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_METER]), y=YPOS_METER-12.5, label=R06_size_text, hjust=0) + 
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_LEGEND]), y=YPOS_LEGEND, label=Pond_legend, hjust=0) +
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_LEGEND]), y=YPOS_LEGEND-4, label=R02_legend, hjust=0) +
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_LEGEND]), y=YPOS_LEGEND-8, label=R03_legend, hjust=0) +
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_LEGEND]), y=YPOS_LEGEND-12, label=R06_legend, hjust=0) +
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_LEGEND]), y=YPOS_LEGEND-16, label=count, hjust=0) + 
  annotate(geom="text", x=as.Date(test$Date[length(test$Date)-XPOS_LEGEND]), y=YPOS_LEGEND-20, label="PR (Total)", hjust=0) +
  geom_point(data=legend_Pond, aes(x=xname, y=ypos), colour="#63b8ff", shape=15) +
  #geom_point(data=legend_R02, aes(x=xname, y=ypos), colour="#ff0000", shape=16) +
  #geom_point(data=legend_R03, aes(x=xname, y=ypos), colour="#ffa500", shape=17) +
  #geom_point(data=legend_R06, aes(x=xname, y=ypos), colour="#000000", shape=18) +
  #geom_point(data=legend_R11, aes(x=xname, y=ypos), colour="#a020f0", shape=19) +
  #scale_colour_manual('Daily Irradiation [kWh/m2]', values = c("blue", "deepskyblue1", "orange","darkorange3"), labels = c('< 2','2 ~ 4','4 ~ 6','> 6'),                                 guide = guide_legend(title.position = "left", ncol = 4, nrow=1)) +
  annotate(geom="segment", x=test$Date[length(test$Date)-XPOS_LEGEND-13], xend=test$Date[length(test$Date)-XPOS_LEGEND-7], y=YPOS_LEGEND-20, yend=YPOS_LEGEND-20, colour="#00008b", size=1.5)
  #scale_shape_manual(values = 18)+
  #guides(colour = guide_legend(override.aes = list(shape = 18)))+
graph


save_name = paste("[", SITE_NAME, "] - POND-", test$Date[length(test$Date)], ".pdf", sep="")
ggsave(save_name, plot=graph, path=SAVE_PATH, device="pdf", width=29.7, height=21, units="cm")
