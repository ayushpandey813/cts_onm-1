rm(list=ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsMY008LMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/MY008LDigest/summaryFunctions.R')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
RESETHISTORICAL=0
daysAlive = 0
reorderStnPaths = c(32,30,31,1,12,23,24,25,26,27,28,29,2,3,4,5,6,7,8,9,10,11,13,14,15,16,17,18,19,20,21,22)

source('/home/admin/CODE/MasterMail/timestamp.R')
require('mailR')
require('lubridate')
source('/home/admin/CODE/MY008LDigest/aggregateInfo.R')

METERNICKNAMES = c("WMS_1","MFM-1","MFM-2","INV-1","INV-2","INV-3","INV-4","INV-5","INV-6","INV-7","INV-8","INV-9","INV-10",
                   "INV-11","INV-12","INV-13","INV-14","INV-15","INV-16","INV-17","INV-18","INV-19","INV-20","INV-21","INV-22","INV-23","INV-24","INV-25","INV-26","INV-27","INV-28","INV-29")

METERACNAMES = c("WMS_1_Reference Cell","MFM_1_PV Meter-01","MFM_2_PV Meter-02","INVERTER_1","INVERTER_2","INVERTER_3","INVERTER_4","INVERTER_5","INVERTER_6","INVERTER_7","INVERTER_8","INVERTER_9","INVERTER_10",
                 "INVERTER_11","INVERTER_12","INVERTER_13","INVERTER_14","INVERTER_15","INVERTER_16","INVERTER_17","INVERTER_18","INVERTER_19","INVERTER_20","INVERTER_21","INVERTER_22","INVERTER_23","INVERTER_24","INVERTER_25","INVERTER_26","INVERTER_27","INVERTER_28","INVERTER_29")

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

COD = as.Date("2021-02-01", format = "%Y-%m-%d")
currDate <- as.Date(format(Sys.time(), "%Y-%m-%d"), format = "%Y-%m-%d")
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = c('operationsMY@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','solarmy@cutechgroup.com')
pwd = 'CTS&*(789'
referenceDays = NA
extractDaysOnly = function(days)
{
	if(length(days!=0))
	{
	seq = seq(from=1,to=length(days)*6,by=6)
	days = unlist(strsplit(days,"-"))
	days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
	return(days)
	}
}
analyseDays = function(days,ref)
{
  if(length(days) == length(ref))
    return(days)
  daysret = unlist(rep(NA,length(ref)))
  if(length(days) == 0)
    return(daysret)
  days2 = extractDaysOnly(days)
  idxmtch = match(days2,ref)
  idxmtch2 = idxmtch[complete.cases(idxmtch)]
  if(length(idxmtch) != length(idxmtch2))
  {
    print(".................Missmatch..................")
    print(days2)
    print("#########")
    print(ref)
    print("............................................")
    return(days)
  }
  if(is.finite(idxmtch))
  {
    daysret[idxmtch] = as.character(days)
    if(!(is.na(daysret[length(daysret)])))
      return(daysret)
  }	
  return(days)
}
performBackWalkCheck = function(day)
{
  path = "/home/admin/Dropbox/Gen 1 Data/[MY-008L]"
  retval = 0
  yr = substr(day,1,4)
  yrmon = substr(day,1,7)
  pathprobe = paste(path,yr,yrmon,sep="/")
  stns = dir(pathprobe)
  print(stns)
  idxday = c()
  for(t in 1 : length(stns))
  {
    pathdays = paste(pathprobe,stns[t],sep="/")
    print(pathdays)
    days = dir(pathdays)
    dayMtch = days[grepl(day,days)]
    if(length(dayMtch))
    {
      idxday[t] = match(dayMtch,days)
    }
    print(paste("Match for",day,"is",idxday[t]))
  }
  idxmtch = match(NA,idxday[t])
  if(length(unique(idxday))==1 || length(idxmtch) < 5)
  {
    print('All days present ')
    retval = 1
  }
  return(retval)
}
sendMail= function(pathall)
{
  path = pathall[1]
  dataread = read.table(path,header = T,sep="\t")
  currday = as.character(dataread[1,1])
  Cur=as.Date(currday)
  filenams = c()
  body = ""
  body = paste(body,"Site Name: Kellogg Asia Products Sdn Bhd",sep="")
  body = paste(body,"\n\nLocation: Lot 1-5, Jalan Teknologi 5, Taman Teknologi Enstek, 71760 Labu, Negeri Sembilan.")
  body = paste(body,"\n\nO&M Code: MY-008")
  body = paste(body,"\n\nSystem Size [kWp]:",sum(INSTCAPM))
  body = paste(body,"\n\nNumber of Energy Meters:",NOMETERS)
  body = paste(body,"\n\nModule Brand / Model / Nos: Jinko Solar / JM325PP W / 5,580")
  body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / Solid Q-50 / 29")
  body = paste(body,"\n\nSite COD:",COD)
  body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(currDate - COD))))
  body = paste(body,"\n\nSystem age [years]:",as.character(round(time_length(difftime(currDate, COD), "years"), 1)))
  bodyac = body
  body = ""
  TOTALGENCALC = 0
  MYLD = c()
  
  acname = METERNICKNAMES
  noInverters = NOINVS
  InvReadings = unlist(rep(NA,noInverters))
  InvAvail = unlist(rep(NA,noInverters))
  for(t in 1 : length(pathall))
  {
    type = 0
    meteridx = t
    if(grepl("WMS",pathall[t])) 
      type = 1
    else if(grepl("MFM",pathall[t]))
      type=1
    else
      type = 0
    if( t > (NOMETERS))
    {
      splitpath = unlist(strsplit(pathall[t],"INVERTER_"))
      if(length(splitpath)>1)
      {
        meteridx = unlist(strsplit(splitpath[2],"/"))
        meteridx = as.numeric(meteridx[1])
      }
    }
    path = pathall[t]
    dataread = read.table(path,header = T,sep="\t")
    currday = as.character(dataread[1,1])
    if(type==0)
    {
      filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx+2],".txt",sep="")
    }
    else if(type==1 && grepl("WMS",pathall[t]))
    {
      filenams[t] = paste(currday,"-",'WMS',".txt",sep="")
    }
    else
    {
      filenams[t] = paste(currday,"-",'MFM',".txt",sep="") 
    }
    print("---------------------")
    print(path)
    print(filenams[t])
    print("---------------------")
    if(type == 1)
    {
      body = paste(body,"\n\n________________________________________________\n\n")
      body = paste(body,currday,acname[t])
      body = paste(body,"\n________________________________________________\n\n")
      body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
      {
        if(grepl("MFM",pathall[t]))
        {
          body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(format(round(dataread[1,3],1), nsmall = 1)),"\n\n")
          body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(format(round(dataread[1,4],1), nsmall = 1)),"\n\n")
          TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
          body = paste(body,"Yield-1 [kWh/kWp]:",as.character(format(round(dataread[1,5],2), nsmall = 2)),"\n\n")
          body = paste(body,"Yield-2 [kWh/kWp]:",as.character(format(round(dataread[1,6],2), nsmall = 2)),"\n\n")
          body = paste(body,"PR-1 (GHI) [%]:",as.character(format(round(dataread[1,7],1), nsmall = 1)),"\n\n")
          body = paste(body,"PR-2 (GHI) [%]:",as.character(format(round(dataread[1,8],1), nsmall = 1)),"\n\n")
          body = paste(body,"Last recorded value [kWh]:",as.character(format(round(dataread[1,9],1), nsmall = 1)),"\n\n")
          body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n\n")
          body = paste(body,"Grid Availability [%]:",as.character(dataread[1,13]),"\n\n")
          body = paste(body,"Plant Availability [%]",as.character(dataread[1,14]))
        }
        else
        {
	      	body = paste(body,"GHI [kWh/m^2]:",as.character(format(round(dataread[1,3],2), nsmall = 2)),"\n\n")
		      body = paste(body,"Avg Tmod [C]:",as.character(format(round(dataread[1,5],1), nsmall = 1)),"\n\n")
	      	body = paste(body,"Avg Tmod solar hours [C]:",as.character(format(round(dataread[1,7],1), nsmall = 1)))
        }
      }
      next
    }
    InvReadings[meteridx] = as.numeric(dataread[1,7])
    InvAvail[meteridx]= as.numeric(dataread[1,10])
    MYLD[(meteridx)] = as.numeric(dataread[1,7])
  }
  body = paste(body,"\n\n________________________________________________\n\n")
  body = paste(body,currday,"Inverters")
  body = paste(body,"\n________________________________________________")
  MYLDCPY = MYLD
  if(length(MYLD))
  {
    MYLD = MYLD[complete.cases(MYLD)]
  }
  addwarn = 0
  sddev = covar = NA
  if(length(MYLD))
  {
    addwarn = 1
    sddev = round(sdp(MYLD),2)
    meanyld = mean(MYLD)
    covar = round((sdp(MYLD)*100/meanyld),1)
  }
  MYLD = MYLDCPY
  for(t in 1 : noInverters)
  {
    path=pathall[t+3]
    dataread = read.table(path,header=T,sep="\t")
    InvReading = as.numeric(dataread[1,7])
    body = paste(body,"\n\n Yield ",acname[(t+NOMETERS+1)],": ",as.character(InvReading),sep="")
    if( addwarn && is.finite(covar) && covar > 5 &&
        (!is.finite(InvReadings[t]) || (abs(InvReadings[t] - meanyld) > 2*sddev)))
    {
      body = paste(body,">>> Inverter outside range")
    }
  }
  body = paste(body,"\n\nStdev/COV Yields: ",sddev," / ",covar,"%",sep="")
  for(t in 1 : noInverters)
  {
    path=pathall[t+3]
    dataread = read.table(path,header=T,sep="\t")
    InvAvail = as.numeric(dataread[1,10])
    body = paste(body,"\n\n Inverter Availability ",acname[(t+NOMETERS+1)]," [%]: ",as.character(InvAvail),sep="")
  }
  GTI = getGTIData(currday)
  GTI = GTI[1]
  bodyac = paste(bodyac,"\n\nIrradiation from MY-008L [kWh/m2]:",GTI)
  body = gsub("\n ","\n",body)
  body = paste(bodyac,body,sep="")
  graph_command1=paste('python3 /home/admin/CODE/EmailGraphs/Inverter_CoV_Graph.py "MY-008"',currday,5,'2020-08-01','" "','"Inverter CoV"',sep=" ")
  system(graph_command1)
  graph_path1=paste('/home/admin/Graphs/Graph_Output/MY-008/[MY-008] Graph ',currday,' - Inverter CoV.pdf',sep="")
  graph_extract_path1=paste('/home/admin/Graphs/Graph_Extract/MY-008/[MY-008] Graph ',currday,' - Inverter CoV.txt',sep="")

  graph_command1=paste('python3 /home/admin/CODE/EmailGraphs/Meter_CoV_Graph.py "MY-008"',currday,3,sep=" ")
  system(graph_command1)
  graph_path2=paste('/home/admin/Graphs/Graph_Output/MY-008/[MY-008] Graph ',currday,' - Meter CoV.pdf',sep="")
  graph_extract_path2=paste('/home/admin/Graphs/Graph_Extract/MY-008/[MY-008] Graph ',currday,' - Meter CoV.txt',sep="")

  filestosend=c(graph_path1,graph_extract_path1,graph_path2,graph_extract_path2,pathall)
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [MY-008L] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filestosend,
            debug = F)
  recordTimeMaster("MY-008L","Mail",currday)
}


path = "/home/admin/Dropbox/Gen 1 Data/[MY-008L]"
path2G = '/home/admin/Dropbox/Second Gen/[MY-008L]'
path3G = '/home/admin/Dropbox/Third Gen/[MY-008L]'
path4G = '/home/admin/Dropbox/Fourth_Gen/[MY-008L]'


if(RESETHISTORICAL)
{
  command = paste("rm -rf",path2G)
  system(command)
  command = paste("rm -rf",path3G)
  system(command)
  command = paste("rm -rf",path4G)
  system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "MY-008L"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-I29-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 1 : length(years))
{
  path2Gyr = paste(path2G,years[x],sep = "/")
  pathyr = paste(path,years[x],sep="/")
  checkdir(path2Gyr)
  months = dir(pathyr)
  for(y in 1 : length(months))
  {
    path2Gmon = paste(path2Gyr,months[y],sep = "/")
    pathmon = paste(pathyr,months[y],sep="/")
    checkdir(path2Gmon)
    stns = dir(pathmon)
    stns = stns[reorderStnPaths]
    dunmun = 0
    for(t in 1 : length(stns))
    {
      type = 1
      if(grepl("MFM",stns[t]))
        type = 0
      if(grepl("WMS",stns[t])){
          type = 2
      }
      pathmon1 = paste(pathmon,stns[t],sep="/")
      days = dir(pathmon1)
      path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
      if(!file.exists(path2Gmon1))
      {	
        dir.create(path2Gmon1)
      }
      
      
      if(length(days)>0)
      {
        for(z in 1 : length(days))
        {
          if(ENDCALL == 1)
            break
          if((z==length(days)) && (y == length(months)) && (x ==length(years)))
            next
          print(days[z])
          pathfinal = paste(pathmon1,days[z],sep = "/")
          path2Gfinal = paste(path2Gmon1,days[z],sep="/")
          if(RESETHISTORICAL)
          {
            secondGenData(pathfinal,path2Gfinal,type)
          }
          if(days[z] == stopDate)
          {
            ENDCALL = 1
            print('Hit end call')
            next
          }
        }
      }
      dunmun = 1
      if(ENDCALL == 1)
        break
    }
    if(ENDCALL == 1)
      break
  }
  if(ENDCALL == 1)
    break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1)
{
  recipients = c('operationsMY@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','solarmy@cutechgroup.com')
  recordTimeMaster("MY-008L","Bot")
  years = dir(path)
  noyrs = length(years)
  path2Gfinalall = vector('list')
  for(x in prevx : noyrs)
  {
    pathyr = paste(path,years[x],sep="/")
    path2Gyr = paste(path2G,years[x],sep="/")
    checkdir(path2Gyr)
    mons = dir(pathyr)
    nomons = length(mons)
    startmn = prevy
    endmn = nomons
    if(startmn>endmn)
    {
      startmn = 1
      prevx = x-1
      prevz = 1
    }
    for(y in startmn:endmn)
    {
      pathmon = paste(pathyr,mons[y],sep="/")
      path2Gmon = paste(path2Gyr,mons[y],sep="/")
      checkdir(path2Gmon)
      stns = dir(pathmon)
      if(length(stns) < 7)
      {
        print('Station sync issue.. sleeping for an hour')
        Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
        stns = dir(pathmon)
      }
      stns = stns[reorderStnPaths]
      for(t in 1 : length(stns))
      {
        newlen = (y-startmn+1) + (x-prevx)
        print(paste('Reset newlen to',newlen))
        type = 1
        if(grepl("MFM",stns[t]))
          type = 0
        if(grepl("WMS",stns[t])){
          type = 2
        }
        pathmon1 = paste(pathmon,stns[t],sep="/")
        days = dir(pathmon1)
        path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
        chkcopydays = days[grepl('Copy',days)]
        if(!file.exists(path2Gmon1))
        {
          dir.create(path2Gmon1)
        }
        if(length(chkcopydays) > 0)
        {
          print('Copy file found they are')
          print(chkcopydays)
          idxflse = match(chkcopydays,days)
          print(paste('idx matches are'),idxflse)
          for(innerin in 1 : length(idxflse))
          {
            command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
            print(paste('Calling command',command))
            system(command)
          }
          days = days[-idxflse]
        }
        days = days[complete.cases(days)]
        print(paste("days is",days))
        if(length(days)>0)
        {
        if(t==1){
          referenceDays <<- extractDaysOnly(days)
        }else{
          days = analyseDays(days,referenceDays)}
        }
        nodays = length(days)
        if(y > startmn)
        {
          z = prevz = 1
        }
        if(nodays > 0)
        {
          startz = prevz
          if(startz > nodays)
            startz = nodays
          for(z in startz : nodays)
          {
            if(t == 1)
            {
              path2Gfinalall[[newlen]] = vector('list')
            }
            if(is.na(days[z]))
              next
            pathdays = paste(pathmon1,days[z],sep = "/")
            path2Gfinal = paste(path2Gmon1,days[z],sep="/")
            secondGenData(pathdays,path2Gfinal,type)
            if((z == nodays) && (y == endmn) && (x == noyrs))
            {
              if(!repeats)
              {
                print('No new data')
                repeats = 1
              }
              next
            }
            if(is.na(days[z]))
            {
              print('Day was NA, do next in loop')
              next
            }
            SENDMAILTRIGGER <<- 1
            if(t == 1)
            {
              splitstr =unlist(strsplit(days[z],"-"))
              daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
              print(paste("Sending",daysSend))
              if(performBackWalkCheck(daysSend) == 0)
              {
                print("Sync issue, sleeping for an 1 hour")
                Sys.sleep(3600) # Sync issue -- meter data comes a little later than
                # MFM and WMS
              }
            }
            repeats = 0
            print(paste('New data, calculating digests',days[z]))
            path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
            newlen = newlen + 1
            print(paste('Incremented newlen by 1 to',newlen))
          }
        }
      }
    }
  }
  if(SENDMAILTRIGGER)
  {
    print('Sending mail')
    print(paste('newlen is ',newlen))
    for(lenPaths in 1 : (newlen - 1))
    {
      if(lenPaths < 1)
        next
      print(unlist(path2Gfinalall[[lenPaths]]))
      pathsend = unlist(path2Gfinalall[[lenPaths]])
      daysAlive = daysAlive+1
      sendMail(pathsend)
    }
    SENDMAILTRIGGER <<- 0
    newlen = 1
    print(paste('Reset newlen to',newlen))
    
  }
  
  prevx = x
  prevy = y
  prevz = z
  Sys.sleep(900)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()
