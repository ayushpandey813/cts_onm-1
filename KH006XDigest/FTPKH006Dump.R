require('RCurl')
require('utils')
require('R.utils')
source('/home/admin/CODE/MasterMail/timestamp.R')
FIREMAIL = 0
retryCnt = 0
serverdownmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "KH-006X FTP server down",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
serverupmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "KH-006X FTP server up and running",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
dumpftp = function(days,path)
{
	print('Calling function')
  url = "ftp://KH-006X:KD786HDHD1@ftpnew.cleantechsolar.com/"
  filenames = try(withTimeout(getURLContent(url,ftp.use.epsv = FALSE,dirlistonly = TRUE),
	 timeout = 600, onTimeout = "error"),silent=T)
	if(class(filenames) == 'try-error')
	{
		print('Error in FTP server, will reconnect in 10 mins')
		retryCnt <<- retryCnt + 1
		if(FIREMAIL==0 && retryCnt > 3)
		{
			serverdownmail()
			FIREMAIL<<-1
		}
		return(0)
	}
	retryCnt <<- 0
	if(FIREMAIL==1)
	{
		print('Server Back running...')
		serverupmail()
		FIREMAIL<<-0
	}
	print('Passed test')
	if(class(filenames) != 'character')
	{
		print('filenames isnt character')
		return(1)
	}
	recordTimeMaster("KH-006X","FTPProbe")
	newfilenames = unlist(strsplit(filenames,"\n"))
	print('Strsplit done')
	newfilenames = newfilenames[!grepl("tmp",newfilenames)]
  newfilenames = newfilenames[grepl("zip",newfilenames)]
	newfilenamesa = newfilenames[grepl("MSB01",newfilenames)]
	newfilenamesb = newfilenames[grepl("MSB02",newfilenames)]
	newfilenamesc = newfilenames[grepl("GHI",newfilenames)]
	newfilenamesd = newfilenames[grepl("Tmod",newfilenames)]
	newfilenames = c(newfilenamesa,newfilenamesb,newfilenamesc,newfilenamesd)
	seq1 = seq(from =1 ,to=(length(newfilenames)*2),by=2)
	newfilenames2 = unlist(strsplit(newfilenames,"\\."))
	zipsalone = which(newfilenames2 %in% "zip")
	newfilenames2 = newfilenames2[-zipsalone]
	days2 = unlist(strsplit(days,"\\."))
	csvsalone = which(days2 %in% "csv")
	days2 = days2[-csvsalone]
	print('zip match found')
  match = match(days2,newfilenames2)
	match=match[complete.cases(match)]
	if(length(match) < 1)
	{
	  print('No new files')
		return(1)
	}
	print('Matches found')
  newfilenames = newfilenames[-match]
  if(length(newfilenames) < 1)
	{
	  print('No new files')
		return(1)
	}

	print('New file names are')
	print(newfilenames)

	for(y in 1 : length(newfilenames))
  {
    urlnew = paste(url,newfilenames[y],sep="")
    vals = try(download.file(urlnew,destfile = paste(path,newfilenames[y],sep="/")))
		if(class(vals)=='try-error')
		{
			print(paste('Error downloading file',newfilenames))
			next
		}
	  unzipping = try(unzip(paste(path,newfilenames[y],sep="/"),exdir = path),silent = T)
		if(class(unzipping) == 'try-error')
		{
			print(paste('Couldnt extract',newfilenames[y]))
		}
		system(paste('rm "',path,'/',newfilenames[y],'"',sep = ""))
  }
	recordTimeMaster("KH-006X","FTPNewFiles",as.character(newfilenames[length(newfilenames)]))
	rm(filenames,newfilenames)
	gc()
	return(1)
}
