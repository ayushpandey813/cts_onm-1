import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import math
import pytz
import sys
import pyodbc
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

stn=sys.argv[1]
end_date=sys.argv[2]
threshold=float(sys.argv[3])
df_Yld=pd.DataFrame()
path_write='/home/admin/Graphs/'

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

SQL_Query = pd.read_sql_query('SELECT  [Station_Id]  FROM [dbo].[Stations] WHERE [Station_Name] = \''+stn+"'", connStr)
df_meters = pd.DataFrame(SQL_Query, columns=['Station_Id'])
stn_id=df_meters['Station_Id'].values[0]

SQL_Query = pd.read_sql_query('SELECT [Meter_id] FROM [dbo].[Meters] WHERE Station_Id='+str(stn_id), connStr)
df_meters = pd.DataFrame(SQL_Query, columns=['Meter_id'])
print(df_meters['Meter_id'].values)

for index,i in enumerate(df_meters['Meter_id'].values.tolist()):
    print(i)
    if index==0:
        SQL_Query = pd.read_sql_query('SELECT [Date],[Yield] FROM [dbo].[Stations_Data] WHERE Meter_Id=\''+str(i)+"'", connStr)
        df_data = pd.DataFrame(SQL_Query, columns=['Date','Yield'])
    else:
        SQL_Query = pd.read_sql_query('SELECT [Yield] FROM [dbo].[Stations_Data] WHERE Meter_Id=\''+str(i)+"'", connStr)
        df_temp=pd.DataFrame(SQL_Query, columns=['Date','Yield'])
        df_data['Yield_'+str(index+1)]=df_temp['Yield']

connStr.close()


df_data['Date']=df_data['Date'].astype(str)
df_Yld=df_data
df_Yld['COV']=((df_Yld.std(axis=1,skipna=True,ddof=0)*100)/df_Yld.mean(axis=1,skipna=True))
df=df_Yld
no_points=len(df)
df=df[df['Date']<=end_date]
df=df.replace(0, np.nan)
df = df.drop_duplicates(df.columns[1:])
last_7=df.tail(7)
last_30=df.tail(30)
last_60=df.tail(60).copy()
avg_std_7=last_7['COV'].mean()
avg_std_30=last_30['COV'].mean()
avg_std_60=last_60['COV'].mean()
avg_std_lifetime=df['COV'].mean()
plt.rcParams.update({'font.size': 28})
plt.rcParams['axes.facecolor'] = 'gainsboro'
fig = plt.figure(num=None, figsize=(55  , 30))
ax = fig.add_subplot(111)
df_outlier=last_60[(np.abs(stats.zscore(last_60['COV'].fillna(0)))> 3)]
outlier_dates=df_outlier['Date'].astype('datetime64[s]').tolist()
outlier_values=df_outlier['COV'].values.tolist()
last_60['Date']=pd.to_datetime(last_60['Date'])
last_date=last_60['Date'].tail(1).values[0].astype('datetime64[s]')
first_date=last_60['Date'].head(1).values[0].astype('datetime64[s]')
dates=last_60['Date'].astype('datetime64[s]').tolist()
last_60.index=last_60['Date']
upsampled=last_60.resample('2H')
interpolated = upsampled.interpolate(method='cubic')
plt.plot(interpolated['COV'],color='black',linewidth=3)
cov_values=last_60['COV'].values.tolist()
#plt.plot(last_60['Date'].tolist(),last_60['COV'].tolist(),color='black',linewidth=3)
plt.xticks(last_60['Date'].tolist(),rotation=90)

plt.xlim([first_date,last_date])

if(len(outlier_values)>0 and not math.isnan(outlier_values[0])):
    if(max(outlier_values)<100):
        plt.ylim([0,max(outlier_values)+5])
    else:
        plt.ylim([0,50])
else:
    if(max(cov_values)<100):
        if(abs(threshold-max(cov_values)<2)):
            plt.ylim([0,max(cov_values)+10])
        else:
            plt.ylim([0,max(cov_values)+5])
    else:
        plt.ylim([0,50])
myFmt = mdates.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_formatter(myFmt)
ttl = ax.set_title('From '+str(first_date)[0:10]+' to '+str(last_date)[0:10], fontdict={'fontsize': 45, 'fontweight': 'medium'})
ttl.set_position([.485, 1.02])
ttl_main=fig.suptitle(stn+' Daily Meter CoV Graph',fontsize=60)
ax.annotate('No. of Points: '+str(no_points)+' days\nLast 7-day CoV average [%]: '+str(round(avg_std_7,1))+'\nLast 30-day CoV average [%]: '+str(round(avg_std_30,1))+'\nLast 60-day CoV average [%]: '+str(round(avg_std_60,1))+'\nLifetime CoV average [%]: '+str(round(avg_std_lifetime,1)), (.01, .98),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='white',backgroundcolor='k',ha='left', va='top',size=25)
plt.axhline(y=threshold, color='green',linewidth=8)
if(stn=='VN-002'):
    plt.axhline(y=(threshold+2), color='orange',linewidth=8)
else:
    plt.axhline(y=(threshold+5), color='orange',linewidth=8)
ax.set_ylabel('CoV [%]', fontsize=32)
ln1=None
ln2=None
ln3=None
for index,i in enumerate(cov_values):
    if(i<=(threshold)):
        ln1=plt.scatter(dates[index], i, marker='D',color='green',s=300)
    elif(i>(threshold) and i<(threshold+5)):
        ln2=plt.scatter(dates[index], i, marker='s',color='orange',s=300)
    elif(i>=(threshold+5)):
        ln3=plt.scatter(dates[index], i, marker='^',color='red',s=300)
if(ln1!=None and ln2!=None and ln3!=None):
    plt.legend((ln1, ln2, ln3),
            ('Cov [%] <= '+str(threshold), 'Cov [%] > '+str(threshold)+' and CoV [%] < '+str(threshold+5), 'Cov [%] >= '+str(threshold+5)),
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
elif(ln1!=None and ln2!=None):
    plt.legend((ln1, ln2),
            ('Cov [%] <= '+str(threshold), 'Cov [%] > '+str(threshold)+' and CoV [%] < '+str(threshold+5)),
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
elif(ln1!=None and ln3!=None):
    plt.legend((ln1, ln3),
            ('Cov [%] <= '+str(threshold), 'Cov [%] >= '+str(threshold+5)),
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
elif(ln1!=None):
    plt.legend(([ln1]),
            (['Cov [%] <= '+str(threshold)]),
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
for i, label in enumerate(outlier_dates):
    if(outlier_values[i]>50):   
        pass
    else:
        ax.annotate(str(label)[0:10], (label, outlier_values[i]+.25),size=25,color='red')
chkdir(path_write+"Graph_Output/"+stn)
chkdir(path_write+"Graph_Extract/"+stn)
fig.savefig(path_write+"Graph_Output/"+stn+'/['+stn+"] Graph "+sys.argv[2]+" - "+"Meter CoV"+".pdf",bbox_inches='tight')
last_60.to_csv(path_write+"Graph_Extract/"+stn+'/['+stn+"] Graph "+sys.argv[2]+" - "+"Meter CoV"+".txt",sep='\t',index=False)