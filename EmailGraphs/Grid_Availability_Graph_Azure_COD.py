import requests, json
import requests.auth
import pandas as pd
import datetime as dt
import os
import re 
import time
import shutil
import pytz
import sys
import matplotlib
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.backends.backend_pdf
import matplotlib.dates as mdates
import logging
import pyodbc

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

stn=sys.argv[1]
end_date=sys.argv[2]
cod_date=sys.argv[3]

path_write='/home/admin/Graphs/'
types={'Grid Availability':['GA','red'],'Plant Availability':['PA','green'],'Data Availability':['DA','blue'],'System Availability':['SA','orange']}

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

SQL_Query = pd.read_sql_query('''SELECT TOP (1000) [Station_Id],[COD],[Station_Name],[Station_Columns],[Station_Irradiation_Center],[Station_No_Meters],[Provider],[Alarm_Status] FROM [dbo].[Stations] ''', connStr)
df_stations = pd.DataFrame(SQL_Query, columns=['Station_Id','COD','Station_Name','Station_Columns','Station_Irradiation_Center','Station_No_Meters','Provider','Alarm_Status'])
stn_id=str(df_stations.loc[(df_stations['Station_Name'].str.strip()==stn ),'Station_Id'].values[0])
cod=str(cod_date)

SQL_Query = pd.read_sql_query('SELECT [Meter_id],[Capacity] FROM [dbo].[Meters] WHERE Station_Id='+stn_id, connStr)
df_meters= pd.DataFrame(SQL_Query, columns=['Meter_id','Capacity'])

SQL_Query = pd.read_sql_query('SELECT [Date],[GA],[PA],[DA],[SA],[Meter_Id] FROM [dbo].[System_Uptime] WHERE Station_Id='+stn_id, connStr)
df_daily= pd.DataFrame(SQL_Query, columns=['Date','GA','PA','DA','SA','Meter_Id'])

connStr.close()


if(df_daily['Meter_Id'].nunique(dropna = True) >1):
    for index,row in df_meters.iterrows():
        df_daily.loc[df_daily['Meter_Id']==row['Meter_id'],['GA','PA','DA','SA']]=df_daily.loc[df_daily['Meter_Id']==row['Meter_id'],['GA','PA','DA','SA']]*row['Capacity']
    df_merged = pd.merge(df_daily,df_meters, left_on='Meter_Id', right_on='Meter_id')
    df_merged.loc[df_merged['DA']==0,'Capacity'] = 0
    df_merged = df_merged.resample('d', on='Date')['GA','PA','DA','SA','Capacity'].sum(min_count=1)
    df_merged['GA']=df_merged['GA']/df_merged['Capacity']
    df_merged['PA']=df_merged['PA']/df_merged['Capacity']
    df_merged['DA']=df_merged['DA']/df_merged['Capacity']
    df_merged['SA']=df_merged['SA']/df_merged['Capacity']
    df_daily = df_merged.copy()
    
df_daily.reset_index(level=0, inplace=True)
df_daily=df_daily[df_daily['Date']>=cod]
df_daily=df_daily[df_daily['Date']<=end_date]
df_daily=df_daily.round(1)

#Monthly DA
df_monthly=df_daily
df_monthly.index=pd.to_datetime(df_monthly['Date'])
df_monthly=df_monthly.resample("1m").mean()
df_monthly['Date'] = df_monthly.index.to_series().apply(lambda x: dt.datetime.strftime(x, '%b-%Y'))
df_monthly=df_monthly.round(1)
cols = list(df_monthly.columns)
cols = [cols[-1]] + cols[:-1]
df_monthly = df_monthly[cols]

#Graph Creation
font = {'size'   : 12}
plt.rcParams['axes.facecolor'] = 'white'
csfont = {'fontname':'Arial'}
plt.title('title',**csfont)
plt.xlabel('xlabel', **csfont)
matplotlib.rc('font', **font)

df_daily.index=pd.to_datetime(df_daily['Date'])
df_daily=df_daily.resample("1d").mean()
df_daily['Date']=df_daily.index
chkdir(path_write+"Graph_Output/"+stn)
chkdir(path_write+"Graph_Extract/"+stn)
for i in types: 
    fig, ax = plt.subplots(figsize=(12.8, 8.8))
    avg_14d=round(df_daily[types[i][0]][-14:].mean(),1)
    avg_cur=round(df_monthly[types[i][0]].iloc[[-1]].values[0],2)
    if(len(df_monthly)>1):
        avg_prev=round(df_monthly[types[i][0]].iloc[[-2]].values[0],2)
    else:
        avg_prev=avg_cur
    if(len(df_monthly)>1):
        avg_3m=round(df_monthly[types[i][0]][-3:].mean(),1)
    else:
        avg_3m=avg_cur
    avg_lifetime=round(df_daily[types[i][0]].mean(),1)
    plt.plot(df_daily['Date'],df_daily[types[i][0]],linewidth=1.5,color=types[i][1])
    ax.set_xlabel('')
    ax.set_ylabel(i+' [%]')
    ax.set_ylim([0,102])
    ax.set_xlim([df_daily['Date'].head(1).values[0],df_daily['Date'].tail(1).values[0]])
    ttl = ax.set_title('From '+str(df_daily['Date'].head(1).values[0])[0:10]+' to '+str(df_daily['Date'].tail(1).values[0])[0:10], fontdict={'fontsize': 11, 'fontweight': 'bold'})

    ttl_main=fig.suptitle(stn+' '+i,fontsize=11,x=.512, fontweight='bold',y=0.95)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b-%y')) 
    if(len(df_daily)>=365):
        ax.xaxis.set_major_locator(mdates.MonthLocator(bymonth=[3,6,9,12]))
    else:
        ax.xaxis.set_major_locator(mdates.MonthLocator(bymonth=[1,2,3,4,5,6,7,8,9,10,11,12]))
    ax.annotate('Avg '+types[i][0]+' Last 14-Days [%]: '+str(avg_14d), (.72, .3),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',color='black',ha='left', va='bottom',size=10)
    ax.annotate('Avg '+types[i][0]+' Current Month [%]: '+str(avg_cur), (.72, .25),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',color='black',ha='left', va='bottom',size=10)
    ax.annotate('Avg '+types[i][0]+' Previous Month [%]: '+str(avg_prev), (.72, .2),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',color='black',ha='left', va='bottom',size=10)
    ax.annotate('Avg '+types[i][0]+' Last 3 Months [%]: '+str(avg_3m), (.72, .15),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',color='black',ha='left', va='bottom',size=10)
    ax.annotate('Avg '+types[i][0]+' Lifetime [%]: '+str(avg_lifetime), (.72, .1),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',color='black',ha='left', va='bottom',size=10)
    fig.savefig(path_write+"Graph_Output/"+stn+"/["+stn+"] Graph "+str(df_daily['Date'].tail(1).values[0])[0:10]+" - "+i+".pdf", bbox_inches='tight') #Graph


df_daily.to_csv(path_write+"Graph_Extract/"+stn+'/['+stn+'] - Master Extract Daily.txt',sep='\t',mode='w',header=True) #Daily Extract
df_monthly.to_csv(path_write+"Graph_Extract/"+stn+'/['+stn+'] - Master Extract Monthly.txt',sep='\t',index=False) #Monthly Extract