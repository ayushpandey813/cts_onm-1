library(MASS)

pdf.options(paper = "a4r")

measure1List = list()
measure2List = list()

redPyrList = list()
orangePyrList = list()
bluePyrList = list()
redSiList = list()
orangeSiList = list()
blueSiList = list()

num0 = 0
num1 = 0
num2 = 0
num3 = 0
sumSMP = 0

usefulInfoList = list()

dataScraper = function() {
  #loop through each file in abovementioned location
  for (j in seq(from = 1, to = length(files))) {
    #import data from .txt files
    data = read.table(files[j], header = T, sep = "\t")
    
    #scrape required data and append it to a list (will form the coordinates for the points to be plotted eventually)
    measure1 = sum(data$AvgSMP10[data$AvgSMP10 > 10])/60000
    measure2 = sum(data$AvgGsi00[data$AvgSMP10 > 10])/60000
    measure1List <<- append(measure1List,measure1)
    measure2List <<- append(measure2List,measure2)
    
    #counting the number of data points following a stipulated criterion, to be used in the legend
    num0 <<- if (measure1 > 0) {num0 + 1} else {num0}
    num1 <<- if (measure1 > 0 && measure1 < 3) {num1 + 1} else {num1}
    num2 <<- if (measure1 >= 3 && measure1 <= 5) {num2 + 1} else {num2}
    num3 <<- if (measure1 > 5) {num3 + 1} else {num3}
    
    #obtaining total for SMP, to be used to calculate average reading later
    sumSMP <<- ifelse(measure1 > 0, sumSMP + measure1, sumSMP)
    
    #assigning points to red, orange, or blue zone
    bluePyrList <<- if (measure1 > 0 && measure1 < 3) {append(bluePyrList,measure1)} else {bluePyrList}
    blueSiList <<- if (measure1 > 0 && measure1 < 3) {append(blueSiList,measure2)} else {blueSiList}
    orangePyrList <<- if (measure1 > 3 && measure1 < 5) {append(orangePyrList,measure1)} else {orangePyrList}
    orangeSiList <<- if (measure1 > 3 && measure1 < 5) {append(orangeSiList,measure2)} else {orangeSiList}
    redPyrList <<- if (measure1 > 5) {append(redPyrList,measure1)} else {redPyrList}
    redSiList <<- if (measure1 > 5) {append(redSiList,measure2)} else {redSiList}
    
    #obtaining every single date for information purposes, and last date for plot title purposes
    dataDate = substr(files[j], (nchar(files[j]) - 13), (nchar(files[j]) - 4))
    
    #calculating ratio for information purposes
    ratioVal = measure2/measure1
    
    #adding all useful information to lists
    usefulInfoList <<- append(usefulInfoList,list(dataDate,measure1,measure2,ratioVal))
  }
}

yearFiles = list.files(path = "/home/admin/Dropbox/SerisData/1min/[724]",all.files = F, full.names = T)
idx = match("_gsdata_",yearFiles)
if(is.finite(idx)) {
  yearFiles = yearFiles[-idx]  
}
numOfYears = length(yearFiles) - 1

#year loop
for (n in seq(from = 2018, to = 2018 + numOfYears)) {
  #month loop
  for(i in 1:12) {
    if (i < 10) {
      pathprobe = paste0("/home/admin/Dropbox/SerisData/1min/[724]/",toString(n),"/",toString(n),"-","0",toString(i))
      if (file.exists(pathprobe)) {
        print(paste0("We are looking at files for ",toString(month.name[i])," ", toString(n),"."))
        #get a sequence of all the files in abovementioned location
        files = list.files(path = pathprobe, all.files = F, full.names = T)
        
        dataScraper()
      }
      else {
        print(paste0("The folder ", pathprobe," does not exist!"))
      }
    }
    else {
      pathprobe = paste0("/home/admin/Dropbox/SerisData/1min/[724]/",toString(n),"/",toString(n),"-",toString(i))
      if (file.exists(pathprobe)) {
        print(paste0("We are looking at files for ",toString(month.name[i])," ", toString(n),"."))
        #get a sequence of all the files in abovementioned location
        files = list.files(path = pathprobe, all.files = F, full.names = T)
        
        dataScraper()
      }
      else {
        print(paste0("The folder ", pathprobe," does not exist!"))
      }
    }
  }
}

#compiling all useful information
usefulInfoMatrix = matrix(nrow = (length(usefulInfoList)/4) + 1, ncol = 4)
usefulInfoMatrix[1,] = c("Date", "Pyr", "GSi00", "Ratio GSi00/Pyr")
for (m  in seq(from = 1, to = length(usefulInfoList)/4)) {
  r1 = (4 * m) - 3
  r2 = (4 * m) - 2
  r3 = (4 * m) - 1
  r4 = (4 * m)
  usefulInfoMatrix[m+1,] = c(unlist(usefulInfoList[r1]),unlist(usefulInfoList[r2]),unlist(usefulInfoList[r3]),unlist(usefulInfoList[r4]))
}

#defining axis labels
label1 = paste0(toString(num1)," (",toString(round(100 * num1/(num1 + num2 + num3),digits=1)),"%)")
label2 = paste0(toString(num2)," (",toString(round(100 * num2/(num1 + num2 + num3),digits=1)),"%)")
label3 = paste0(toString(num3)," (",toString(round(100 * num3/(num1 + num2 + num3),digits=1)),"%)")

#define margins to maximize plot area and axis label size
par(mgp = c(2.6,1,0), mar = c(5,5,4,2) + 0.1)
outerLim = ceiling(as.numeric(max(max(unlist(lapply(usefulInfoMatrix[2:dim(usefulInfoMatrix)[1],2],as.numeric))),max(unlist(lapply(usefulInfoMatrix[2:dim(usefulInfoMatrix)[1],3],as.numeric))))))

#plot scatter diagram of points
setwd("/home/admin/Jason/cec intern/results/[724]")
pdf("Singapore.pdf",width=11,height=8)
plot(unlist(lapply(usefulInfoMatrix[2:nrow(usefulInfoMatrix),3],as.numeric)),unlist(lapply(usefulInfoMatrix[2:nrow(usefulInfoMatrix),2],as.numeric)), xlab = expression('Daily Global Horizontal Irradiation [kWh/m'^2*', Silicon Sensor]'), ylab = expression('Daily Global Horizontal Irradiation [kWh/m'^2*', Pyranometer]'), las = 1, xlim = c(0,outerLim), ylim = c(0,outerLim), col = ifelse(measure1List < 3,"blue",ifelse(measure1List < 5, "orange", "red")), pch = ifelse(measure1List < 3,0,ifelse(measure1List < 5, 1, 2)), cex.lab = 1.3)
title(main = paste("Pyranometer vs. Silicon Sensor - Singapore"), line = 2.2,cex.main = 1.5)
title(main = paste0("From ", usefulInfoMatrix[2,1], " to ", usefulInfoMatrix[nrow(usefulInfoMatrix),1]), line = 0.75, cex.main = 1.2)

#plot straight line (y=x) for comparison
abline(a = 0, b = 1, col = "black",lwd = 2)

#individual regions' labels
text(x = -0.2,y = 6.6, labels = expression("GHI > 5"), col="red", cex = 0.75, adj = 0)
text(x = -0.2,y = 3.75, labels = expression('3'<='GHI'), col="orange", cex = 0.75, adj = 0)
text(x = 0.12,y = 3.75, labels = expression(''<='5'), col="orange", cex = 0.75, adj = 0)
text(x = -0.2,y = 2.3, labels = expression("GHI"<"3"), col="blue", cex = 0.75, adj = 0)

#horizontal delineations (boundaries for 3 colors)
abline(h = 0, lty = 2, col = "blue")
abline(h = 3, lty=2, col = "orange")
abline(h = 5, lty=2, col = "red")

#main legend
legend("bottomright", c(label3,label2,label1),col = c("red","orange","blue"), pch = c(2,1,0))

#total number of points text (in main legend)
text(par("usr")[2] - 0.22, -0.13, paste0("Total number of points = ",toString(num1 + num2 + num3)), adj = 1)

#calculating, plotting, and labelling average of Pyranometer data
averagePyr = sumSMP/(num1 + num2 + num3)
abline(h=averagePyr,lwd=3)
text(par("usr")[2] - 0.22, averagePyr - 0.3, paste0("Average Daily GHI (Pyranometer) = ", round(averagePyr,digits = 2)),cex = 0.9, adj = 1)

#Adding in names
title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
dev.off()
#navigate to output folder

#get .txt output for usefulData
write.matrix(usefulInfoMatrix, "allData.txt", sep = "\t")
