import pandas as pd
import sharepy
import datetime
import pytz
import pyodbc
import os
from calendar import monthrange
import math
import pymsteams

path='/home/admin/Dropbox/HQDigest/HQtest/Summary.xlsx'
webhook='https://outlook.office.com/webhook/632ff151-a2b4-47d6-bab8-e5bf793cc933@8ec327b2-e844-412c-8463-3e633202b00f/IncomingWebhook/2c37622a367149f0b19171ebe2a4c4fd/b91f3ec7-71fb-437f-8eab-f5905ff0c963'

#send mail function
def send_mail(date,data,rec):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    recipients = ['OM@cleantechsolar.com']# must be a list
    TO=", ".join(recipients)
    SUBJECT = 'Auto Injection Alert'
    text2="Date: "+str(date)+"\n\nStatus: "+str(data) 
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    server.quit()

def teams_alert(date,data):
    try:
        myTeamsMessage = pymsteams.connectorcard(webhook)
        myTeamsMessage.title("Auto Injection Alert")
        myTeamsMessage.text("<pre>Status: "+str(data)+"</pre>")
        myTeamsMessage.send()
        time.sleep(5)
    except:
        pass

#Initializing variables
tz = pytz.timezone('Asia/Calcutta')
date=(datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime("%Y-%m-%d")
first_date=(datetime.datetime.now(tz)+datetime.timedelta(days=-1)).replace(day=1).strftime("%Y-%m-%d")
no_days=monthrange((datetime.datetime.now(tz)+datetime.timedelta(days=-1)).year, (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).month)
alert_date=(datetime.datetime.now(tz).strftime("%Y-%m-%d"))
dfout = pd.DataFrame()
df_final= pd.DataFrame()
date_status=0

#Getting data from Master file
df_gis= pd.read_csv("/home/admin/Dropbox/GIS/Master/Z_AllSites_Master.txt", delimiter='\t')
df_gis_w= pd.read_csv("/home/admin/Dropbox/GIS_API2/W_AllSites_Master.txt", delimiter='\t')
IN036_files=sorted(os.listdir("/home/admin/Dropbox/Second Gen/[IN-036S]/"+date[0:4]+'/'+date[0:7]),reverse=True)
IN036_df=pd.read_csv("/home/admin/Dropbox/Second Gen/[IN-036S]/"+date[0:4]+'/'+date[0:7]+'/'+IN036_files[0], delimiter='\t')
IN036_df=IN036_df[['Date','LastReadStr','LastReadCtr']]
IN036_df['Date']=pd.to_datetime(IN036_df['Date']).dt.date

#Getting data from Azure
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
SQL_Query = pd.read_sql_query('SELECT TOP (1000) [Station_Id],[Station_Name],[GIS_Location] FROM [dbo].[stations] ', connStr)
df_stations = pd.DataFrame(SQL_Query, columns=['Station_Id','Station_Name','GIS_Location'])
SQL_Query = pd.read_sql_query('SELECT [Meter_Id],[Reference] FROM [dbo].[Meters] ', connStr)
df_meters = pd.DataFrame(SQL_Query, columns=['Meter_Id','Reference'])
SQL_Query = pd.read_sql_query("SELECT [Date],[Station_Id],[Meter_Id],[LastR-MFM] FROM [dbo].[Stations_Data] Where Date='"+date+"'", connStr)
df_stations_data = pd.DataFrame(SQL_Query, columns=['Date','Station_Id','Meter_Id','LastR-MFM'])
SQL_Query = pd.read_sql_query("SELECT [Station_Name],[GIS_Type] FROM [dbo].[Stations]", connStr)
df_gis_type = pd.DataFrame(SQL_Query, columns=['Station_Name','GIS_Type'])
SQL_Query = pd.read_sql_query("SELECT [GHI] FROM [dbo].[Stations_Data] WHERE Meter_Id=50 AND DATE BETWEEN'"+first_date+"'AND'"+date+"'", connStr) #SG-003 Irr Data
df_SG003 = pd.DataFrame(SQL_Query, columns=['GHI'])
SQL_Query = pd.read_sql_query("SELECT [GHI] FROM [dbo].[Stations_Data] WHERE Meter_Id=130 AND DATE BETWEEN'"+first_date+"'AND'"+date+"'", connStr) #SG-004 Irr Data
df_SG004 = pd.DataFrame(SQL_Query, columns=['GHI'])
SQL_Query = pd.read_sql_query("SELECT [Eac-MFM] FROM [dbo].[Stations_Data] WHERE Meter_Id=201 AND DATE BETWEEN'"+first_date+"'AND'"+date+"'", connStr) #SG-008 EAC Data
df_SG008 = pd.DataFrame(SQL_Query, columns=['Eac-MFM'])
SQL_Query = pd.read_sql_query("SELECT Meter_Id,GIS_Location FROM [dbo].[Meters] p LEFT JOIN [dbo].[Stations] o ON o.Station_Id = p.Station_Id", connStr) 
df_location = pd.DataFrame(SQL_Query, columns=['Meter_Id','GIS_Location'])#All locations to fill up GIS locations for sites with missing Last-R

#Merging
df_merge=pd.merge(df_meters,df_stations_data,on='Meter_Id',how="left")
df_merge=pd.merge(df_merge,df_stations,on='Station_Id',how="left")

#Updating if Last-R is null for the day

df_na=df_merge[((df_merge['LastR-MFM'].isnull()) | (df_merge['LastR-MFM']==0))]
for index, row in df_na.iterrows():
    SQL_Query = pd.read_sql_query("SELECT TOP(1) [Date],[LastR-MFM] FROM [dbo].[Stations_Data] WHERE Meter_Id="+str(df_na['Meter_Id'][index])+"AND [LastR-MFM]!=0 ORDER BY [Date] DESC", connStr) 
    df_temp = pd.DataFrame(SQL_Query, columns=['Date','LastR-MFM'])
    if(df_temp.empty):
        pass
    else:
        df_merge.loc[df_merge['Meter_Id']==df_na['Meter_Id'][index],'LastR-MFM'] = df_temp['LastR-MFM'][0]
        df_merge.loc[df_merge['Meter_Id']==df_na['Meter_Id'][index],'Date'] = df_temp['Date'][0]
        df_merge.loc[df_merge['Meter_Id']==df_na['Meter_Id'][index],'GIS_Location'] = df_location.loc[df_location['Meter_Id']==df_na['Meter_Id'][index],'GIS_Location'].values[0]
connStr.close()

#Formatting
df_merge['Extrapolated Value']=None
df_merge['Date']=df_merge['Date'].dt.date
df_merge['Station_Name']=df_merge['Reference'].str[0:6]
df_gis_type['Station_Name']=df_gis_type['Station_Name'].str.strip()

if((df_merge['Date'].astype(str).dropna()==date).any):
    date_status=0
else:
    date_status=1

#Hardcoding IN-036 Meter Reading

df_merge.loc[df_merge['Reference']=='IN-036A','LastR-MFM']=IN036_df['LastReadStr'][0]
df_merge.loc[df_merge['Reference']=='IN-036B','LastR-MFM']=IN036_df['LastReadCtr'][0]
df_merge.loc[df_merge['Reference']=='IN-036A','Date']=IN036_df['Date'][0]
df_merge.loc[df_merge['Reference']=='IN-036B','Date']=IN036_df['Date'][0]

#SG-003 Hardcoding Extrapolated Value
df_merge.loc[df_merge['Reference']=='SG-003A','Extrapolated Value']=round((df_SG003.mean()*no_days[1]).values[0],2)
df_merge.loc[df_merge['Reference']=='SG-003A','GIS_Location']='Mazak'
df_merge.loc[df_merge['Reference']=='SG-003B','Extrapolated Value']=round((df_SG003.mean()*no_days[1]).values[0],2)
df_merge.loc[df_merge['Reference']=='SG-003B','GIS_Location']='Mazak'
df_merge.loc[df_merge['Reference']=='SG-003C','Extrapolated Value']=round((df_SG003.mean()*no_days[1]).values[0],2)
df_merge.loc[df_merge['Reference']=='SG-003C','GIS_Location']='Mazak'

#SG-004 Hardcoding Extrapolated Value
df_merge.loc[df_merge['Reference']=='SG-004A','Extrapolated Value']=round((df_SG004.mean()*no_days[1]).values[0],2)
df_merge.loc[df_merge['Reference']=='SG-004A','GIS_Location']='K&N'
df_merge.loc[df_merge['Reference']=='SG-004B','Extrapolated Value']=round((df_SG004.mean()*no_days[1]).values[0],2)
df_merge.loc[df_merge['Reference']=='SG-004B','GIS_Location']='K&N'

df_merge.loc[df_merge['Reference']=='SG-008A','LastR-MFM']=df_SG008.sum().values[0]

df_merge=df_merge[['Station_Name','Reference','GIS_Location','Extrapolated Value','LastR-MFM','Date']]
df_merge.columns=['Operation','MeterRef','Location','Extrapolated Value','LastRead','LastDate']

#Adding GHI Value from W
for index, row in df_merge.iterrows():
    g_type=df_gis_type.loc[(df_gis_type['Station_Name']==row["Operation"]),'GIS_Type'].values[0].strip()
    irr=df_gis_w.loc[((df_gis_w['SummaryName'].str.upper()==row["Location"]) & (df_gis_w['G_Type']==g_type)),'ExtrapolatedIrradiationMonth']
    print(irr,row)
    if(irr.empty):
        pass
    else:
        df_merge.loc[index,'Extrapolated Value']=round(irr.values[0],2)

#Writing as seperate sheets
df_in=df_merge.loc[df_merge['MeterRef'].str[0:2]=='IN',]
df_my=df_merge.loc[df_merge['MeterRef'].str[0:2]=='MY',]
df_sg=df_merge.loc[df_merge['MeterRef'].str[0:2]=='SG',]
df_kh=df_merge.loc[df_merge['MeterRef'].str[0:2]=='KH',]
df_th=df_merge.loc[df_merge['MeterRef'].str[0:2]=='TH',]
df_vn=df_merge.loc[df_merge['MeterRef'].str[0:2]=='VN',]
writer = pd.ExcelWriter(path,index=False)
frames = {'IN': df_in, 'VN': df_vn, 'MY': df_my, 'SG': df_sg,'KH': df_kh,'TH': df_th,}
for sheet, frame in  sorted(frames.items()):
    frame.to_excel(writer, sheet_name = sheet,index=False)
writer.save()

#Initializing variables
username = 'test-account@cleantechsolar.com'
password = 'Cleantechsolarpv@123'
site_name = "/OM/"
base_path = 'https://cleantechenergycorp.sharepoint.com'
doc_libary = "Customer Invoicing/Meter Readings/AA Master Meter Readings"
file_name = path

#Pushing file to sharepoint
auth = sharepy.connect(base_path, username = username, password = password)
with open(file_name, 'rb') as read_file:
  content = read_file.read()
link = base_path + site_name + "_api/web/GetFolderByServerRelativeUrl('" + doc_libary + "')/Files/add(url='Summary.xlsx', overwrite=true)"
status = auth.post(link, data=content)
if(status.status_code==423):
    send_mail(alert_date,'Failed. Could not upload the file to SP.','asd')
    teams_alert(alert_date,'Failed. Could not upload the file to SP.')
elif(status.status_code==200 and date_status==1):
    send_mail(alert_date,'Failed. File uploaded to SP but readings behind.','asd')
    teams_alert(alert_date,'Failed. File uploaded to SP but readings behind.')
elif(status.status_code==200 and date_status==0):
    send_mail(alert_date,'Success. File uploaded to SP with updated readings.','asd')
print(status.status_code)
#Success, if this line gets printed else failure