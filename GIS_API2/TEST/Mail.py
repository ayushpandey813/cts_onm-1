import pandas as pd
import openpyxl
from itertools import islice
import numpy as np
#for mailing 
import smtplib
from email.mime.multipart import MIMEMultipart
import datetime
import pytz
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import sys
import os
import datetime
# from datetime import datetime
from datetime import date
from datetime import timedelta
from functools import cmp_to_key

#--------CONSTANTS----------
user = 'admin'
script_dir = os.path.dirname(__file__)
excel_sheet_path = os.path.join(script_dir, 'GIS_info.xlsx')
w_all_file_path = os.path.join(script_dir, 'W_AllSites_Master.txt')
recipients = ['amogh.diwan@cleantechsolar.com', 'anusha.agarwal@cleantechsolar.com', 'sai.pranav@cleantechsolar.com']
today = date.today()
yesterday = today - timedelta(days = 1)
date =  yesterday.strftime("%Y-%m-%d")
tz = pytz.timezone('Asia/Calcutta')
NaN = 'NaN'
#----------------------------

def retrieve_site_details(excel_sheet_path):
    '''retrieves all the site details from the excel sheet 
    and returns them as a datafram
    Details retrieved:
    * none right now
    Return format:
    dataframe object
    '''
    # reads excel file and converts to dataframe
    wb = openpyxl.load_workbook(filename=excel_sheet_path)
    ws = wb.active
    data = ws.values
    cols = next(data)
    data = list(data)
    df = pd.DataFrame(data, columns=cols)
    return df


def process_site_data_for_digest():
    '''
    Use the site data to create record for the daily digest
    return format: a string formatted for the email body
    '''
    # Get information for all sites from the excel
    df = retrieve_site_details(excel_sheet_path)
    #Create new columns and assign values
    new_column_dict = {
        'GHI': 0,
        'GHI_30_MA': np.nan,
        'ghi_30_sum': np.nan,
        'GHI_365_SUM': np.nan,
        'GTI': np.nan,
        'GTI_30_MA': np.nan,
        'GTI_365_SUM': np.nan,
        'vsTMY': np.nan
    }
    for k,v in new_column_dict.items():
        df[k] = v
    
    
    #Find file for each site and make sure the db is suitably unique
    df_grouped = df.groupby(['G_Type'])
    df_ghi = pd.DataFrame(df_grouped.get_group('GHI'))
    df_ghi.set_index('SummaryName', inplace=True)
    site_names = list(df_ghi.index)
    site_names = sorted(site_names)
    relevant_list = []
    #iterate over site names to get relevant data
    for site_name in site_names:
        data_path = '/home/admin/Dropbox/GIS_API3/{site_name}/{site_name}_AGGREGATE.txt'.format(site_name=site_name)
        # print(data_path)
        try:
            site_df = pd.read_csv(data_path, sep='\t')
        except FileNotFoundError:
            print("No file in folder ", site_name)
            continue
        except Exception as e:
                print('Extra Tab error in ', site_name)
                site_df = pd.read_csv(data_path, sep='\t',error_bad_lines=False)
        try:
            try:
                site_df['Date'] = pd.to_datetime(site_df['Date'].apply(datefixer))
            except Exception as e:
                print("DT ERROR", site_name)
                print(str(e))
                continue
        except:
            print('Headers not for ', site_name)
            site_df = pd.read_csv(data_path, names = ['Date', 'GHI'], sep = '\t')
            try:
                site_df['Date'] = pd.to_datetime(site_df['Date'].apply(datefixer))
            except:
                print("DT ERROR", site_name)
        relevant_list.append(site_name)
        df_ghi.loc[site_name, ['GHI']] = round(site_df.iloc[-1]['GHI'],2)
        df_ghi.loc[site_name, ['GHI_30_MA']] = round(site_df.tail(30)['GHI'].mean(), 2)
        df_ghi.loc[site_name, ['GHI_365_SUM']] = round(site_df.tail(365)['GHI'].sum(), 2)
        try:
            site_tmy = df_ghi.loc[site_name, 'TMY']
            site_tmy = float(str(site_tmy))
        except ValueError:
            site_tmy = df_ghi.loc[site_name, 'TMY']
            site_tmy = float(str(site_tmy))
        except KeyError:
            continue
        tmy = float((df_ghi.loc[site_name, ['GHI_365_SUM']]/site_tmy -1)*100) #converted to float, otherwise causes TypeError
        tmy = round(tmy, 1)
        df_ghi.loc[site_name, ['vsTMY']] = tmy
        if 'GTI' in site_df.columns:
            df_ghi.loc[site_name, ['GTI']] = round(site_df.iloc[-1]['GTI'],2)
            df_ghi.loc[site_name, ['GTI_30_MA']] = round(site_df.tail(30)['GTI'].sum().mean(), 2)
            df_ghi.loc[site_name, ['GTI_365_SUM']] = round(site_df.tail(365)['GTI'].sum(), 2)

    df_relevant = df_ghi.loc[relevant_list]
    data_body = ["Last date received for {len_df_relevant} stations and daily irradiation for the given day:\n\n".format(len_df_relevant = df_relevant.shape[0])]
    for site in df_relevant.index:
        site_data = df_relevant.loc[site]
        if site_data['GTI'] != np.nan:
            site_info_string = "{site_name} ({site_country}): {date}, {ghi_daily} kWh/m2, 30-d MA: {ghi_30_d_ma}, 365-d sum: {ghi_365_d_sum}, {tmy}% vsTMY\n\n".format(
                site_name = site, site_country=site_data['Country'], date=date, ghi_daily=site_data['GHI'],
                ghi_30_d_ma=site_data['GHI_30_MA'], ghi_365_d_sum=site_data['GHI_365_SUM'], tmy=site_data['vsTMY']
            )
        else:
            site_info_string = "{site_name} ({site_country}): {date}, {ghi_daily} kWh/m2, 30-d MA: {ghi_30_d_ma}, 365-d sum: {ghi_365_d_sum}, {tmy}% vsTMY\n\n".format(
                site_name = site, site_country=site_data, date=date, ghi_daily=site_data['GHI'],
                ghi_30_d_ma=site_data['GHI_30_MA'], ghi_365_d_sum=site_data['GHI_365_SUM'], tmy=site_data['vsTMY']
            )
        data_body.append(site_info_string)
    #-----------SUMMARY SECTION FOR INDIA SITES----------------------------------
    df_ghi_ind = pd.DataFrame(df_ghi.groupby('Country').get_group('IN'))
    mean_ghi= round(df_ghi_ind['GHI'].mean(), 2)
    std_dev_ghi = round(np.std(df_ghi_ind['GHI']), 2)
    cov_pct = round((std_dev_ghi*100)/mean_ghi,2)
    summary = []
    summary.append('******************************\n\n')
    summary.append('Average GHI Indian cities [kWh/m2]:  {mean_ghi}\n\n'.format(mean_ghi=mean_ghi))
    summary.append('SD GHI Indian cities [kWh/m2]:  {std_dev_ghi}\n\n'.format(std_dev_ghi=std_dev_ghi))
    summary.append('COV Indian cities [%]:  {cov_pct}\n\n'.format(cov_pct=cov_pct))
    summary.append('******************************\n\n')
    #---------------------------------------------------------------------------------
    data_body.extend(summary)
    data_body = ''.join(data_body)
    return data_body


def datefixer(x):
 try:
      datetime.datetime.strptime(x, "%d-%m-%Y")
      return(datetime.datetime.strptime(x, "%d-%m-%Y").strftime("%Y-%m-%d"))
 except:
      datetime.datetime.strptime(x, "%Y-%m-%d")
      return(datetime.datetime.strptime(x, "%Y-%m-%d").strftime("%Y-%m-%d"))


def col_cmp(col1, col2):
    col1year, col1month = list(map(int, col1.split('_')[-1].split('.')))
    col2year, col2month = list(map(int, col2.split('_')[-1].split('.')))
    if col1year < col2year:
        return -1
    if col1year == col2year:
        return -1 if col1month < col2month else 1
    return 1


def extrapolatedIrradiationMonth(last_date, sum_tot_irradiation):
    month = last_date.month
    day_of_the_month = last_date.day
    year = last_date.year
    days_in_month = 30
    if month in [1, 3, 5, 7, 8, 9, 10, 12]:
        days_in_month = 31
    if month == 2:
        days_in_month = 28 if year%4 != 0 else 29
    extrapolatedIrradiation = round((sum_tot_irradiation/day_of_the_month) * days_in_month, 2)
    return extrapolatedIrradiation


def gen_master_file():
    '''
    takes data from the excel sheet extracts information from it to create W_AllSites_Master.txt'''
    df = retrieve_site_details(excel_sheet_path)
    # ADD RELEVANT EXTRA COLUMNS
    col_dict = {
        'LastDayRecord': NaN,
        'LastDayValue' : NaN,
        'Last30MA' : NaN,
        'Last365DayTot' : NaN,
        'ExtrapolatedIrradiationMonth' : NaN,
    }
    for name, val in col_dict.items():
        df[name] = val
    col_set = set(df.columns)

    #ITERATE OVER ALL SITE NAMES
    site_names = list(df['SummaryName'])
    for site_name in site_names:
        criteria_list = [
            (((df['SummaryName'] == site_name) & (df['G_Type'] == 'GHI')), 'GHI'), 
            (((df['SummaryName'] == site_name) & (df['G_Type'] == 'GTI')), 'GTI')
            ]

        #READ DATA AND HANDLE THE EXCEPTIONS
        data_path = '/home/admin/Dropbox/GIS_API3/{site_name}/{site_name}_AGGREGATE.txt'.format(site_name=site_name.upper())
        try:
            site_df = pd.read_csv(data_path, sep='\t')
        except FileNotFoundError:
            print("No file in folder ", site_name)
            continue
        except Exception as e:
                print('Extra Tab error in ', site_name)
                site_df = pd.read_csv(data_path, sep='\t',error_bad_lines=False)
        try:
            try:
                site_df['Date'] = pd.to_datetime(site_df['Date'].apply(datefixer))
            except Exception as e:
                print("DT ERROR", site_name)
                print(str(e))
                continue
        except:
            print('Headers not for ', site_name)
            site_df = pd.read_csv(data_path, names = ['Date', 'GHI'], sep = '\t')
            try:
                site_df['Date'] = pd.to_datetime(site_df['Date'].apply(datefixer))
            except:
                print("DT ERROR", site_name)

        #FILL VALUES FOR COLUMNS CREATED
        site_df_monthwise_sum = site_df.resample('M', on='Date').sum()        
        for criteria, gtype in criteria_list:
            try: 
                df.loc[criteria, ['LastDayRecord']] = site_df.iloc[-1]['Date'].strftime('%Y-%m-%d')
                df.loc[criteria, ['LastDayValue']] = round(site_df.iloc[-1][gtype],2)
                df.loc[criteria, ['Last30MA']] = round(site_df.tail(30)[gtype].sum().mean(), 2)
                df.loc[criteria, ['Last365DayTot']] = round(site_df.tail(365)[gtype].sum(), 2)
                df.loc[criteria, ['ExtrapolatedIrradiationMonth']] = extrapolatedIrradiationMonth(site_df.iloc[-1]["Date"], site_df_monthwise_sum.iloc[-1][gtype])
                site_df_monthwise_sum['year_month_format'] = site_df_monthwise_sum.index.to_series().map(lambda x: '{gtype}Tot_{y}.{m}'.format(gtype=gtype, y = x.year, m = x.month))
                for i in range(site_df_monthwise_sum.shape[0]):
                    new_col = site_df_monthwise_sum.iloc[i]['year_month_format']
                    col_val = round(site_df_monthwise_sum.iloc[i][gtype], 2)
                    if  new_col not in col_set:
                        col_set.add(new_col)
                        df[new_col] = 'NaN'
                    df.loc[criteria, new_col] = col_val
            except KeyError:
                continue
    # SORT COLUMNS AND ROWS IN DESIRED ORDER
    gti_tot_list = sorted(list(filter(lambda x: True if "GTITot" in x else False, list(df.columns))), key=cmp_to_key(col_cmp))
    ghi_tot_list = sorted(list(filter(lambda x: True if "GHITot" in x else False, list(df.columns))), key = cmp_to_key(col_cmp))
    col_list_ordered = ['City', 'Country', 'Lat', 'Long', 'G_Type', 'Tilt', 'Azimuth', 'FirstMonth', 'SummaryName']
    col_list_ordered.extend(col_dict.keys())
    col_list_ordered.extend(ghi_tot_list)
    col_list_ordered.extend(gti_tot_list)
    df = df[col_list_ordered]
    df.sort_values(by='SummaryName', inplace=True)
    df.to_csv(w_all_file_path, sep='\t', index = False)



def send_mail(date, recipients, email_data_body, attachment_path_list=None):
    '''
    will take the list of recipients, 
    retrieve processed site data, format it 
    and send the mail
    
    '''
    server = smtplib.SMTP("smtp.office365.com")
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    msg = MIMEMultipart()
    sender = 'operations@cleantechsolar.com'
    print(date)
    dates = pd.to_datetime(date).strftime("%Y-%m-%d")
    msg['Subject'] = "TEST API GIS Files Updates "+ str(dates)
    #if sender is not None:
    msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    email_body = email_data_body
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
            # try:
            file_name = each_file_path.split("/")[-1]
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(each_file_path, "rb").read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
            msg.attach(part)
        #   except:
                # print("could not attach file")

    msg.attach(MIMEText(email_body))
    server.sendmail(sender, recipients, msg.as_string())


def gen_attachment_list():
    attachment_list = []
    df = retrieve_site_details(excel_sheet_path)
    site_names = list(df['SummaryName'])
    for name in site_names:
        file_path =  '/home/admin/Dropbox/GIS_API3/{site_name}/{site_name}_AGGREGATE.txt'.format(site_name=name)
        if os.path.isfile(file_path):
            attachment_list.append(file_path)
    attachment_list = list(set(attachment_list))
    attachment_list = (sorted(attachment_list))
    attachment_list.append(w_all_file_path)
    return attachment_list


if __name__ == '__main__':
    email_data_body = process_site_data_for_digest()
    gen_master_file()
    attachment_path_list = gen_attachment_list()
    send_mail(date, recipients, email_data_body, attachment_path_list)