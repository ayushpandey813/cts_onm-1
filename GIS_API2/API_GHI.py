import datetime as DT
import requests
import os
from lxml import etree
import pandas as pd
import csv
from datetime import timedelta 
import shutil
import pytz
tz = pytz.timezone('Asia/Calcutta')

#GHI CODE

today=(DT.datetime.now(tz).date()+DT.timedelta(days=-1))
print("Printing today")
print(today)
api_key = 'HesHutsorc8mnk95sc'
url = 'https://solargis.info/ws/rest/datadelivery/request?key=%s' % api_key
headers = {'Content-Type': 'application/xml'}
xmlfile = open('/home/admin/CODE/GIS_API2/API_req.xml', 'r')
body = xmlfile.read()

site = [  'CHANGI', 'CHENNAI', 'CHIPLUN', 'HOSUR', 'HYDERABAD', 'JHAJJHAR', 'LALRU', 'NEW_DELHI', 'ORAGADAM', 'PAKA', 'PUNE', 'TAK', 'TUAS', 'WOODLANDS', 'NAN', 'KAN', 'CEBU', 'PENANG', 'SUKHA', 'GUDANG', 'MANILA', 'ENSTEK','KORAT','HALDIA','TANGKAK','MPIP','SUPHAN','PHRAE','LOPBURI','UTTARADIT','ROJANA','HO CHI MINH','THAYANG','PHITLOK','CHUMPHON','TAIPING','TANJUNG_PELEPAS','SAMUT_SAKHON','SAMUT_SKRM','CHAKSU','SAMPHRAN','PHNOM_PENH','NEGERI_PERAK','KAMPNGSAEN','BANGPAIN','CHACHOENGSAO','CMIC','BURHANPUR','MAESOT','CHIANGKM','CHIANGKNG','CHONBURI','NAVI_MUMBAI','NELLORE','AHMEDABAD','LILORA','PRACHUAP','SURAT','BANGYAI','CHAENGWATTANA','PATHUM','PAKTHONGCHAI','CHOKCHAI','DETUDOM','UTHUMPHON','NONTHAI','PRATHAI','CHIANGYUEN','MUKDAHAN','THATPHANOM','SAWANG','THABO','DAIWA','SAMUTPRAKAN','LATKRABANG','KHAOYOI','BOSCH','BAXTER','KELLOGG', 'PHUKET', 'RANONG', 'KHLONG_7', 'MAESAI', 'SRINAKARIN']

lat = [ 1.355533, 13.039953, 17.597365, 12.836017, 17.480219, 28.503888, 30.465364, 28.669777, 12.860411, 4.646769, 18.516117, 16.865858, 1.323308, 1.436414,18.765055, 14.004411, 10.290214, 5.319842, 13.825511, 1.467633, 14.395792, 2.724341,14.732772,22.063374,2.235694,11.128467,14.46955,18.152649,15.126091,17.607411,14.311383,10.825091,12.982552,16.799203,10.503535,4.895764,1.355286,13.521487,13.402244,26.6,13.743175,11.508292,5.078597,13.991998,14.188882,13.782145,10.635906,21.306951,16.712223,19.529891,20.243891,13.028887,19.076172,14.426052,23.143449,22.288740,11.7976,9.10855,13.823902,13.895674,14.020474,14.721060,14.739368,14.910321,15.114456,15.195140,15.536238,16.407719, 16.548843, 16.957502, 17.469771,17.852435,14.346271,13.568364,13.765607,13.303419,13.068353, 12.943486, 13.004205,7.90448, 9.93612, 14.03597, 20.40730, 13.62090]

lng = [ 103.987964, 80.166937, 73.489269, 77.796608, 78.914536, 76.758162, 76.793125, 77.083597, 79.944721, 103.342539, 73.705956, 99.135101, 103.671919,103.766664, 100.76118, 99.544515, 123.967864, 100.290572, 100.659688, 103.923453,120.943183,101.762426,102.404386,88.129701,102.570583,106.617374,100.12945,100.156511,101.079252,100.081953,100.645438,106.799662,99.89935,100.216446,99.138481,100.708348,103.556258,100.144479,100.00656,75.95,100.219878,104.775469,100.5817,99.996946,100.609381,100.973196,104.523626,76.200939,98.552192,100.303094,100.4099,101.10837,73.137231,80.01403,72.453163,73.367600,99.777031,99.1677,100.410125,100.556144,100.526558,102.015884,102.159165,105.066758,104.135922,102.069210,102.722782,103.101998,104.716318,104.726806,103.440000,102.574861,100.675516,100.776806,100.787802,99.754628,101.189805,101.09887,101.173722,98.36928, 98.63115, 100.76663, 99.88323, 100.62041]

tilt = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

azimuth = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]


week_ago = today - DT.timedelta(days=7)
yesterday = today - DT.timedelta(days=1)


day = today.day
print("printing day")
print(day)
month = today.month-1
if(day == 2):
  print("Replacing data")
  last_day = today - timedelta(days = 2) 
  first_day_last = last_day.replace(day=1) 
  delta = last_day - first_day_last
  delta = delta.days
  delta = delta+2
  print(delta)
  first_day = today.replace(day=1) 
  start = [first_day_last, first_day]
  end = [last_day, today]
  print(start)
  print(end)
  print(start[0])
  print(end[0])
  for (a, b, c, d, e) in zip(site, lat, lng, tilt, azimuth): 
    print(a, b, c, d, e)
    response = requests.post(url, data=body.format(Start_Date= start[0], End_Date = end[0], latitude= b , longitude = c, tilt = d, azimuth = e, para = "GHI"), headers=headers)   
    data = []
    ghi_rounded = []
    ts_rounded = []
    root = etree.fromstring(bytes(response.text, encoding='utf-8'))
    for element in root.iter("*"):
      data.append(element.items())
    tuple_list = [item for t in data for item in t]
    first_tuple_elements = []
            
    for a_tuple in tuple_list:
      first_tuple_elements.append(a_tuple[1])
         
    ts = first_tuple_elements[1::2]
    for i in ts:
      x = slice(0, 10)
      ts_rounded.append(i[x])
            
    ghi = first_tuple_elements[2::2]
    for i in ghi:
      i = float(i)
      i = round(i,4)
      ghi_rounded.append(i)
    list_of_tuples = list(zip(ts_rounded, ghi_rounded)) 
    df = pd.DataFrame(list_of_tuples) 
    df = df.iloc[1:]
    ts_rounded.pop(0)
    ghi_rounded.pop(0)
    path = "/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATE.txt"
    #df_path = pd.read_csv(path)
    #df_path = df_path.iloc[:-delta]
    path_new = "/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATENEW.txt"
    lines = open(path).readlines()
    open(path_new, 'w').writelines(lines[:-delta])
      
    file = open("/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATENEW.txt", "a")
    #print(file)
    #print("Written")
    for index in range(len(ts_rounded)):
      file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) + "\n")
    file.close() 
    with open(path_new, 'rb') as f2, open(path, 'wb') as f1:
      shutil.copyfileobj(f2, f1)
      
    
  for (a, b, c, d, e) in zip(site, lat, lng, tilt, azimuth): 
      #print(yesterday, a, b, c)
      response = requests.post(url, data=body.format(Start_Date= start[1], End_Date = end[1], latitude= b , longitude = c, tilt = d, azimuth = e, para = "GHI"), headers=headers)      
      data = []
      ghi_rounded = []
      ts_rounded = []
            #Do request for data, response = r#
      root = etree.fromstring(bytes(response.text, encoding='utf-8'))
      for element in root.iter("*"):
          data.append(element.items())
            #print(data)
      tuple_list = [item for t in data for item in t]
      first_tuple_elements = []
            
      for a_tuple in tuple_list:
          first_tuple_elements.append(a_tuple[1])
         
      ts = first_tuple_elements[1::2]
      for i in ts:
          x = slice(0, 10)
          ts_rounded.append(i[x])
            
      ghi = first_tuple_elements[2::2]
      for i in ghi:
          i = float(i)
          i = round(i,4)
          ghi_rounded.append(i)
      list_of_tuples = list(zip(ts_rounded, ghi_rounded)) 
      df = pd.DataFrame(list_of_tuples) 
      df = df.iloc[1:]
      ts_rounded.pop(0)
      ghi_rounded.pop(0)

      file = open("/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATE.txt", "a")
      for index in range(len(ts_rounded)):
          file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) + "\n")
      file.close()     
      
else: 
  for (a, b, c, d, e) in zip(site, lat, lng, tilt, azimuth): 
      #print(yesterday, a, b, c)
      response = requests.post(url, data=body.format(Start_Date= today, End_Date = today, latitude= b , longitude = c, tilt = d, azimuth = e, para = "GHI"), headers=headers)
            
      data = []
      ghi_rounded = []
      ts_rounded = []
            #Do request for data, response = r#
      root = etree.fromstring(bytes(response.text, encoding='utf-8'))
      for element in root.iter("*"):
          data.append(element.items())
            #print(data)
      tuple_list = [item for t in data for item in t]
      first_tuple_elements = []
            
      for a_tuple in tuple_list:
          first_tuple_elements.append(a_tuple[1])
         
      ts = first_tuple_elements[1::2]
      for i in ts:
          x = slice(0, 10)
          ts_rounded.append(i[x])
            
      ghi = first_tuple_elements[2::2]
      for i in ghi:
          i = float(i)
          i = round(i,4)
          ghi_rounded.append(i)
      list_of_tuples = list(zip(ts_rounded, ghi_rounded)) 
      df = pd.DataFrame(list_of_tuples) 
      df = df.iloc[1:]
      ts_rounded.pop(0)
      ghi_rounded.pop(0)

      file = open("/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATE.txt", "a")
      for index in range(len(ts_rounded)):
          file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) + "\n")
      file.close() 


