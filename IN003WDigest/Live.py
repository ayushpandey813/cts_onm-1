from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib
import re 
import os
import numpy as np
import gzip
import shutil
import logging
import urllib.request

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

def write(Type,cols,path_temp,df,date):
    df.columns=cols
    date=str(date)
    chkdir(path_temp+'/'+Type)
    if(os.path.exists(path_temp+'/'+Type+'/'+'[IN-003W]-'+Type+'-'+date[0:10]+'.txt')):
        df.to_csv(path_temp+'/'+Type+'/'+'[IN-003W]-'+Type+'-'+date[0:10]+'.txt',sep='\t',index=False,header=False,mode='a')
    else:
        df.to_csv(path_temp+'/'+Type+'/'+'[IN-003W]-'+Type+'-'+date[0:10]+'.txt',sep='\t',index=False,header=True,mode='a')


path='/home/admin/Dropbox/Gen 1 Data/[IN-003W]/'
startpath="/home/admin/Start/IN003.txt"
logging.basicConfig(filename='/home/pranav/LogsIN003.txt')

chkdir(path)
tz = pytz.timezone('Asia/Kolkata')
curr=datetime.datetime.now(tz)

print("Start time is",curr)
print("Starting Live Bot!")
ftp = FTP('ftpnew.cleantechsolar.com')
ftp.login(user='IN-003W', passwd = 'w9dgoJzR7r3Wyu3o')
ftp.cwd('DATA/MODBUS')
mfmcols=['Date','Apparent power total','Active power total','Current average','Apparent power, phase 1','Active power, phase 1','Current, phase 1','Apparent power, phase 2','Active power, phase 2','Current, phase 2','Apparent power, phase 3','Active power, phase 3','Current, phase 3','Forward apparent energy','Forward active energy','n hours','Forward run seconds','Number of power interruptions']
wmscols=['Date','Record_Id','Irradiation','Record_Id_2']
invcols=['Date','Inverter ID','RTC Year','RTC Month','RTC Day','RTC Hour','RTC Minute','RTC Second','Reserved','FW1 revision','FW1 date','FW2 revision','FW2 date','FW3 revision','FW3 date','PCS inverter status','Reconnected Time','Device Type','Total Power','AC Voltage_1','AC Current_1','AC Power_1','AC frequency_1','AC voltage_2','AC current_2','AC power_2','AC frequency_2','AC voltage_3','AC current_3','AC power_3','AC frequency_3','total power','DC voltage_1','DC current_1','DC power_1','DC voltage_2','DC current_2','DC power_2','Today Wh','Today Runtime','Life Wh','Life Runtime','temp_1','temp_2','temp_3','temp_4','event_index','event_index_10','event_index_11','event_index_12','event_index_13','event_index_14','event_index_15','event_index_16','event_index_17','event_index_18','event_index_19']
components={1:['WMS',wmscols,4],2:['MFM',mfmcols,18],4:['INV_4',invcols,57],3:['INV_3',invcols,57],5:['INV_2',invcols,57],6:['INV_1',invcols,57]}

def chk_date(date):
    try:
        date=datetime.datetime.strptime(date[0:6]+'20'+date[6:], '%d/%m/%Y-%H:%M:%S')
        return 1
    except:
        return 0


if(os.path.exists(startpath)):
    pass
else:
    try:
        shutil.rmtree(path,ignore_errors=True)
    except Exception as e:
        print(e)
    with open(startpath, "w") as file:
        print('ff')
        file.write("2020-03-12\n00:00:00")   
with open(startpath) as f:
    startdate = f.readline(11)[:-1]
    starttime = f.readline(10)
print(startdate)
print(starttime)

start=startdate+starttime
start=start.replace('\n','')
start=datetime.datetime.strptime(start, "%Y-%m-%d%H:%M:%S")
end=datetime.datetime.now(tz).replace(tzinfo=None)-datetime.timedelta(minutes=1)
print(start)
files=ftp.nlst() 
while(start<end):
    try:
        start_str=start.strftime("%Y%m%d_%H%M")
        start_str='_'+start_str[2:]
        print(start_str)
        for i in sorted(files):
            if start_str in i:
                path_temp=path+'20'+start_str[1:3]+'/'+'20'+start_str[1:3]+'-'+start_str[3:5]
                chkdir(path_temp)
                req = urllib.request.Request('ftp://IN-003W:w9dgoJzR7r3Wyu3o@ftpnew.cleantechsolar.com/DATA/MODBUS/'+i)
                with urllib.request.urlopen(req) as response:
                    s =  gzip.decompress(response.read())
                cols = [1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60]
                df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep=';',names=cols)
                print(df)
                for index, row in df.iterrows():
                    if(row[1]=='ADDRMODBUS'):
                        val=row[2]
                    if(chk_date(row[1])):
                        data=row.to_frame().T.iloc[:,:int(components[int(val)][2])]
                        date=datetime.datetime.strptime(row[1][0:6]+'20'+row[1][6:], '%d/%m/%Y-%H:%M:%S')
                        if(date.year<2015):
                            continue
                        data[1]=date.replace(second=0)
                        write(components[int(val)][0],components[int(val)][1],path_temp,data,date)
        start=start+datetime.timedelta(minutes=1)
    except:
        start=start+datetime.timedelta(minutes=1)
        logging.exception("His")
        pass
print('Historical Done!')
flag=0
while(1):
    time_now=datetime.datetime.now(tz)
    try:
        ftp = FTP('ftpnew.cleantechsolar.com')
        ftp.login(user='IN-003W', passwd = 'w9dgoJzR7r3Wyu3o')
        ftp.cwd('DATA/MODBUS')
        if(flag==0):
            start=time_now
            files=ftp.nlst()  
            files_sorted=sorted(files,reverse=True)
            new_file=[files_sorted[0]]
            flag=1
        else:
            files_new=ftp.nlst()  
            with open("/home/admin/Start/MasterMail/IN-003W_FTPProbe.txt", "w") as file:
                file.write(str(date.replace(microsecond=0)))
            new_file=list(set(files_new) - set(files))
            print(new_file)
            files=files_new
        for i in sorted(new_file):
            date=i.split('_')[2]
            path_temp=path+'20'+date[0:2]+'/'+'20'+date[0:2]+'-'+date[2:4]
            chkdir(path_temp)
            url='ftp://IN-003W:w9dgoJzR7r3Wyu3o@ftpnew.cleantechsolar.com/DATA/MODBUS/'+i
            try:
                response = urllib.request.urlopen(url, timeout=120).read()
            except (HTTPError, URLError) as error:
                logging.error('Data of %s not retrieved because %s\nURL: %s', name, error, url)
            except timeout:
                logging.error('socket timed out - URL %s', url)
            else:
                logging.info('Access successful.')
            s =  gzip.decompress(response)
            cols = [1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60]
            df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep=';',names=cols)
            for index, row in df.iterrows():
                if(row[1]=='ADDRMODBUS'):
                    val=row[2]
                if(chk_date(row[1])):
                    data=row.to_frame().T.iloc[:,:int(components[int(val)][2])]
                    date=datetime.datetime.strptime(row[1][0:6]+'20'+row[1][6:], '%d/%m/%Y-%H:%M:%S')
                    data[1]=date.replace(second=0)
                    if(date.year<2015):
                        continue
                    print(data)
                    write(components[int(val)][0],components[int(val)][1],path_temp,data,date)
                    with open(startpath, "w") as file:
                        file.write(str(date.date())+"\n"+str(date.time().replace(microsecond=0)))
                    with open("/home/admin/Start/MasterMail/IN-003W_FTPNewFiles.txt", "w") as file:
                        file.write(str(date.replace(microsecond=0)))
    except Exception as e:
        print(e)
        logging.exception("Main")
    time.sleep(7200)

    