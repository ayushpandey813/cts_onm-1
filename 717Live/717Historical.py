from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib.request
import re 
import os
import numpy as np
import pyodbc

pd.options.mode.chained_assignment = None

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)
        
path='/home/pranav/Dropbox/SERIS_Live_Data/[SG-004S]/'
chkdir(path)
tz = pytz.timezone('Asia/Singapore')
curr=datetime.datetime.strptime('2020-04-20','%Y-%m-%d',)
end=datetime.datetime.strptime('2020-04-20 16:18:00','%Y-%m-%d %H:%M:%S',)
print("Start time is",curr)
cols=[]
for i in range(600):
    cols.append(i+1)
print("Starting Live Bot!")
ftp = FTP('ftpnew.cleantechsolar.com')
ftp.login(user='cleantechsolar', passwd = 'bscxataB2')
ftp.cwd('1min')
flag=1
starttime=time.time()
cols2=['TimeStamp','GSi00','Tmod','Act_Pwr-Tot_A','Act_Pwr-Tot_B','Act_E-Recv_A','Act_E-Recv_B']
curr=curr.replace(tzinfo=None)
currtime=curr


def convert_data_type(fields):
    for i in range(len(fields)):
        if fields[i] == 'NULL':
            fields[i] = None
    return fields

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

#Insert Function
def insert(df):
    df=df.fillna("NULL")
    df=df.values.tolist()
    counter=0
    i=0
    while True:            
        try:
            cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
            cursor = cnxn.cursor()
            #Dumping data in the new table
            while(i<len(df)):
                convert_data_type(df[i])
                with cursor.execute("INSERT INTO [Cleantech Meter Readings].dbo.[SG-004S-L] ([TimeStamp],[GSi00],[Tmod],[Act_Pwr-Tot_A],[Act_Pwr-Tot_B],[Act_E-Recv_A],[Act_E-Recv_B]) VALUES (?,?,?,?,?,?,?)",df[i]): 
                    print('Successfuly Inserted Row!',df[i]) 
                i=i+1 
            cnxn.commit()
            cursor.close()
            cnxn.close()
            break 
        except Exception as e:
            
            err_num = 'gg'
            if(err_num=='23000'):
                print ('DUPLICATE:', err_num, df[i])
            else:
                print ('ERROR:', err_num, df[i])
            i=i+1
            cursor.close()
            cnxn.close()
            if err_num == '08S01':
                counter = counter + 1
                print ('Sleeping for 5')
                time.sleep(300)
                if counter > 10:
                    exit('Persistent connection error')                    
                print ('%%%Re-trying connection%%%')             
            else:
                pass
temppath='/home/pranav/Dropbox/SERIS_Live_Data/[SG-004S]/2020/2020-04/[SG-004S] 2020-04-20.txt'
data=pd.read_csv(temppath,sep='\t')
insert(data)
    
        