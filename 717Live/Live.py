from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib.request
import re 
import os
import numpy as np
import logging

pd.options.mode.chained_assignment = None

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)
        
path='/home/admin/Dropbox/SERIS_Live_Data/[SG-004S]/'
logging.basicConfig(filename='/home/pranav/Logs717Live.txt')
chkdir(path)
tz = pytz.timezone('Asia/Singapore')
curr=datetime.datetime.now(tz)
currnextmin=curr + datetime.timedelta(minutes=1)
currnextmin=currnextmin.replace(second=8,microsecond=0)
while(curr!=currnextmin):
    curr=datetime.datetime.now(tz)
    curr=curr.replace(microsecond=0)
    time.sleep(1)
currtime=curr.replace(tzinfo=None)
print("Start time is",curr)
cols=[]
for i in range(600):
    cols.append(i+1)
print("Starting Live Bot!")
ftp = FTP('ftpnew.cleantechsolar.com')
ftp.login(user='cleantechsolar', passwd = 'bscxataB2')
ftp.cwd('1min')
flag=1
starttime=time.time()
cols2=['TimeStamp','GSi00','Tmod','Act_Pwr-Tot_A','Act_Pwr-Tot_B','Act_E-Recv_A','Act_E-Recv_B']
while(1):
    try:
        if(flag==1):
            cur=currtime.strftime("%Y-%m-%d_%H-%M")
        else:
            curr=datetime.datetime.now(tz)
            currtime=curr.replace(tzinfo=None)
            cur=currtime.strftime("%Y-%m-%d_%H-%M")
        print('Now adding for',currtime)
        flag2=1
        j=0
        while(j<5 and flag2!=0):
            j=j+1
            files=ftp.nlst()        
            for i in files:
                if(re.search(cur,i)):
                    req = urllib.request.Request('ftp://cleantechsolar:bscxataB2@ftpnew.cleantechsolar.com/1min/Cleantech-'+cur+'.xls')
                    try:
                        with urllib.request.urlopen(req) as response:
                            s = response.read()
                        df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep='\t',names=cols)
                        a=df.loc[df[1] == 717] 
                        if(flag==1):
                            b=a[[2,3,4,18,65,34,81]] 
                            flag=0
                        else:
                            b=a[[2,3,4,18,65,34,81]] 
                            if(os.path.exists(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-004S] '+cur[0:10]+'.txt')):
                                df= pd.read_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-004S] '+cur[0:10]+'.txt',sep='\t')
                                df2= pd.DataFrame( np.concatenate( (df.values, b.values), axis=0 ) )
                                df2.columns = cols2
                            std1=df2['Act_E-Recv_A'].std()
                            std2=df2['Act_E-Recv_B'].std()
                            if(std1>5000):
                                b.loc[:, 34] = 'NaN' 
                            if(std2>5000):
                                b.loc[:, 81] = 'NaN'      
                        #Taking care of Power
                        b.loc[(b[3] < 0), 3] = 0
                        b.loc[:,[18,65]]=b.loc[:,[18,65]]*-1
                        b.loc[(b[18] > 800) | (b[18] < 0), 18] = 'NaN'
                        b.loc[(b[65] > 800) | (b[65] < 0), 65] = 'NaN'
                        b.columns=cols2
                        chkdir(path+cur[0:4]+'/'+cur[0:7])
                        if(os.path.exists(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-004S] '+cur[0:10]+'.txt')):
                            b.to_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-004S] '+cur[0:10]+'.txt',header=False,sep='\t',index=False,mode='a')
                        else:
                            b.to_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-004S] '+cur[0:10]+'.txt',header=True,sep='\t',index=False)
                        flag2=0
                    except Exception as e:
                        logging.exception('msg')
                        flag2=0
                        print(e)
                        print("Failed for",currtime)
            if(flag2!=0 and j<5):
                print('sleeping for 10 seconds!')            
                time.sleep(10)
        if(flag2==1):
            print('File not there!',cur)
        print('sleeping for a minute!')
    except:
        pass
    time.sleep(60.0 - ((time.time() - starttime) % 60.0))
    
        