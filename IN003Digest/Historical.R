rm(list=ls())
#errHandle = file('/home/admin/Logs/LogsIN001History.txt',open='w',encoding='UTF-8')
#sink(errHandle,type='message',append = T)
#sink(errHandle,type='output',append = T)
source('/home/admin/CODE/IN003Digest/Functions.R')

x = 1

path = "/home/admin/Data/TORP Data/[IN-003T]"
pathgen1 = "/home/admin/Dropbox/Gen 1 Data/[IN-003T]"

if(!file.exists(path))
{
	dir.create(path)
}
if(!file.exists(pathgen1))
{
	dir.create(pathgen1)
}

yr = 2016
pathyr = paste(path,yr,sep="/")
pathyrgen1 = paste(pathgen1,yr,sep="/")

if(!file.exists(pathyr))
{
	dir.create(pathyr)
}
if(!file.exists(pathyrgen1))
{
	dir.create(pathyrgen1)
}

months = c("01","02","03","04","05","06","07","08","09","10","11","12")
daysmonths = c(31,28,31,30,31,30,31,31,30,31,30,31)

for(outer in 5:12)
{
	pathmonth = paste(pathyr,"/",yr,"-",months[outer],sep="")
	pathmonthgen1 = paste(pathyrgen1,"/",yr,"-",months[outer],sep="")
	
	if(!file.exists(pathmonth))
		dir.create(pathmonth)
	if(!file.exists(pathmonthgen1))
		dir.create(pathmonthgen1)
		x=1
	#req = httr::POST("http://52.70.243.223/torp/ServiceRouter/login?loginid=operations@cleantechsolar.com&pwd=torp1227")
	print('Logged in')
	while(x <= daysmonths[outer])
	{
  #	if(outer == 12 && x ==  25)
#		break
		day = x
  	if(x < 10)
  	{
   	 day = paste("0",x,sep="")
  	}
 		day1 = paste(day,"/",months[outer],"/",yr,sep="")
  	day2 = day1
  	day11 = paste("2016-",months[outer],"-",day,sep="")
		#df = fetchrawdata(day1,day2)
  	#write.table(df,file=paste(pathmonth,"/[IN-003T] ",day11,".txt",sep=""),row.names = F,
    #          col.names = T,sep = "\t",append = F)
  	df2 = cleansedata(paste(pathmonth,"/[IN-003T] ",day11,".txt",sep=""))
  	write.table(df2,file=paste(pathmonthgen1,"/[IN-003T] ",day11,".txt",sep=""),row.names = F,
              col.names = T,sep = "\t",append = F)
		print(paste(x,'done'))
  	x = x + 1
	}
}
