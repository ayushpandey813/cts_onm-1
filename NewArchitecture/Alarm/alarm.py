
import time
import pytz
import pymsteams
from twilio.rest import Client
from database_operations import *

def sendmail(station,type,data,rec,smsrec,sms_flag):
    if(station[1:3]!='IN'):
        SERVER = "smtp.office365.com"
        FROM = 'operations@cleantechsolar.com'
        recipients = rec# must be a list
        cleantech_rec=[]
        for i in recipients:
            if(i.split('@')[1]=='cleantechsolar.com' or i.split('@')[1]=='comin.com.kh'):
                cleantech_rec.append(i)
        TO=", ".join(cleantech_rec)
        SUBJECT = station+' Network Issue/Power Trip Alarm'
        text2='Last Timestamp Read: '+str(data[0]) +'\n\n Last Power Reading: '+str(data[2])+'\n\n Meter Name: '+str(data[1])
        TEXT = text2
        # Prepare actual message
        message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
        # Send the mail
        import smtplib
        server = smtplib.SMTP(SERVER)
        server.starttls()
        server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
        server.sendmail(FROM, recipients, message)
        server.quit()
    try:
        teams_alert(station,data)
    except:
        print('Teams Alert Failed')
    if(sms_flag==1):
        account = "ACcac80891fc3a1edb14f942975a0a8939"
        token = "d39acf2278b2194b59aa15aa9e3a2072"
        client = Client(account, token)
        for t in smsrec:
            message = client.messages.create(to="+"+str(int(t)), from_="+12402930809",body=SUBJECT +"\n\n"+TEXT)

def get_text_recipients(stn):
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    SQL_Query = pd.read_sql_query('''SELECT * FROM [dbo].[Text_Recipients] ''', connStr)
    df_recipients = pd.DataFrame(SQL_Query, columns=['Name','Number','Site','Country','Status'])
    df_temp = df_recipients.loc[df_recipients['Country'].str.strip()==stn[0:2],]
    connStr.close()
    df_temp = df_temp.loc[(((df_temp['Site'].str.strip()=='All') | (df_temp['Site'].str.strip()==stn)) & (df_temp['Status']==1)),'Number']
    df_temp = df_temp.unique()
    numbers= df_temp.tolist()
    return numbers

def teams_alert(station,data):
    try:
        webhook=df_webhook.loc[df_webhook['O&M_Code'].str.strip()==station[1:-2],'Webhook'].values[0].strip()
        myTeamsMessage = pymsteams.connectorcard(webhook)
        myTeamsMessage.title(station[1:-2]+" Network Issue/Power Trip Alarm")
        myTeamsMessage.text("<pre>Last Timestamp Read: "+str(data[0])+"<br>Last Power Reading: "+str(round(data[2],1))+"<br>Meter Name: "+str(data[1])+"</pre>")
        myTeamsMessage.send()
        time.sleep(5)
    except:
        pass

def rectolist(station,path):
    df=pd.read_csv(path)
    df2=df[['Recipients',station]]
    a=df2.loc[df2[station] == 1]['Recipients'].tolist()
    return a

cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
timezones={'IN':'Asia/Calcutta','VN':'Asia/Bangkok','KH':'Asia/Phnom_Penh','TH':'Asia/Bangkok','MY':'Asia/Kuala_Lumpur','SG':'Asia/Singapore'}
site_df = get_sites('Locus',cnxn)
site_df = site_df[site_df['SiteCode'].str[:]=='IN-052']
site_df = site_df.sort_values(by=['SiteCode'])
site_dict = {}
for index,row in site_df.iterrows():
    site = row['SiteCode']
    tz=pytz.timezone(timezones[site[0:2]])
    components = get_cleantech_components(site,cnxn)
    mfm_id = components.loc[components['ComponentType']=='MFM','ComponentId'].tolist()
    site_dict[site] = [mfm_id]
    site_dict[site].append([0]*len(mfm_id)) 
    site_dict[site].append([(datetime.datetime.now(tz))]*len(mfm_id)) 
cnxn.close()

while(1):
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    for site in site_dict:
        print(site)
        tz=pytz.timezone(timezones[site[0:2]])
        date=(datetime.datetime.now(tz)).strftime('%Y-%m-%d')
        time_now = (datetime.datetime.now(tz)).strftime('%H:%M:%S')
        if((datetime.datetime.now(tz)).hour>7):
            #start_time = time.time()
            df = get_raw_data(date, time_now, site, cnxn)
            #print("--- %s seconds ---" % (time.time() - start_time))
            if(df.empty):
                print('Data not in')
                continue
            for index,mfm_id in enumerate(site_dict[site][0]):
                df_mfm = df[df['ComponentId']==mfm_id]
                df_power = df_mfm[['AC_Power']]
                pow1 = df_power.tail(6).sum()
                last_pow = df_power.tail(1).fillna(0).values[0][0]
                date = pd.to_datetime(df.tail(1)['Timestamp'].values)
                time_check = date
                print(pow1)
                
                if(((pow1[0]<1) and site_dict[site][1][index] == 0) and time_check.hour>7):
                    try:
                        recipients = rectolist(site[1:-1]+'_Mail',mailpath)
                    except:
                        if(site[1:3] == 'MY'):
                            recipients = ['operationsMY@cleantechsolar.com','om-interns@cleantechsolar.com','sai.pranav@cleantechsolar.com']
                        else:
                            recipients = ['sai.pranav@cleantechsolar.com']
                    print('Sending firt time')
                    smsrec = get_text_recipients(site[1:-2])
                    #sendmail(i,'pow',[date[0],k,last_pow],recipients,smsrec,1)
                    site_dict[site][2][index] = (datetime.datetime.now(tz))
                    site_dict[site][1][index] = 1
                elif(((pow1[0]<1) and site_dict[site][1][index]==1) and time_check.hour>7):
                    diff=(datetime.datetime.now(tz))-site_dict[site][2][index]
                    if((diff.seconds)//60>120):
                        print('Sending second time')
                        site_dict[site][2][index]=(datetime.datetime.now(tz))
                        try:
                            recipients=rectolist(site[1:-1]+'_Mail',mailpath)
                        except:
                            if(site[1:3]=='MY'):
                                recipients = ['operationsMY@cleantechsolar.com','om-interns@cleantechsolar.com','sai.pranav@cleantechsolar.com']
                            elif(site[1:3]=='MY'):
                                recipients = ['operationsTH@cleantechsolar.com','sai.pranav@cleantechsolar.com']
                            else:
                                recipients = ['sai.pranav@cleantechsolar.com']
                        smsrec = get_text_recipients(site[1:-2])
                        #sendmail(i,'pow',[date[0],k,last_pow],recipients,smsrec,0)
                else:
                    site_dict[site][1][index] = 0
        time.sleep(5)
    cnxn.close()
    time.sleep(900)