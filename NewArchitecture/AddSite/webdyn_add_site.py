import pytz
import os
from ftplib import FTP
import time
import pytz
import io
import urllib.request
import re 
import gzip
import shutil
import logging
from database_operations import *
from locus_api import *
from process_functions import *
import process_functions

#Create Raw String
#Add parameters
#



#####
site = 'IN-006'
site_id = get_site_id(site)
irr_center = 'Self'
data_granularity = get_data_granularity(site)
locus_parameters = []
azure_locus_parameters = []
component_id = []
tz = pytz.timezone("Asia/Calcutta")
main_path = '/home/pranav/New_Architecture/RawData/'

df_cols = pd.read_csv("Cols.csv")
print(df_cols)
mfmcols = df_cols.loc[df_cols['ComponentType']=='MFM','CSName'].tolist()
invcols = df_cols.loc[df_cols['ComponentType']=='INV','CSName'].tolist()
wmscols = df_cols.loc[df_cols['ComponentType']=='WMS','CSName'].tolist()
components={'WD00D47C':{1:['MFM_1_ABS',mfmcols,93.94]},
'WD00DE9F':{1:['MFM_2_EMR',mfmcols,57.645]},
'WD00D486':{1:['MFM_3_ER&D',mfmcols,29.89]},
'WD00D4BC':{1:['MFM_4_SS',mfmcols,87.84]},
'WD00E05E':{1:['MFM_5_Unit21',mfmcols,180.56]},
'WD00D477':{1:['MFM_6_Unit24',mfmcols,36.6]},
'WD00D47C_INV':{1:['INVERTER_1_ABS',invcols,19.21],2:['INVERTER_2_ABS',invcols,18.91],3:['INVERTER_3_ABS',invcols,18.6],4:['INVERTER_4_ABS',invcols,18.6],5:['INVERTER_5_ABS',invcols,18.6]},
'WD00DE9F_INV':{1:['INVERTER_1_EMR',invcols,19.215],2:['INVERTER_2_EMR',invcols,19.215],3:['INVERTER_3_EMR',invcols,19.215]},
'WD00D486_INV':{1:['INVERTER_1_ER&D',invcols,14.945],2:['INVERTER_2_ER&D',invcols,14.945]},
'WD00D4BC_INV':{1:['INVERTER_1_SS',invcols,17.385],2:['INVERTER_2_SS',invcols,17.385],3:['INVERTER_3_SS',invcols,17.385],4:['INVERTER_4_SS',invcols,17.385],5:['INVERTER_5_SS',invcols,18.3]},
'WD00E05E_INV':{1:['INVERTER_1_Unit21',invcols,17.69],2:['INVERTER_2_Unit21',invcols,17.69],3:['INVERTER_3_Unit21',invcols,17.69],4:['INVERTER_4_Unit21',invcols,17.69],5:['INVERTER_5_Unit21',invcols,18.3],6:['INVERTER_6_Unit21',invcols,18.3],7:['INVERTER_7_Unit21',invcols,18.3],8:['INVERTER_8_Unit21',invcols,18.3],9:['INVERTER_9_Unit21',invcols,18.3],10:['INVERTER_10_Unit21',invcols,18.3]},
'WD00D477_INV':{1:['INVERTER_1_Unit24',invcols,18.3],2:['INVERTER_2_Unit24',invcols,18.3]},
}

meter_info = {'WD00D47C':['IN-006A','a0T28000005oArTEAU'],'WD00DE9F':['IN-006B','a0T28000005oAriEAE'],'WD00D486':['IN-006C','a0T28000005oArxEAE'],
'WD00D4BC':['IN-006D','a0T28000005oArYEAU'],'WD00E05E':['IN-006G','a0T0K00000FIFtZUAX'],'WD00D477':['IN-006D','a0T28000005oArsEAE']
}

#Have to add INV Cap
#Adding Components
for j in components:
    for i in components[j]:
        if(components[j][i][0][0:3] == 'INV'):
            print(i)
            all_parameters = get_all_parameters()
            c_id = add_component(components[j][i][0], 'INV', site_id)
            add_webdynmetadata(c_id, site_id, j+'_'+str(i), ','.join(components[j][i][1]))
            add_inv_info_auto(c_id, site_id, components[j][i][2])
            for i in components[j][i][1][1:]:
                if(i in all_parameters['CSName'].tolist()):
                    parameter_id = all_parameters.loc[all_parameters['CSName'] == i,'ParameterId'].values[0]
                    add_cleantech_metadata(parameter_id, c_id, site_id)
                else:
                    print('Parameter Skipped!')
        elif(components[j][i][0][0:3] == 'WMS'):
            all_parameters = get_all_parameters()
            c_id = add_component(components[j][i][0], 'WMSIRR', site_id)
            add_webdynmetadata(c_id, site_id, j+'_'+str(i), ','.join(components[j][i][1]))
            for i in components[j][i][1][1:]:
                if(i in all_parameters['CSName'].tolist()):
                    parameter_id = all_parameters.loc[all_parameters['CSName'] == i,'ParameterId'].values[0]
                    add_cleantech_metadata(parameter_id, c_id, site_id)
                else:
                    print('Parameter Skipped!')
        else:
            all_parameters = get_all_parameters()
            print('MFM',i)
            c_id = add_component(components[j][i][0], 'MFM', site_id)
            add_webdynmetadata(c_id, site_id, j+'_'+str(i), ','.join(components[j][i][1]))
            add_mfm_info_auto(c_id, site_id, components[j][i][2], meter_info[j][0], meter_info[j][1])
            for i in components[j][i][1][1:]:
                if(i in all_parameters['CSName'].tolist()):
                    parameter_id = all_parameters.loc[all_parameters['CSName'] == i,'ParameterId'].values[0]
                    add_cleantech_metadata(parameter_id, c_id, site_id)
                else:
                    print('Parameter Skipped!')

if(irr_center=='Self'):
    irr_site_id = site_id
else:
    irr_site_id = get_site_id(irr_center)
add_irr_center(site_id,irr_site_id)
#Need WMS First for PR calculation so sorted in get_ceantec_components function
component_df = get_cleantech_components(site)
print(component_df)
cleantech_component_id = component_df['ComponentId'].tolist()
locus_metadata_df = get_locus_metadata(site)
all_parameter_df = get_all_parameters()


#Add processed Parameters
if(irr_center=='Self'):
    processed_parameters_dict = {"WMS":["DA","GHI","Avg Tmod"],"MFM":['DA','EAC method-1','EAC method-2','Yield-1','Yield-2','PR-1 (GHI)','PR-2 (GHI)','Full Site PR (GHI)','Last recorded value','Grid Availability','Plant Availability'],"INV":['Yield-2','Inverter Availability']}
else:
    processed_parameters_dict = {"MFM":['DA','EAC method-1','EAC method-2','Yield-1','Yield-2','PR-1 (GHI)','PR-2 (GHI)','Full Site PR (GHI)','Last recorded value','Grid Availability','Plant Availability'],"INV":['Yield-2','Inverter Availability']}

for index,row in component_df.iterrows():
    #if silicon or pyranometer in name then use this else
    c_name = row['ComponentName']
    processed_parameters_dict = {"WMSIRR":["DA","GHI","Avg Tmod"],"WMS":["DA","Avg Tmod"],"MFM":['DA','EAC method-1','EAC method-2','Yield-1','Yield-2','PR-1 (GHI)','PR-2 (GHI)','Full Site PR (GHI)','Last recorded value','Grid Availability','Plant Availability'],"INV":['Yield-2','Inverter Availability'],'OTH':[]}
    for i in processed_parameters_dict[row['ComponentType']]:
        parameter_id = all_parameter_df.loc[all_parameter_df['MPName']==i,'ParameterId'].values[0]
        try:
            add_cleantech_metadata(parameter_id, row['ComponentId'], site_id)
        except:
            pass

#Adding MFM, INV COV
inv_component_df = component_df.loc[component_df['ComponentType']=='INV',:]
mfm_component_df = component_df.loc[component_df['ComponentType']=='MFM',:]

if(len(inv_component_df)>1):
    last_inv = inv_component_df.tail(1)
    print(last_inv)
    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Stdev','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_inv['ComponentId'].values[0], site_id)
    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='COV','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_inv['ComponentId'].values[0], site_id)

if(len(mfm_component_df)>1):
    last_mfm = mfm_component_df.tail(1)
    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Stdev','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id)
    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='COV','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id)

parameter_df = get_parameter(site)

#Getting Capacities
mfm_capacity_df = get_mfm_capacities(site) 
inv_capacity_df = get_inv_capacities(site) 

#Raw Parameters
raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
parameter_name_mapping = dict(zip(raw_parameter_df['MPName'],raw_parameter_df['CSName']))
parameter_name_mapping['ts']='Timestamp'
parameter_name_mapping['id']='ComponentId' #This is mapped to LocusId initially

#Processed Parameters
processed_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Processed',:]

#Creatin Table
create_table(site,raw_parameter_df['CSName'].unique().tolist()) 
component_df = get_cleantech_components(site)
print(component_df)
cleantech_component_id = component_df['ComponentId'].tolist()

parameter_df = get_parameter(site)

#Getting Capacities
mfm_capacity_df = get_mfm_capacities(site) 
inv_capacity_df = get_inv_capacities(site) 

#Raw Parameters
raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
parameter_name_mapping = dict(zip(raw_parameter_df['MPName'],raw_parameter_df['CSName']))
parameter_name_mapping['ts']='Timestamp'
parameter_name_mapping['id']='ComponentId' #This is mapped to LocusId initially


cols = raw_parameter_df.loc[raw_parameter_df['ComponentId']==126,'CSName'].tolist()
print(cols)
main_path = '/home/admin/Dropbox/Gen 1 Data/[IN-005W]/2020/2020-09/MFM_1/'
data_df=pd.DataFrame()
for i in sorted(os.listdir((main_path))):
    temp = pd.read_csv(main_path+i,sep='\t')
    data_df=data_df.append(temp)
    print(i)
print(data_df)



