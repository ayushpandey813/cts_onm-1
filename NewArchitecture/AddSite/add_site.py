import pytz
import os
from database_operations import *
from locus_api import *
from process_functions import *
import process_functions
import time

def monthdelta(date, delta):
    m, y = (date.month+delta) % 12, date.year + ((date.month)+delta-1) // 12
    if not m: m = 12
    d = min(date.day, [31,
        29 if y%4==0 and not y%400==0 else 28,31,30,31,30,31,31,30,31,30,31][m-1])
    return date.replace(day=d,month=m, year=y)

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

def save_file(df,site):
    grouped_df = df.groupby('ComponentName')
    for name,group in sorted(grouped_df):
        group['ts']=group['ts'].str.replace('T',' ')
        group.ts=pd.to_datetime(group.ts.str[0:19])
        date_grouped_df=group.groupby(group['ts'].dt.date)
        for name2,group2 in sorted(date_grouped_df):
            chkdir(main_path+site+'/'+str(name2)[0:4]+'/'+str(name2)[0:7]+'/'+str(name))
            group2.to_csv(main_path+site+'/'+str(name2)[0:4]+'/'+str(name2)[0:7]+'/'+str(name)+'/'+str(name2)+'-'+str(name)+'.txt',sep='\t',index=False)

#Main Variables
cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
auto_site_df = pd.read_csv('Generalinfo.csv', sep=',', error_bad_lines=False, index_col=False, dtype='unicode')
auto_comp_df = pd.read_csv('ComponentInfo.csv', sep=',', error_bad_lines=False, index_col=False, dtype='unicode')


#Get Locus Metadata
locus_data = auto_comp_df.groupby('O&M Reference')

for name,group in locus_data:
    main_info_df = auto_site_df.loc[auto_site_df['O&M Reference']==name,:]
    site = name
    site_name = main_info_df['Operation: Operation Name'].values[0]
    location = main_info_df['Location / City'].values[0]
    module_brand = main_info_df['Number and brand/model of modules'].values[0]
    inverter_brand = main_info_df['Number and brand/model of inverters'].values[0]
    cod = main_info_df['COD'].values[0]
    monitoring_provider='Locus'
    gis_loc = None
    alarm_status = 0
    gis_type = 'GHI'
    irr_center = main_info_df['IrrCenter'].values[0].strip()
    locus_site_id = str(int(main_info_df['LocusId'].values[0]))
    print(locus_site_id)
    locus_parameters = []
    azure_locus_parameters = []
    component_id=[] 
    main_path = '/home/pranav/New_Architecture/RawData/'
    chkdir(main_path+site) 
    
    auth = locus_authenticate(site)
    tz = pytz.timezone("Asia/Kuala_Lumpur")

    #Add site
    add_site(site, monitoring_provider,alarm_status,cnxn) #Adding Site
    site_id = get_site_id(site, cnxn)
    if(irr_center=='Self'):
        irr_site_id = site_id
        irr_site = site
    else:
        irr_site_id = get_site_id(irr_center, cnxn)
        irr_site = irr_center
    add_irr_center(site_id,irr_site_id,cnxn) #Adding Irr center Info
    add_site_information(site_id, site_name, location, module_brand, inverter_brand, cod, cnxn) #Adding Site Information

    for index,row in group.iterrows():
        cleantech_name = row['ComponentName']
        cleantech_type = row['ComponentType']
        data_available = get_locus_parameters(auth, row['LocusId'])
        locus_parameters.append(data_available) 
        temp_id = add_component_locus(cleantech_name, cleantech_type, row['LocusId'], ','.join(data_available) , site_id, locus_site_id, cnxn)
        if(cleantech_type=='MFM'):
            size = row['Size']
            meter_ref = row['MeterRef']
            salesforce_ref = row['SalesforceRef']
            add_mfm_info_auto(temp_id, site_id, size, meter_ref, salesforce_ref, cnxn)
        elif(cleantech_type=='INV'):
            size =row['Size']
            add_inv_info_auto(temp_id, site_id, size, cnxn)
        component_id.append(temp_id)

    for i in locus_parameters:
        print(i)
        temp=[]
        for j in i:
            print(j)
            if(('avg' in j) or ('TotWhExp_max' in j) or ('TotWhImp_max' in j) or ('Wh_sum' in j)):
                temp.append(j)
        azure_locus_parameters.append(temp)

    #Add Locus and Cleantech Parameters/Metadata
    for index,c in enumerate(component_id):
        print(c)
        for i in azure_locus_parameters[index]:
            all_parameters = get_all_parameters(cnxn)
            if(i!='ts'):
                if(i in all_parameters['MPName'].tolist()):
                    parameter_id = all_parameters.loc[all_parameters['MPName'] == i,'ParameterId'].values[0]
                    print(i)
                    add_cleantech_metadata(parameter_id, c, site_id, cnxn)
                else:
                    res = input("Add Parameter "+i+"? (y or n)")
                    print(res)
                    if(res=='y'):
                        cs_name = input("Enter Cleantech Parameter Name:")
                        parameter_id = add_parameter(i, cs_name, 'Raw', cnxn)
                        print(i)
                        add_cleantech_metadata(parameter_id, c, site_id, cnxn)
                    elif(res=='n'):
                        print('Parameter not added')

    #Need WMS First for PR calculation so sorted in get_ceantec_components function
    component_df = get_cleantech_components(site, cnxn)
    print(component_df)
    cleantech_component_id = component_df['ComponentId'].tolist()
    locus_metadata_df = get_locus_metadata(site, cnxn)
    all_parameter_df = get_all_parameters(cnxn)

    irr_center_component_df = get_cleantech_components(irr_site, cnxn)
    print(irr_center_component_df)
    wms_component_df = irr_center_component_df.loc[irr_center_component_df['ComponentType']=='WMSIRR',:]
    if(len(wms_component_df)==0):
        pass
    elif(len(wms_component_df)>1):
        print(wms_component_df)
        c_id = input("Enter Main Irradiance Sensor Id:")
        add_irr_component(site_id, c_id, cnxn) 
    else:
        c_id = wms_component_df['ComponentId'].values[0]
        add_irr_component(site_id, c_id, cnxn) 

    inv_component_df = component_df.loc[component_df['ComponentType']=='INV',:]
    mfm_component_df = component_df.loc[component_df['ComponentType']=='MFM',:]
    last_inv = inv_component_df.tail(1)
    last_mfm = mfm_component_df.tail(1)
    
    for index,row in component_df.iterrows():
        #if silicon or pyranometer in name then use this else
        c_name = row['ComponentName']
        if group[group['ComponentType']=='WMSIRR']['ComponentType'].count() <= 1:
          processed_parameters_dict = {"WMSIRR":["DA","GHI","Avg Tmod"],"WMS":["DA","Avg Tmod"],"MFM":['DA','EAC method-1','EAC method-2','Yield-1','Yield-2','PR-1 (GHI)','PR-2 (GHI)','Last recorded value','Grid Availability','Plant Availability'],"INV":['Yield-2','Inverter Availability'],'OTH':[]}
        elif group[group['ComponentType']=='WMSIRR']['ComponentType'].count() == 2:
          processed_parameters_dict = {"WMSIRR":["DA","GHI","Avg Tmod"],"WMS":["DA","Avg Tmod"],"MFM":['DA','EAC method-1','EAC method-2','Yield-1','Yield-2','PR-1 (GHI)','PR-2 (GHI)','PR-1 (GHI) (Sensor 2)','PR-2 (GHI) (Sensor 2)','Last recorded value','Grid Availability','Plant Availability'],"INV":['Yield-2','Inverter Availability'],'OTH':[]}
        else:
          processed_parameters_dict = {"WMSIRR":["DA","GHI","Avg Tmod"],"WMS":["DA","Avg Tmod"],"MFM":['DA','EAC method-1','EAC method-2','Yield-1','Yield-2','PR-1 (GHI)','PR-2 (GHI)','PR-1 (GHI) (Sensor 2)','PR-2 (GHI) (Sensor 2)','PR-1 (GHI) (Sensor 3)','PR-2 (GHI) (Sensor 3)','Last recorded value','Grid Availability','Plant Availability'],"INV":['Yield-2','Inverter Availability'],'OTH':[]}
        for i in processed_parameters_dict[row['ComponentType']]:
            parameter_id = all_parameter_df.loc[all_parameter_df['MPName']==i,'ParameterId'].values[0]
            try:
                add_cleantech_metadata(parameter_id, row['ComponentId'], site_id, cnxn)
            except:
                pass
            
    #Adding MFM, INV COV
    if(len(inv_component_df)>1):
        parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Stdev','ParameterId'].values[0]
        add_cleantech_metadata(parameter_id, last_inv['ComponentId'].values[0], site_id, cnxn)
        parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='COV','ParameterId'].values[0]
        add_cleantech_metadata(parameter_id, last_inv['ComponentId'].values[0], site_id, cnxn)

    if(len(mfm_component_df)>1):
        parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Stdev','ParameterId'].values[0]
        add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id, cnxn)
        parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='COV','ParameterId'].values[0]
        add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id, cnxn)

    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Full Site PR (GHI)','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id, cnxn) 

    #Only if GTI
    #parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Full Site PR (GTI)','ParameterId'].values[0]
    #add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id, cnxn)    

    parameter_df = get_parameter(site, cnxn)

    #Getting Capacities
    mfm_capacity_df = get_mfm_capacities(site, cnxn) 
    inv_capacity_df = get_inv_capacities(site, cnxn) 

    #Raw Parameters
    raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
    parameter_name_mapping = dict(zip(raw_parameter_df['MPName'],raw_parameter_df['CSName']))
    parameter_name_mapping['ts']='Timestamp'
    parameter_name_mapping['id']='ComponentId' #This is mapped to LocusId initially

    #Processed Parameters
    processed_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Processed',:]

    #Creatin Table
    create_table(site,raw_parameter_df['CSName'].unique().tolist(), cnxn)
    if(int(site[3])>=4):
        shell_cols  = get_columns(site, cnxn)
        for col in raw_parameter_df['CSName'].unique().tolist():
            if col in shell_cols:
                pass  
            else:
                add_column(site,col, cnxn)
    print('Done',site)
    time.sleep(5)

 
cnxn.close()






