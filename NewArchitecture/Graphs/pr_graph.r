options(warn=-1)
suppressMessages(library(ggplot2))
suppressMessages(library(zoo))
suppressMessages(library(grid))
suppressMessages(library(lubridate))
suppressMessages(library(RODBC))

args<-commandArgs(TRUE)
code = args[1]
date = args[2]

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

checkdir(paste("/home/admin/Graphs/Graph_Extract", code, sep="/"))
checkdir(paste("/home/admin/Graphs/Graph_Output", code, sep="/"))
pathwritetxt <- paste("/home/admin/Graphs/Graph_Extract", code, paste("[", code, "] Graph ", date, " - PR Evolution.txt", sep=""), sep="/")
graph <- paste("/home/admin/Graphs/Graph_Output", code, paste("[", code, "] Graph ", date, " - PR Evolution.pdf", sep=""), sep="/")

RODBC_connection <- odbcDriverConnect('driver={ODBC Driver 17 for SQL Server};server=cleantechsolar.database.windows.net;database=Cleantech Meter Readings;uid=RohanKN;pwd=R@h@nKN1')
query = paste("SELECT * FROM [Portfolio].[Limit] WHERE SiteId = (SELECT SiteId FROM [Portfolio].Site WHERE SiteCode='",code,"')",sep="")
limit_info<- sqlQuery(channel=RODBC_connection, query = query)
prbudget = limit_info[,'PRLimit']
rate = limit_info[,'DegradationRate']
g_type =limit_info[,'Tilt']

query = paste("SELECT  [SiteId],[SiteName],[Location],[ModuleInfo],[InverterInfo],[COD] FROM [Portfolio].[SiteInformation] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='",code,"')",sep="")
site_info<- sqlQuery(channel=RODBC_connection, query = query)
site_info = site_info[1,]

coddate = site_info[,6] #Change this

if(coddate!='TBC'){
  coddate=as.Date(coddate)
}
print(coddate)
query = paste("SELECT [SiteId],[IrradiationCenterSiteId],[MainIrradiationSensor] FROM [Portfolio].[IrrInfo] WHERE SiteId =(SELECT SiteId FROM Portfolio.Site WHERE SiteCode = '",code,"')",sep="")
irr_info<- sqlQuery(channel=RODBC_connection, query = query)
main_irr_id = irr_info[,3]
if(g_type=='GHI'){
query = paste("SELECT [Date],[Value] FROM [Portfolio].[ProcessedData] WHERE ComponentId =",main_irr_id," AND [ParameterId]=2 ORDER BY [Date]",sep="")
irr_df<- sqlQuery(channel=RODBC_connection, query = query)
colnames(irr_df)[2] <- "GHI"
query = paste("SELECT [Date],Portfolio.ProcessedData.ComponentId,[Value] FROM [Portfolio].[ProcessedData] INNER JOIN Portfolio.Component ON Portfolio.Component.ComponentId = Portfolio.ProcessedData.ComponentId  WHERE Portfolio.ProcessedData.SiteId=(SELECT SiteId FROM Portfolio.Site WHERE SiteCode='",code,"') AND ComponentType='MFM' AND ParameterId=69 ORDER BY [Date]",sep="")
pr_df<- sqlQuery(channel=RODBC_connection, query = query)
colnames(pr_df)[2] <- "Meter_Id"
colnames(pr_df)[3] <- "PR"
}else{
query = paste("SELECT [Date],[Value] FROM [Portfolio].[ProcessedData] WHERE ComponentId =",main_irr_id," AND [ParameterId]=304 ORDER BY [Date]",sep="")
irr_df<- sqlQuery(channel=RODBC_connection, query = query)
colnames(irr_df)[2] <- "GHI"
query = paste("SELECT  [Date],Portfolio.ProcessedData.ComponentId,[Value] FROM [Portfolio].[ProcessedData] INNER JOIN Portfolio.Component ON Portfolio.Component.ComponentId = Portfolio.ProcessedData.ComponentId  WHERE Portfolio.ProcessedData.SiteId=(SELECT SiteId FROM Portfolio.Site WHERE SiteCode='",code,"') AND ComponentType='MFM' AND ParameterId=307 ORDER BY [Date]",sep="")
pr_df<- sqlQuery(channel=RODBC_connection, query = query)
colnames(pr_df)[2] <- "Meter_Id"
colnames(pr_df)[3] <- "PR"
}
odbcClose(RODBC_connection)

dff = merge(x = pr_df, y = irr_df, by = "Date", all.x = TRUE)
dff = dff[,c(1,4,3,2)]

#Getting rid of low PR's
#for(x in 1 :nrow(dff))
#{
#	if(is.finite(dff[x,3]) && (as.numeric(dff[x,3]) < 10))# || (as.numeric(dff[x,4]) < 50)
#		dff[x,3] = NA
#}

dff = dff[complete.cases(as.numeric(dff[,3])),]

dff = dff[(dff[,3] <= 95),]
dff = dff[complete.cases(as.numeric(dff[,2])),]
dff = dff[(as.Date(dff$Date)<= date),]

yr_cnt=0
if(inherits(coddate, c("Date", "POSIXt"))){
  dff = dff[(as.Date(dff$Date)>= coddate),]
  yr_cnt=time_length(difftime(as.Date(date), coddate), "years")
}

dff = unique(dff)
if(irr_info[,1]!=irr_info[,2]){
  dff[(dff[,3] >= 60) & (dff[,3] <= 70),'PR']=70
  dff[(dff[,3] >= 90),'PR']=85
}
#moving average 7d/30d
zoo.PR <- zoo(dff[,3], as.Date(dff$Date))
if(nrow(dff)<120){
ma1 <- rollapplyr(zoo.PR, list(-(6:1)), mean, fill = NA, na.rm = T)
}else{
ma1 <- rollapplyr(zoo.PR, list(-(29:1)), mean, fill = NA, na.rm = T)
}
dff$ambPR.av = coredata(ma1)
row.names(dff) <- NULL

firstdate <- as.Date(dff[1,1])
lastdate <- as.Date(dff[length(dff[,1]),1])
PRUse = as.numeric(dff$PR)
PRUse = PRUse[complete.cases(PRUse)]
last7 = round(mean(tail(PRUse,7)),1)
last30 = round(mean(tail(PRUse,30)),1)
last60 = round(mean(tail(PRUse,60)),1)
last90 = round(mean(tail(PRUse,90)),1)
last365 = round(mean(tail(PRUse,365)),1)
lastlt = round(mean(PRUse),1)
rightIdx = nrow(dff) * 0.75
text_pos = round(nrow(dff) * 0.40, 0)-nrow(dff)* yr_cnt *0.02
seg_start=nrow(dff)* 0.35-nrow(dff)* yr_cnt *0.02
seg_end=nrow(dff)* 0.38-nrow(dff)* yr_cnt *0.02

#data sorting for visualisation
dff$colour[dff[,2] < 2] <- '< 2'
dff$colour[dff[,2] >= 2 & dff[,2] <= 4] <- '2 ~ 4'
dff$colour[dff[,2] >= 4 & dff[,2] <= 6] <- '4 ~ 6'
dff$colour[dff[,2] > 6] <- '> 6'
dff$colour = factor(dff$colour, levels = c("< 2", "2 ~ 4", "4 ~ 6", "> 6"), labels =c('< 2', '2 ~ 4', '4 ~ 6', '> 6')) #to fix legend arrangement
titlesettings <- theme(plot.title = element_text(face = "bold", size = 12, lineheight = 0.7, hjust = 0.5, margin = margin(0,0,7,0)), plot.subtitle = element_text(face = "bold", size = 12, lineheight = 0.9, hjust = 0.5))

p <- ggplot() + theme_bw()

#30-d / 7-d average line

p <- p + geom_point(data=dff, aes(x=as.Date(Date), y = PR, shape = as.factor(18), colour = colour), size = 2) + guides(shape = FALSE)
p <- p + ylab("Performance Ratio [%]") + xlab("") + coord_cartesian(ylim = c(0,100))
if(nrow(dff)<120){
    p <- p + scale_x_date(expand=c(0,0),date_breaks = "7 days", date_labels = "%d-%b", limits = c(firstdate, NA)) + scale_y_continuous(breaks=seq(0, 100, 10))
    p <- p + annotate("segment", x = as.Date(dff[seg_start,1]), xend = as.Date(dff[seg_end,1]), y=45, yend=45, colour="red", size=1.5)
    p <- p + annotate('text', label = "7-d moving average of PR", y = 45, x = as.Date(dff[text_pos,1]), colour = "red",fontface =2,hjust = 0)
}else{
    p <- p + scale_x_date(expand=c(0,0),date_breaks = "3 months", date_labels = "%b/%y", limits = c(firstdate, NA)) + scale_y_continuous(breaks=seq(0, 100, 10))
    p <- p + annotate("segment", x = as.Date(dff[seg_start,1]), xend = as.Date(dff[seg_end,1]), y=45, yend=45, colour="red", size=1.5)
    p <- p + annotate('text', label = "30-d moving average of PR", y = 45, x = as.Date(dff[text_pos,1]), colour = "red",fontface =2,hjust = 0)
}

if(yr_cnt>=4){
p <- p + theme(plot.margin = margin(10, 20, 0, 10),axis.title.x = element_text(size=13), axis.title.y = element_text(size=13), axis.text = element_text(size=13), panel.grid.minor = element_blank(),                 panel.border = element_blank(), axis.line = element_line(colour = "black"), legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97),axis.text.x = element_text(angle = 90,vjust = 0.5, hjust=1))                     #LEGNED AT RHS
}else{
p <- p + theme(plot.margin = margin(10, 20, 0, 10),axis.title.x = element_text(size=13), axis.title.y = element_text(size=13), axis.text = element_text(size=13), panel.grid.minor = element_blank(),                 panel.border = element_blank(), axis.line = element_line(colour = "black"), legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97))   
}

p <- p + titlesettings + ggtitle(paste0('Performance Ratio Evolution - ', code, sep=''), subtitle = paste0('From ', firstdate, ' to ', lastdate))
p <- p + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))
p <- p + scale_colour_manual('Daily Irradiation [kWh/m2]', values = c("blue", "deepskyblue1", "orange","darkorange3"), labels = c('< 2','2 ~ 4','4 ~ 6','> 6'),                                 guide = guide_legend(title.position = "left", ncol = 4, nrow=1)) 
p <- p + scale_shape_manual(values = 18)
p <- p + theme(legend.box = "horizontal", legend.direction="horizontal") #legend.position = "bottom"

coddate_og=coddate

if(inherits(coddate, c("Date", "POSIXt"))){
  invisible()
}else{
  coddate=firstdate
}
#Creating steps using budget PR
prthresh = c(prbudget,prbudget)
dates = c(firstdate,coddate)
temp=prbudget
while(coddate<as.Date(date) && (as.Date(date)-coddate)>365){#
     coddate= coddate %m+% months(12)
     temp=temp-(temp*rate)
     prthresh=c(prthresh,temp)
     dates=c(dates,coddate)
}
prthresh=c(prthresh,temp)
dates=c(dates,lastdate)
p <- p +geom_step(mapping=aes(x=dates,y=prthresh),size=1,color='darkgreen')
dff$Date=as.Date(dff$Date, format = "%Y-%m-%d")
total=0
text="Target Budget Yield Performance Ratio ["
for(i in 2:(length(dates)-1)){#n-1 because we are taking number of years between. 
    if(i==(length(prthresh)-1)){
    text=paste(text,i-1,'Y-',format(round(prthresh[i],1),nsmall=1), "%]", sep="")
    }else{
    text=paste(text,i-1,'Y-',format(round(prthresh[i],1),nsmall=1), "%,", sep="")
    }
}
for(i in 1:(length(dates)-1)){
    if(i==(length(dates)-1)){
      total=total+sum(dff[dff$Date >= dates[i] & dff$Date <=dates[i+1],'PR']>prthresh[i])
    }else{
      total=total+sum(dff[dff$Date >= dates[i] & dff$Date < dates[i+1],'PR']>prthresh[i])
    }
}

#Adding Legend + Stats
p <- p + guides(colour = guide_legend(override.aes = list(shape = 18)))
p <- p + geom_line(data=dff, aes(x=as.Date(Date), y=ambPR.av), color="red", size=1.5)
p <- p + annotate("segment", x = as.Date(dff[seg_start,1]), xend = as.Date(dff[seg_end,1]), y=50, yend=50, colour="darkgreen", size=1.5)
p <- p + annotate('text', label = text, y = 50, x = as.Date(dff[text_pos,1]), colour = "darkgreen",fontface =2,hjust = 0)
p <- p + annotate('text', label = paste("Points above Target Budget PR = ",total,'/',nrow(dff),' = ',round((total/nrow(dff))*100,1),'%',sep=''), y = 40, x = as.Date(dff[text_pos,1]), colour = "black",fontface =2,hjust = 0)
p <- p + annotate('text', label = paste('Commissioning Date: ',coddate_og,sep=" "), y = 35, x = as.Date(dff[text_pos,1]), colour = "black",fontface =2,hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 7-d:", last7, "%"), y=30, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 30-d:", last30, "%"), y=25, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 60-d:", last60, "%"), y=20, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 90-d:", last90, "%"), y=15, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 365-d:", last365,"%"), y=10, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR Lifetime:", lastlt,"%"), y=5, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0,fontface=2)
p <- p + annotate("segment", x = as.Date(dff[nrow(dff),1]), xend = as.Date(dff[nrow(dff),1]), y=0, yend=100, colour="deeppink4", size=1.5, alpha=0.5)

ggsave(paste0(graph), p, width = 11.5, height=8)
write.table(dff, na = "", pathwritetxt, row.names = FALSE, sep ="\t")
