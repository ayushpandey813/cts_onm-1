import pandas as pd
import os
import numpy as np
import pandas as pd
import datetime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats
import matplotlib.dates as mdates
from matplotlib.ticker import MaxNLocator
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.patches as mpatches
import pyodbc
import sys
from bisect import bisect
from matplotlib.text import Annotation
from matplotlib.transforms import Affine2D
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import pymsteams

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

class LineAnnotation(Annotation):
    def __init__(
        self, text, line, x , line_type, xytext=(0, 5), textcoords="offset points", **kwargs
    ):
        assert textcoords.startswith(
            "offset "
        ), "*textcoords* must be 'offset points' or 'offset pixels'"

        self.line = line
        self.xytext = xytext

        # Determine points of line immediately to the left and right of x
        xs, ys = line.get_data()

        assert (
            np.diff(xs) >= 0
        ).all(), "*line* must be a graph with datapoints in increasing x order"

        i = np.clip(bisect(xs, x), 1, len(xs) + 1)
        self.neighbours = n1, n2 = np.asarray([(xs[i - 1], ys[i - 1]), (xs[i], ys[i])])

        # Calculate y by interpolating neighbouring points
        if(line_type == 'below'):
            y = n1[1] + ((x - n1[0]) * (n2[1] - n1[1]) / (n2[0] - n1[0]))-3
        else:
            y = n1[1] + ((x - n1[0]) * (n2[1] - n1[1]) / (n2[0] - n1[0]))

        kwargs = {
            "horizontalalignment": "center",
            "rotation_mode": "anchor",
            **kwargs,
        }
        super().__init__(text, (x, y), xytext=xytext, textcoords=textcoords, **kwargs)

    def get_rotation(self):

        transData = self.line.get_transform()
        dx, dy = np.diff(transData.transform(self.neighbours), axis=0).squeeze()
        return np.rad2deg(np.arctan2(dy, dx))

    def update_positions(self, renderer):
        xytext = Affine2D().rotate_deg(self.get_rotation()).transform(self.xytext)
        self.set_position(xytext)
        super().update_positions(renderer)


def line_annotate(text, line, x , line_type, *args, **kwargs):
    ax = line.axes
    a = LineAnnotation(text, line, x, line_type, *args, **kwargs)
    if "clip_on" in kwargs:
        a.set_clip_path(ax.patch)
    ax.add_artist(a)
    return a

def get_total_eac(date):
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    SQL_Query = pd.read_sql_query("SELECT SUM([Eac-MFM]) AS SUM FROM [dbo].[Stations_Data] WHERE Station_Id=(SELECT Station_Id FROM [dbo].[Stations] WHERE Station_Name='"+site+"') AND Date='"+date+"' ", connStr)
    df_sum = pd.DataFrame(SQL_Query, columns=['SUM'])
    return df_sum

def teams_alert(df_today,slope,intercept,webhook,site):
    info ='Date: '+df_today['Date'].values[0]+'<br>'

    today_g = df_today['GHI'].values[0]
    today_pr = df_today['PR'].values[0]
    info = info+"GHI : "+str(today_g)+'<br>'
    info = info+"PR : "+str(round(today_pr,1))+'<br>'
    temp_trig = (slope*today_g+(intercept))
    trig_val = round(((temp_trig - today_pr)/temp_trig)*100,1)
    info = info + 'Alarm Trigger Value: '+str(trig_val)+' %<br>'
    #myTeamsMessage = pymsteams.connectorcard("https://cleantechenergycorp.webhook.office.com/webhookb2/264f6275-69ea-4733-80c7-c29cea53c91e@8ec327b2-e844-412c-8463-3e633202b00f/IncomingWebhook/de083bfce91644178ca6c18676261768/b91f3ec7-71fb-437f-8eab-f5905ff0c963")

    myTeamsMessage = pymsteams.connectorcard(webhook)
    myTeamsMessage.title(site +" PR Corridor ALERT")
    myTeamsMessage.text("<pre>"+info+"</pre>")
    myTeamsMessage.send()


def abline(slope, intercept, df, df_today):
    x_vals = np.array(ax.get_xlim())
    y_vals = intercept + slope * x_vals
    y_vals2 = (intercept+0.05*intercept) + slope * x_vals
    y_vals3 = (intercept-0.05*intercept) + slope * x_vals
    g = df['GHI'].tolist()
    p = df['PR'].tolist()
    cnt=0
    for index,i in enumerate(g):
        temp = slope*i+(intercept+0.05*intercept)
        temp2 = slope*i+(intercept-0.05*intercept)
        if(p[index]>temp or p[index]<temp2):
            cnt=cnt+1
    total = len(df)
    ax.annotate('Inside corridor = '+str(total-cnt)+'/'+str(total)+' = '+str(round(((total-cnt)/total)*100,1))+'%', (.99, .05),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='right', va='bottom',size=11)
    plt.plot(x_vals, y_vals,color='darkgreen')
    line_1 = plt.plot(x_vals, y_vals2,'--',color='#008000')
    line_2 = plt.plot(x_vals, y_vals3,'--',color='#008000')
    line_annotate('+5% variation from mean',line_1[0],4,'above',color='#008000')
    line_annotate('-5% variation from mean',line_2[0],3.8,'below',color='#008000')
    try:
        if((today_pr<slope*today_g+(intercept-0.05*intercept))):
            print('Sending Alert')
            teams_alert(df_today,slope,intercept,webhook,site)
    except:
        pass
      


    
font = {'size'   : 11}

matplotlib.rc('font', **font)


site = sys.argv[1]
pr_ghi_columns = sys.argv[2]
start_date = sys.argv[3]
date_today = sys.argv[4]

try:
    pr_limit= sys.argv[5]
except:
    pr_limit = 0

if(site=='SG-003'):
    path = '/home/admin/Graphs/Graph_Extract/'+site+'/[SG-003] Graph '+date_today+' - PR Evolution - Full Site.txt'
else:
    path='/home/admin/Graphs/Graph_Extract/'+site+'/['+site+'] Graph '+date_today+' - PR Evolution.txt'
file_path = '/home/admin/Dropbox/Second Gen/['+site+'S]/'+date_today[0:4]+'/'+date_today[0:7]+'/['+site+'S] '+date_today+'.txt'

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
limits = pd.read_sql_query('''  SELECT  * FROM [dbo].[Station_Limits] ''', connStr)
df_limits = pd.DataFrame(limits, columns=['O&M_Code','MFM_limit','INV_limit','PR_limit','Webhook'])
df_limits['O&M_Code'] = df_limits['O&M_Code'].str.strip()
webhook = df_limits.loc[df_limits['O&M_Code']==site,'Webhook'].values[0].strip()
connStr.close()

df=pd.read_csv(path,sep='\t')
df = df.iloc[:,:3]
df.columns = ['Date','GHI','PR']
try:
    df=df[df['PR']>=float(pr_limit)]
    print('done')
except Exception as e:
    print(e)
    print('failed')

if(site=='MY-006'):
    df=df[df['PR']>10]

df=df[df['Date']>=start_date]
df_last_14 = df.tail(15)[:-1]
print(df_last_14)
df_temp = df.copy()

if(os.path.exists(file_path)):
    df_today = pd.read_csv(file_path,sep='\t')
    df_today = df_today[['Date']+pr_ghi_columns.split(',')]
    if(site=='SG-003'):
        print('in')
        eac_sg003 = round(df_today['FullSiteProd'].values[0]/1000,1)
        df_today['FullSiteProd'] = df_today['FullSiteProd']/1621.8 
        df_today['FullSiteProd'] = (df_today['FullSiteProd']/df_today['Gsi00'])*100
    df_today.columns = ['Date','GHI','PR']
else:
    df_today = df[df['Date']==date_today]

if(df_today.empty):
    df_today = pd.DataFrame({'Date':[date_today],'GHI':[0],"PR":[0]})

print(df_today)
df = df[(df['Date']!=date_today)]

fig3 = plt.figure(num=None, figsize=(13.2  , 8.8))

ax = fig3.add_subplot(111)

shape_dict = {1:'1',2:'2',3:'<',4:'4',5:'p',6:'h',7:'v',8:'>',9:'s',10:'^',11:'o',12:'d',2020:'.'}
temp_dict_2 = {}

dates = df['Date'].tolist()
g_vals = df['GHI'].tolist()
pr_vals = df['PR'].tolist()


for index,i in enumerate(pr_vals):
    month = datetime.datetime.strptime(dates[index], '%Y-%m-%d').month
    year = datetime.datetime.strptime(dates[index], '%Y-%m-%d').year
    if(g_vals[index]<2):
        c='darkblue'
    elif(g_vals[index]>=2 and g_vals[index]<=4):
        c='deepskyblue'
    elif(g_vals[index]>=4 and g_vals[index]<=6):
        c='orange'
    elif(g_vals[index]>6):
        c='chocolate'
    if(year==2020):
        ln15=ax.scatter(g_vals[index], i, marker=shape_dict[year],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[year] = [ln15,'2020']
    elif(month == 1 and year==2021):
        ln14=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = [ln14,datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b-%y')]
    elif(month == 2 and year==2021):
        ln13=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = [ln13,datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b-%y')]
    elif(month == 3 and year==2021):
        ln12=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = [ln12,datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b-%y')]
    elif(month == 4 and year==2021):
        ln11=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = [ln11,datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b-%y')]
    elif(month == 5 and year==2021):
        ln10=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = [ln10,datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b-%y')]
    elif(month == 6 and year==2021):
        ln9=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = [ln9,datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b-%y')]
    elif(month == 7 and year==2021):
        ln8=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = [ln8,datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b-%y')]
    elif(month == 8 and year==2021):
        ln7=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = [ln7,datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b-%y')]
    elif(month == 9 and year==2021):
        ln1=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = [ln1,datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b-%y')]
    elif(month == 10 and year==2021):
        ln2=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = [ln2,datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b')]
    elif(month == 11 and year==2021):
        ln3=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = [ln3,datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b')]   
    elif(month == 12 and year==2021):
        ln6=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = [ln6,datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b')]
   

for index,row in df_last_14.iterrows():
    ln16=ax.scatter(row['GHI'], row['PR'], marker='*',color='black',s=60, alpha=0.75, zorder=3)
    temp_dict_2[14] = [ln16,'Last 14 days']

today_g = df_today['GHI'].values[0]
today_pr = df_today['PR'].values[0]
ln5 = ax.scatter(today_g, today_pr, marker='*',color='red',s=60, alpha=0.75, zorder=3)
temp_dict_2[15] = [ln5,date_today]

g_patch = mpatches.Patch(color='white', label='GHI [kWh/m2]')
kh_patch = mpatches.Patch(color='darkblue', label='< 2')
th_patch = mpatches.Patch(color='deepskyblue', label='2 ~ 4')
vn_patch = mpatches.Patch(color='orange', label='4 ~ 6')
ph_patch = mpatches.Patch(color='chocolate', label='> 6')

X = df['GHI'].values.reshape(-1, 1)
Y = df['PR'].values.reshape(-1, 1)
linear_regressor = LinearRegression()  # create object for the class
linear_regressor.fit(X, Y)  # perform linear regression
slope=linear_regressor.coef_[0][0]
intercept=linear_regressor.intercept_[0]
keys = []
vals = []
for i in temp_dict_2:
    keys.append(temp_dict_2[i][0])
    vals.append(temp_dict_2[i][1])
legend = plt.legend(keys,vals,scatterpoints=1,fontsize=11,loc=(0.05,.05),frameon=False)
totlen=len(vals)
for m in range(totlen-1):
    legend.legendHandles[m].set_color('black')

plt.gca().add_artist(legend)
plt.legend(handles=[g_patch,kh_patch,th_patch,vn_patch,ph_patch],loc=(0.38,.92),frameon=False,ncol=5,fontsize=11)
x=[today_g]
y=[today_pr]
plt.ylim(0,100)
plt.xlim(0,8)
plt.vlines(x, 0, y, linestyle="dashed",color='red')
plt.hlines(y, 0, x, linestyle="dashed",color='red' )
plt.text(x[0]-.15,10, str(x[0])+ ' [kWh/m2]', ha='left', va='center', rotation=90,color='red')
plt.text(0.05,y[0]-2, str(round(y[0],1))+' [%]', ha='left', va='center',color='red')
df_sum = get_total_eac(date_today)
if(site=='SG-003'):
    total_eac = eac_sg003
else:
    total_eac = (df_sum['SUM'].values[0])/1000
plt.text(x[0]/2,y[0]/2, 'EAC = '+str(round(total_eac,1))+' MWh', ha='left', va='center',color='red')
abline(slope,intercept,df_temp,df_today)

#ax3.plot(df['GHI'],Y_pred,color='black',lw=2,ls='-',label='Linear Fit')

ax.set_ylabel("PR [%]")
ax.set_xlabel("GHI [kWh/m2]")
ttl = ax.set_title('From '+str(df['Date'].head(1).values[0])[0:10]+' to '+str(df_temp['Date'].tail(1).values[0])[0:10], fontdict={'fontsize': 13, 'fontweight': 'medium'})
ttl.set_position([.495, .15])
ttl_main=fig3.suptitle(''+site+' GHI vs PR',fontsize=15,x=.512,y=0.95)

ax.annotate('y='+str(round(slope,2))+'x+'+str(round(intercept,2)), (.99, .1),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='right', va='bottom',size=11)

fig3.savefig("/home/admin/Graphs/Graph_Output/"+site+"/["+site+"] Graph "+date_today+" - GHI vs PR.pdf", bbox_inches='tight')
df_temp.to_csv("/home/admin/Graphs/Graph_Extract/"+site+"/["+site+"] Graph "+date_today+" - GHI vs PR.txt",sep='\t')


