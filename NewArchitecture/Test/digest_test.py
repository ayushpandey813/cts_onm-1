import pytz
import os
import numpy as np
import sys
from database_operations import *
from locus_api import *
from process_functions import *
from exception import *
import process_functions
from html_mail import *
import html_mail

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

def get_tz(site):
    if(site[0:2]=='MY'):
        timezone = "Asia/Kuala_Lumpur"
    elif(site[0:2]=='TH'):
        timezone = "Asia/Bangkok"
    elif(site[0:2]=='VN'):
        timezone = "Asia/Saigon"
    elif(site[0:2]=='PH'):
        timezone = "Asia/Manila"
    elif(site[0:2]=='KH'):
        timezone = "Asia/Phnom_Penh"
    else:
        timezone = "Asia/Calcutta"
    return timezone

def process_data(date,df,component_id,site_id,cnxn):
    date = date.strftime('%Y-%m-%d')
    component_parameters_df = processed_parameter_df.loc[processed_parameter_df['ComponentId']==component_id,:]
    #component_parameters_df = processed_parameter_df.loc[((processed_parameter_df['ParameterId']==14) | (processed_parameter_df['ParameterId']==324) | (processed_parameter_df['ParameterId']==325) | (processed_parameter_df['ParameterId']==344) | (processed_parameter_df['ParameterId']==345)),:]
    component_type = component_df.loc[component_df['ComponentId']==component_id,'ComponentType'].values[0]
    if(component_type.strip()=='MFM'):
        capacity = mfm_capacity_df.loc[mfm_capacity_df['ComponentId']==component_id,'Size'].values[0]
    elif(component_type.strip()=='INV'):
        capacity = inv_capacity_df.loc[inv_capacity_df['ComponentId']==component_id,'Size'].values[0]
    elif(component_type.strip()=='WMSIRR'):
        capacity = 0 
    elif(component_type.strip()=='WMS'):
        capacity = 0    
    for index,row in component_parameters_df.iterrows():
        params=getattr(process_functions, 'function_mapping')[row['CSName']] #To use 1s,2nd or 3rd Irr sensor to calculate PR with other sensors.
        if(len(params)>1):
            result=getattr(process_functions, function_mapping[row['CSName']][0])(df=df,date=date,component_type=component_type,capacity=capacity,component_df=component_df,irr_center_component_df=irr_center_component_df,site=site,site_id=site_id,mfm_capacity_df=mfm_capacity_df,wms_raw_data = wms_raw_data,custom_parameter = params[1],gran=5,irr_center_parameter_df=irr_center_parameter_df,cnxn=cnxn)
        else:
            result=getattr(process_functions, function_mapping[row['CSName']][0])(df=df,date=date,component_type=component_type,capacity=capacity,component_df=component_df,irr_center_component_df=irr_center_component_df,site=site,site_id=site_id,mfm_capacity_df=mfm_capacity_df,wms_raw_data = wms_raw_data,custom_parameter = 0,gran=5,irr_center_parameter_df=irr_center_parameter_df,cnxn=cnxn)
        upsert_processed(date,result,row['ParameterId'],component_id,site_id,cnxn)

cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
site = sys.argv[1]
only_mfm_flag = 1
digest_flag = 0
tz = pytz.timezone("Asia/Calcutta")
locus_parameters = []
azure_locus_parameters = []
component_id=[]
site_id = get_site_id(site,cnxn) 
auth = locus_authenticate(site)
irr_center = get_irr_center(site_id,cnxn)
irr_center_id = get_site_id(irr_center,cnxn)


#Need WMS First for PR calculation so sorted in get_ceantec_components function
component_df = get_cleantech_components(site,cnxn)
cleantech_component_id = component_df['ComponentId'].tolist()
locus_metadata_df = get_locus_metadata(site,cnxn)
locus_site_id = locus_metadata_df['LocusSiteId'].values[0] 
all_parameter_df = get_all_parameters(cnxn)
parameter_df = get_parameter(site,cnxn)

#Getting Capacities
mfm_capacity_df = get_mfm_capacities(site,cnxn) 
inv_capacity_df = get_inv_capacities(site,cnxn) 

#Raw Parameters
raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
parameter_name_mapping = dict(zip(raw_parameter_df['MPName'],raw_parameter_df['CSName']))

#If both GTI and GHI are recorded
if('POAI_avg' in list(parameter_name_mapping.keys()) and 'GHI_avg' in list(parameter_name_mapping.keys()) ):
    print('Converting Irr Column')
    parameter_name_mapping['POAI_avg'] = 'Irradiance_GTI'

parameter_name_mapping = column_mismatch(site,parameter_name_mapping)
parameter_name_mapping['ts']='Timestamp'
parameter_name_mapping['id']='ComponentId' #This is mapped to LocusId initially

#Processed Parameters
processed_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Processed',:]


#Getting Irr center components
if(irr_center == site):
    irr_center_component_df = component_df
    irr_center_parameter_df = get_parameter(site,cnxn)
else:
    irr_center_component_df = get_cleantech_components(irr_center,cnxn)
    irr_center_parameter_df = get_parameter(irr_center,cnxn)

#----------------------------------------------------------------------------#
#Getting Date
test_start_date = (datetime.datetime.now(tz)+datetime.timedelta(days=-3)).date()
test_end_date = (datetime.datetime.now(tz)+datetime.timedelta(days=-2)).date()

test_start_date = (datetime.datetime.strptime("2020-04-01","%Y-%m-%d")).date()
test_end_date =(datetime.datetime.strptime("2020-04-02","%Y-%m-%d")).date()

print('Start Date:'+str(test_start_date),'\nEnd Date:'+str(test_end_date))

#SEND DIGEST
if(digest_flag==1):
    digest(site,test_start_date.strftime("%Y-%m-%d"))

#ONLY MFM
if(only_mfm_flag==1):
    component_df = component_df.loc[component_df['ComponentType']=='MFM',:]
    cleantech_component_id = component_df['ComponentId'].tolist()


#Main Processing
append_df = pd.DataFrame()
save_file_df = pd.DataFrame()
auth = locus_authenticate(site)
for index,i in enumerate(cleantech_component_id):
    print(component_df.loc[component_df['ComponentId']==i,'ComponentName'].values[0])
    locus_parameter = raw_parameter_df.loc[raw_parameter_df['ComponentId']==i,:]
    locus_parameter = list(parameter_name_mapping.keys()) + ['ts']
    cleantech_parameter = raw_parameter_df.loc[raw_parameter_df['ComponentId']==i,'CSName'].tolist()
    locus_id = locus_metadata_df.loc[locus_metadata_df['ComponentId']==i,'LocusComponentId'].values[0]
    locus_data_available = locus_metadata_df.loc[locus_metadata_df['ComponentId']==i,'DataAvailable'].values[0]
    timezone = get_tz(site[0:2])
    data=get_locus_data(auth,test_start_date,test_end_date,locus_id,locus_data_available,timezone)
    component_type = component_df.loc[component_df['ComponentId']==i,'ComponentType'].values[0]
    azure_data = data.loc[:,data.columns.isin(locus_parameter)]
    azure_data.columns = azure_data.columns.to_series().map(parameter_name_mapping)	
    col = azure_data.pop('Timestamp')
    azure_data.insert(0, 'Timestamp', col)
    azure_data.loc[:, 'Timestamp'] = azure_data['Timestamp'].str.replace('T',' ')
    azure_data.loc[:, 'Timestamp']  = azure_data['Timestamp'].str[0:19]



    #If both Irr_GHI and Irr present hen swap
    if(component_type=='MFM'):
        wms_raw_data = get_irr_raw(test_start_date.strftime("%Y-%m-%d"), site_id, irr_center,cnxn)
    else:
        wms_raw_data = pd.DataFrame()
    azure_data = data_mismatch(site,azure_data,component_type)
    process_data(test_start_date,azure_data,i,site_id,cnxn)
    azure_data['ComponentId']=i
    data['ComponentId']=i
    #-----------------------Main Upsert------------------------#
    col = azure_data.pop("ComponentId")
    if(int(site[3])>=4): #For shell need SiteId
        azure_data['SiteId']=site_id
        col2 = azure_data.pop("SiteId")
        azure_data.insert(1, col.name, col)
        azure_data.insert(2, col2.name, col2)
    else:
        azure_data.insert(1, col.name, col)
    azure_data = azure_data.replace({np.nan: None})
    fast_upsert_final(site,azure_data,cnxn)
    #-----------------------Main Upsert------------------------#
    if(index==0):  
        append_df = azure_data
        save_file_df = data
    else:
        append_df = append_df.append(azure_data,sort=False) 
        save_file_df = save_file_df.append(data,sort=False)
save_file_df = save_file_df.merge(component_df,how='left',on='ComponentId')
digest(site,test_start_date.strftime("%Y-%m-%d"))
cnxn.close()







