import pytz
import os
import numpy as np
from database_operations import *
from locus_api import *
from process_functions import *
from exception import *
import process_functions

def fast_upsert_processed(site,df):
    cols=df.columns.tolist()
    str_site=site.replace('-','')
    query_1='DECLARE @Date_'+str_site+' datetime,'+'@value_'+str_site+' float,'+'@parameterid_'+str_site+' int,'
    query_vals='SET @Date_'+str_site+' =?\n'+'SET @value_'+str_site+ '=?\n'+'SET @parameterid_'+str_site+ '=?\n'
    query_2='UPDATE [Portfolio].[ProcessedData] SET [Date]=@Date_'+str_site+',[Value]=@value_'+str_site+','+'[ParameterId]=@parameterid_'+str_site+','
    query_3_1='INSERT INTO [Portfolio].[ProcessedData]([Date],[Value],[ParameterId],'
    query_3_2='VALUES(@Date_'+str_site+',@value_'+str_site+','+'@parameterid_'+str_site+','
    for i in cols[3:]:
        i = i.replace(' ','_')
        try:
            if(math.isnan(i)):
                continue
        except:
            pass
        query_vals=query_vals+'SET @'+i.replace('-','_')+'_'+str_site+'=?\n'
        if(i=='SiteId'):
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' int,'
        else:
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' float,'
        query_2=query_2+'['+i.replace('-','_')+']=@'+i.replace('-','_')+'_'+str_site+','
        query_3_1=query_3_1+'['+i.replace('-','_')+'],'
        query_3_2=query_3_2+'@'+i.replace('-','_')+'_'+str_site+','
    query_3=query_3_1[:-1]+')'+query_3_2[:-1]+')'
    final_query=query_1[:-1]+'\n'+query_vals[:-1]+'\n'+query_2[:-1]+' WHERE [Date] = @Date_'+str_site+' AND [ComponentId]=@Componentid_'+str_site+' AND [ParameterId]=@parameterid_'+str_site+'\nif @@ROWCOUNT = 0\n'+query_3
    cursor = cnxn.cursor()
    cursor.fast_executemany = True
    cursor.executemany(final_query, df.values.tolist())
    cnxn.commit()

def calc_ga_2(mfm_df,wms_df):
    mfm_df['Timestamp'] = pd.to_datetime(mfm_df['Timestamp'])
    wms_df['Timestamp'] = pd.to_datetime(wms_df['Timestamp'])
    wms_df=wms_df[wms_df['Irradiance'].notnull()]
    irr_timestamps=wms_df.loc[wms_df['Irradiance']>20,'Timestamp']
    if(len(mfm_df['AC_Power'].dropna())==0):
        return None
    if(len(irr_timestamps)>0):
        freq_timestamps = mfm_df.loc[mfm_df['Frequency']>40,'Timestamp']
        ga_common = list(set(freq_timestamps) & set(irr_timestamps))            
        GA = (float(len(ga_common))/float(len(irr_timestamps)))*100
        return GA   
    elif(len(mfm_df['Timestamp'])>0):
        mask = (mfm_df['Timestamp'].dt.hour > 6) & (mfm_df['Timestamp'].dt.hour < 18)
        mfm_df = mfm_df[mask]
        freq_timestamps = mfm_df.loc[mfm_df['Frequency']>40,'Timestamp']
        if(len(mfm_df['Timestamp'])>0):
            GA = (float(len(freq_timestamps))/float(len(mfm_df['Timestamp'])))*100
            return GA
        else:
            return None
    else:
        return None

site='IN-052'

""" cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
df_component = get_cleantech_components(site,cnxn)
c_id = df_component.loc[(df_component['ComponentType']=='MFM')|(df_component['ComponentType']=='WMSIRR'),'ComponentId'].tolist()
c_id = map(str, c_id)
df = get_raw_data_2('2018-01-01', '2021-05-20', site, c_id,['Timestamp','ComponentId','Irradiance','AC_Power','Frequency'],cnxn)
print(df)
df.to_csv('/home/pranav/IN-052.txt',sep='\t',index=False)
cnxn.close() """
cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

df_final = pd.DataFrame()
mfm_ids = [62,63]
df = pd.read_csv('/home/pranav/IN-052.txt',sep='\t')
df['Timestamp'] = pd.to_datetime(df['Timestamp'])
for i,j in df.groupby(df['Timestamp'].dt.date):
    wms_df = j[j['ComponentId']==76]
    for n in mfm_ids:
        mfm_df = j[j['ComponentId']==n]
        ga = calc_ga_2(mfm_df,wms_df)
        df_temp = pd.DataFrame({'Date':[i],'Value':[ga],'ParameterId':[13],'ComponentId':[n],'SiteId':[1]})
        df_final = df_final.append(df_temp)
print(df_final)

        
df_final = df_final.replace({np.nan: None})
fast_upsert_processed(site,df_final)
cnxn.close()