import pytz
import os
import numpy as np
from database_operations import *
from locus_api import *
from process_functions import *
import process_functions
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import smtplib
from ftplib import FTP
import pytz
import urllib.request
import gzip
import io
import time
from socket import timeout
from html_mail import *
import html_mail

def chk_date(date):
    try:
        date=datetime.datetime.strptime(date[0:6]+'20'+date[6:], '%d/%m/%Y-%H:%M:%S')
        return 1
    except:
        return 0

def filter_files(names,file_name):
    return [k for k in names if file_name in k]

def process_data(date,df,site_id):
    date = date.strftime('%Y-%m-%d')
    print(df.columns)
    component_id = df['ComponentId'].unique()
    for c in component_id:
        component_parameters_df = processed_parameter_df.loc[processed_parameter_df['ComponentId']==c,:]
        temp_comp_df = df.loc[df['ComponentId']==c,:].dropna(axis=1, how='all')
        print(df['ComponentId'].dtype)
        if('Irradiance' in df.columns.tolist()):
            wms_raw_data = df.loc[df['ComponentId']==main_irr_component,['Timestamp','Irradiance']]
        elif('Irradiance_GTI' in df.columns.tolist()):
            wms_raw_data = df[df['ComponentId']==main_irr_component,['Timestamp','Irradiance_GTI']].unique()
        else:
            wms_raw_data = pd.DataFrame()
        component_type = component_df.loc[component_df['ComponentId']==c,'ComponentType'].values[0]
        if(component_type.strip()=='MFM'):
            capacity = mfm_capacity_df.loc[mfm_capacity_df['ComponentId']==c,'Size'].values[0]
        elif(component_type.strip()=='INV'):
            capacity = inv_capacity_df.loc[inv_capacity_df['ComponentId']==c,'Size'].values[0]
        elif(component_type.strip()=='WMSIRR'):
            capacity = 0 
        elif(component_type.strip()=='WMS'):
            capacity = 0    
        for index,row in component_parameters_df.iterrows():
            params=getattr(process_functions, 'function_mapping')[row['CSName']] #To use 1s,2nd or 3rd Irr sensor to calculate PR with other sensors.
            if(len(params)>1):
                result=getattr(process_functions, function_mapping[row['CSName']][0])(df=temp_comp_df,date=date,component_type=component_type,capacity=capacity,component_df=component_df,irr_center_component_df=irr_center_component_df,site_id=site_id,mfm_capacity_df=mfm_capacity_df,wms_raw_data = wms_raw_data,custom_parameter = params[1],gran=1,irr_center_parameter_df=irr_center_parameter_df)
            else:
                result=getattr(process_functions, function_mapping[row['CSName']][0])(df=temp_comp_df,date=date,component_type=component_type,capacity=capacity,component_df=component_df,irr_center_component_df=irr_center_component_df,site_id=site_id,mfm_capacity_df=mfm_capacity_df,wms_raw_data = wms_raw_data,custom_parameter = 0,gran=1,irr_center_parameter_df=irr_center_parameter_df)
            upsert_processed(date,result,int(row['ParameterId']),int(c),int(site_id))

startdate = '2021-03-2516:37:00'
site='IN-004'
irr_center='Self'
tz = pytz.timezone('Asia/Kolkata')
digest_flag=0

site_id = get_site_id(site) 
main_irr_component = get_main_irr_component(site_id)
component_df = get_cleantech_components(site)
webdyn_metadata_df = get_webdyn_metadata(site)
component_df = pd.merge(component_df,webdyn_metadata_df,on='ComponentId',how='left')

component_df['FileMetadata'] = component_df['WebdynComponentId'].str.split("_").str[:-1].str.join('_')
component_df['WebdynComponentId'] = component_df['WebdynComponentId'].str.split("_").str[-1]
grouped_component_df = component_df.groupby('FileMetadata')
print(component_df)

mfm_capacity_df = get_mfm_capacities(site) 
inv_capacity_df = get_inv_capacities(site) 

parameter_df = get_parameter(site)
raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
parameter_name_mapping = dict(zip(raw_parameter_df['CSName'],raw_parameter_df['CSName']))
parameter_name_mapping['Timestamp']='Timestamp'
parameter_name_mapping['ComponentId']='ComponentId' #This is mapped to LocusId initially
processed_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Processed',:]

if(irr_center == 'Self'):
    irr_center_component_df = component_df
    irr_center_parameter_df = get_parameter(site)
else:
    irr_center_component_df = get_cleantech_components(irr_center)
    irr_center_parameter_df = get_parameter(irr_center)


temp_cols = [n+1 for n in range(0,60)]

for g_index,group in grouped_component_df:
    ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
    ftp.login(user='IN-004W', passwd = 'FV9S857C8eq4PAwB')
    if 'INV' in g_index:
        c_type='INV'
        ftp.cwd('DATA/INV')
        print('Doing inv')
    else:
        c_type='MODBUS'
        ftp.cwd('DATA/MODBUS')
        print('Doing MFMs')
    files=ftp.nlst()
    start=startdate
    start=start.replace('\n','')
    start=datetime.datetime.strptime(start, "%Y-%m-%d%H:%M:%S")
    end=datetime.datetime.now(tz).replace(tzinfo=None)-datetime.timedelta(hours=1,minutes=1)  
    print(end)
    while(start<end):
        date_tracker = start
        files_temp=filter_files(files,g_index)
        start_str=start.strftime("%Y%m%d_%H%M")
        start_str='_'+start_str[2:]
        for i in sorted(files_temp):
            if start_str in i:
                print(start_str)
                req = urllib.request.Request('ftp://IN-004W:FV9S857C8eq4PAwB@ftpnew.cleantechsolar.com/DATA/'+c_type+'/'+i)
                with urllib.request.urlopen(req) as response:
                    s =  gzip.decompress(response.read())
                df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep=';',names=temp_cols)
                for index, row in df.iterrows():
                    if(row[1]=='ADDRMODBUS' or row[1]=='SNINV'):
                        webdyn_id=str(int(row[3]))
                    if(chk_date(row[1])):
                        try:
                            raw_cols = component_df.loc[((component_df['WebdynComponentId']==webdyn_id) & (component_df['FileMetadata']==g_index)),'DataAvailable'].values[0].split(',')
                            c_id = component_df.loc[((component_df['WebdynComponentId']==webdyn_id) & (component_df['FileMetadata']==g_index)),'ComponentId'].values[0]
                        except:
                            raw_cols = component_df.loc[((component_df['WebdynComponentId']=='2') & (component_df['FileMetadata']==g_index)),'DataAvailable'].values[0].split(',')
                            c_id = component_df.loc[((component_df['WebdynComponentId']=='2') & (component_df['FileMetadata']==g_index)),'ComponentId'].values[0]
                        data=row.to_frame().T.iloc[:,:len(raw_cols)]
                        date=datetime.datetime.strptime(row[1][0:6]+'20'+row[1][6:], '%d/%m/%Y-%H:%M:%S')
                        if(date.year<2015):
                            continue
                        data[1]=date.replace(second=0)
                        
                        data.columns = raw_cols
                        azure_data = data.loc[:,data.columns.isin(list(parameter_name_mapping.values()))]
                        azure_data.insert(1, "ComponentId", c_id, True) 
                        azure_data = azure_data.replace({np.nan: None}) 
                        fast_upsert_final(site,azure_data)   
        start=start+datetime.timedelta(minutes=1)   
        print(start,date_tracker)
        if(start.date()>date_tracker.date()):
            prev_day = start+datetime.timedelta(days=-1)
            raw_data = get_raw_data(prev_day,site)
            process_data(prev_day,raw_data,site_id)

ftp.close()
print('Historical Done!')

live_metadata = [[0,0],[]]
while(1):
    try:
        time_now=datetime.datetime.now(tz)
        cnt=0
        new_sorted=[]
        append_df = pd.DataFrame()
        for g_index,group in grouped_component_df:
            ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
            ftp.login(user='IN-004W', passwd = 'FV9S857C8eq4PAwB')
            if 'INV' in g_index:
                c_type='INV'
                ftp.cwd('DATA/INV')
            else:
                c_type='MODBUS'
                ftp.cwd('DATA/MODBUS')
            files=ftp.nlst()     
            if(live_metadata[0][cnt]==0): #First time sort in reverse and pick the first file.
                start=time_now
                files_temp=filter_files(files,g_index)
                live_metadata[1].append(sorted(files_temp,reverse=True))
                new_file=[live_metadata[1][cnt][0]]
                live_metadata[0][cnt]=1
            else:
                files_temp=filter_files(files,g_index)  #Get only latest incoming file.
                new_sorted.append(sorted(files_temp,reverse=True))
                new_file=list(set(new_sorted[cnt]) - set(live_metadata[1][cnt]))
                live_metadata[1][cnt]=new_sorted[cnt]
            for i in sorted(new_file):
                url= 'ftp://IN-004W:FV9S857C8eq4PAwB@ftpnew.cleantechsolar.com/DATA/'+c_type+'/'+i
                try:
                    response = urllib.request.urlopen(url, timeout=120).read()
                except timeout:
                    print('Timeout')

                s =  gzip.decompress(response)
                df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep=';',names=temp_cols)
                for index, row in df.iterrows():
                    if(row[1]=='ADDRMODBUS' or row[1]=='SNINV'):
                        webdyn_id=str(int(row[3]))
                    if(chk_date(row[1])):
                        raw_cols = component_df.loc[((component_df['WebdynComponentId']==webdyn_id) & (component_df['FileMetadata']==g_index)),'DataAvailable'].values[0].split(',')
                        c_id = component_df.loc[((component_df['WebdynComponentId']==webdyn_id) & (component_df['FileMetadata']==g_index)),'ComponentId'].values[0]
                        data=row.to_frame().T.iloc[:,:len(raw_cols)]
                        date=datetime.datetime.strptime(row[1][0:6]+'20'+row[1][6:], '%d/%m/%Y-%H:%M:%S')
                        if(date.year<2015):
                            continue
                        data[1]=date.replace(second=0)
                        data.columns = raw_cols
                        azure_data = data.loc[:,data.columns.isin(list(parameter_name_mapping.values()))]
                        azure_data.insert(1, "ComponentId", c_id, True) 
                        azure_data = azure_data.replace({np.nan: None}) 
                        append_df = append_df.append(azure_data)
            cnt=cnt+1
        append_df = append_df.replace({np.nan: None})
        if(append_df.empty):
            pass
        else:
            fast_upsert_final(site,append_df) 
        ftp.close()
        if(time_now.minute%10==0):
            raw_data = get_raw_data(time_now,site)
            process_data(time_now,raw_data,site_id)
        else:
            pass
        print('Sleeping')
        if(digest_flag==1):
            print(mfm_capacity_df)
            digest(site,time_now.strftime("%Y-%m-%d"),mfm_capacity_df)
        else:
            pass
    except:
        print('failed')
    time.sleep(60) 
