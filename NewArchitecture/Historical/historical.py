import pytz
import os
import numpy as np
from database_operations import *
from locus_api import *
from process_functions import *
from exception import *
import process_functions
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import smtplib

def monthdelta(date, delta):
    m, y = (date.month+delta) % 12, date.year + ((date.month)+delta-1) // 12
    if not m: m = 12
    d = min(date.day, [31,
        29 if y%4==0 and not y%400==0 else 28,31,30,31,30,31,31,30,31,30,31][m-1])
    return date.replace(day=d,month=m, year=y)

def process_data(date,df,component_id,site_id,cnxn):
    date = date.strftime('%Y-%m-%d')
    component_parameters_df = processed_parameter_df.loc[processed_parameter_df['ComponentId']==component_id,:]
    #component_parameters_df = processed_parameter_df.loc[((processed_parameter_df['ParameterId']==14) | (processed_parameter_df['ParameterId']==324) | (processed_parameter_df['ParameterId']==325) | (processed_parameter_df['ParameterId']==344) | (processed_parameter_df['ParameterId']==345)),:]
    component_type = component_df.loc[component_df['ComponentId']==component_id,'ComponentType'].values[0]
    if(component_type.strip()=='MFM'):
        capacity = mfm_capacity_df.loc[mfm_capacity_df['ComponentId']==component_id,'Size'].values[0]
    elif(component_type.strip()=='INV'):
        capacity = inv_capacity_df.loc[inv_capacity_df['ComponentId']==component_id,'Size'].values[0]
    elif(component_type.strip()=='WMSIRR'):
        capacity = 0 
    elif(component_type.strip()=='WMS'):
        capacity = 0    
    for index,row in component_parameters_df.iterrows():
        params=getattr(process_functions, 'function_mapping')[row['CSName']] #To use 1s,2nd or 3rd Irr sensor to calculate PR with other sensors.
        if(len(params)>1):
            result=getattr(process_functions, function_mapping[row['CSName']][0])(df=df,date=date,component_type=component_type,capacity=capacity,component_df=component_df,irr_center_component_df=irr_center_component_df,site=site,site_id=site_id,mfm_capacity_df=mfm_capacity_df,wms_raw_data = wms_raw_data,custom_parameter = params[1],gran=5,irr_center_parameter_df=irr_center_parameter_df,cnxn=cnxn)
        else:
            result=getattr(process_functions, function_mapping[row['CSName']][0])(df=df,date=date,component_type=component_type,capacity=capacity,component_df=component_df,irr_center_component_df=irr_center_component_df,site=site,site_id=site_id,mfm_capacity_df=mfm_capacity_df,wms_raw_data = wms_raw_data,custom_parameter = 0,gran=5,irr_center_parameter_df=irr_center_parameter_df,cnxn=cnxn)
        upsert_processed(date,result,row['ParameterId'],component_id,site_id,cnxn)

def send_mail(date, stn, info, recipients, attachment_path_list=None):
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  msg['Subject'] = "Alert " + str(date)
  #if sender is not None:
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  if attachment_path_list is not None:
    for each_file_path in attachment_path_list:
      try:
        file_name = each_file_path.split("/")[-1]
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(each_file_path, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
        msg.attach(part)
      except:
        print("could not attache file")
  msg.attach(MIMEText(info,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())
  
def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

def save_file(df,site):
    grouped_df = df.groupby('ComponentName')
    for name,group in sorted(grouped_df):
        group['ts']=group['ts'].str.replace('T',' ')
        group.ts=pd.to_datetime(group.ts.str[0:19])
        date_grouped_df=group.groupby(group['ts'].dt.date)
        for name2,group2 in sorted(date_grouped_df):
            chkdir(main_path+site+'/'+str(name2)[0:4]+'/'+str(name2)[0:7]+'/'+str(name))
            group2.to_csv(main_path+site+'/'+str(name2)[0:4]+'/'+str(name2)[0:7]+'/'+str(name)+'/'+str(name2)+'-'+str(name)+'.txt',sep='\t',index=False)

cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
auto_site_df = pd.read_csv('Generalinfo.csv', sep=',', error_bad_lines=False, index_col=False, dtype='unicode')
auto_comp_df = pd.read_csv('ComponentInfo.csv', sep=',', error_bad_lines=False, index_col=False, dtype='unicode')
only_mfm_flag = 0

#Get Locus Metadat
locus_data = auto_comp_df.groupby('O&M Reference')

historical_start_dates = ['2018-06-01'] 
sites  = ['IN-038']
#Main Variables
for index,site in enumerate(sites):
    historical_start_date = historical_start_dates[index]
    main_info_df = auto_site_df.loc[auto_site_df['O&M Reference']==site,:]
    irr_center = main_info_df['IrrCenter'].values[0].strip()
    if(irr_center == 'Self'):
        irr_site = site
    else:
        irr_site = irr_center
    locus_site_id = int(auto_site_df.loc[auto_site_df['O&M Reference']==site,'LocusId'].values[0])
    print(locus_site_id)
    locus_parameters = []
    azure_locus_parameters = []
    component_id=[]
    site_id = get_site_id(site, cnxn) 
    auth = locus_authenticate(site)
    tz = pytz.timezone("Asia/Calcutta")
    main_path = '/home/admin/public/RawData/'
    chkdir(main_path+site) 

    #Need WMS First for PR calculation so sorted in get_ceantec_components function
    component_df = get_cleantech_components(site, cnxn)
    print(component_df)
    cleantech_component_id = component_df['ComponentId'].tolist()
    locus_metadata_df = get_locus_metadata(site, cnxn)
    all_parameter_df = get_all_parameters(cnxn)

    wms_component_df = component_df.loc[component_df['ComponentType']=='WMS',:]
    inv_component_df = component_df.loc[component_df['ComponentType']=='INV',:]
    mfm_component_df = component_df.loc[component_df['ComponentType']=='MFM',:]
    last_inv = inv_component_df.tail(1)
    last_mfm = mfm_component_df.tail(1)

    for index,row in component_df.iterrows():
        #if silicon or pyranometer in name then use this else
        c_name = row['ComponentName']
        processed_parameters_dict = {"WMSIRR":["DA","GHI","Avg Tmod"],"WMS":["DA","Avg Tmod"],"MFM":['DA','EAC method-1','EAC method-2','Yield-1','Yield-2','PR-1 (GHI)','PR-2 (GHI)','Last recorded value'],"INV":['Yield-2'],'OTH':[]}
        for i in processed_parameters_dict[row['ComponentType']]:
            parameter_id = all_parameter_df.loc[all_parameter_df['MPName']==i,'ParameterId'].values[0]
            try:
                add_cleantech_metadata(parameter_id, row['ComponentId'], site_id)
            except:
                pass

    #Adding MFM, INV COV


    if(len(inv_component_df)>1):
        parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Stdev','ParameterId'].values[0]
        try:
            add_cleantech_metadata(parameter_id, last_inv['ComponentId'].values[0], site_id)
        except:
            pass
        parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='COV','ParameterId'].values[0]
        try:
            add_cleantech_metadata(parameter_id, last_inv['ComponentId'].values[0], site_id)
        except:
            pass


    if(len(mfm_component_df)>1):
        try:
            parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Stdev','ParameterId'].values[0]
            add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id)
        except:
            pass
        try:
            parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='COV','ParameterId'].values[0]
            add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id)
        except:
            pass

    try:
        parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Full Site PR (GHI)','ParameterId'].values[0]
        add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id)    
    except:
        pass

    parameter_df = get_parameter(site, cnxn)
    

    #Getting Capacities
    mfm_capacity_df = get_mfm_capacities(site, cnxn) 
    inv_capacity_df = get_inv_capacities(site, cnxn) 

    #Raw Parameters
    raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
    parameter_name_mapping = dict(zip(raw_parameter_df['MPName'],raw_parameter_df['CSName']))
    

    #If both GTI and GHI are recorded
    if('POAI_avg' in list(parameter_name_mapping.keys()) and 'GHI_avg' in list(parameter_name_mapping.keys()) ):
        print('Converting Irr Column')
        parameter_name_mapping['POAI_avg'] = 'Irradiance_GTI'

    parameter_name_mapping = column_mismatch(site,parameter_name_mapping)
    parameter_name_mapping['ts']='Timestamp'
    parameter_name_mapping['id']='ComponentId' #This is mapped to LocusId initially

    #Processed Parameters
    processed_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Processed',:]

    #Creatin Table
    create_table(site,raw_parameter_df['CSName'].unique().tolist(),cnxn)

    #Getting Irr center components
    if(irr_center == 'Self'):
        irr_center_component_df = component_df
        irr_center_parameter_df = get_parameter(site, cnxn)
    else:
        irr_center_component_df = get_cleantech_components(irr_center, cnxn)
        irr_center_parameter_df = get_parameter(irr_center, cnxn)


    #----------------------------------------------------------------------------#
    #Getting Date
    #historical_start_date = input('Enter the first date of communication:')
    historical_start_date=datetime.datetime.strptime(historical_start_date,'%Y-%m-%d')
    date_today = datetime.datetime.now()

    #ONLY MFM
    if(only_mfm_flag==1):
        component_df = component_df.loc[component_df['ComponentType']=='MFM',:]
        cleantech_component_id = component_df['ComponentId'].tolist()


    #Main Processing
    while(historical_start_date<datetime.datetime.now()):
        temp_end_date = monthdelta(historical_start_date,1)
        append_df = pd.DataFrame()
        save_file_df = pd.DataFrame()
        print(historical_start_date)
        auth = locus_authenticate(site)
        update_botmetadata(site_id,str(historical_start_date),'Raw', cnxn)
        for index,i in enumerate(cleantech_component_id):
            print(i)
            temp_day=historical_start_date
            locus_parameter = raw_parameter_df.loc[raw_parameter_df['ComponentId']==i,:]
            locus_parameter = locus_parameter['MPName'].tolist() + ['ts']
            cleantech_parameter = raw_parameter_df.loc[raw_parameter_df['ComponentId']==i,'CSName'].tolist()
            locus_id = locus_metadata_df.loc[locus_metadata_df['ComponentId']==i,'LocusComponentId'].values[0]
            locus_data_available = locus_metadata_df.loc    [locus_metadata_df['ComponentId']==i,'DataAvailable'].values[0]
            if(site[0:2]=='MY'):
                timezone = "Asia/Kuala_Lumpur"
            elif(site[0:2]=='TH'):
                timezone = "Asia/Bangkok"
            elif(site[0:2]=='VN'):
                timezone = "Asia/Saigon"
            elif(site[0:2]=='PH'):
                timezone = "Asia/Manila"
            elif(site[0:2]=='KH'):
                timezone = "Asia/Phnom_Penh"
            else:
                timezone = "Asia/Calcutta"
            data=get_locus_data(auth,historical_start_date,temp_end_date,locus_id,locus_data_available,timezone)
            component_type = component_df.loc[component_df['ComponentId']==i,'ComponentType'].values[0]
            print(component_type)
            azure_data = data.loc[:,data.columns.isin(locus_parameter)]
            azure_data.columns = azure_data.columns.to_series().map(parameter_name_mapping)	
            col = azure_data.pop('Timestamp')
            azure_data.insert(0, 'Timestamp', col)
            azure_data.loc[:, 'Timestamp'] = azure_data['Timestamp'].str.replace('T',' ')
            azure_data.loc[:, 'Timestamp']  = azure_data['Timestamp'].str[0:19]
            while(temp_day<temp_end_date):
                if(component_type=='MFM'):
                    wms_raw_data = get_irr_raw(temp_day, site_id, irr_site, cnxn)
                    print('Getting wms')
                else:
                    wms_raw_data = pd.DataFrame()
                azure_data = data_mismatch(site,azure_data,component_type)
                data_temp=azure_data[((azure_data['Timestamp']>=(temp_day.strftime("%Y-%m-%d"))) & (azure_data['Timestamp']<(temp_day+datetime.timedelta(days=1)).strftime("%Y-%m-%d")))]
                if(temp_day>date_today):
                    print('Not processing')
                    temp_day=temp_day+datetime.timedelta(days=1)
                    continue
                process_data(temp_day,data_temp,i,site_id,cnxn)
                temp_day=temp_day+datetime.timedelta(days=1)
            azure_data['ComponentId']=i
            data['ComponentId']=i

            #-----------------------Main Upsert------------------------#
            print('Started Upsert Raw')
            col = azure_data.pop("ComponentId")
            if(int(site[3])>=4): #For shell need SiteId
                azure_data['SiteId']=site_id
                col2 = azure_data.pop("SiteId")
                azure_data.insert(1, col.name, col)
                azure_data.insert(2, col2.name, col2)
            else:
                azure_data.insert(1, col.name, col)
            azure_data = azure_data.replace({np.nan: None})
            fast_upsert_final(site,azure_data,cnxn)
            print('Done')
            #-----------------------Main Upsert------------------------#

            if(index==0):  
                append_df = azure_data
                save_file_df = data
            else:
                append_df = append_df.append(azure_data,sort=False) 
                save_file_df = save_file_df.append(data,sort=False)
        save_file_df = save_file_df.merge(component_df,how='left',on='ComponentId')
        save_file(save_file_df,site)  
        historical_start_date=temp_end_date

    send_mail('2020-11-28', 'Alert', site+' Site Push Complete', ['sai.pranav@cleantechsolar.com'], attachment_path_list=None)

cnxn.close()







