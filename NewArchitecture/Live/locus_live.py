import os
import sys

import numpy as np
import pytz


import html_mail
import process_functions
from database_operations import *
from html_mail import *
from locus_api import *
from process_functions import *

from exception import *


def monthdelta(date, delta):
    m, y = (date.month+delta) % 12, date.year + ((date.month)+delta-1) // 12
    if not m: m = 12
    d = min(date.day, [31,
        29 if y%4==0 and not y%400==0 else 28,31,30,31,30,31,31,30,31,30,31][m-1])
    return date.replace(day=d,month=m, year=y)

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

def rounddown(n):
    rem=n%10
    return (n-rem)

def process_data(date,df,component_id,site_id,cnxn):
    date = date.strftime('%Y-%m-%d')
    component_parameters_df = processed_parameter_df.loc[processed_parameter_df['ComponentId']==component_id,:]
    component_type = component_df.loc[component_df['ComponentId']==component_id,'ComponentType'].values[0]
    if(component_type.strip()=='MFM'):
        capacity = mfm_capacity_df.loc[mfm_capacity_df['ComponentId']==component_id,'Size'].values[0]
    elif(component_type.strip()=='INV'):
        capacity = inv_capacity_df.loc[inv_capacity_df['ComponentId']==component_id,'Size'].values[0]
    elif(component_type.strip()=='WMSIRR'):
        capacity = 0 
    elif(component_type.strip()=='WMS'):
        capacity = 0    
    for index,row in component_parameters_df.iterrows():
        params=getattr(process_functions, 'function_mapping')[row['CSName']] #To use 1s,2nd or 3rd Irr sensor to calculate PR with other sensors.
        if(len(params)>1):
            result=getattr(process_functions, function_mapping[row['CSName']][0])(df=df,date=date,component_type=component_type,capacity=capacity,component_df=component_df,irr_center_component_df=irr_center_component_df,site=site,site_id=site_id,mfm_capacity_df=mfm_capacity_df,wms_raw_data = wms_raw_data,custom_parameter = params[1],gran=5,irr_center_parameter_df=irr_center_parameter_df,cnxn=cnxn)
        else:
            result=getattr(process_functions, function_mapping[row['CSName']][0])(df=df,date=date,component_type=component_type,capacity=capacity,component_df=component_df,irr_center_component_df=irr_center_component_df,site=site,site_id=site_id,mfm_capacity_df=mfm_capacity_df,wms_raw_data = wms_raw_data,custom_parameter = 0,gran=5,irr_center_parameter_df=irr_center_parameter_df,cnxn=cnxn)
        upsert_processed(date,result,row['ParameterId'],component_id,site_id,cnxn)

def save_file(df,site):
    grouped_df = df.groupby('ComponentName')
    for name,group in sorted(grouped_df):
        group['ts']=group['ts'].str.replace('T',' ')
        group.ts=pd.to_datetime(group.ts.str[0:19])
        date_grouped_df=group.groupby(group['ts'].dt.date)
        for name2,group2 in sorted(date_grouped_df):
            chkdir(main_path+site+'/'+str(name2)[0:4]+'/'+str(name2)[0:7]+'/'+str(name))
            group2.to_csv(main_path+site+'/'+str(name2)[0:4]+'/'+str(name2)[0:7]+'/'+str(name)+'/'+str(name2)+'-'+str(name)+'.txt',sep='\t',index=False)

def get_tz(site):
    if(site[0:2]=='MY'):
        timezone = "Asia/Kuala_Lumpur"
    elif(site[0:2]=='TH'):
        timezone = "Asia/Bangkok"
    elif(site[0:2]=='VN'):
        timezone = "Asia/Saigon"
    elif(site[0:2]=='PH'):
        timezone = "Asia/Manila"
    elif(site[0:2]=='KH'):
        timezone = "Asia/Phnom_Penh"
    else:
        timezone = "Asia/Calcutta"
    return timezone

cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
country_code = sys.argv[1]
site_df = get_sites('Locus', cnxn)
irr_info_df = get_irr_info(cnxn)
merged_site_df = pd.merge(site_df, irr_info_df, left_on='SiteId', right_on='SiteId', how='left')
merged_site_df = merged_site_df.fillna(0)
merged_site_df['IrradiationCenterSiteId'] = merged_site_df['IrradiationCenterSiteId'].astype('int')
merged_site_df['SensorPresent'] = (merged_site_df['IrradiationCenterSiteId'] == merged_site_df['SiteId']).astype(int)
merged_site_df = merged_site_df.sort_values(by=['SensorPresent'], ascending=False)
merged_site_df = merged_site_df[merged_site_df['SiteCode'].str[:]=='IN-052']
print(merged_site_df.to_string())
tz = pytz.timezone(get_tz(country_code))
main_path = '/home/admin/public/RawData/'
cnxn.close()

for index,row in merged_site_df.iterrows():
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    #Get startdate and check if more than one month
    site = row['SiteCode']
    print(site)
    locus_parameters = []
    azure_locus_parameters = []
    component_id=[]
    site_id = get_site_id(site, cnxn) 
    auth = locus_authenticate(site)
    chkdir(main_path+site) 
    irr_center = get_irr_center(site_id, cnxn)
    if(irr_center == 'Self'):
        irr_site = site
    else:
        irr_site = irr_center


    #Need WMS First for PR calculation so sorted in get_ceantec_components function
    component_df = get_cleantech_components(site, cnxn)
    cleantech_component_id = component_df['ComponentId'].tolist()
    locus_metadata_df = get_locus_metadata(site, cnxn)
    locus_site_id = locus_metadata_df['LocusSiteId'].values[0] 
    all_parameter_df = get_all_parameters(cnxn)
    parameter_df = get_parameter(site, cnxn)


    #Getting Capacities
    mfm_capacity_df = get_mfm_capacities(site, cnxn) 
    inv_capacity_df = get_inv_capacities(site, cnxn) 

    #Raw Parameters
    raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
    parameter_name_mapping = dict(zip(raw_parameter_df['MPName'],raw_parameter_df['CSName']))
    #If both GTI and GHI are recorded
    if('POAI_avg' in list(parameter_name_mapping.keys()) and 'GHI_avg' in list(parameter_name_mapping.keys())):
        parameter_name_mapping['POAI_avg'] = 'Irradiance_GTI'
    parameter_name_mapping = column_mismatch(site,parameter_name_mapping)
    parameter_name_mapping['ts']='Timestamp'
    parameter_name_mapping['id']='ComponentId' #This is mapped to LocusId initially

    #Processed Parameters
    processed_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Processed',:]


    #Getting Irr center components
    if(irr_center == 'Self'):
        irr_center_component_df = component_df
        irr_center_parameter_df = get_parameter(site, cnxn)
    else:
        irr_center_component_df = get_cleantech_components(irr_center, cnxn)
        irr_center_parameter_df = get_parameter(irr_center, cnxn)


    #----------------------------------------------------------------------------#
    #Getting Date
    start_date = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).date()
    end_date = datetime.datetime.now(tz).date()
    date_today = datetime.datetime.now(tz).replace(tzinfo=None)
    try:
        historical_start_date = get_bot_metadata(site, cnxn)
    except:
        historical_start_date = datetime.datetime(2021,3,1)
    print('Start date is',historical_start_date)
    if(site=='IN-043'):
        print('Already Done')
        continue
    while(historical_start_date.date()<date_today.date()):
            if((date_today-historical_start_date).days<30):
                temp_end_date = date_today.replace(microsecond=0)
            else:
                temp_end_date = monthdelta(historical_start_date,1)
            append_df = pd.DataFrame()
            save_file_df = pd.DataFrame()
            print(historical_start_date,temp_end_date)
            auth = locus_authenticate(site)
            for index,i in enumerate(cleantech_component_id):
                print(component_df.loc[component_df['ComponentId']==i,'ComponentName'].values[0])
                temp_day=historical_start_date
                locus_parameter = raw_parameter_df.loc[raw_parameter_df['ComponentId']==i,:]
                locus_parameter = list(parameter_name_mapping.keys())
                cleantech_parameter = raw_parameter_df.loc[raw_parameter_df['ComponentId']==i,'CSName'].tolist()
                locus_id = locus_metadata_df.loc[locus_metadata_df['ComponentId']==i,'LocusComponentId'].values[0]
                locus_data_available = locus_metadata_df.loc    [locus_metadata_df['ComponentId']==i,'DataAvailable'].values[0]
                timezone = get_tz(site[0:2])
                data=get_locus_data(auth,historical_start_date.date(),temp_end_date.date(),locus_id,locus_data_available,timezone)
                if(data.empty): #If auth fails
                    auth = locus_authenticate(site)
                    data=get_locus_data(auth,historical_start_date.date(),temp_end_date.date(),locus_id,locus_data_available,timezone)
                component_type = component_df.loc[component_df['ComponentId']==i,'ComponentType'].values[0]
                print(component_type)
                azure_data = data.loc[:,data.columns.isin(locus_parameter)]
                azure_data.columns = azure_data.columns.to_series().map(parameter_name_mapping)	
                col = azure_data.pop('Timestamp')
                azure_data.insert(0, 'Timestamp', col)
                azure_data.loc[:, 'Timestamp'] = azure_data['Timestamp'].str.replace('T',' ')
                azure_data.loc[:, 'Timestamp']  = azure_data['Timestamp'].str[0:19]
                azure_data = data_mismatch(site,azure_data,component_type)
                while(temp_day<temp_end_date):
                    print(temp_day)
                    if(component_type=='MFM'):
                        wms_raw_data = get_irr_raw(temp_day, site_id, irr_site, cnxn)
                        print('Getting wms')
                    else:
                        wms_raw_data = pd.DataFrame()
                    data_temp=azure_data[((azure_data['Timestamp']>=(temp_day.strftime("%Y-%m-%d"))) & (azure_data['Timestamp']<(temp_day+datetime.timedelta(days=1)).strftime("%Y-%m-%d")))]
                    if(temp_day>date_today):
                        print('Not processing')
                        temp_day=temp_day+datetime.timedelta(days=1)
                        continue
                    process_data(temp_day, data_temp, i, site_id, cnxn)
                    temp_day=temp_day+datetime.timedelta(days=1)
                azure_data['ComponentId']=i
                data['ComponentId']=i

                #-----------------------Main Upsert------------------------#
                col = azure_data.pop("ComponentId")
                if(int(site[3])>=4): #For shell need SiteId
                    azure_data['SiteId']=site_id
                    col2 = azure_data.pop("SiteId")
                    azure_data.insert(1, col.name, col)
                    azure_data.insert(2, col2.name, col2)
                else:
                    azure_data.insert(1, col.name, col)
                azure_data = azure_data.replace({np.nan: None})
                fast_upsert_final(site,azure_data,cnxn)
                #-----------------------Main Upsert------------------------#

                if(index==0):  
                    append_df = azure_data
                    save_file_df = data
                else:
                    append_df = append_df.append(azure_data,sort=False) 
                    save_file_df = save_file_df.append(data,sort=False)
            save_file_df = save_file_df.merge(component_df,how='left',on='ComponentId')
            save_file(save_file_df,site)  
            update_botmetadata(site_id,str(temp_end_date.date()),'Raw',cnxn) 
            historical_start_date=temp_end_date
    cnxn.close()


while(1):
    
    print(datetime.datetime.now(tz))
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    print('Running Live!')
    for index,row in merged_site_df.iterrows():
        try:
            site = row['SiteCode']
            print(site)
            locus_parameters = []
            azure_locus_parameters = []
            component_id=[]
            site_id = get_site_id(site, cnxn) 
            auth = locus_authenticate(site)
            chkdir(main_path+site) 
            irr_center = get_irr_center(site_id, cnxn)
            if(irr_center == 'Self'):
                irr_site = site
            else:
                irr_site = irr_center

            #Need WMS First for PR calculation so sorted in get_ceantec_components function
            component_df = get_cleantech_components(site, cnxn)
            cleantech_component_id = component_df['ComponentId'].tolist()
            print(site)
            locus_metadata_df = get_locus_metadata(site, cnxn)
            locus_site_id = locus_metadata_df['LocusSiteId'].values[0] 
            all_parameter_df = get_all_parameters(cnxn)
            parameter_df = get_parameter(site, cnxn)


            #Getting Capacities
            mfm_capacity_df = get_mfm_capacities(site, cnxn)
            inv_capacity_df = get_inv_capacities(site, cnxn) 

            #Raw Parameters
            raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
            parameter_name_mapping = dict(zip(raw_parameter_df['MPName'],raw_parameter_df['CSName']))
            if('POAI_avg' in list(parameter_name_mapping.keys()) and 'GHI_avg' in list(parameter_name_mapping.keys())):
                parameter_name_mapping['POAI_avg'] = 'Irradiance_GTI'
            parameter_name_mapping = column_mismatch(site,parameter_name_mapping)
            parameter_name_mapping['ts']='Timestamp'
            parameter_name_mapping['id']='ComponentId' #This is mapped to LocusId initially

            #Processed Parameters
            processed_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Processed',:]


            #Getting Irr center components
            if(irr_center == 'Self'):
                irr_center_component_df = component_df
                irr_center_parameter_df = get_parameter(site, cnxn)
            else:
                irr_center_component_df = get_cleantech_components(irr_center, cnxn)
                irr_center_parameter_df = get_parameter(irr_center, cnxn)
            
            if(rounddown(datetime.datetime.now(tz).minute)%30!=0):
                component_df = component_df.loc[component_df['ComponentType']!='INV',:]
                cleantech_component_id = component_df['ComponentId'].tolist()
            #----------------------------------------------------------------------------#
            #Getting Date
            start_date = datetime.datetime.now(tz).date()
            end_date = (datetime.datetime.now(tz)+datetime.timedelta(days=1)).date()
            #Main Processing
            append_df = pd.DataFrame()
            save_file_df = pd.DataFrame()
            auth = locus_authenticate(site)
            for index,i in enumerate(cleantech_component_id):
                print(component_df.loc[component_df['ComponentId']==i,'ComponentName'].values[0])
                locus_parameter = raw_parameter_df.loc[raw_parameter_df['ComponentId']==i,:]
                locus_parameter = locus_parameter['MPName'].tolist() + ['ts']
                cleantech_parameter = raw_parameter_df.loc[raw_parameter_df['ComponentId']==i,'CSName'].tolist()
                locus_id = locus_metadata_df.loc[locus_metadata_df['ComponentId']==i,'LocusComponentId'].values[0]
                locus_data_available = locus_metadata_df.loc    [locus_metadata_df['ComponentId']==i,'DataAvailable'].values[0]
                timezone = get_tz(site[0:2])
                data=get_locus_data(auth,start_date,end_date,locus_id,locus_data_available,timezone)
                if(data.empty):
                    auth = locus_authenticate(site)
                    data=get_locus_data(auth,historical_start_date,temp_end_date,locus_id,locus_data_available,timezone)
                component_type = component_df.loc[component_df['ComponentId']==i,'ComponentType'].values[0]
                azure_data = data.loc[:,data.columns.isin(locus_parameter)]
                azure_data.columns = azure_data.columns.to_series().map(parameter_name_mapping)	
                col = azure_data.pop('Timestamp')
                azure_data.insert(0, 'Timestamp', col)
                azure_data.loc[:, 'Timestamp'] = azure_data['Timestamp'].str.replace('T',' ')
                azure_data.loc[:, 'Timestamp']  = azure_data['Timestamp'].str[0:19]
                azure_data = data_mismatch(site,azure_data,component_type)
                if(component_type=='MFM'):
                    wms_raw_data = get_irr_raw(start_date.strftime("%Y-%m-%d"), site_id, irr_site, cnxn)
                    print('Getting wms')
                else:
                    wms_raw_data = pd.DataFrame()
                process_data(start_date,azure_data,i,site_id, cnxn)
                azure_data['ComponentId']=i
                data['ComponentId']=i
                #-----------------------Main Upsert------------------------#
                col = azure_data.pop("ComponentId")
                if(int(site[3])>=4): #For shell need SiteId
                    azure_data['SiteId']=site_id
                    col2 = azure_data.pop("SiteId")
                    azure_data.insert(1, col.name, col)
                    azure_data.insert(2, col2.name, col2)
                else:
                    azure_data.insert(1, col.name, col)
                azure_data = azure_data.replace({np.nan: None})
                fast_upsert_final(site,azure_data,cnxn)
                #-----------------------Main Upsert------------------------#
                if(index==0):  
                    append_df = azure_data
                    save_file_df = data
                else:
                    append_df = append_df.append(azure_data,sort=False) 
                    save_file_df = save_file_df.append(data,sort=False)
            update_botmetadata(site_id,str(end_date),'Raw',cnxn) 
            save_file_df = save_file_df.merge(component_df,how='left',on='ComponentId')
            save_file(save_file_df,site)  
        except:
            print('Failed')
    cnxn.close()
    print('Sleeping')
    time.sleep(7200)


