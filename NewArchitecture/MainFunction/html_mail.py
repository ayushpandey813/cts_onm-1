import smtplib
import numpy as np
import os
import datetime
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from database_operations import *

def attach_sensor_name(merged_df,wms_components_ghi,wms_components_gti):
    pr_df = merged_df[merged_df['CSName'].str.contains('PR-')]
    for index,row in merged_df.iterrows():
        if('PR-' in row['CSName']):
            if('PR-' in row['CSName'] and 'GHI' in row['CSName']):
                wms_components = wms_components_ghi
            elif('PR-' in row['CSName'] and 'GTI' in row['CSName']):
                wms_components = wms_components_gti
            if(len(row['CSName'])<15):
                merged_df.loc[index,'CSName'] = row['CSName']+' ('+wms_components[0]+')'
            elif('Sensor 2' in row['CSName']):
                merged_df.loc[index,'CSName'] = row['CSName'][0:11]+'('+wms_components[1]+')'
            elif('Sensor 3' in row['CSName']):
                merged_df.loc[index,'CSName'] = row['CSName'][0:11]+'('+wms_components[2]+')'
    return merged_df




    #Get Wms components and merge with Parameters
#If CSName has PR then go through GHI and PR and len()<10 then Sensor 2, Sensor3 
#then GTI and PR and len()<10 then Sensor 2, Sensor3 


#Sending Message
def send_mail(date, stn, info, provider, recipients, attachment_path_list=None):
    server = smtplib.SMTP("smtp.office365.com")
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    msg = MIMEMultipart()
    sender = 'operations@cleantechsolar.com'
    msg['Subject'] = "Station " + "[" +stn + provider+"]" + " Digest " + str(date)
    #if sender is not None:
    msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
            try:
                file_name = each_file_path.split("/")[-1]
                part = MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
            except:
                print("could not attache file")
    part2 = MIMEText(info, "html")
    msg.attach(part2)
    server.sendmail(sender, recipients, msg.as_string())
  

def site_info(date, site_name, location, stn, capacity, nometers, cod_date,info,module_brand,inverter_brand):
    info = info + '<tr></tr><tr></tr>' + add_html_info('','General Information','','header')
    info = add_html_info(info,'Site Name:',site_name,'item')
    info = add_html_info(info,'Location:',location,'item')
    info = add_html_info(info,'O&M Code:',stn,'item')
    info = add_html_info(info,'System Size [kWp]:',str(capacity),'item')
    info = add_html_info(info,'Number of Energy Meters:',str(nometers),'item')
    info = add_html_info(info,'Module Brand / Model / Nos:',str(module_brand),'item')
    info = add_html_info(info,'Inverter Brand / Model / Nos:',str(inverter_brand),'item')
    if cod_date == 'TBC':
        info = add_html_info(info,'Site COD:',cod_date,'item')
    else:
        info = add_html_info(info,'Site COD:',cod_date,'item')
        info = add_html_info(info,'System age [Days]:',str((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days),'item')
        info = add_html_info(info,'System age [Years]:',"%.2f"%(float(((datetime.datetime.strptime(date,"%Y-%m-%d")-datetime.datetime.strptime(cod_date,"%Y-%m-%d")).days))/365) ,'item')
    return info

def add_html_info(info,header,value,body_type):
    if(body_type=='header'):
        temp_info = '<tr class="heading"><td>'+header+'</td><td>'+value+'</td></tr>'
    elif(body_type=='item'):
        temp_info = '<tr class="item"><td>'+header+'</td><td>'+value+'</td></tr>'          
    info = info+temp_info
    return info

def calc_full_params(df):
    df['Value_Temp'] = df['Value']*df['Size']
    full_eac = df.loc[(df['CSName']=='EAC method-2') & (df['ComponentType']=='MFM'),'Value'].sum()
    full_yld = df.loc[(df['CSName']=='Yield-2') & (df['ComponentType']=='MFM'),'Value_Temp'].sum()/df.loc[(df['CSName']=='Yield-2') & (df['ComponentType']=='MFM'),'Size'].sum()
    if(df['CSName'].str.match('Full Site PR \(GTI\)').any()):
        full_pr = df.loc[(df['CSName']=='Full Site PR (GTI)') & (df['ComponentType']=='MFM'),'Value'].values[0]
    else:
        full_pr = df.loc[(df['CSName']=='Full Site PR (GHI)') & (df['ComponentType']=='MFM'),'Value'].values[0]
    return [['Energy Generation [kWh]','Yield [kWh/kWp]','PR [%]'],[full_eac,full_yld,full_pr],[2,2,1]]

def add_graphs(site,end_date,cnxn):
    attachment_df = get_attachments(site,cnxn)
    graph_list = []
    for index,row in attachment_df.iterrows():
        print(" ".join([row['AttachmentCommand'], site,end_date]))
        os.system(" ".join([row['AttachmentCommand'], site,end_date]))
        for n in row['AttachmentName'].split(','):
            graph_list.append(n)
    path_list = []
    for g in graph_list:
        check = '/home/admin/Graphs/Graph_Output/' + site + '/[' + site + '] Graph ' + end_date + ' - ' + g + '.pdf'
        if(os.path.exists(check)):
            path_list.append(check)
        check = '/home/admin/Graphs/Graph_Extract/' + site + '/[' + site + '] Graph ' + end_date + ' - ' + g + '.txt'
        if(os.path.exists(check)):
            path_list.append(check)
    check = '/home/admin/Graphs/Graph_Extract/' + site + '/[' + site + '] - Master Extract Daily.txt'
    if(os.path.exists(check)):
        path_list.append(check)
    check = '/home/admin/Graphs/Graph_Extract/' + site + '/[' + site + '] - Master Extract Monthly.txt'
    if(os.path.exists(check)):
        path_list.append(check) 
    return(path_list)
  
def digest(om_code,digest_date):
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    digest_body = """<!doctype html>
     <html>
     <head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
    </head>

    <body>
    <div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
       
        <img src="http://cleantechsolar.com/content/uploads/Logo_black_600.png" style="height: 55px;width:260px;border:0;" height="55" width="260"> <br>

    """
    site=om_code
    date=digest_date
    component_df = get_cleantech_components(site, cnxn)
    component_df['ComponentType']=component_df['ComponentType'].str.strip()
    component_df['ComponentName']=component_df['ComponentName'].str.strip()
    component_df = component_df.sort_values('ComponentType', ascending=False)
    site_information = get_site_information(site, cnxn)
    site_name=site_information['SiteName'][0]
    location=site_information['Location'][0]
    module_brand=site_information['ModuleInfo'][0]
    inverter_brand=site_information['InverterInfo'][0]
    provider = get_provider(site, cnxn)[0]
    mfm_capacity_df = get_mfm_capacities(site, cnxn)
    nometers=len(mfm_capacity_df)
    if site_information['COD'][0] == 'TBC':
      cod_date = 'TBC'
    else:
      try:
        datetime.datetime.strptime(site_information['COD'][0], "%d-%b-%y")
        cod_date=datetime.datetime.strptime(site_information['COD'][0],"%d-%b-%y").strftime("%Y-%m-%d")
      except:
        datetime.datetime.strptime(site_information['COD'][0], "%Y-%m-%d")
        cod_date=datetime.datetime.strptime(site_information['COD'][0], "%Y-%m-%d").strftime("%Y-%m-%d")
    
    size = round(mfm_capacity_df['Size'].sum(),2)

    digest_body = site_info(date, site_name, location, site, size, nometers, cod_date,digest_body,module_brand,inverter_brand)


    site_id=get_site_id(site, cnxn)

    #Getting Template
    template_df = get_processed_template(date,site_id, cnxn)

    #Getting Processed Data
    data_df = get_processed_data(date,site_id, cnxn)
    data_df.fillna(value=np.nan, inplace=True)
    #merging template and processed data to capture all parameters in case of missing data
    merged_df = pd.merge(template_df, data_df,  how='left', left_on=['ComponentId','ParameterId'], right_on = ['ComponentId','ParameterId'], suffixes=('', '_dup'))
    
    merged_df = pd.merge(merged_df, mfm_capacity_df,  how='left', left_on=['ComponentId'], right_on = ['ComponentId'], suffixes=('', '_dup2'))
    
    #get WMS
    grouped_df = merged_df.groupby('ComponentType')
    ia_body = ''
    full_site_body = add_html_info('','Full Site','','header')
    mfm_cov_body = '<div>'
    inv_cov_body =  '<div>'
    wms_body =  '<div>'
    mfm_body =  '<div>'
    inv_body =  '<div>'
    
    #Rounding
    """     merged_df['Rounding'] = merged_df['Rounding'].astype('float')
    f = lambda x: '{{:0.{}f}}'.format(int(x['Rounding'])).format(x['Value'])
    merged_df = merged_df.assign(Value = merged_df.apply(f, 1))
    print(merged_df)
    merged_df['Value']  =  merged_df['Value'].astype('float')
    merged_df['Value'] = merged_df['Value'].apply(lambda x : "{:,}".format(x)) """

    #If no WMS add to WMS body else pass
    if merged_df['ComponentType'].str.contains('WMSIRR').any():
        wms_components_ghi = template_df.loc[(template_df['ComponentType']=='WMSIRR') & (template_df['CSName']=='GHI'),'ComponentName'].tolist()
        wms_components_gti = template_df.loc[(template_df['ComponentType']=='WMSIRR') & (template_df['CSName']=='GTI'),'ComponentName'].tolist()
        pass
    else:
        irr_site_id = get_site_id(get_irr_center(site_id, cnxn), cnxn)
        main_irr_id = get_main_irr_component(site_id, cnxn)
        irr_data_df = get_processed_data(date,irr_site_id, cnxn)
        irr_data_df  = irr_data_df[((irr_data_df['ComponentType']=='WMSIRR') & (irr_data_df['ComponentId']==main_irr_id))]
        irr_template_df = get_processed_template(date,irr_site_id, cnxn)
        wms_components_ghi = irr_template_df.loc[(irr_template_df['ComponentType']=='WMSIRR') & (irr_template_df['CSName']=='GHI'),'ComponentName'].tolist()
        wms_components_gti = irr_template_df.loc[(irr_template_df['ComponentType']=='WMSIRR') & (irr_template_df['CSName']=='GTI'),'ComponentName'].tolist()
        irr_data_df = pd.merge(irr_data_df,irr_template_df,  how='left', left_on=['ComponentId','ParameterId'], right_on = ['ComponentId','ParameterId'], suffixes=('', '_dup'))
        irr_data_df.fillna(value=np.nan, inplace=True)
        wms_body = wms_body + add_html_info('','WMS (From '+get_irr_center(site_id, cnxn)+')','','header') 
        for index,row in irr_data_df.iterrows():
            wms_body = wms_body + add_html_info('',str(row['CSName']) +' ['+str(row['Unit'])+']: ',str(round(row['Value'],int(row['Rounding']))),'item') 
    merged_df = attach_sensor_name(merged_df,wms_components_ghi,wms_components_gti)
    #have to group again to handle multi MFM,WMS
    for name,group in sorted(grouped_df,reverse=True):
        group = group.sort_values('DigestOrder')
        grouped_df_2 = group.groupby('ComponentName')
        no_inv = 0
        if(name=='INV'):
            no_inv = len(group['ComponentId'].drop_duplicates())
            inv_body = inv_body +add_html_info('','Inverters','','header')
        elif(name=='MFM'):
            no_mfm = len(group['ComponentId'].drop_duplicates())
        for comp_name,comp in grouped_df_2:
            if(name=='WMS' or name=='WMSIRR'):
                wms_body = wms_body + add_html_info('',comp_name,'','header') 
            elif(name=='MFM'):
                mfm_body = mfm_body + add_html_info('',comp_name,'','header') 
            for index,row in comp.iterrows():
                if(name=='WMS' or name=='WMSIRR'):
                    wms_body = wms_body + add_html_info('',str(row['CSName']) +' ['+str(row['Unit'])+']: ',str(round(row['Value'],int(row['Rounding']))),'item') 
                if(name=='MFM' and row['CSName']!='COV' and row['CSName']!='Stdev' and row['CSName']!='Full Site PR (GHI)' and row['CSName']!='Full Site PR (GTI)' and row['CSName']!='Last recorded time'):
                    mfm_body = mfm_body + add_html_info('',str(row['CSName']) +' ['+str(row['Unit'])+']: ',str(round(row['Value'],int(row['Rounding']))),'item') 
                elif(name=='MFM' and row['CSName']=='Last recorded time'):
                    if(np.isnan(row['Value'])):
                        mfm_body = mfm_body + add_html_info('',str(row['CSName']) +' : ',str(row['Value']),'item')                     
                    else:
                        mfm_body = mfm_body + add_html_info('',str(row['CSName']) +' : ',datetime.datetime.fromtimestamp(row['Value']).strftime('%Y-%m-%d %H:%M:%S'),'item')                     
                elif(name=='INV' and row['CSName']=='Yield-2'):
                    inv_body = inv_body + add_html_info('','Yield '+ str(row['ComponentName']).strip() +':',str(round(row['Value'],int(row['Rounding']))),'item') 
                elif(name=='INV' and row['CSName']=='Inverter Availability'):
                    ia_body = ia_body + add_html_info('','Inverter Availability '+ str(row['ComponentName']).strip() +':',str(round(row['Value'],int(row['Rounding']))),'item') 

    full_site_params = calc_full_params(merged_df)
    for full_index,i in enumerate(full_site_params[0]):
        full_site_body = full_site_body + add_html_info('',i+':',str(round(full_site_params[1][full_index],int(full_site_params[2][full_index]))),'item')
        
    if(no_inv>1):
        inv_std_df = merged_df.loc[(merged_df['ComponentType']=='INV') & (merged_df['CSName']=='Stdev'),:]
        inv_cov_df = merged_df.loc[(merged_df['ComponentType']=='INV') & (merged_df['CSName']=='COV'),:]
        inv_cov_body =  add_html_info('','INV Stdev / COV Yields :',str(round(inv_std_df['Value'].values[0],int(inv_std_df['Rounding'])))+' / '+str(round(inv_cov_df['Value'].values[0],int(inv_cov_df ['Rounding'])))+'%','item') 

    if(no_mfm>1):
        mfm_std_df = merged_df.loc[(merged_df['ComponentType']=='MFM') & (merged_df['CSName']=='Stdev'),:]
        mfm_cov_df = merged_df.loc[(merged_df['ComponentType']=='MFM') & (merged_df['CSName']=='COV'),:]
        mfm_cov_body =  add_html_info('','MFM Stdev / COV Yields[%] :',str(round(mfm_std_df['Value'].values[0],int(mfm_std_df['Rounding'])))+' / '+str(round(mfm_cov_df['Value'].values[0],int(mfm_cov_df ['Rounding']))),'item') 

    digest_body =  digest_body + full_site_body + mfm_cov_body + wms_body + mfm_body + inv_body + inv_cov_body +ia_body +'</table></div></body></html>'
    graph_paths = add_graphs(om_code,digest_date,cnxn)
    send_mail(date, site, digest_body, provider, ['sai.pranav@cleantechsolar.com'], attachment_path_list=graph_paths)
    cnxn.close()


