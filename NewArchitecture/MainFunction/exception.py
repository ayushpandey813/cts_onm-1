def column_mismatch(site,mapping):
    if(site=='MY-005' or site=='IN-019' or site=='IN-078'):
        mapping['TotWhImp_max'] = 'Export_Meter_Reading'
        mapping['TotWhExp_max'] = 'Import_Meter_Reading'
    elif(site=='IN-053'):
        mapping['TotWhNet_avg'] = 'Export_Meter_Reading'
        mapping['TotWhExp_max'] = 'Total_Net_Energy'
    elif(site=='IN-081'):
        del mapping['TotWhExp_max'] 
        mapping['TotWh_min'] = 'Export_Meter_Reading'
    elif(site=='IN-069'):
        mapping['OTI_avg'] = 'Irradiance_GTI'
    return mapping

def data_mismatch(site,df,c_type):
    if(site=='IN-019' and c_type=='MFM'):
        df['AC_Power'] = df['AC_Power']*-1 
    elif(site=='MY-005' and c_type=='INV'):
        if('AC_Energy' in df.columns.tolist()):
            df['AC_Energy'] = df['AC_Energy']*10
    elif(site=='IN-070' and c_type=='INV'):
        if('AC_Energy' in df.columns.tolist()):
            df = df[df['AC_Energy']>0]
    return df
     