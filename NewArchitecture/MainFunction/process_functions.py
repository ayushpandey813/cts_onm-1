import pandas as pd
import numpy as np
import math
import time
from database_operations import *

function_mapping={'EAC method-1':['calc_eac_method_1'],
'EAC method-2':['calc_eac_method_2'],
'DA':['calc_data_availability'],
'GHI':['calc_ghi'],
'GTI':['calc_gti'],
'Last recorded value':['calc_last_recorded_value'],
'PR-1 (GHI)':['calc_pr_1_ghi',0],
'PR-2 (GHI)':['calc_pr_2_ghi',0],
'PR-1 (GHI) (Sensor 2)':['calc_pr_1_ghi',1],
'PR-2 (GHI) (Sensor 2)':['calc_pr_2_ghi',1],
'PR-1 (GHI) (Sensor 3)':['calc_pr_1_ghi',2],
'PR-2 (GHI) (Sensor 3)':['calc_pr_2_ghi',2],
'PR-1 (GTI)':['calc_pr_1_gti',0],
'PR-2 (GTI)':['calc_pr_2_gti',0],
'PR-1 (GTI) (Sensor 2)':['calc_pr_1_gti',1],
'PR-2 (GTI) (Sensor 2)':['calc_pr_2_gti',1],
'PR-1 (GTI) (Sensor 3)':['calc_pr_1_gti',2],
'PR-2 (GTI) (Sensor 3)':['calc_pr_2_gti',2],
'Yield-1':['calc_yield_1'],
'Yield-2':['calc_yield_2'],
'Last recorded time':['calc_last_recorded_time'],
'Stdev':['calc_std'],
'COV':['calc_cov']
,'Grid Availability':['calc_ga'],
'Plant Availability':['calc_pa'],
'Inverter Availability':['calc_ia'],
'Avg Tmod':['calc_tmod'],
'Full Site PR (GHI)':['calc_full_pr_ghi'],
'Full Site PR (GTI)':['calc_full_pr_gti'],
'Avg Tamb':['calc_tamb'],
'Avg Wind Speed':['calc_ws'],
'Avg Wind Direction':['calc_wd'],
}

def check_eac(eac,capacity):
    if(eac<0):
        return 0
    elif(eac>10*capacity):
        return 0
    else:
        return eac
        
def calc_eac_method_1(**kwargs):
    if(kwargs['gran']==5):
        div = 12000
    elif(kwargs['gran']==1):
        div = 60000
    if 'AC_Power' in kwargs['df'].columns.tolist():
        return kwargs['df']['AC_Power'].sum()/div
    else: 
        return None

def calc_eac_method_2(**kwargs):
    if 'Export_Meter_Reading' in kwargs['df'].columns.tolist():
        if len(kwargs['df']['Export_Meter_Reading'].dropna())>0:
            df_temp = kwargs['df'].dropna(subset=['Export_Meter_Reading'])
            return check_eac((df_temp['Export_Meter_Reading'].tail(1).values[0] - df_temp['Export_Meter_Reading'].head(1).values[0])/1000,kwargs['capacity'])
    elif 'AC_Energy' in kwargs['df'].columns.tolist():
        return check_eac((kwargs['df']['AC_Energy'].sum())/1000,kwargs['capacity'])
    else: 
        return None

def calc_ghi(**kwargs):
    if(kwargs['gran']==5):
        div = 12000
    elif(kwargs['gran']==1):
        div = 60000
    #Corner case to handle where GHI_avg is GHI and POAI_avg is GTI
    if 'Irradiance' in kwargs['df'].columns.tolist():
        return kwargs['df']['Irradiance'].sum()/div
    else: 
        return None

def calc_gti(**kwargs):
    if(kwargs['gran']==5):
        div = 12000
    elif(kwargs['gran']==1):
        div = 60000
    if 'Irradiance_GTI' in kwargs['df'].columns.tolist():
        return kwargs['df']['Irradiance_GTI'].sum()/div
    else: 
        return None

def calc_data_availability(**kwargs):
    if(kwargs['gran']==5):
        if 'AC_Power' in kwargs['df'].columns.tolist():
            return  len(kwargs['df']['AC_Power'].dropna())/2.88
        elif 'Irradiance' in kwargs['df'].columns.tolist():
            return  len(kwargs['df']['Irradiance'].dropna())/2.88
        elif 'Irradiance_GTI' in kwargs['df'].columns.tolist():
            return  len(kwargs['df']['Irradiance_GTI'].dropna())/2.88
        elif 'Module_Temperature' in kwargs['df'].columns.tolist() and 'Irradiance'  not in kwargs['df'].columns.tolist() and 'Irradiance_GTI'  not in kwargs['df'].columns.tolist():
            return  len(kwargs['df']['Module_Temperature'].dropna())/2.88
        elif 'Ambient_Temperature' in kwargs['df'].columns.tolist() and 'Irradiance'  not in kwargs['df'].columns.tolist() and 'Irradiance_GTI'  not in kwargs['df'].columns.tolist():
            return  len(kwargs['df']['Ambient_Temperature'].dropna())/2.88
    elif(kwargs['gran']==1):
        if 'AC_Power' in kwargs['df'].columns.tolist():
            return  len(kwargs['df']['AC_Power'].dropna())/14.40
        elif 'Irradiance' in kwargs['df'].columns.tolist():
            return  len(kwargs['df']['Irradiance'].dropna())/14.40
        elif 'Irradiance_GTI' in kwargs['df'].columns.tolist():
            return  len(kwargs['df']['Irradiance_GTI'].dropna())/14.40

def calc_last_recorded_value(**kwargs):
    if 'Export_Meter_Reading' in kwargs['df'].columns.tolist():
        if len(kwargs['df']['Export_Meter_Reading'].dropna())>0:
            return (kwargs['df']['Export_Meter_Reading'].dropna().tail(1).values[0])/1000
    else: 
        return None

def calc_last_recorded_time(**kwargs):
    if 'AC_Power' in kwargs['df'].columns.tolist():
        if len(kwargs['df']['AC_Power'].dropna())>0:
            try:
                kwargs['df']['Timestamp'] = pd.to_datetime(kwargs['df']['Timestamp'])
            except:
                pass
            return (kwargs['df'][['Timestamp','AC_Power']].dropna().tail(1).values[0][0].timestamp())
    else: 
        return None

def calc_yield_1(**kwargs):
    if(kwargs['gran']==5):
        div = 12000
    elif(kwargs['gran']==1):
        div = 60000
    if 'AC_Power' in kwargs['df'].columns.tolist():
        eac = kwargs['df']['AC_Power'].sum()/div
        return eac/kwargs['capacity']
    else: 
        return None


def calc_yield_2(**kwargs):
    if 'Export_Meter_Reading' in kwargs['df'].columns.tolist():
        if len(kwargs['df']['Export_Meter_Reading'].dropna())>0:
            df_temp = kwargs['df'].dropna(subset=['Export_Meter_Reading'])
            eac = check_eac((df_temp['Export_Meter_Reading'].tail(1).values[0] - df_temp['Export_Meter_Reading'].head(1).values[0])/1000,kwargs['capacity'])
            return eac/kwargs['capacity']
    elif 'Inverter_Reading' in kwargs['df'].columns.tolist():
        if len(kwargs['df']['Inverter_Reading'].dropna())>0:
            df_temp = kwargs['df'].dropna(subset=['Inverter_Reading'])
            eac = (df_temp['Inverter_Reading'].tail(1).values[0] - df_temp['Inverter_Reading'].head(1).values[0])/1000
            return eac/kwargs['capacity']
    elif 'AC_Energy' in kwargs['df'].columns.tolist():
        eac = check_eac((kwargs['df']['AC_Energy'].sum())/1000,kwargs['capacity'])
        return eac/kwargs['capacity']
    else: 
        return None


def calc_pr_1_ghi(**kwargs):
    if(kwargs['gran']==5):
        div = 12000
    elif(kwargs['gran']==1):
        div = 60000
    if 'AC_Power' in kwargs['df'].columns.tolist():
        if(kwargs['custom_parameter'] == 0):
            eac = kwargs['df']['AC_Power'].sum()/div
            ghi = get_irr(kwargs['date'], kwargs['irr_center_component_df'],'GHI',kwargs['custom_parameter'],kwargs['irr_center_parameter_df'],kwargs['cnxn'])
        elif(kwargs['custom_parameter'] == 1):
            eac = kwargs['df']['AC_Power'].sum()/div
            ghi = get_irr(kwargs['date'], kwargs['irr_center_component_df'],'GHI',kwargs['custom_parameter'],kwargs['irr_center_parameter_df'],kwargs['cnxn'])
        elif(kwargs['custom_parameter'] == 2):
            eac = kwargs['df']['AC_Power'].sum()/div
            ghi = get_irr(kwargs['date'], kwargs['irr_center_component_df'],'GHI',kwargs['custom_parameter'],kwargs['irr_center_parameter_df'],kwargs['cnxn'])    
        if(ghi==0 or ghi==None):
            return None
        yld = eac/kwargs['capacity']
        pr1 = yld/ghi
        if(pr1<0):
            pr1 = 0
        return pr1*100
    else: 
        return None


def calc_pr_2_ghi(**kwargs):
    if 'Export_Meter_Reading' in kwargs['df'].columns.tolist():
        if len(kwargs['df']['Export_Meter_Reading'].dropna())>0:
            df_temp = kwargs['df'].dropna(subset=['Export_Meter_Reading'])
            if(kwargs['custom_parameter'] == 0):
                eac = check_eac((df_temp['Export_Meter_Reading'].tail(1).values[0] - df_temp['Export_Meter_Reading'].head(1).values[0])/1000,kwargs['capacity'])
                ghi = get_irr(kwargs['date'], kwargs['irr_center_component_df'],'GHI',kwargs['custom_parameter'],kwargs['irr_center_parameter_df'],kwargs['cnxn'])
            elif(kwargs['custom_parameter'] == 1):
                eac = check_eac((df_temp['Export_Meter_Reading'].tail(1).values[0] - df_temp['Export_Meter_Reading'].head(1).values[0])/1000,kwargs['capacity'])
                ghi = get_irr(kwargs['date'], kwargs['irr_center_component_df'],'GHI',kwargs['custom_parameter'],kwargs['irr_center_parameter_df'],kwargs['cnxn'])
            elif(kwargs['custom_parameter'] == 2):
                eac = check_eac((df_temp['Export_Meter_Reading'].tail(1).values[0] - df_temp['Export_Meter_Reading'].head(1).values[0])/1000,kwargs['capacity'])
                ghi = get_irr(kwargs['date'], kwargs['irr_center_component_df'],'GHI',kwargs['custom_parameter'],kwargs['irr_center_parameter_df'],kwargs['cnxn'])  
            if(ghi==0 or ghi==None):
                return None
            yld = eac/kwargs['capacity']
            pr2 = yld/ghi
            if(pr2<0):
                pr2 = 0
            return pr2*100
    else: 
        return None


def calc_pr_1_gti(**kwargs):
    if(kwargs['gran']==5):
        div = 12000
    elif(kwargs['gran']==1):
        div = 60000
    if 'AC_Power' in kwargs['df'].columns.tolist():
        if(kwargs['custom_parameter'] == 0):
            eac = kwargs['df']['AC_Power'].sum()/div
            gti = get_irr(kwargs['date'], kwargs['irr_center_component_df'],'GTI',kwargs['custom_parameter'],kwargs['irr_center_parameter_df'],kwargs['cnxn'])
        elif(kwargs['custom_parameter'] == 1):
            eac = kwargs['df']['AC_Power'].sum()/div
            gti = get_irr(kwargs['date'], kwargs['irr_center_component_df'],'GTI',kwargs['custom_parameter'],kwargs['irr_center_parameter_df'],kwargs['cnxn'])
        elif(kwargs['custom_parameter'] == 2):
            eac = kwargs['df']['AC_Power'].sum()/div
            gti = get_irr(kwargs['date'], kwargs['irr_center_component_df'],'GTI',kwargs['custom_parameter'],kwargs['irr_center_parameter_df'],kwargs['cnxn'])  
        if(gti==0 or gti==None):
            return None
        yld = eac/kwargs['capacity']
        pr1 = yld/gti
        if(pr1<0):
            pr1 = 0
        return pr1*100
    else: 
        return None


def calc_pr_2_gti(**kwargs):
    if 'Export_Meter_Reading' in kwargs['df'].columns.tolist():
        if len(kwargs['df']['Export_Meter_Reading'].dropna())>0:
            df_temp = kwargs['df'].dropna(subset=['Export_Meter_Reading'])
            if(kwargs['custom_parameter'] == 0):
                eac = check_eac((df_temp['Export_Meter_Reading'].tail(1).values[0] - df_temp['Export_Meter_Reading'].head(1).values[0])/1000,kwargs['capacity'])
                gti = get_irr(kwargs['date'], kwargs['irr_center_component_df'],'GTI',kwargs['custom_parameter'],kwargs['irr_center_parameter_df'],kwargs['cnxn'])
            elif(kwargs['custom_parameter'] == 1):
                eac = check_eac((df_temp['Export_Meter_Reading'].tail(1).values[0] - df_temp['Export_Meter_Reading'].head(1).values[0])/1000,kwargs['capacity'])
                gti = get_irr(kwargs['date'], kwargs['irr_center_component_df'],'GTI',kwargs['custom_parameter'],kwargs['irr_center_parameter_df'],kwargs['cnxn'])
            elif(kwargs['custom_parameter'] == 2):
                eac = check_eac((df_temp['Export_Meter_Reading'].tail(1).values[0] - df_temp['Export_Meter_Reading'].head(1).values[0])/1000,kwargs['capacity'])
                gti = get_irr(kwargs['date'], kwargs['irr_center_component_df'],'GTI',kwargs['custom_parameter'],kwargs['irr_center_parameter_df'],kwargs['cnxn']) 
            if(gti==0 or gti==None):
                return None
            yld = eac/kwargs['capacity']
            pr2 = yld/gti
            if(pr2<0):
                pr2 = 0
            return pr2*100
    else: 
        return None


def calc_std(**kwargs):
    ylds = get_ylds(kwargs['date'],kwargs['component_df'],kwargs['site_id'],kwargs['component_type'],kwargs['cnxn'])
    if(len(ylds)==0): #All meter not communicating
        return None
    std = np.std(ylds)
    return std

def calc_cov(**kwargs):
    ylds = get_ylds(kwargs['date'],kwargs['component_df'],kwargs['site_id'],kwargs['component_type'],kwargs['cnxn'])
    std = np.std(ylds)
    if(len(ylds)==0): #All meter not communicating
        return None
    if(np.mean(ylds)==0):
        return None
    else:
        cov = (std*100)/np.mean(ylds)
        return cov

def calc_full_pr_ghi(**kwargs):
    pr_df = get_pr(kwargs['date'],kwargs['irr_center_component_df'],kwargs['site_id'],kwargs['mfm_capacity_df'],'GHI',kwargs['irr_center_parameter_df'],kwargs['cnxn'])
    prs = pr_df['Value'].values.tolist()
    capacities = pr_df['Size'].values.tolist()
    temp2 = 0
    temp_cap = []
    for index,i in enumerate(prs):
        if(i==np.nan or i==None or math.isnan(i) or i==0):
            continue
        temp2=temp2+capacities[index]*i
        temp_cap.append(capacities[index])
    if(len(temp_cap)==0):
        PR=None
    else:
        PR=temp2/sum(temp_cap)
        if(PR<0):
            PR=None
    print('Full PR is ',PR)
    return PR

def calc_full_pr_gti(**kwargs):
    pr_df = get_pr(kwargs['date'],kwargs['component_df'],kwargs['site_id'],kwargs['mfm_capacity_df'],'GTI',kwargs['irr_center_parameter_df'],kwargs['cnxn'])
    prs = pr_df['Value'].values.tolist()
    capacities = pr_df['Size'].values.tolist()
    temp2 = 0
    temp_cap = []
    for index,i in enumerate(prs):
        if(i==np.nan or i==None or math.isnan(i) or i==0):
            continue
        temp2=temp2+capacities[index]*i
        temp_cap.append(capacities[index])
    if(len(temp_cap)==0):
        PR=None
    else:
        PR=temp2/sum(temp_cap)
        if(PR<0):
            PR=None
    return PR


def calc_ga(**kwargs):
    if 'Frequency' in kwargs['df'].columns.tolist() and kwargs['wms_raw_data'] is not None: #is not none in case no WMS 
        mfm_df = kwargs['df']
        wms_df = kwargs['wms_raw_data']
        mfm_df['Timestamp'] = pd.to_datetime(mfm_df['Timestamp'])
        wms_df['Timestamp'] = pd.to_datetime(wms_df['Timestamp'])
        wms_df=wms_df[wms_df['Irradiance'].notnull()]
        irr_timestamps=wms_df.loc[wms_df['Irradiance']>20,'Timestamp']
        if(len(mfm_df['AC_Power'].dropna())==0):
            return None
        if(len(irr_timestamps)>0):
            if(kwargs['site'][3]=='3'):
                null_rows = mfm_df[mfm_df.isnull().any(axis=1)]
                mfm_df_temp = mfm_df.dropna()
                wms_df=wms_df[~wms_df['Timestamp'].isin(null_rows['Timestamp'])]
                freq_timestamps=mfm_df_temp.loc[mfm_df_temp['Frequency']>40,'Timestamp']
            else:
                freq_timestamps = mfm_df.loc[mfm_df['Frequency']>40,'Timestamp']
            ga_common = list(set(freq_timestamps) & set(irr_timestamps))            
            GA = (float(len(ga_common))/float(len(irr_timestamps)))*100
            return GA   
        elif(len(mfm_df['Timestamp'])>0):
            mask = (mfm_df['Timestamp'].dt.hour > 6) & (mfm_df['Timestamp'].dt.hour < 18)
            mfm_df = kwargs['df'][mask]
            freq_timestamps = mfm_df.loc[mfm_df['Frequency']>40,'Timestamp']
            if(len(mfm_df['Timestamp'])>0):
                GA = (float(len(freq_timestamps))/float(len(mfm_df['Timestamp'])))*100
                return GA
            else:
                return None
        else:
            return None

def calc_pa(**kwargs):
    if (('AC_Power' in kwargs['df'].columns.tolist()) and ('Frequency' in kwargs['df'].columns.tolist()) and kwargs['wms_raw_data'] is not None): #is not none in case no WMS 
        mfm_df = kwargs['df']
        mfm_df['Timestamp'] = pd.to_datetime(mfm_df['Timestamp'])
        wms_df = kwargs['wms_raw_data']
        mfm_df['Timestamp'] = pd.to_datetime(mfm_df['Timestamp'])
        wms_df['Timestamp'] = pd.to_datetime(wms_df['Timestamp'])
        wms_df=wms_df[wms_df['Irradiance'].notnull()]
        irr_timestamps=wms_df.loc[wms_df['Irradiance']>20,'Timestamp']
        freq_timestamps = mfm_df.loc[mfm_df['Frequency']>40,'Timestamp']
        if(len(irr_timestamps)>0 and len(freq_timestamps)>0):
            if(kwargs['site'][3]=='3'):
                null_rows = mfm_df[mfm_df.isnull().any(axis=1)]
                mfm_df_temp = mfm_df.dropna()
                wms_df=wms_df[~wms_df['Timestamp'].isin(null_rows['Timestamp'])]
                power_timestamps=mfm_df.loc[mfm_df['AC_Power']>2,'Timestamp']
            else:
                power_timestamps=mfm_df.loc[mfm_df['AC_Power']>2,'Timestamp']
            pa_common=list(set(freq_timestamps) & set(irr_timestamps)  & set(power_timestamps))
            ga_common = list(set(freq_timestamps) & set(irr_timestamps))
            if(len(ga_common)>0):
                PA = (float(len(pa_common))/float(len(ga_common)))*100
                return PA
            else:
                return None
        else:
            mask = (mfm_df['Timestamp'].dt.hour > 7) & (mfm_df['Timestamp'].dt.hour < 18)
            mfm_df = kwargs['df'][mask]
            power_timestamps=mfm_df.loc[mfm_df['AC_Power']>2,'Timestamp']
            freq_timestamps = mfm_df.loc[mfm_df['Frequency']>40,'Timestamp']
            pa_common=list(set(freq_timestamps) & set(power_timestamps))
            if(len(freq_timestamps)>0):
                PA = (float(len(pa_common))/float(len(freq_timestamps)))*100
                return PA
            else:
                return None

def calc_ia(**kwargs):
    if (('AC_Power' in kwargs['df'].columns.tolist()) and ('Frequency' in kwargs['df'].columns.tolist())):
        mfm_df = kwargs['df']
        mfm_df['Timestamp'] = pd.to_datetime(mfm_df['Timestamp'])
        mask = (mfm_df['Timestamp'].dt.hour > 7) & (mfm_df['Timestamp'].dt.hour < 18)
        mfm_df = kwargs['df'][mask]
        freq_timestamps=mfm_df.loc[mfm_df['Frequency']>40,'Timestamp']
        power_timestamps=mfm_df.loc[mfm_df['AC_Power']>2,'Timestamp']
        ia_common=list(set(freq_timestamps) & set(power_timestamps))
        if(len(freq_timestamps)>0):
            IA = (float(len(ia_common))/float(len(freq_timestamps)))*100
            return IA
        else:
            return None
    elif (('AC_Power' in kwargs['df'].columns.tolist()) and ('AC_Phase_A_Voltage' in kwargs['df'].columns.tolist())):
        mfm_df = kwargs['df']
        mfm_df['Timestamp'] = pd.to_datetime(mfm_df['Timestamp'])
        mask = (mfm_df['Timestamp'].dt.hour > 7) & (mfm_df['Timestamp'].dt.hour < 18)
        mfm_df = kwargs['df'][mask]
        freq_timestamps=mfm_df.loc[mfm_df['AC_Phase_A_Voltage']>150,'Timestamp']
        power_timestamps=mfm_df.loc[mfm_df['AC_Power']>2,'Timestamp']
        ia_common=list(set(freq_timestamps) & set(power_timestamps))
        if(len(freq_timestamps)>0):
            IA = (float(len(ia_common))/float(len(freq_timestamps)))*100
            return IA
        else:
            return None

def calc_tmod(**kwargs):
    if 'Module_Temperature' in kwargs['df'].columns.tolist():
        return kwargs['df']['Module_Temperature'].mean()

def calc_tamb(**kwargs):
    if 'Ambient_Temperature' in kwargs['df'].columns.tolist():
        return kwargs['df']['Ambient_Temperature'].mean()

def calc_ws(**kwargs):
    if 'Wind_Speed' in kwargs['df'].columns.tolist():
        return kwargs['df']['Wind_Speed'].mean()

def calc_wd(**kwargs):
    if 'WndDir_Avg' in kwargs['df'].columns.tolist():
        return kwargs['df']['WndDir_Avg'].mean()



