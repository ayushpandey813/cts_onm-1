import pytz
import time
import os
import sys
import sharepy
import json
import numpy as np
from database_operations import *
pd.set_option('mode.chained_assignment',None)

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

def get_source_id(site):
    query = pd.read_sql_query('''SELECT [Station_Id] FROM [dbo].[Stations] WHERE [Station_Name]='''+'\''+str(site)+'\'', cnxn)
    df = pd.DataFrame(query, columns=['Station_Id'])
    return df['Station_Id'].values[0]
    
def get_source_mfm(site):
    query = pd.read_sql_query("SELECT [Meter_id] FROM [dbo].[Meters] WHERE [Station_Id]=(SELECT [Station_Id] FROM [dbo].[Stations] WHERE [Station_Name] ="+"'"+site+"')", cnxn)
    df = pd.DataFrame(query, columns=['Meter_id'])
    return df['Meter_id'].values

def get_dest_id(site):
    query = pd.read_sql_query('''SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\'', cnxn)
    df = pd.DataFrame(query, columns=['SiteId'])
    return df['SiteId'].values[0]

def get_dest_mfm(site):
    query = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[Component] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE SiteCode ="+"'"+site+"')" +" AND ComponentType='MFM'", cnxn)
    df = pd.DataFrame(query, columns=['ComponentId'])
    return df['ComponentId'].values  

#Upsert Function
def upsert(Date, Value, ParameterId, ComponentId, SiteId, BypassFlag):
  cursor = cnxn.cursor()
  with cursor.execute("UPDATE [Portfolio].[ProcessedData] SET [Value]=?, [ParameterId]=?, [ComponentId]=?, [SiteId]=?, [BypassFlag]=? where [Date]=? AND [ComponentId]=? AND [ParameterId]=? if @@ROWCOUNT = 0 INSERT into [Portfolio].[ProcessedData]([Date], [Value], [ParameterId], [ComponentId], [SiteId], [BypassFlag]) values(?, ?, ?, ?, ?, ?)", [Value, ParameterId, ComponentId, SiteId, BypassFlag, Date, ComponentId, ParameterId, Date, Value, ParameterId, ComponentId, SiteId, BypassFlag]):
    pass
  cnxn.commit()

def fast_upsert_processed(site,df):
    cols=df.columns.tolist()
    str_site=site.replace('-','')
    query_1='DECLARE @Date_'+str_site+' datetime,'+'@value_'+str_site+' float,'+'@parameterid_'+str_site+' int,'
    query_vals='SET @Date_'+str_site+' =?\n'+'SET @value_'+str_site+ '=?\n'+'SET @parameterid_'+str_site+ '=?\n'
    query_2='UPDATE [Portfolio].[ProcessedData] SET [Date]=@Date_'+str_site+',[Value]=@value_'+str_site+','+'[ParameterId]=@parameterid_'+str_site+','
    query_3_1='INSERT INTO [Portfolio].[ProcessedData]([Date],[Value],[ParameterId],'
    query_3_2='VALUES(@Date_'+str_site+',@value_'+str_site+','+'@parameterid_'+str_site+','
    for i in cols[3:]:
        i = i.replace(' ','_')
        try:
            if(math.isnan(i)):
                continue
        except:
            pass
        query_vals=query_vals+'SET @'+i.replace('-','_')+'_'+str_site+'=?\n'
        if(i=='SiteId'):
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' int,'
        else:
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' float,'
        query_2=query_2+'['+i.replace('-','_')+']=@'+i.replace('-','_')+'_'+str_site+','
        query_3_1=query_3_1+'['+i.replace('-','_')+'],'
        query_3_2=query_3_2+'@'+i.replace('-','_')+'_'+str_site+','
    query_3=query_3_1[:-1]+')'+query_3_2[:-1]+')'
    final_query=query_1[:-1]+'\n'+query_vals[:-1]+'\n'+query_2[:-1]+' WHERE [Date] = @Date_'+str_site+' AND [ComponentId]=@Componentid_'+str_site+' AND [ParameterId]=@parameterid_'+str_site+'\nif @@ROWCOUNT = 0\n'+query_3
    cursor = cnxn.cursor()
    cursor.fast_executemany = True
    cursor.executemany(final_query, df.values.tolist())
    cnxn.commit()

def create_monthly_file(site,date,cnxn):
    first_date = datetime.datetime.strptime(date,'%Y-%m-%d').replace(day=1).strftime('%Y-%m-%d')
    print(first_date)
    component_df = get_cleantech_components(site,cnxn)
    component_df = component_df[((component_df['ComponentType']=='MFM') | (component_df['ComponentType']=='WMSIRR'))]
    df_final = pd.DataFrame()
    for index,row in component_df.iterrows():
        if(os.path.exists('/home/admin/public/Daily Values/'+site+'/'+row['ComponentType']+'/'+row['ComponentName']+'.csv')):
            df = pd.read_csv('/home/admin/public/Daily Values/'+site+'/'+row['ComponentType']+'/'+row['ComponentName']+'.csv')
            df = df[df['Date']>=first_date]
            if(df_final.empty):
                df_final= df
            else:
                df_final = pd.merge(df_final,df,how='left',on='Date',suffixes=(None,'_'+row['ComponentName'] ))
    chkdir('/home/admin/public/Monthly/'+site)
    df_final.to_csv('/home/admin/public/Monthly/'+site+'/'+site+' '+date_today[0:7]+'.txt',sep='\t',index=False)

def get_da(site,provider,date):
    date = datetime.datetime.strptime(date,'%Y-%m-%d')
    final_df = pd.DataFrame()
    if(provider=='Seris'):
        for i in range(14):
            date = date+datetime.timedelta(days=+1)
            str_date = date.strftime("%Y-%m-%d")
            if(os.path.exists('/home/admin/Dropbox/Second Gen/['+site+'S]/'+str_date[0:4]+'/'+str_date[0:7]+'/['+site+'S] '+str_date+'.txt')):
                df = pd.read_csv('/home/admin/Dropbox/Second Gen/['+site+'S]/'+str_date[0:4]+'/'+str_date[0:7]+'/['+site+'S] '+str_date+'.txt',sep='\t')
                temp_df = df[['Date','DA']]
                final_df = final_df.append(temp_df)    
    else:
        for i in range(14):
            date = date+datetime.timedelta(days=+1)
            str_date = date.strftime("%Y-%m-%d")
            if(os.path.exists('/home/admin/Dropbox/Second Gen/['+site+'X]/'+str_date[0:4]+'/'+str_date[0:7]+'/['+site+'X] '+str_date+'.txt')):
                df = pd.read_csv('/home/admin/Dropbox/Second Gen/['+site+'X]/'+str_date[0:4]+'/'+str_date[0:7]+'/['+site+'X] '+str_date+'.txt',sep='\t')
                temp_df = df[['Date','DA']]
                final_df = final_df.append(temp_df)
    return final_df

def upload_file(file_name,site,c_type,c_name,auth,sp_type):
    if(sp_type == 'Daily'):
        doc_library2="Data/Daily Values/"+site+"/"+c_type
        link = base_path + site_name + "_api/web/GetFolderByServerRelativeUrl('" + doc_library2 +"')/Files/add(url='"+c_name+'.csv'+"', overwrite=true)"

    else:
        doc_library2="Data/Monthly Values/"
        link = base_path + site_name + "_api/web/GetFolderByServerRelativeUrl('" + doc_library2 +"')/Files/add(url='"+c_name+' - Monthly.csv'+"', overwrite=true)"
    with open(file_name, 'rb') as read_file:
        content = read_file.read()
    status = auth.post(link, data=content)

def sharepoint_upload(path,site,c_type,c_name,auth):
    create_folder(site+'/'+c_type+'/',auth)
    upload_file(path,site,c_type,c_name,auth,'Daily')

def create_folder(path,auth):
    temp_path='Data/Daily Values/'
    for i in path.split('/')[:-1]:
        update_data = {}
        update_data['__metadata'] = {'type': 'SP.Folder'}
        temp_path=temp_path+i+'/'
        update_data['ServerRelativeUrl'] = temp_path
        body = json.dumps(update_data)
        link = base_path + site_name + "_api/web/folders/"
        status = auth.post(link,data=body) 

arg_site = sys.argv[1]
tz = pytz.timezone("Asia/Calcutta")
cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
seris_site_df = get_sites('Seris',cnxn)
ebx_site_df = get_sites('Ebx',cnxn)
site_df = ebx_site_df.append(seris_site_df)
site_df = site_df[site_df['SiteCode'] == arg_site]
username = 'test-account@cleantechsolar.com'
password = 'Cleantechsolarpv@123'
site_name = "/OM/"
base_path = 'https://cleantechenergycorp.sharepoint.com'
doc_libary = "Data"

path='/home/admin/public/Daily Values/'
path_monthly='/home/admin/public/Monthly Values/'
chkdir(path)
chkdir(path_monthly)

sites = [arg_site]
auth = sharepy.connect(base_path, username = username, password = password)
for site in sites:
    try:
        print(site)
        capacities_df = get_mfm_capacities(site,cnxn)
        parameter_df = get_all_parameters(cnxn)
        component_df = get_cleantech_components(site,cnxn)
        parameter_name_mapping = dict(zip(parameter_df['ParameterId'],parameter_df['CSName']))
        site_id = get_site_id(site,cnxn)
        df = get_processed_data_fullsite(site_id,cnxn)

        grouped_df = df.groupby('ComponentId')


        for c_id,group in grouped_df:
            component_name = component_df.loc[component_df['ComponentId']==c_id,'ComponentName'].values[0]
            component_type = component_df.loc[component_df['ComponentId']==c_id,'ComponentType'].values[0]
            df_temp = group.pivot( index = 'Date',columns='ParameterId', values=['Value'])
            df_temp.columns = df_temp.columns.get_level_values(1)
            df_temp.index.name = 'Date'
            df_temp.columns = df_temp.columns.to_series().map(parameter_name_mapping)
            chkdir(path + '/' + site + '/' + component_type)	
            df_temp.to_csv(path + '/' + site + '/' + component_type + '/' + component_name + '.csv')
            sharepoint_upload(path + '/' + site + '/' + component_type + '/' + component_name + '.csv',site,component_type,component_name,auth)
        
        mfm_df = pd.DataFrame()    
        for c_id in capacities_df['ComponentId'].tolist():
            temp_mfm_df = df[df['ComponentId'] == c_id]
            mfm_df = mfm_df.append(temp_mfm_df)

        #Adding GA,PA,DA
        grouped_mfm_df = mfm_df.groupby('ComponentId')
        df_monthly = pd.DataFrame()
        for c_id,group in grouped_mfm_df:
            df_temp = group.pivot( index = 'Date',columns='ParameterId', values=['Value'])
            df_temp.columns = df_temp.columns.get_level_values(1)
            df_temp.index.name = 'Date'
            df_temp.columns = df_temp.columns.to_series().map(parameter_name_mapping)
            if('DA' not in df_temp.columns.tolist()):
                df_temp['DA'] = np.nan
            if('Grid Availability' not in df_temp.columns.tolist()):
                df_temp['Grid Availability'] = np.nan
            if('Plant Availability' not in df_temp.columns.tolist()):   
                df_temp['Plant Availability'] = np.nan
            df_temp['System Availability'] = (df_temp['Grid Availability'] * df_temp['Plant Availability'])/100
            if(df_monthly.empty):
                df_monthly = df_temp[['DA','Yield-2','Grid Availability','Plant Availability','System Availability']]*capacities_df.loc[capacities_df['ComponentId']==c_id,'Size'].values[0]
                df_monthly_eac = df_temp['EAC method-2']
            else:
                df_monthly = df_monthly+df_temp[['DA','Yield-2','Grid Availability','Plant Availability','System Availability']]*capacities_df.loc[capacities_df['ComponentId']==c_id,'Size'].values[0]
                df_monthly_eac = df_monthly_eac + df_temp['EAC method-2']
        df_monthly = df_monthly/capacities_df['Size'].sum()
        df_monthly.rename(columns = {"DA": "MFM_DA"}, inplace = True)
        #Adding EAC
        df_monthly['EAC method-2'] = df_monthly_eac
        
        #Adding GHI/GTI
        irr_comp_id = get_main_irr_component(site_id,cnxn)
        print(irr_comp_id)
        wms_df = get_processed_data_component(irr_comp_id,cnxn)
        wms_df = wms_df[(wms_df['ParameterId'] == 2) | (wms_df['ParameterId'] == 304) | (wms_df['ParameterId'] == 1)]
        wms_df =wms_df.pivot( index = 'Date',columns='ParameterId', values=['Value'])
        wms_df.columns = wms_df.columns.get_level_values(1)
        wms_df.index.name = 'Date'
        wms_df.columns = wms_df.columns.to_series().map(parameter_name_mapping)
        wms_df.rename(columns = {"DA": "WMS_DA"}, inplace = True)
        if('WMS_DA' not in wms_df.columns.tolist()):
            wms_df['WMS_DA'] = np.nan
        df_monthly = pd.concat([df_monthly,wms_df])
        
        #Resampling
        df_monthly.index = pd.to_datetime(df_monthly.index)
        df_monthly = df_monthly.resample('1m').agg({'WMS_DA':'mean','GHI':'sum','MFM_DA':'mean','EAC method-2':'sum','Yield-2':'mean','Grid Availability':'mean','Plant Availability':'mean','System Availability':'mean'})
        df_monthly = df_monthly.round(2)
        df_monthly.insert(0,'Date',df_monthly.index.to_series().apply(lambda x: datetime.datetime.strftime(x, '%b-%Y')))
        df_monthly.to_csv(path_monthly  + site + ' - Monthly.csv',index=False)
        upload_file(path_monthly  + site + ' - Monthly.csv',site,'FULL SITE',site,auth,'Monthly')
    except Exception as e:
        print(e)
        pass




date_last_14 = (datetime.datetime.now(tz) - datetime.timedelta(days=14)).strftime('%Y-%m-%d')
date_today = datetime.datetime.now(tz).strftime('%Y-%m-%d')
print(date_last_14)
for index,row in site_df.iterrows():
    site=row['SiteCode']
    provider = row['MonitoringProvider']
    print(site)
    create_monthly_file(site,date_today,cnxn)
    #PR Upsert
    sourcesiteid = get_source_id(site)
    sourcecomponentid = get_source_mfm(site)
    destinationsiteid = get_dest_id(site)
    destinationcomponentid = get_dest_mfm(site)
    source_meters = pd.read_sql_query("SELECT [Meter_Id], [Reference] FROM [dbo].[Meters] where [Station_Id]=" + str(sourcesiteid), cnxn)
    dest_meters = pd.read_sql_query("SELECT [ComponentId], [MeterReference] FROM [Portfolio].[Meter] where [SiteId]=" + str(destinationsiteid), cnxn)
    source_dest_merge = pd.merge(source_meters, dest_meters, how='left', left_on=['Reference'], right_on=['MeterReference'])
    meterid_mapping = dict(zip(source_dest_merge.Meter_Id, source_dest_merge.ComponentId))
    
    if(site=='SG-005'):
        meterid_mapping={ 134: 3546.0, 135: 3547.0, 136: 3548.0, 137: 3549.0, 138: 3550.0, 299: 3583, 300: 3584, 301: 3585, 302: 3586, 303: 3587}
    elif(site=='KH-001'):
        meterid_mapping={263: 3801.0, 304: 3802}
    elif(site=='KH-002'):
        meterid_mapping={247: 3482.0, 305: 3843}

    source_data = pd.read_sql_query("SELECT [Date], [Meter_Id], [Eac-MFM], [LastR-MFM], [Yield], [Master-Flag] FROM Stations_Data where Station_Id=" + str(sourcesiteid)+" AND Date>='"+date_last_14+"'", cnxn)
    source_data.columns = ['Date', 'Meter_Id', 'EAC Method-2', 'Last Recorded Value', 'Yield-2', 'BypassFlag']
    source_data['Date'] = source_data['Date'].dt.date
    parameter_mapping = {'EAC Method-2':5, 'Last Recorded Value':10, 'Yield-2':7}
    final = pd.DataFrame()
    for i in meterid_mapping:
        df_temp = source_data.copy()
        for j in parameter_mapping:
            final2 = df_temp[df_temp['Meter_Id'] == i]
            final2 = final2[['Date','Meter_Id',j,'BypassFlag']]
            final2['Meter_Id'] = meterid_mapping[i]
            final2['ParameterId'] = parameter_mapping[j]
            final2['SiteId'] = destinationsiteid
            final2 = final2[['Date',j,'ParameterId','Meter_Id','SiteId', 'BypassFlag']]
            final2.columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag']
            final = final.append(final2)
    fast_upsert_processed(site,final) 

    #PR Upsert
    full_site_meter = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[CleantechMetadata] where ParameterId=69 and SiteId=" + str(destinationsiteid), cnxn)
    dest_ComponentId = full_site_meter['ComponentId'].values[0]
    final_df = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag'])
    source_data = pd.read_sql_query("SELECT Stations_Data.Date, Stations_Data.Meter_Id, Meters.Capacity, Stations_Data.PR FROM Stations_Data INNER JOIN Meters ON Stations_Data.Meter_Id=Meters.Meter_id where Stations_Data.Station_Id=" + str(sourcesiteid)+" AND Date>='"+date_last_14+"'", cnxn)
    source_data['Date'] = source_data['Date'].dt.date
    dates = source_data['Date'].unique()
    for date in dates:
        temp = source_data[source_data['Date']==date]
        temp['Weight'] = temp['Capacity']*temp['PR']
        temp_full_pr = temp['Weight'].sum()/temp['Capacity'].sum()
        final_df = final_df.append({'Date' : date,'Value' : temp_full_pr, 'ParameterId' : 69,'ComponentId' : int(dest_ComponentId),'SiteId' : int(destinationsiteid),'BypassFlag' : 0},ignore_index = True)
    fast_upsert_processed(site, final_df)

    #GHI Upsert
    wms_id = get_main_irr_component(destinationsiteid,cnxn)
    dest_ComponentId = wms_id
    final_df = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag'])
    source_data = pd.read_sql_query("SELECT Stations_Data.Date, Stations_Data.Meter_Id, Meters.Capacity, Stations_Data.GHI FROM Stations_Data INNER JOIN Meters ON Stations_Data.Meter_Id=Meters.Meter_id where Stations_Data.Station_Id=" + str(sourcesiteid)+" AND Date>='"+date_last_14+"'", cnxn)
    source_data['Date'] = source_data['Date'].dt.date
    final_df = source_data.drop_duplicates(subset=['Date'])
    final_df['ParameterId'] = 2
    final_df['SiteId'] = destinationsiteid
    final_df['BypassFlag'] = 0
    final_df['ComponentId'] = wms_id
    final_df = final_df[['Date','GHI','ParameterId','ComponentId','SiteId', 'BypassFlag']]
    final_df.columns = ['Date', 'Value', 'ParameterId','ComponentId', 'SiteId', 'BypassFlag']
    fast_upsert_processed(site, final_df)

    if(provider=='Seris' or provider=='Ebx'):
        da_df = get_da(site,provider,date_last_14)
        if(da_df.empty):
            pass
        else:
            da_df.columns = ['Date','Value']
            component_df = get_cleantech_components(site,cnxn)
            for index,c in component_df.iterrows():
                da_df['ParameterId'] = 1
                da_df['SiteId'] = get_site_id(site,cnxn)
                da_df['BypassFlag'] = 0
                da_df['ComponentId'] = c['ComponentId']
                fast_upsert_processed(site, da_df)

cnxn.close()

 


