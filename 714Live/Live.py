from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib.request
import re 
import os
import numpy as np
import pyodbc
import logging

pd.options.mode.chained_assignment = None

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)
        
path='/home/admin/Dropbox/SERIS_Live_Data/[KH-001S]/'
chkdir(path)
tz = pytz.timezone('Asia/Singapore')
curr=datetime.datetime.now(tz)

def azure_push(df):
        connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
        cursor = connStr.cursor()
        df=df.fillna('NULL')
        df=df.replace('NaN','NULL')
        for index,row in df.iterrows():
            try:
                with cursor.execute("INSERT INTO [dbo].[KH-001S-L] ([Timestamp],[AvgGsi00],[AvgGmod10E],[AvgGmod10W],[AvgSMP10],[AvgHamb],[AvgTamb],[AvgTmodE],[AvgTmodW],[Act_Pwr-Tot],[Act_E-Del] ) VALUES ('"+row['Timestamp']+"',"+str(row['AvgGsi00'])+","+str(row['AvgGmod10E'])+","+str(row['AvgGmod10W'])+","+str(row['AvgSMP10'])+","+str(row['AvgHamb'])+","+str(row['AvgTamb'])+","+str(row['AvgTmodE'])+","+str(row['AvgTmodW'])+","+str(row['Act_Pwr-Tot'])+","+str(row['Act_E-Del'])+')'):
                    pass
            except Exception as e:
                print(e)
                print('Duplicate')
                pass
        connStr.commit()
        connStr.close() 

currtime=curr.replace(tzinfo=None)
print("Start time is",curr)
cols=[]
for i in range(600):
    cols.append(i+1)
print("Starting Live Bot!")
ftp = FTP('ftpnew.cleantechsolar.com')
ftp.login(user='cleantechsolar', passwd = 'bscxataB2')
ftp.cwd('1min')
noneflag=1
flag=1
starttime=time.time()
cols2=['Timestamp','AvgGsi00','AvgGmod10E','AvgGmod10W','AvgSMP10','AvgHamb','AvgTamb','AvgTmodE','AvgTmodW','Act_Pwr-Tot','Act_E-Del']
while(1):
    try:
        if(flag==1):
            cur=currtime.strftime("%Y-%m-%d_%H-%M")
            cur2=(curr+datetime.timedelta(hours = -1)).strftime("%Y-%m-%d_%H-%M")
            prev=currtime+datetime.timedelta(days=-1)
            prev=str(prev)
        else:
            curr=datetime.datetime.now(tz)
            currtime=curr.replace(tzinfo=None)
            prev=currtime+datetime.timedelta(days=-1)
            prev=str(prev)
            cur=currtime.strftime("%Y-%m-%d_%H-%M")
        flag2=1
        j=0
        while(j<5 and flag2!=0):
            j=j+1
            files=ftp.nlst()
            for i in files:
                if(re.search(cur,i)):
                    req = urllib.request.Request('ftp://cleantechsolar:bscxataB2@ftpnew.cleantechsolar.com/1min/Cleantech-'+cur+'.xls')
                    try:
                        with urllib.request.urlopen(req) as response:
                            s = response.read()
                        df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep='\t',names=cols)
                        a=df.loc[df[1] == 722] 
                        if((os.path.exists(path+cur[0:4]+'/'+cur[0:7]+'/'+'[KH-001S] '+cur[0:10]+'.txt') and noneflag==0)):
                            b=a[[2,3,4,5,6,7,8,9,10,24,39]] 
                            print(b)
                            df= pd.read_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[KH-001S] '+cur[0:10]+'.txt',sep='\t')
                            df2= pd.DataFrame( np.concatenate( (df.values, b.values), axis=0 ) )
                            df2.columns = cols2
                            std1=df2['Act_E-Del'].std()
                            if(std1>10000):
                                b.loc[:, 39] = 'NaN' 
                            b.loc[b[39] == 0, 39] = 'NaN'
                        else:
                            print("In else")
                            noneflag=1
                            b=a[[2,3,4,5,6,7,8,9,10,24,39]] 
                            print(b,prev)
                            if(os.path.exists(path+cur2[0:4]+'/'+cur2[0:7]+'/'+'[KH-001S] '+cur2[0:10]+'.txt')):
                                dfprev= pd.read_csv(path+cur2[0:4]+'/'+cur2[0:7]+'/'+'[KH-001S] '+cur2[0:10]+'.txt',sep='\t')
                                dfprev2= pd.DataFrame( np.concatenate( (dfprev.values, b.values), axis=0 ) )
                                dfprev2.columns = cols2
                                print(dfprev2.tail())
                                std1=dfprev2['Act_E-Del'].std()
                                if(std1>10000):
                                    b.loc[:, 39] = 'NaN'   
                                b.loc[b[39] == 0, 39] = 'NaN'
                                print(std1)
                                if(b[39].isnull().values.any()  or std1>10000):
                                    pass
                                else:
                                    noneflag=0
                                flag=0
                            else:
                                noneflag=0
                                flag=0
                        #Taking care of Power
                        b.loc[(b[4] < 0), 4] = 0
                        b.loc[(b[24] > 3000) | (b[24] < 0), 24] = 'NaN'
                        b.columns=cols2
                        chkdir(path+cur[0:4]+'/'+cur[0:7])
                        azure_push(b)
                        if(os.path.exists(path+cur2[0:4]+'/'+cur2[0:7]+'/'+'[KH-001S] '+cur2[0:10]+'.txt')):
                            b.to_csv(path+cur2[0:4]+'/'+cur2[0:7]+'/'+'[KH-001S] '+cur2[0:10]+'.txt',header=False,sep='\t',index=False,mode='a')
                        else:
                            b.to_csv(path+cur2[0:4]+'/'+cur2[0:7]+'/'+'[KH-001S] '+cur2[0:10]+'.txt',header=True,sep='\t',index=False)
                        flag2=0
                    except Exception as e:
                        flag2=0
                        logging.exception("message")
                        print("Failed for",currtime)
            if(flag2!=0 and j<5):
                print('sleeping for 10 seconds!')            
                time.sleep(10)
        if(flag2==1):
            print('File not there!',cur)
        print('sleeping for a minute!')
    except:
        pass
    time.sleep(60.0 - ((time.time() - starttime) % 60.0))
    
        