from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib.request
import re 
import os
import numpy as np

pd.options.mode.chained_assignment = None

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)
        
path='/home/admin/Dropbox/SERIS_Live_Data/[KH-008S]/'
chkdir(path)

tz = pytz.timezone('Asia/Singapore')
curr=datetime.datetime.now(tz)
currnextmin=curr + datetime.timedelta(minutes=1)
currnextmin=currnextmin.replace(second=9,microsecond=0)
while(curr!=currnextmin):
    curr=datetime.datetime.now(tz)
    curr=curr.replace(microsecond=0)
    time.sleep(1)
currtime=curr.replace(tzinfo=None)
print("Start time is",curr)
cols=[]
for i in range(600):
    cols.append(i+1)
print("Starting Live Bot!")
ftp = FTP('ftpnew.cleantechsolar.com')
ftp.login(user='cleantechsolar', passwd = 'bscxataB2')
ftp.cwd('1min')
noneflag=1
flag=1
starttime=time.time()
cols2=['Time Stamp','AvgSMP10','AvgGsi00_Shr','AvgGmod_Shr','AvgGmod_Wtr','AvgTmod_Rf','AvgTmod_Wtr','AvgTamb_Shr','AvgHamb_Shr','AvgTamb_Wtr','AvgHamb_Wtr','AvgTroom_LV',
'AvgHroom_LV','Troom_MV','Hroom_MV','AvgTcab','AvgHcab','AvgWindS','AvgWindD','AvgWaterLvl','AvgWaterTemp','AvgTroof1','AvgTroof2','AvgTroof3','AvgTroof4','Act_Pwr-Tot_Pond','Act_Pwr-Tot_R06',
'Act_Pwr-Tot_R02','Act_Pwr-Tot_R03','Act_Pwr-Tot_R11','Act_E-Del_Pond','Act_E-Del_R06','Act_E-Del_R02','Act_E-Del_R03','Act_E-Del_R11']
while(1):
    try:
        if(flag==1):
            cur=currtime.strftime("%Y-%m-%d_%H-%M")
            cur2=currtime+datetime.timedelta(hours = -1)
            currtime2=cur2.replace(tzinfo=None)
            prev=currtime2+datetime.timedelta(days=-1)
            prev=str(prev)
            cur3=currtime2.strftime("%Y-%m-%d_%H-%M")
            print('Now adding for',currtime2)
        else:
            curr=datetime.datetime.now(tz)
            cur2=curr+datetime.timedelta(hours = -1)
            currtime2=cur2.replace(tzinfo=None)
            prev=currtime2+datetime.timedelta(days=-1)
            prev=str(prev)
            cur3=currtime2.strftime("%Y-%m-%d_%H-%M")
            currtime=curr.replace(tzinfo=None)
            cur=currtime.strftime("%Y-%m-%d_%H-%M")
            print('Now adding for',currtime)
        flag2=1
        j=0
        while(j<5 and flag2!=0):
            j=j+1
            files=ftp.nlst()
            for i in files:
                if(re.search(cur,i)):
                    req = urllib.request.Request('ftp://cleantechsolar:bscxataB2@ftpnew.cleantechsolar.com/1min/Cleantech-'+cur+'.xls')
                    try:
                        with urllib.request.urlopen(req) as response:
                            s = response.read()
                        df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep='\t',names=cols)
                        a=df.loc[df[1] == 728] 
                        if(os.path.exists(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[KH-008S] '+cur3[0:10]+'.txt') and noneflag==0):
                            b=a[[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,40,87,134,181,228,55,102,149,196,243]]
                            print("B is",b)  
                            df= pd.read_csv(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[KH-008S] '+cur3[0:10]+'.txt',sep='\t')
                            df2= pd.DataFrame( np.concatenate( (df.values, b.values), axis=0 ) )
                            df2.columns = cols2
                            std1=df2['Act_E-Del_Pond'].std()
                            std2=df2['Act_E-Del_R06'].std()
                            std3=df2['Act_E-Del_R02'].std()
                            std4=df2['Act_E-Del_R03'].std()
                            std5=df2['Act_E-Del_R11'].std()
                            if(std1>12000):
                                b.loc[:, 55] = 'NaN' 
                            if(std2>12000):
                                b.loc[:, 102] = 'NaN'  
                            if(std3>12000):
                                b.loc[:, 149] = 'NaN' 
                            if(std4>12000):
                                b.loc[:, 196] = 'NaN'  
                            if(std5>12000):
                                b.loc[:, 243] = 'NaN'  
                            b.loc[b[55] == 0, 55] = 'NaN'
                            b.loc[b[102] == 0, 102] = 'NaN'
                            b.loc[b[149] == 0, 149] = 'NaN'
                            b.loc[b[196] == 0, 196] = 'NaN'
                            b.loc[b[243] == 0, 243] = 'NaN'
                        else:
                            print("In else")
                            noneflag=1
                            b=a[[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,40,87,134,181,228,55,102,149,196,243]]
                            print(b,prev)
                            dfprev= pd.read_csv(path+prev[0:4]+'/'+prev[0:7]+'/'+'[KH-008S] '+prev[0:10]+'.txt',sep='\t')
                            dfprev2= pd.DataFrame( np.concatenate( (dfprev.values, b.values), axis=0 ) )
                            dfprev2.columns = cols2
                            print(dfprev2.tail())
                            std1=dfprev2['Act_E-Del_Pond'].std()
                            std2=dfprev2['Act_E-Del_R06'].std()
                            std3=dfprev2['Act_E-Del_R02'].std()
                            std4=dfprev2['Act_E-Del_R03'].std()
                            std5=dfprev2['Act_E-Del_R11'].std()
                            if(std1>12000):
                                b.loc[:, 55] = 'NaN' 
                            if(std2>12000):
                                b.loc[:, 102] = 'NaN'  
                            if(std3>12000):
                                b.loc[:, 149] = 'NaN' 
                            if(std4>12000):
                                b.loc[:, 196] = 'NaN'  
                            if(std5>12000):
                                b.loc[:, 243] = 'NaN' 
                            b.loc[b[55] == 0, 55] = 'NaN'
                            b.loc[b[102] == 0, 102] = 'NaN'
                            b.loc[b[149] == 0, 149] = 'NaN'
                            b.loc[b[196] == 0, 196] = 'NaN'
                            b.loc[b[243] == 0, 243] = 'NaN'
                            print(std1)
                            print(std2)
                            print(std3)
                            print(std4)
                            print(std5)
                            if(b[55].isnull().values.any() or b[102].isnull().values.any() or b[149].isnull().values.any() or b[196].isnull().values.any() or b[243].isnull().values.any() or std1>12000 or std2>12000 or std3>12000 or std4>12000 or std5>12000 or std1==np.nan or std2==np.nan or std3==np.nan or std4==np.nan or std5==np.nan):
                                pass
                            else:
                                noneflag=0
                            flag=0
                        #Taking care of Power
                        b.loc[(b[4] < 0), 4] = 0
                        b.loc[(b[40] > 3000) | (b[40] < 0), 40] = 'NaN'
                        b.loc[(b[87] > 3000) | (b[87] < 0), 87] = 'NaN'
                        b.loc[(b[134] > 3000) | (b[134] < 0), 134] = 'NaN'
                        b.loc[(b[181] > 3000) | (b[181] < 0), 181] = 'NaN'
                        b.loc[(b[228] > 3000) | (b[228] < 0), 228] = 'NaN'
                        b.columns=cols2
                        chkdir(path+cur3[0:4]+'/'+cur3[0:7])
                        if(os.path.exists(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[KH-008S] '+cur3[0:10]+'.txt')):
                            b.to_csv(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[KH-008S] '+cur3[0:10]+'.txt',header=False,sep='\t',index=False,mode='a')
                        else:
                            b.to_csv(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[KH-008S] '+cur3[0:10]+'.txt',header=True,sep='\t',index=False)
                        flag2=0
                    except Exception as e:
                        flag2=0
                        print(e)
                        print("Failed for",currtime2)
            if(flag2!=0 and j<5):
                print('sleeping for 10 seconds!')            
                time.sleep(10)
        if(flag2==1):
            print('File not there!',cur)
        print('sleeping for a minute!')
    except:
        pass
    time.sleep(60.0 - ((time.time() - starttime) % 60.0))
    
        