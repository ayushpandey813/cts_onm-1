import os
import re
import pyodbc
import csv,time
import datetime
import collections
import os
import time
import pytz
import sys
import numpy
import pandas as pd

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

tz = pytz.timezone('Asia/Kolkata')

read_path = '/home/admin/Dropbox/GIS/Master/Z_AllSites_Master.txt'

data=pd.read_csv(read_path,sep='\t')
columns=data.columns
print("columns are",columns)
print("No of cols",len(columns))
originallen=len(columns)

#Function to update at 12PM and 6PM
def determine_datetime():
    tz = pytz.timezone('Asia/Kolkata')
    now_time = datetime.datetime.now()
    curr_date = datetime.datetime.now().date()
    curr_time = curr_date.strftime("%Y-%m-%d %H:%M:%S")
    curr_time = datetime.datetime.strptime(curr_time, "%Y-%m-%d %H:%M:%S")
    curr_time12 = curr_time + datetime.timedelta(hours = 12)
    curr_time13 = curr_time + datetime.timedelta(hours = 13)
    curr_time18 = curr_time + datetime.timedelta(hours = 18)
    curr_time19 = curr_time + datetime.timedelta(hours = 19)
    if ((curr_time12 <= now_time <= curr_time13) or (curr_time18 <= now_time <= curr_time19)):
        print(now_time)
        result = 1
    else:
        result = 0    
    return result

def changename(names):#dynamic for naming columns
    names2=[]
    for i in names:
        names2.append('['+i+']')
    return names2

def insert(fields,updatecol):
    names=changename(updatecol)
    nameslen=len(names)
    values='?,'*nameslen
    values=values[:-1]
    namesstring=','.join(names)
    counter=0
    while True:            
        try:
            cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
            cursor = cnxn.cursor()
            #Truncating table if already present
            with cursor.execute("IF OBJECT_ID('[Cleantech Meter Readings].dbo.[GIS_ALL_MASTER]', 'U') IS NOT NULL TRUNCATE TABLE [Cleantech Meter Readings].dbo.[GIS_ALL_MASTER]"): 
                    print('Successfuly Deleted cause already present!')
            #Dumping data in the new table
            for i in range(0,len(fields)):
                with cursor.execute("INSERT INTO [Cleantech Meter Readings].dbo.[GIS_ALL_MASTER] ("+namesstring+") VALUES ("+values+")",fields[i]): 
                    print('Successfuly Inserted Row!',fields[i][0])  
            cnxn.commit()
            cursor.close()
            cnxn.close()
            break 
        except Exception as e:
            err_num = e[0]
            print ('ERROR:', err_num, fields[0])
            cursor.close()
            cnxn.close()
            if err_num == '08S01':
                counter = counter + 1
                print ('Sleeping for 5')
                time.sleep(300)
                if counter > 10:
                    exit('Persistent connection error')                    
                print ('%%%Re-trying connection%%%')             
            else:
                break 
                   
            
while(1):
#--------------------------------------------------------Updating
    result = determine_datetime()
    if result == 1:
        counter=0
        try:
            data2=pd.read_csv(read_path,sep='\t')
            updatecol=data2.columns
            updatelen=len(updatecol)
        except:
            pass
        while True:            
            try:
                cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
                cursor = cnxn.cursor()
                for i in range(originallen,updatelen):
                    with cursor.execute("ALTER TABLE [Cleantech Meter Readings].dbo.[GIS_ALL_MASTER] ADD [" + str(updatecol[i]) + "] FLOAT"):
                        print('Successfuly Inserted Column!',str(updatecol[i]))
                    originallen=updatelen
                cnxn.commit()
                cursor.close()
                cnxn.close()
                break 
            except Exception as e:
                err_num = e[0]
                print ('ERROR:', err_num, fields[0])
                cursor.close()
                cnxn.close()
                if err_num == '08S01':
                    counter = counter + 1
                    print ('Sleeping for 5')
                    time.sleep(300)
                    if counter > 10:
                        exit('Persistent connection error')                    
                    print ('%%%Re-trying connection%%%')             
                else:
                    break 
     #-------------------------------------------------------Inserting   
        mat=[]
        try:        
            with open(read_path, 'r') as read_file:
                    next(read_file)
                    for line in read_file:
                        fields =[]
                        fields = line.split("\t")
                        if fields[0] == 'NA':
                            continue
                        for i in range(len(fields)):   
                            fields[i] = fields[i].replace('"', '').strip()
                            if fields[i] == 'NA' or fields[i] == 'NA\n' :
                                fields[i] = None
                        mat.append(fields)
        except:
            pass
        #To check if there are rows present.
        if not mat:
            pass
        else:
            insert(mat,updatecol)
    print("Sleeping for 1 Hour")
    time.sleep(3600)

        
 

