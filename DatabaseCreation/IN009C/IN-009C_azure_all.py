import os
import re
import pyodbc
import csv,time
import datetime
import collections
import os
import time
import pytz


server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

##global start_tstamp
##global start_tstamp_update

stationname = 'IN-009C'
sitename = 'BectonDickinson'

tz = pytz.timezone('Asia/Kolkata')
last_date = datetime.datetime.now(tz).date()

#read_path = '/home/saradindu/FlexiMC_Data/Azure_Fourth_Gen/['+sitename+']/['+sitename+']-lifetime.txt' ###local
read_path = '/home/admin/Dropbox/FlexiMC_Data/Azure_Fourth_Gen/['+sitename+']/['+sitename+']-lifetime.txt' ###server


#Function to check for new day
def determine_date():
	global last_date    
	tz = pytz.timezone('Asia/Kolkata')
	today_date = datetime.datetime.now(tz).date()
	if today_date != last_date:
		last_date = today_date
		print('New day, Deleting old records')
		delete_records(last_date)

#Function delete old records
def delete_records(last_date):
	last_date = last_date + datetime.timedelta(days=-9)
	last_date = last_date.strftime("%Y-%m-%d %H:%M:%S")

	while True:
		cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
		try:
			cursor = cnxn.cursor()
			with cursor.execute("DELETE FROM [Cleantech Meter Readings].dbo.["+stationname+"-ALL] WHERE [Tstamp]<?",last_date):
				print('Detetion successful - All records deleted before '+last_date)
			cnxn.commit()
			cursor.close()
			cnxn.close()
			break
			
		except Exception as e:
				err_num = e[0]
				print ('ERROR:', err_num,e[1])
				cursor.close()
				cnxn.close()
				if err_num == '08S01':
					counter = counter + 1
					print ('Sleeping for 5')
					time.sleep(300)
					if counter > 10:
						exit('Persistent connection error')                    
					print ('%%%Re-trying connection%%%')             
				else:
					break
		
	

#Function to read start timestamp
def read_timestamp():
	global start_tstamp
    file = open('/home/saradindu/Start/'+stationname[0:2]+stationname[3:]+'AA.txt','r') ###local
##	file = open('/home/admin/Start/'+stationname[0:2]+stationname[3:]+'AA.txt','r') ###server
	time_read = file.read(19)
	start_tstamp = datetime.datetime.strptime(time_read, "%Y-%m-%d %H:%M:%S")

#Function to update start file 
def update_start_file(curr_tstamp):
    file = open('/home/saradindu/Start/'+stationname[0:2]+stationname[3:]+'AA.txt','w') ###local
##	file = open('/home/admin/Start/'+stationname[0:2]+stationname[3:]+'AA.txt','w') ###server
	file.write(str(curr_tstamp))
	file.close()

def insert(fields,curr_tstamp):
 
	counter = 0
	while True:            
		try:
			cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
			cursor = cnxn.cursor()
			update_start_file(fields[0])
			global start_tstamp_update
			start_tstamp_update = curr_tstamp
			
			with cursor.execute("INSERT INTO [Cleantech Meter Readings].dbo.["+stationname+"-ALL] ([Tstamp],[WMS.GTI],[WMS.Tamb],[WMS.Tmod],[WMS.TambSH],[WMS.TmodSH],[WMS.GTI.1],[WMS.Tamb.1],[WMS.Tmod.1],[MFM.Eac1],[MFM.Eac2],[MFM.Yld1],[MFM.Yld2],[MFM.PR1],[Inverter_1.Eac1],[Inverter_1.EacAC],[Inverter_1.EacDC],[Inverter_1.Yld1],[Inverter_1.Yld2],[Inverter_1.LastRead],[Inverter_1.LastTime],[Inverter_2.Eac1],[Inverter_2.EacAC],[Inverter_2.EacDC],[Inverter_2.Yld1],[Inverter_2.Yld2],[Inverter_2.LastRead],[Inverter_2.LastTime],[Inverter_3.Eac1],[Inverter_3.EacAC],[Inverter_3.EacDC],[Inverter_3.Yld1],[Inverter_3.Yld2],[Inverter_3.LastRead],[Inverter_3.LastTime],[Inverter_4.Eac1],[Inverter_4.EacAC],[Inverter_4.EacDC],[Inverter_4.Yld1],[Inverter_4.Yld2],[Inverter_4.LastRead],[Inverter_4.LastTime],[Inverter_5.Eac1],[Inverter_5.EacAC],[Inverter_5.EacDC],[Inverter_5.Yld1],[Inverter_5.Yld2],[Inverter_5.LastRead],[Inverter_5.LastTime],[Inverter_6.Eac1],[Inverter_6.EacAC],[Inverter_6.EacDC],[Inverter_6.Yld1],[Inverter_6.Yld2],[Inverter_6.LastRead],[Inverter_6.LastTime],[Inverter_7.Eac1],[Inverter_7.EacAC],[Inverter_7.EacDC],[Inverter_7.Yld1],[Inverter_7.Yld2],[Inverter_7.LastRead],[Inverter_7.LastTime]) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",fields): 
				print('Successfuly Inserted!' + fields[0])                
			
			cnxn.commit()
			cursor.close()
			cnxn.close()
			break
		
		except Exception as e:
			err_num = e[0]
			print ('ERROR:', err_num,e[1], fields[0])
			cursor.close()
			cnxn.close()
			if err_num == '08S01':
				counter = counter + 1
				print ('Sleeping for 5')
				time.sleep(300)
				if counter > 10:
					exit('Persistent connection error')                    
				print ('%%%Re-trying connection%%%')             
			else:
				break




def convert_data_type(fields):
	for i in range(len(fields)):
		if i == 0 or i == 6:
			continue
		if fields[i] == 'NA' or fields[i] == 'NA\n' :
			fields[i] = None
		else:
			fields[i] = float(fields[i])

	return fields
		





read_timestamp()
start_tstamp_update = start_tstamp
while True:

	determine_date()
	   
	try:
		if not os.path.exists(read_path):
				raise Exception('Empty')        
	except Exception as e:
			print('No file found -',read_path)
			time.sleep(600)
			continue

	start_tstamp = start_tstamp_update
	with open(read_path, 'r') as read_file:
		first = 1
		for line in read_file:
			if first == 1:
				first = 0
				continue
			fields =[]
			fields = line.split("\t")
			fields[0] = fields[0].replace('"', '').strip()
			fields[6] = fields[6].replace('"', '').strip()
	
			curr_tstamp = datetime.datetime.strptime(fields[0], "%Y-%m-%d %H:%M:%S")
			if curr_tstamp > start_tstamp:
				fields = convert_data_type(fields)
				insert(fields,curr_tstamp)
				
	time.sleep(1800)
