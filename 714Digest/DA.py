import datetime
import os
import pandas as pd
from matplotlib.pyplot import figure
import matplotlib.pyplot as plt
import matplotlib.colors as clrs
import matplotlib.ticker as ticker
from datetime import datetime
import matplotlib.dates as mdates
from datetime import datetime as dt
from datetime import timedelta 
import numpy as np

date = []
date_new = []
DA = []
DA_new = []
current_DA = []
prev_DA = []
prevMonth = 12

yesterday = datetime.now() - timedelta(days=1)
yesterday  = str(yesterday)
yesterday = yesterday[0:11]
currentMonth = int(datetime.now().month)
currentYear = str(datetime.now().year)
print(currentMonth)
print(currentYear)
if(currentMonth == 1):
  prevMonth == 12
else:
  prevMonth = currentMonth-1

if(currentMonth == 1 or currentMonth == 2 or currentMonth == 3 or currentMonth == 4 or currentMonth == 5 or currentMonth == 6 or currentMonth == 7 or currentMonth == 8 or currentMonth == 9):
  currentMonth = str(currentMonth)
  currentMonth = "0"+currentMonth
prevMonth = str(prevMonth)
if(prevMonth!= '12' and prevMonth!= '11' and prevMonth != '10'):
    prevMonth = "0"+prevMonth
currentMonth = str(currentMonth)
if(currentMonth == "01"):
  prevyrmon = "2020"+"-"+prevMonth
else:
  prevyrmon = currentYear+"-"+prevMonth
yrmon = currentYear+"-"+currentMonth
print(yrmon)

  
print(prevMonth)
path = '/home/admin/Dropbox/Third Gen/SERIS_SITES/[KH-714S]/[KH-714S]_lifetime.txt'
with open(path) as file:
  df1 = pd.read_csv(file,sep='\t',header=0,skipinitialspace=True)
for i in df1['Date']:
  date.append(i)
for i in df1['DA']:
  DA.append(i)
for i in DA: 
  i = float(i)
  DA_new.append(i)
  
for i in date:
   i = datetime.strptime(i, '%Y-%m-%d')
   date_new.append(i)
   
 
df = pd.DataFrame(list(zip(date_new, DA_new)),columns=['Date','DA_new'])

#df.to_csv('/home/anusha/GRAPHS/Graph [SG_724S] DA - '+yesterday+'.csv')
df.to_csv('/home/admin/Graphs/Graph_Extract/KH-714/Graph [KH-714S] DA - '+yesterday+'.csv')
fig, ax = plt.subplots(figsize=(13.2  , 8.8))
tick_spacing  = 90
y = df['DA_new']
plt.rcParams.update({'font.size': 11})
x = df['Date']
  
  
#CALCULATION:
avg = round(np.mean(DA_new),1)
print(avg)
ax.plot(x,y,  color = 'red', linewidth = '2')

for i,j in zip(date_new,DA_new):
  i = str(i)
  if(prevyrmon in i):
    prev_DA.append(j)

for i,j in zip(date_new,DA_new):
  i = str(i)
  if(yrmon in i):
    current_DA.append(j)
    
prev_DA_avg = round(np.mean(prev_DA),1)
current_DA_avg = round(np.mean(current_DA),1)
plt.rcParams.update({'font.size': 12})
ax.set_xlim(x[0], x.iloc[-1])
ax.set_ylim(0, 105)
ax.set_ylabel('Data Availability [%]', fontsize = 12)
ax.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))
ax.text(0.72, 0.065, 'Lifetime DA [%]: '+str(avg), va="top",transform=ax.transAxes,weight='bold', size = 10)
ax.text(0.72, 0.135, 'Previous Month DA [%]: '+str(prev_DA_avg), va="top",transform=ax.transAxes,weight='bold', size = 10)
ax.text(0.72, 0.10, 'Current Month DA [%]: '+str(current_DA_avg), va="top",transform=ax.transAxes,weight='bold', size = 10)

plt.suptitle('Data Availability - KH-714', x=0.51, y=.95, horizontalalignment='center', verticalalignment='top', fontsize = 15)
plt.title('From 2016-06-05 to ' +yesterday , fontsize = 13)
ax.grid(axis='y')
ax.tick_params(axis='both', which='major', labelsize=11)
plt.xticks(rotation=90)
plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%b-%y'))
#fig.savefig('/home/anusha/GRAPHS/Graph [SG_724S] DA - '+yesterday+'.pdf', dpi=fig.dpi,bbox_inches='tight')
fig.savefig('/home/admin/Graphs/Graph_Output/KH-714/Graph [KH_714S] DA - '+yesterday+'.pdf', dpi=fig.dpi,bbox_inches='tight')
plt.show()
